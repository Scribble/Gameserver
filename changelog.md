# Forever Dereth Change Log - March 2018

### 2018-03-23
**Scribble**

* Added logic for AwardTrainingCredit (Award Skill Credit) emote types.

### 2018-03-21
**Crimson Mage**

* Created BannedWords.cs to load TabooTable.json
* Created TabooTable.json from 
* Added BadWords search on character.Name to allowed names list and send session message.
* Dat now included in NameFilter with CustomWords from TabooTable.json 

### 2018-03-19
**Coral Golem**

* Added more classes to physics
* Filled in missing methods
* Added get set
* Learned a lot of cool stuff about RE work.
* Still very much work in progress but this is a good stopping point.
* Added new constants to reduce magic numbers


### 2018-03-19
**Scribble**

* Added logic for inq string and inq float emote types.

### 2018-03-19
**Coral Golem**

* Added a few more classes to physics as well as enums sourced from the client.


### 2018-03-14
**Gold Golem**

* Added SpawnMap searching in the API.
* Added Dungeon Name caching; thanks Beheomoth!
* Added Spawn Map Instance updating and removal.
* Updated Unit Tests for Spawn Maps.

### 2018-03-14
**Gold Golem**

* Worked on Spawn map functionality in `WorldJsonDatabase`:
*   Added a method for Saving of all spawn maps.
*   Added a method that will Save a spawn map.
*   Added a method too Add or Update a Spawn map.
*   Added a method for retreiving a spawn map.
*   Added a method too Change a single object instance, within a spawn map.
* Added a Unit Tests spawn map saving and updating.

### 2018-03-02
**Coral Golem**

* Fixed non-players being able to wield child items.
* Added createTable for generators to DataObject
* Added ability to pull that data in from Phat json
* Added the ability to import new phat weenie json files into DF format.
* Added several new enums to support child item placement
* Moved some methods from player to creature for code reuse.
* Refactored SetChild to address some edge case bugs
* Modified landblock broadcast code to include child items.
* Removed some magic numbers in the code with new enums
* General Code cleanup.   

**Gold Golem**

* Added server restart functionality and commands - `restart` will begin the restart timer that you can stop with `cancel-shutdown`, like the `shutdown` command.
* Updated world redeployment - enhanced the world database redeployment functionality, to support the new json data format.
* Added stub for downloading a resource (json archive) from a website; now on to build out supporting functionality in the content editor.
* Removed Github resource support.
* Removed database redeplomyent unit tests - check the git history to retreieve.
* Updated a few paths in the Unit tests.

### 2018-03-01
**Crimson Mage**

* Updated Startergear.json to include Summoning
* added DualWield check to Characterhandler.cs for character creation.
