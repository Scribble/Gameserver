/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using BulletSharp;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace DerethForever.Physics.Core
{
    public class MouseController
    {
        public Vector3 Vector { get; set; }
        public float Sensitivity { get; set; }

        Input input;
        Point mouseOrigin;
        double angleOriginX, angleOriginY;

        public MouseController(Input input)
        {
            this.input = input;
            Sensitivity = 0.005f;
            SetByAngles(0, 0);
        }

        // HorizontalAngle - left-right movement (parallel to XZ-plane)
        // VerticalAngle - up-down movement (angle between Vector and Y-axis)
        public void SetByAngles(double horizontalAngle, double verticalAngle)
        {
            Vector = new Vector3(
                (float)(Math.Cos(horizontalAngle) * Math.Cos(verticalAngle)),
                (float)Math.Sin(verticalAngle),
                (float)(Math.Sin(horizontalAngle) * Math.Cos(verticalAngle)));
        }

        public bool Update()
        {
            if ((input.MouseDown & MouseButtons.Left) == 0)
                return false;

            // When mouse button is clicked, store cursor position and angles
            if ((input.MousePressed & MouseButtons.Left) != 0)
            {
                mouseOrigin = input.MousePoint;

                // Calculate angles from the vector
                angleOriginX = Math.Atan2(Vector.Z, Vector.X);
                angleOriginY = Math.Asin(Vector.Y);
            }

            // Calculate how much to change the angles
            double angleDeltaX = -(input.MousePoint.X - mouseOrigin.X) * Sensitivity;
            double angleDeltaY = (input.MousePoint.Y - mouseOrigin.Y) * Sensitivity;

            SetByAngles(angleOriginX + angleDeltaX, angleOriginY + angleDeltaY);

            return true;
        }
    }
}
