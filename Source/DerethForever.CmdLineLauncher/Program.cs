/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DerethForever.CmdLineLauncher
{
    public class Program
    {
        private static readonly LauncherConfig config = LauncherConfig.Load();

        public static void Main(string[] args)
        {
            if (config != null)
            {
                string command = null;

                while (command != "1")
                {
                    Console.WriteLine("Options:");
                    Console.WriteLine("1) Exit");
                    Console.WriteLine("2) Authenticated Login");
                    Console.WriteLine("3) No-Auth Login");
                    Console.WriteLine("4) Help");
                    Console.Write(@"Enter Option (1-4) >> ");
                    command = Console.ReadLine();

                    switch (command)
                    {
                        case "2":
                            AuthenticatedLogin(args);
                            break;
                        case "3":
                            NoAuthLogin(args);
                            break;
                        case "4":
                            Help();
                            break;
                    }

                    args = new string[0];
                }
            } else
                Console.WriteLine("Please check your launcher_config.json file for errors.");
        }

        public static void Help()
        {
            Console.WriteLine("Authenticated Login:");
            Console.WriteLine("   Prompts for username and password.  if more than 1 subscriptions exists, you will");
            Console.WriteLine("   also be prompted to select one.  if no subscriptions exists, one will be created");
            Console.WriteLine("   for you.  this works regardless of server security settings as the auth token is");
            Console.WriteLine("   ignored and not validated.");
            Console.WriteLine("");
            Console.WriteLine("No-Auth Login:");
            Console.WriteLine("   This requires the server to allow unsecure connections.  Your characters and items");
            Console.WriteLine("   are subject to being taken by anybody that can access this server and guess/discover");
            Console.WriteLine("   your username.");
            Console.WriteLine("");
        }

        public static void NoAuthLogin(string[] args)
        {
            string username;
            if (args.Length > 0)
                username = args[0];
            else
            {
                Console.Write(@"Username: ");
                username = Console.ReadLine();
            }

            string exe = config.ClientExe;
            string gameServer = config.GameServer;
            string gameArgs = $"-a {username} -h {gameServer} -glsticketdirect null";
            ProcessStartInfo psi = new ProcessStartInfo(exe, gameArgs);
            Console.WriteLine($"Game Args: {gameArgs}");
            psi.WorkingDirectory = System.IO.Path.GetDirectoryName(exe);
            Process.Start(psi);
        }

        public static void AuthenticatedLogin(string[] args)
        {
            string username;
            string password;

            if (args.Length > 0)
                username = args[0];
            else
            {
                Console.Write(@"Username: ");
                username = Console.ReadLine();
            }

            if (string.IsNullOrWhiteSpace(username))
                return;

            if (args.Length > 1)
                password = args[1];
            else
            {
                Console.Write(@"Password: ");
                password = Console.ReadLine();
            }

            if (string.IsNullOrWhiteSpace(password))
                return;

            // attempt to log in with the password
            RestClient authClient = new RestClient(config.GameApi);
            RestRequest authRequest = new RestRequest("Account/Authenticate", Method.POST);
            authRequest.AddJsonBody(new { Username = username, Password = password });
            IRestResponse authResponse = authClient.Execute(authRequest);

            if (authResponse.StatusCode != System.Net.HttpStatusCode.OK)
            {
                // show the error
                Console.WriteLine("Error logging in");
                Console.WriteLine(authResponse.Content);
                Console.ReadLine();
                return;
            }

            Console.WriteLine("Auth successful, retrieving subscriptions...");
            JObject response = JObject.Parse(authResponse.Content);
            string authToken = (string)response.SelectToken("authToken");

            RestClient subClient = new RestClient(config.GameApi);
            RestRequest subsRequest = new RestRequest("Subscription/Get", Method.GET);
            subsRequest.AddHeader("Authorization", "Bearer " + authToken);
            IRestResponse subsResponse = subClient.Execute(subsRequest);

            if (subsResponse.StatusCode != System.Net.HttpStatusCode.OK)
            {
                // show the error
                Console.WriteLine("Error getting subscriptions:");
                Console.WriteLine(subsResponse.Content);
                Console.ReadLine();
                return;
            }

            List<Subscription> subs = JsonConvert.DeserializeObject<List<Subscription>>(subsResponse.Content);

            if (subs.Count < 1)
            {
                Console.Write(@"DerethForever >> ");
                string yesno = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(yesno))
                    yesno = "y";

                if (yesno.ToLower() != "y")
                    return;

                Console.Write(@"DerethForever >> ");
                string subName = Console.ReadLine();

                // attempt to create subscription
                Console.WriteLine("Attempting to create a subscription...");
                subsRequest = new RestRequest("/Subscription/Create", Method.POST);
                subsRequest.AddQueryParameter("subscriptionName", subName);
                subsResponse = subClient.Execute(subsRequest);

                if (subsResponse.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    // show the error
                    Console.WriteLine(subsResponse.Content);
                    Console.ReadLine();
                    return;
                }

                Subscription sub = JsonConvert.DeserializeObject<Subscription>(subsResponse.Content);
                subs = new List<Subscription>() { sub };
            }

            string subscriptionId = null;
            int subIndex = 0;

            if (subs.Count == 1)
            {
                subscriptionId = subs[0].SubscriptionGuid.ToString();
            }
            else if (args.Length > 2 && subs.Count > 1 && int.TryParse(args[2], out subIndex) && subIndex < subs.Count)
            {
                // third param is the subscription index
                subscriptionId = subs[subIndex].SubscriptionGuid.ToString();
            }
            else
            {
                Console.WriteLine("Select a subscription.");

                // enumerate the subs
                for (int i = 0; i < subs.Count; i++)
                    Console.WriteLine($"{i}) {subs[i].Name}");

                string selectedSub = Console.ReadLine();

                if (int.TryParse(selectedSub, out subIndex) && subIndex < subs.Count)
                {
                    subscriptionId = subs[subIndex].SubscriptionGuid.ToString();
                }
            }

            Console.WriteLine($"ticket length: {authToken.Length}");

            if (subscriptionId != null)
            {
                string exe = config.ClientExe;
                string gameServer = config.GameServer;
                string gameArgs = $"-a {subscriptionId} -h {gameServer} -glsticketdirect {authToken}";
                ProcessStartInfo psi = new ProcessStartInfo(exe, gameArgs);
                Console.WriteLine($"Game Args: {gameArgs}");
                psi.WorkingDirectory = System.IO.Path.GetDirectoryName(exe);
                Process.Start(psi);
            }
        }
    }
}
