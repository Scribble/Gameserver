﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;

namespace DerethForever.DatLoader.Entity
{
    /// <summary>
    /// Helper function to read position data from dat files
    /// </summary>
    public static class PositionExtensions
    {
        /// <summary>
        /// Reads and returns a full spatial position with X,Y,Z and Quaternion W,X,Y,Z values.
        /// </summary>
        public static Position ReadPosition(DatReader datReader)
        {
            Position p = new Position();
            p.Read(datReader);
            return p;
        }

        /// <summary>
        /// Reads and returns just the X,Y,Z portal of a Position
        /// </summary>
        public static Position ReadPositionFrame(DatReader datReader)
        {
            Position p = new Position();
            p.ReadFrame(datReader);
            return p;
        }

        /// <summary>
        /// Reads and returns a full position with Landblock, X,Y,Z and Quaternion W,X,Y,Z values.
        /// </summary>
        public static Position ReadLandblockPosition(DatReader datReader)
        {
            Position p = new Position();
            p.Cell = datReader.ReadUInt32();
            p.Read(datReader);
            return p;
        }

        public static void Read(this Position p, DatReader datReader)
        {
            p.PositionX = datReader.ReadSingle();
            p.PositionY = datReader.ReadSingle();
            p.PositionZ = datReader.ReadSingle();
            p.RotationW = datReader.ReadSingle();
            p.RotationX = datReader.ReadSingle();
            p.RotationY = datReader.ReadSingle();
            p.RotationZ = datReader.ReadSingle();
        }

        public static void ReadFrame(this Position p, DatReader datReader)
        {
            p.PositionX = datReader.ReadSingle();
            p.PositionY = datReader.ReadSingle();
            p.PositionZ = datReader.ReadSingle();
        }
    }
}
