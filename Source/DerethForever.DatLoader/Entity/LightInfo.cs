﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;

namespace DerethForever.DatLoader.Entity
{
    public class LightInfo
    {
        public Position ViewerspaceLocation { get; set; }
        public uint Color { get; set; } // _RGB Color. Red is bytes 3-4, Green is bytes 5-6, Blue is bytes 7-8. Bytes 1-2 are always FF (?)
        public float Intensity { get; set; }
        public float Falloff { get; set; }
        public float ConeAngle { get; set; }

        public static LightInfo Read(DatReader datReader)
        {
            LightInfo obj = new LightInfo();

            obj.ViewerspaceLocation = PositionExtensions.ReadPosition(datReader);
            obj.Color = datReader.ReadUInt32();
            obj.Intensity = datReader.ReadSingle();
            obj.Falloff = datReader.ReadSingle();
            obj.ConeAngle = datReader.ReadSingle();

            return obj;
        }
    }
}