﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;

namespace DerethForever.DatLoader.Entity
{
    public class Contract
    {
        public uint Version { get; set; }
        public uint ContractId { get; set; }
        public string ContractName { get; set; }
        public string Description { get; set; }
        public string DescriptionProgress { get; set; }
        public string NameNPCStart { get; set; }
        public string NameNPCEnd { get; set; }
        public string QuestflagStamped { get; set; }
        public string QuestflagStarted { get; set; }
        public string QuestflagFinished { get; set; }
        public string QuestflagProgress { get; set; }
        public string QuestflagTimer { get; set; }
        public string QuestflagRepeatTime { get; set; }
        public Position LocationNPCStart { get; set; }
        public Position LocationNPCEnd { get; set; }
        public Position LocationQuestArea { get; set; }

        public static Contract Read(DatReader datReader)
        {
            Contract obj = new Contract();

            obj.Version = datReader.ReadUInt32();
            obj.ContractId = datReader.ReadUInt32();
            obj.ContractName = datReader.ReadPString();
            datReader.AlignBoundary();

            obj.Description = datReader.ReadPString();
            datReader.AlignBoundary();
            obj.DescriptionProgress = datReader.ReadPString();
            datReader.AlignBoundary();

            obj.NameNPCStart = datReader.ReadPString();
            datReader.AlignBoundary();
            obj.NameNPCEnd = datReader.ReadPString();
            datReader.AlignBoundary();

            obj.QuestflagStamped = datReader.ReadPString();
            datReader.AlignBoundary();
            obj.QuestflagStarted = datReader.ReadPString();
            datReader.AlignBoundary();
            obj.QuestflagFinished = datReader.ReadPString();
            datReader.AlignBoundary();
            obj.QuestflagProgress = datReader.ReadPString();
            datReader.AlignBoundary();
            obj.QuestflagTimer = datReader.ReadPString();
            datReader.AlignBoundary();
            obj.QuestflagRepeatTime = datReader.ReadPString();
            datReader.AlignBoundary();

            obj.LocationNPCStart = PositionExtensions.ReadLandblockPosition(datReader);
            obj.LocationNPCEnd = PositionExtensions.ReadLandblockPosition(datReader);
            obj.LocationQuestArea = PositionExtensions.ReadLandblockPosition(datReader);

            return obj;
        }
    }
}
