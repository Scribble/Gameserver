﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    public class Generator
    {
        public int Id { get; set; }

        public int Count { get; set; }

        public string Name { get; set; }

        public List<Generator> Items;

        public Generator()
        {
            Id = 0;
            Count = 0;
            Name = "";
            Items = new List<Generator>();
        }

        public Generator GetNextGenerator(DatReader datReader)
        {
            Name = datReader.ReadObfuscatedString();
            datReader.AlignBoundary();
            Id = datReader.ReadInt32();
            Count = datReader.ReadInt32();
                
            // Console.WriteLine($"{Id:X8} {Count:X8} {Name}");

            for (var i = 0; i < Count; i++)
            {
                var child = new Generator();
                child = child.GetNextGenerator(datReader);
                Items.Add(child);
            }
            return this;
        }
    }
}
