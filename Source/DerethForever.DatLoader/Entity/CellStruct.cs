/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

using DerethForever.Entity.Enum;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Entity;

namespace DerethForever.DatLoader.Entity
{
    public class CellStruct : IPhysicsPart
    {
        public CVertexArray VertexArray { get; set; }

        public Dictionary<ushort, Polygon> Polygons { get; set; } = new Dictionary<ushort, Polygon>();

        public List<ushort> Portals { get; set; } = new List<ushort>();

        public BSPTree CellBSP { get; set; }

        public Dictionary<ushort, Polygon> PhysicsPolygons { get; set; } = new Dictionary<ushort, Polygon>();

        public BSPTree PhysicsBSP { get; set; }

        public BSPTree DrawingBSP { get; set; }
        
        public static CellStruct Read(DatReader datReader) { 
            CellStruct obj = new CellStruct();

            uint numPolygons = datReader.ReadUInt32();
            uint numPhysicsPolygons = datReader.ReadUInt32();
            uint numPortals = datReader.ReadUInt32();

            obj.VertexArray = CVertexArray.Read(datReader);

            for (uint i = 0; i < numPolygons; i++)
            {
                ushort poly_id = datReader.ReadUInt16();
                obj.Polygons.Add(poly_id, Polygon.Read(datReader));
            }

            for (uint i = 0; i < numPortals; i++)
                obj.Portals.Add(datReader.ReadUInt16());

            datReader.AlignBoundary();

            obj.CellBSP = BSPTree.Read(datReader, BSPType.Cell);

            for (uint i = 0; i < numPhysicsPolygons; i++)
            {
                ushort poly_id = datReader.ReadUInt16();
                obj.PhysicsPolygons.Add(poly_id, Polygon.Read(datReader));
            }
            obj.PhysicsBSP = BSPTree.Read(datReader, BSPType.Physics);

            uint hasDrawingBSP = datReader.ReadUInt32();
            if (hasDrawingBSP != 0)
                obj.DrawingBSP = BSPTree.Read(datReader, BSPType.Drawing);

            datReader.AlignBoundary();

            return obj;
        }
    }
}
