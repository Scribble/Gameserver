﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    public class SkyTimeOfDay
    {
        public float Begin { get; set; }
        public float DirBright { get; set; }
        public float DirHeading { get; set; }
        public float DirPitch { get; set; }
        public uint DirColor { get; set; }

        public float AmbBright { get; set; }
        public uint AmbColor { get; set; }

        public float MinWorldFog { get; set; }
        public float MaxWorldFog { get; set; }
        public uint WorldFogColor { get; set; }
        public uint WorldFog { get; set; }

        public List<SkyObjectReplace> SkyObjReplace { get; set; } = new List<SkyObjectReplace>();

        public static SkyTimeOfDay Read(DatReader datReader)
        {
            SkyTimeOfDay obj = new SkyTimeOfDay();
            obj.Begin = datReader.ReadSingle();
            obj.DirBright = datReader.ReadSingle();
            obj.DirHeading = datReader.ReadSingle();
            obj.DirPitch = datReader.ReadSingle();
            obj.DirColor = datReader.ReadUInt32();

            obj.AmbBright = datReader.ReadSingle();
            obj.AmbColor = datReader.ReadUInt32();

            obj.MinWorldFog = datReader.ReadSingle();
            obj.MaxWorldFog = datReader.ReadSingle();
            obj.WorldFogColor = datReader.ReadUInt32();
            obj.WorldFog = datReader.ReadUInt32();

            uint num_sky_obj_replace = datReader.ReadUInt32();
            for (uint i = 0; i < num_sky_obj_replace; i++)
                obj.SkyObjReplace.Add(SkyObjectReplace.Read(datReader));

            return obj;
        }
    }
}
