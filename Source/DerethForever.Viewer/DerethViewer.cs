/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
************************************************************************/
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using DerethForever.ClientLib.DatUtil;
using DerethForever.ClientLib.Entity;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GameTime = Microsoft.Xna.Framework.GameTime;

namespace DerethForever.Viewer
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class DerethViewer : Game
    {
        GraphicsDeviceManager graphics;
        Effect effect;

        private static string _datFolder;
        private static string _cellDatFilename = "client_cell_1.dat";
        private static string _cellDat;
        private static string _portalDatFilename = "client_portal.dat";
        private static string _portalDat;
        private List<Landblock> _landblocks = new List<Landblock>();

        private Color groundColor = Color.White;
        private VertexPositionColorNormal[] groundPolys = new VertexPositionColorNormal[0];
        private VertexPositionColorNormal[] groundLines = new VertexPositionColorNormal[0];

        private Color buildingColor = Color.Beige;
        private VertexPositionColorNormal[] buildingPolys = new VertexPositionColorNormal[0];
        private VertexPositionColorNormal[] buildingLines = new VertexPositionColorNormal[0];

        private Color staticObjColor = Color.Azure;
        private VertexPositionColorNormal[] staticObjPolys = new VertexPositionColorNormal[0];
        private VertexPositionColorNormal[] staticObjLines = new VertexPositionColorNormal[0];

        // Arwic 0xBCA8
        // Aerlinthe 
        private uint xBase = 0xB0;
        private uint yBase = 0xE6;

        private Vector3 cameraPosition = new Vector3(1600, -50, 500);
        private Vector3 cameraLookAtVector = new Vector3(1600, 500, 0);
        private float cameraDirection = 0f; // heading, in radians
        private float cameraDistance = 500f;

        public DerethViewer()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1440;
            graphics.PreferredBackBufferHeight = 900;

            Content.RootDirectory = "Content";

            _datFolder = ConfigurationManager.AppSettings["DatFolder"];
            _cellDat = Path.Combine(_datFolder, _cellDatFilename);
            _portalDat = Path.Combine(_datFolder, _portalDatFilename);

            CellDatReader.Initialize(_cellDat);
            PortalDatReader.Initialize(_portalDat);

            graphics.GraphicsProfile = GraphicsProfile.HiDef;
        }

        protected override void LoadContent()
        {
            byte[] effectsFile = File.ReadAllBytes("Content/effects.mgfxo");
            effect = new Effect(GraphicsDevice, effectsFile);
            base.LoadContent();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            for (uint x = xBase; x < (xBase + 20); x++)
            {
                for (uint y = yBase; y < (yBase + 20); y++)
                {
                    uint landblockId = (((x << 8) | y) << 16);
                    _landblocks.Add(Landblock.Load(landblockId));
                }
            }

            LoadGround();
            LoadBuildings();
            LoadStaticObjects();

            effect = new BasicEffect(graphics.GraphicsDevice);

            base.Initialize();
            ConfigureEffect();


            cameraPosition = new Vector3((xBase + 10) * 192f, yBase * 192, 500);
            cameraLookAtVector = new Vector3(cameraPosition.X, cameraPosition.Y + 100, 0);
        }

        private void LoadGround()
        {
            List<VertexPositionColorNormal> tTriangles = new List<VertexPositionColorNormal>();

            foreach (var lb in _landblocks)
            {
                System.Numerics.Vector3 vZero = System.Numerics.Vector3.Zero;
                int xCells, yCells;
                LandDefs.GetOutsideLCoord(lb.Id, vZero, out xCells, out yCells);

                Vector3 offset = new Vector3(xCells * 24f, yCells * 24f, 0f);

                // draw objects
                foreach (var poly in lb.Polygons)
                {
                    tTriangles.Add(new VertexPositionColorNormal() { Position = Vec(poly.Vertices[2]) + offset, Color = groundColor, Normal = Vec(poly.Plane.Normal) });
                    tTriangles.Add(new VertexPositionColorNormal() { Position = Vec(poly.Vertices[1]) + offset, Color = groundColor, Normal = Vec(poly.Plane.Normal) });
                    tTriangles.Add(new VertexPositionColorNormal() { Position = Vec(poly.Vertices[0]) + offset, Color = groundColor, Normal = Vec(poly.Plane.Normal) });
                }
            }
            
            groundPolys = tTriangles.ToArray();
        }

        private void LoadBuildings()
        {
            List<VertexPositionColorNormal> tTriangles = new List<VertexPositionColorNormal>();

            foreach (var lb in _landblocks)
            {
                System.Numerics.Vector3 vZero = System.Numerics.Vector3.Zero;
                int xCells, yCells;
                LandDefs.GetOutsideLCoord(lb.Id, vZero, out xCells, out yCells);

                Vector3 offset = new Vector3(xCells * 24f, yCells * 24f, 0f);

                // draw objects
                foreach (var b in lb.Buildings)
                {
                    foreach (var part in b.PartArray.Parts)
                    {
                        foreach (var g in part.GraphicsObjects)
                        {
                            var translate = Matrix.CreateTranslation(offset + Vec(b.CurrentPosition.Frame.Origin));
                            var rotate = Matrix.CreateFromQuaternion(Quat(b.CurrentPosition.Frame.Quaternion));

                            LoadPhysicsObject(tTriangles, g, rotate * translate, b.PartArray.Scale, buildingColor);
                        }
                    }
                }
            }

            buildingPolys = tTriangles.ToArray();
        }

        private void LoadStaticObjects()
        {
            int staticObjects = 0;
            List<VertexPositionColorNormal> tTriangles = new List<VertexPositionColorNormal>();

            foreach (var lb in _landblocks)
            {
                System.Numerics.Vector3 vZero = System.Numerics.Vector3.Zero;
                int xCells, yCells;
                LandDefs.GetOutsideLCoord(lb.Id, vZero, out xCells, out yCells);

                Vector3 offset = new Vector3(xCells * 24f, yCells * 24f, 0f);

                // draw objects
                foreach (var so in lb.StaticObjects)
                {
                    foreach (var part in so.PartArray.Parts)
                    {
                        foreach (var g in part.GraphicsObjects)
                        {
                            staticObjects++;
                            var placement = Vec(so.CurrentPosition.Frame.Origin);
                            var translate = Matrix.CreateTranslation(offset + placement);
                            var rotate = Matrix.CreateFromQuaternion(Quat(so.CurrentPosition.Frame.Quaternion));

                            if (g.PhysicsPolygons.Count > 0)
                                LoadPhysicsObject(tTriangles, g, rotate * translate, so.PartArray.Scale, staticObjColor);
                            else
                                LoadDrawingObject(tTriangles, g, rotate * translate, so.PartArray.Scale, staticObjColor);
                        }
                    }
                }
            }

            staticObjPolys = tTriangles.ToArray();
        }

        private void LoadPhysicsObject(List<VertexPositionColorNormal> triangles, GraphicsObject g, Matrix offset, System.Numerics.Vector3 scale, Color color)
        {
            foreach (var poly in g.PhysicsPolygons)
            {
                for (int i = 0; i < poly.VertexIds.Count - 2; i++)
                {
                    var v0 = g.VertexArray.Vertices[poly.VertexIds[0]].Vector; // yes, 0
                    var v1 = g.VertexArray.Vertices[poly.VertexIds[i + 1]].Vector;
                    var v2 = g.VertexArray.Vertices[poly.VertexIds[i + 2]].Vector;

                    if (scale != System.Numerics.Vector3.One)
                    {
                        v0 *= scale;
                        v1 *= scale;
                        v2 *= scale;
                    }

                    System.Numerics.Plane p = System.Numerics.Plane.CreateFromVertices(v0, v1, v2);
                    var normal = Vec(p.Normal);

                    triangles.Add(new VertexPositionColorNormal() { Position = Vector3.Transform(Vec(v2), offset), Color = color, Normal = normal });
                    triangles.Add(new VertexPositionColorNormal() { Position = Vector3.Transform(Vec(v1), offset), Color = color, Normal = normal });
                    triangles.Add(new VertexPositionColorNormal() { Position = Vector3.Transform(Vec(v0), offset), Color = color, Normal = normal });
                }
            }
        }

        private void LoadDrawingObject(List<VertexPositionColorNormal> triangles, GraphicsObject g, Matrix offset, System.Numerics.Vector3 scale, Color color)
        {
            Color semiTrans = new Color(color.PackedValue);
            semiTrans.A = 0x70; // half transparent

            foreach (var poly in g.Polygons)
            {
                for (int i = 0; i < poly.VertexIds.Count - 2; i++)
                {
                    var v0 = g.VertexArray.Vertices[poly.VertexIds[0]].Vector; // yes, 0
                    var v1 = g.VertexArray.Vertices[poly.VertexIds[i + 1]].Vector;
                    var v2 = g.VertexArray.Vertices[poly.VertexIds[i + 2]].Vector;

                    if (scale != System.Numerics.Vector3.One)
                    {
                        v0 *= scale;
                        v1 *= scale;
                        v2 *= scale;
                    }

                    System.Numerics.Plane p = System.Numerics.Plane.CreateFromVertices(v0, v1, v2);
                    var normal = Vec(p.Normal);

                    triangles.Add(new VertexPositionColorNormal() { Position = Vector3.Transform(Vec(v2), offset), Color = semiTrans, Normal = normal });
                    triangles.Add(new VertexPositionColorNormal() { Position = Vector3.Transform(Vec(v1), offset), Color = semiTrans, Normal = normal });
                    triangles.Add(new VertexPositionColorNormal() { Position = Vector3.Transform(Vec(v0), offset), Color = semiTrans, Normal = normal });
                }
            }
        }

        protected override void Update(GameTime gameTime)
        {
            var state = Keyboard.GetState();
            float moveSpeed = 2;

            if (state.IsKeyDown(Keys.E))
            {
                cameraPosition.X += moveSpeed * (float)System.Math.Cos(cameraDirection);
                cameraPosition.Y += -moveSpeed * (float)System.Math.Sin(cameraDirection);
            }

            if (state.IsKeyDown(Keys.Q))
            {
                cameraPosition.X -= moveSpeed * (float)System.Math.Cos(cameraDirection);
                cameraPosition.Y -= -moveSpeed * (float)System.Math.Sin(cameraDirection);
            }

            if (state.IsKeyDown(Keys.Up) || state.IsKeyDown(Keys.W))
            {
                cameraPosition.X += moveSpeed * (float)System.Math.Sin(cameraDirection);
                cameraPosition.Y += moveSpeed * (float)System.Math.Cos(cameraDirection);
            }

            if (state.IsKeyDown(Keys.Down) || state.IsKeyDown(Keys.S))
            {
                cameraPosition.X -= moveSpeed * (float)System.Math.Sin(cameraDirection);
                cameraPosition.Y -= moveSpeed * (float)System.Math.Cos(cameraDirection);
            }

            if (state.IsKeyDown(Keys.Space))
                cameraPosition.Z += moveSpeed;
            if (state.IsKeyDown(Keys.LeftShift))
                cameraPosition.Z -= moveSpeed;

            if (state.IsKeyDown(Keys.Left) || state.IsKeyDown(Keys.A))
                cameraDirection -= 0.02f;
            if (state.IsKeyDown(Keys.Right) || state.IsKeyDown(Keys.D))
                cameraDirection += 0.02f;

            Vector3 tempOffset;
            tempOffset.X = (float)System.Math.Sin(cameraDirection) * cameraDistance;
            tempOffset.Y = (float)System.Math.Cos(cameraDirection) * cameraDistance;
            tempOffset.Z = -300f;
            cameraLookAtVector = cameraPosition + tempOffset;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            SetCamera();
            DrawGround();
            DrawBuildings();
            DrawStaticObjects();
            
            base.Draw(gameTime);
        }

        private void DrawGround()
        {
            DrawCollection(groundPolys, 3);
        }

        private void DrawBuildings()
        {
            DrawCollection(buildingPolys, 3);
            // DrawCollection(buildingLines, 2);
        }

        private void DrawStaticObjects()
        {
            DrawCollection(staticObjPolys, 3);
            // DrawCollection(buildingLines, 2);
        }

        private void DrawCollection(VertexPositionColorNormal[] vertecis, int segmentLength)
        {
            int count = vertecis.Length / segmentLength;

            if (count < 1)
                return;

            foreach (var pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphics.GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleList, vertecis, 0, count, VertexPositionColorNormal.VertexDeclaration);
            }
        }

        private void ConfigureEffect()
        {
            effect.Parameters["xWorld"].SetValue(Matrix.Identity);
            effect.Parameters["xAmbient"].SetValue(0.3f);
            effect.Parameters["xShowNormals"].SetValue(true);

            effect.CurrentTechnique = effect.Techniques["Colored"];

            effect.Parameters["xEnableLighting"].SetValue(true);
            Vector3 lightDirection = new Vector3(0.1f, 0.1f, -0.5f);
            effect.Parameters["xLightDirection"].SetValue(lightDirection);
        }

        private void SetCamera()
        {
            // The assignment of effect.View and effect.Projection
            // are nearly identical to the code in the Model drawing code.
            var cameraUpVector = Vector3.UnitZ;
            float aspectRatio = graphics.PreferredBackBufferWidth / (float)graphics.PreferredBackBufferHeight;
            float fieldOfView = Microsoft.Xna.Framework.MathHelper.PiOver2;
            float nearClipPlane = 1;
            float farClipPlane = 5000;

            var projectionMatrix = Matrix.CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearClipPlane, farClipPlane);
            effect.Parameters["xProjection"].SetValue(projectionMatrix);

            var viewMatrix = Matrix.CreateLookAt(cameraPosition, cameraLookAtVector, cameraUpVector);
            effect.Parameters["xView"].SetValue(viewMatrix);
        }

        private Vector3 Vec(System.Numerics.Vector3 snVector)
        {
            return new Vector3(snVector.X, snVector.Y, snVector.Z);
        }

        private Quaternion Quat(System.Numerics.Quaternion q)
        {
            return new Quaternion(q.X, q.Y, q.Z, q.W);
        }
    }
}
