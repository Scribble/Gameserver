/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DerethForever.ClientLib.Entity;

namespace DerethForever.ClientLib.Tests
{
    [TestClass]
    public class PhysicsTests
    {
        [TestMethod]
        public void PhysicsTest_CheckClone()
        {
            InterpretedMotionState ims  = new InterpretedMotionState();
            ims.ForwardSpeed = 300f;
            ims.CopyMovementFrom(out InterpretedMotionState copy, ims);
            Assert.IsTrue(copy != null);
            Assert.IsTrue(Math.Abs(copy.ForwardSpeed - ims.ForwardSpeed) < 0.001);
            copy.ForwardSpeed = 200f;
            Assert.IsFalse(Math.Abs(copy.ForwardSpeed - ims.ForwardSpeed) < 0.001);
        }

    }
}
