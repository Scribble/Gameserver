/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    public class BodyPartSelectionData
    {
        [JsonProperty("highLeftBack")]
        public double HighLeftBack { get; set; }

        [JsonProperty("highLeftFront")]
        public double HighLeftFront { get; set; }

        [JsonProperty("highRightBack")]
        public double HighRightBack { get; set; }

        [JsonProperty("highRightFront")]
        public double HighRightFront { get; set; }

        [JsonProperty("lowLeftBack")]
        public double LowLeftBack { get; set; }

        [JsonProperty("lowLeftFront")]
        public double LowLeftFront { get; set; }

        [JsonProperty("lowRightBack")]
        public double LowRightBack { get; set; }

        [JsonProperty("lowRightFront")]
        public double LowRightFront { get; set; }

        [JsonProperty("midLeftBack")]
        public double MidLeftBack { get; set; }

        [JsonProperty("midLeftFront")]
        public double MidLeftFront { get; set; }

        [JsonProperty("midRightBack")]
        public double MidRightBack { get; set; }

        [JsonProperty("midRightFront")]
        public double MidRightFront { get; set; }
    }
}
