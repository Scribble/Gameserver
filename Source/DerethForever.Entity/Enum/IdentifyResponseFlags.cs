﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.Entity.Enum
{
    [Flags]
    public enum IdentifyResponseFlags
    {
        None                        = 0x0000,
        IntStatsTable               = 0x0001,
        BoolStatsTable              = 0x0002,
        FloatStatsTable             = 0x0004,
        StringStatsTable            = 0x0008,
        SpellBook                   = 0x0010,
        WeaponProfile               = 0x0020,
        HookProfile                 = 0x0040,
        ArmorProfile                = 0x0080,
        CreatureProfile             = 0x0100,
        ArmorEnchantmentBitfield    = 0x0200,
        ResistEnchantmentBitfield   = 0x0400,
        WeaponEnchantmentBitfield   = 0x0800,
        DidStatsTable               = 0x1000,
        Int64StatsTable             = 0x2000,
        ArmorLevels                 = 0x4000,
    }
}
