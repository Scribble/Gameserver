﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
namespace DerethForever.Entity.Enum
{
    // Thanks to tfarley (aclogview) for these enums
    [Flags]
    public enum CharacterOptionDataFlag : uint
    {
        Shortcut                    = 0x00000001,
        SquelchList                 = 0x00000002,
        MultiSpellList              = 0x00000004,
        DesiredComps                = 0x00000008,
        ExtendedMultiSpellLists     = 0x00000010,
        SpellbookFilters            = 0x00000020,
        CharacterOptions2           = 0x00000040,
        TimestampFormat             = 0x00000080,
        GenericQualitiesData        = 0x00000100,
        GameplayOptions             = 0x00000200,
        SpellLists8                 = 0x00000400
    }
}
