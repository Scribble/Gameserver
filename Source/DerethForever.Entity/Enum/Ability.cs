/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Runtime.Serialization;

namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// these enum values are all lower case for the sake of the API.  it is not possible to override the ToString of them
    /// in such a way that "Strength" serializes as "strength".  breaking an internal standard is always better than
    /// breaking an external one.
    /// </summary>
    [Flags]
    public enum Ability : uint
    {
        None            = 0x000,

        strength        = 0x001,
        endurance       = 0x002,
        coordination    = 0X004,
        quickness       = 0x008,
        focus           = 0x010,
        self            = 0x020,

        [AbilityRegen(0.5)]
        [AbilityFormula(endurance, 2)]
        [AbilityVital(Vital.Health)]
        health          = 0x040,

        [AbilityRegen(1.0)]
        [AbilityFormula(endurance)]
        [AbilityVital(Vital.Stamina)]
        stamina         = 0x080,

        [AbilityRegen(0.7)]
        [AbilityFormula(self)]
        [AbilityVital(Vital.Mana)]
        mana            = 0x100,
        Full = strength | endurance | quickness | coordination | focus | self | health | stamina | mana
    }

    public static class AbilityExtensions
    {
        public static AbilityFormulaAttribute GetFormula(this Ability ability)
        {
            return ability.GetAttributeOfType<AbilityFormulaAttribute>();
        }

        public static Vital GetVital(this Ability ability)
        {
            return ability.GetAttributeOfType<AbilityVitalAttribute>().Vital;
        }

        // FIXME(ddevec): This will eventually be a formula...
        public static double GetRegenRate(this Ability ability)
        {
            return ability.GetAttributeOfType<AbilityRegenAttribute>().Rate;
        }
    }
}
