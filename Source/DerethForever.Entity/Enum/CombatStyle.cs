﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// exported from the decompiled client.
    /// </summary>
    public enum CombatStyle
    {
        Undef               = 0x00000,
        Unarmed             = 0x00001,
        OneHanded           = 0x00002,
        OneHandedAndShield  = 0x00004,
        TwoHanded           = 0x00008,
        Bow                 = 0x00010,
        Crossbow            = 0x00020,
        Sling               = 0x00040,
        ThrownWeapon        = 0x00080,
        DualWield           = 0x00100,
        Magic               = 0x00200,
        Atlatl              = 0x00400,
        ThrownShield        = 0x00800,
        Reserved1           = 0x01000,
        Reserved2           = 0x02000,
        Reserved3           = 0x04000,
        Reserved4           = 0x08000,
        StubbornMagic       = 0x10000,
        StubbornProjectile  = 0x20000,
        StubbornMelee       = 0x40000,
        StubbornMissile     = 0x80000,
        Melee               = Unarmed | OneHanded | OneHandedAndShield | TwoHanded | DualWield, // 271
        Missile             = Bow | Crossbow | Sling | ThrownWeapon | Atlatl | ThrownShield, // 3312
        // Magics           = Magic, // 512
        All                 = 65535
    }
}
