using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.Entity.Enum
{
    [Flags]
    public enum TransientState : uint
    {
        Uninitialized       = 0,
        Contact             = 1, // 0x1
        Walkable            = 2, // 0x2
        Sliding             = 4, // 0x4
        WaterContact        = 8, // 0x8
        StationaryFall      = 16, // 0x10
        StationaryStop      = 32, // 0x20
        StationaryStuck     = 64, // 0x40
        Active              = 128, // 0x80
        CheckEthereal       = 256 // 0x100
    }
}
