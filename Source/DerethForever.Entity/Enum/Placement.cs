/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

namespace DerethForever.Entity.Enum
{
    public enum Placement
    {
        Default              = 0,
        RightHandCombat      = 1,
        RightHandNonCombat   = 2,
        LeftHand             = 3,
        Belt                 = 4,
        Quiver               = 5,
        Shield               = 6,
        LeftWeapon           = 7,
        LeftUnarmed          = 8,
        SpecialCrowssbowBolt = 51,
        MissileFlight        = 52,
        Resting              = 101,
        Other                = 102,
        Hook                 = 103,
    }
}
