﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.Linq;

using DerethForever.Entity.Enum.Properties;

namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// Static selection of client enums based on attributes
    /// </summary>
    public static class ClientProperties
    {
        /// <summary>
        /// Method to return a list of enums by attribute type - in this case not [ServerOnly] using generics to enhance code reuse.
        /// </summary>
        /// <typeparam name="T">Enum to list by NOT [ServerOnly]</typeparam>
        /// <typeparam name="TResult">Type of the results</typeparam>
        private static List<TResult> GetValues<T, TResult>()
        {
            return typeof(T).GetFields().Select(x => new
            {
                att = x.GetCustomAttributes(false).OfType<ServerOnlyAttribute>().FirstOrDefault(),
                member = x
            }).Where(x => x.att == null && x.member.Name != "value__").Select(x => (TResult)x.member.GetValue(null)).ToList();
        }

        /// <summary>
        /// returns a list of values for PropertyInt that are NOT [ServerOnly]
        /// </summary>
        public static List<ushort> PropertiesInt = GetValues<PropertyInt, ushort>();

        public static List<ushort> PropertiesInt64 = GetValues<PropertyInt64, ushort>();

        public static List<ushort> PropertiesBool = GetValues<PropertyBool, ushort>();

        public static List<ushort> PropertiesString = GetValues<PropertyString, ushort>();

        public static List<ushort> PropertiesDouble = GetValues<PropertyDouble, ushort>();

        public static List<ushort> PropertiesDataId = GetValues<PropertyDataId, ushort>();

        public static List<ushort> PropertiesInstanceId = GetValues<PropertyInstanceId, ushort>();
    }
}
