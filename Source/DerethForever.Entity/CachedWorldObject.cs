/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using MySql.Data.MySqlClient;

namespace DerethForever.Entity
{
    [DbTable("vw_ace_object")]
    public class CachedWorldObject
    {
        [DbField("landblock", (int)MySqlDbType.UInt32, IsCriteria = true, ListGet = true)]
        public uint Landblock { get; set; }

        [DbField("weenieClassId", (int)MySqlDbType.UInt32)]
        public uint WeenieClassId { get; set; }

        [DbField("aceObjectId", (int)MySqlDbType.UInt32)]
        public uint DataObjectId { get; set; }

        [DbField("landblockRaw", (int)MySqlDbType.UInt32)]
        public uint LandblockRaw { get; set; }

        [DbField("posX", (int)MySqlDbType.Float)]
        public float PositionX { get; set; }

        [DbField("posY", (int)MySqlDbType.Float)]
        public float PositionY { get; set; }

        [DbField("posZ", (int)MySqlDbType.Float)]
        public float PositionZ { get; set; }

        [DbField("qW", (int)MySqlDbType.Float)]
        public float RotationW { get; set; }

        [DbField("qX", (int)MySqlDbType.Float)]
        public float RotationX { get; set; }

        [DbField("qY", (int)MySqlDbType.Float)]
        public float RotationY { get; set; }

        [DbField("qZ", (int)MySqlDbType.Float)]
        public float RotationZ { get; set; }

        [DbField("currentMotionState", (int)MySqlDbType.Text)]
        public string CurrentMotionState { get; set; }
    }
}
