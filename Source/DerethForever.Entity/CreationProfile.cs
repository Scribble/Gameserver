/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;

namespace DerethForever.Entity
{
    [DbTable("creation_profile")]
    public class CreationProfile
    {
        /// <summary>
        /// Table field Primary Key
        /// </summary>
        [JsonProperty("creationProfileGuid")]
        public Guid? CreationProfileGuid { get; set; }

        [JsonIgnore]
        [DbField("creationProfileGuid", (int)MySqlDbType.Binary, Update = false, IsCriteria = true)]
        public byte[] CreationProfileGuid_Binder
        {
            get { return CreationProfileGuid == null ? null : CreationProfileGuid.Value.ToByteArray(); }
            set { CreationProfileGuid = (value == null ? (Guid?)null : new Guid(value)); }
        }

        /// <summary>
        /// owning weenid id
        /// </summary>
        [JsonProperty("ownerId")]
        [DbField("ownerId", (int)MySqlDbType.UInt32, ListGet = true, ListDelete = true)]
        public uint OwnerId { get; set; }

        /// <summary>
        /// Table Field Weenie Class
        /// </summary>
        [JsonProperty("weenieClassId")]
        [DbField("weenieClassId", (int)MySqlDbType.UInt32)]
        public uint? WeenieClassId { get; set; }

        /// <summary>
        /// Table Field Palette
        /// </summary>
        [JsonProperty("palette")]
        [DbField("palette", (int)MySqlDbType.UInt32)]
        public uint? Palette { get; set; }

        /// <summary>
        /// Table Field Shade - used as an offset percentage into palette (? check with Iron Golem)
        /// </summary>
        [JsonProperty("shade")]
        [DbField("shade", (int)MySqlDbType.Double)]
        public double? Shade { get; set; }

        /// <summary>
        /// Table Field Destination - where are we creating the object.
        /// </summary>
        [JsonProperty("destination")]
        [DbField("destination", (int)MySqlDbType.UInt32)]
        public uint? Destination { get; set; }

        /// <summary>
        /// Table Field StackSize - how many are we creating of stackable items.   Not all items have this property.
        /// </summary>
        [JsonProperty("stackSize")]
        [DbField("stackSize", (int)MySqlDbType.Int32)]
        public int? StackSize { get; set; }

        /// <summary>
        /// Table Field TryToBond - flag for the item that we create to be bonded.
        /// </summary>
        [JsonProperty("bond")]
        public bool? TryToBond { get; set; }

        [JsonIgnore]
        [DbField("bond", (int)MySqlDbType.Byte)]
        public object TryToBond_Binder
        {
            get { return TryToBond == null ? (byte?)null : (TryToBond.Value ? (byte)1 : (byte)0); }
            set
            {
                if (value is byte?)
                {
                    byte? v = (byte?)value;
                    TryToBond = (v == null ? (bool?)null : (v.Value != 0));
                }
                else if (value is bool?)
                    TryToBond = (bool?)value;
                else if (value is bool)
                    TryToBond = (bool)value;
                else if (value == null)
                    TryToBond = null;
                else if (value is SByte) // no idea wtf this is, but that's how it comes in
                    TryToBond = ((SByte)value) == 0;
                else
                    throw new ArgumentException("wtf");
            }
        }
    }
}
