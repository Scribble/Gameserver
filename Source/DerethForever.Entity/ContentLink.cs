﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Common;

using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    /// <summary>
    /// a 1-way association of content.  if a 2-way link is desired, it will have to be added
    /// in both places explicitly
    /// </summary>
    [DbTable("ace_content_link")]
    public class ContentLink : ChangeTrackedObject
    {
        /// <summary>
        /// content identifier
        /// </summary>
        [JsonIgnore]
        public Guid ContentGuid { get; set; }

        [JsonIgnore]
        [DbField("contentGuid1", (int)MySqlDbType.Binary, ListGet = true, ListDelete = true)]
        public byte[] ContentGuid_Binder
        {
            get { return ContentGuid.ToByteArray(); }
            set { ContentGuid = new Guid(value); }
        }

        /// <summary>
        /// associated content identifier
        /// </summary>
        [JsonProperty("associatedContentGuid")]
        public Guid AssociatedContentGuid { get; set; }

        [JsonIgnore]
        [DbField("contentGuid2", (int)MySqlDbType.Binary)]
        public byte[] AssociatedContentGuid_Binder
        {
            get { return AssociatedContentGuid.ToByteArray(); }
            set { AssociatedContentGuid = new Guid(value); }
        }
    }
}
