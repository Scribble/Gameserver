﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using MySql.Data.MySqlClient;
using DerethForever.Common;
using DerethForever.Entity.Enum;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    [DbTable("ace_object_properties_bigint")]
    public class DataObjectPropertiesInt64 : BaseDataProperty, ICloneable
    {
        private ulong? _value = 0;

        [JsonProperty("int64PropertyId")]
        [DbField("bigIntPropertyId", (int)MySqlDbType.UInt16, IsCriteria = true, Update = false)]
        public new uint PropertyId { get; set; }

        [JsonProperty("index")]
        [DbField("propertyIndex", (int)MySqlDbType.Byte, IsCriteria = true, Update = false)]
        public byte Index { get; set; } = 0;

        [JsonProperty("value")]
        [DbField("propertyValue", (int)MySqlDbType.UInt64)]
        public ulong? PropertyValue
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                IsDirty = true;
            }
        }

        [JsonIgnore]
        public override ObjectPropertyType PropertyType
        { get { return ObjectPropertyType.PropertyInt64; } }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
