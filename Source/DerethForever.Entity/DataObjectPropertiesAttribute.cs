﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using DerethForever.Common;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    [DbTable("ace_object_properties_attribute")]
    public class DataObjectPropertiesAttribute : BaseDataProperty, ICloneable
    {
        private uint _xpSpent = 0;
        private ushort _ranks = 0;
        private ushort _base = 0;

        [JsonProperty("attributeId")]
        [DbField("attributeId", (int)MySqlDbType.UInt16, IsCriteria = true, Update = false)]
        public ushort AttributeId { get; set; }

        [JsonProperty("baseValue")]
        [DbField("attributeBase", (int)MySqlDbType.UInt16)]
        public ushort AttributeBase
        {
            get
            {
                return _base;
            }
            set
            {
                _base = value;
                IsDirty = true;
            }
        }

        [JsonProperty("ranks")]
        [DbField("attributeRanks", (int)MySqlDbType.UInt16)]
        public ushort AttributeRanks
        {
            get
            {
                return _ranks;
            }
            set
            {
                _ranks = value;
                IsDirty = true;
            }
        }

        [JsonProperty("experienceSpent")]
        [DbField("attributeXpSpent", (int)MySqlDbType.UInt32)]
        public uint AttributeXpSpent
        {
            get
            {
                return _xpSpent;
            }
            set
            {
                _xpSpent = value;
                IsDirty = true;
            }
        }

        [JsonIgnore]
        public uint ActiveValue
        {
            get
            {
                return (uint)AttributeBase + AttributeRanks;
            }
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
