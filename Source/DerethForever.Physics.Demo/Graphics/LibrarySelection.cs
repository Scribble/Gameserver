﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.Windows.Forms;

namespace DerethForever.Physics.Demo
{
    public partial class LibrarySelection : Form
    {
        public LibrarySelection()
        {
            InitializeComponent();

            AcceptButton = runButton;
            CancelButton = cancelButton;

            string[] supportedLibraries = GraphicsLibraryManager.GetSupportedLibraries();
            int selectLibrary = 0;
            foreach (string library in supportedLibraries)
            {
                if (GraphicsLibraryManager.IsLibraryAvailable(library))
                {
                    int index = libraryList.Items.Add(library);
                    logText.Text += library + " OK\r\n";
                    if (library.Equals(GraphicsLibraryManager.GraphicsLibraryName))
                    {
                        selectLibrary = index;
                    }
                }
                else
                {
                    logText.Text += library + " not loaded\r\n";
                }
            }

            if (libraryList.Items.Count != 0)
            {
                runButton.Enabled = true;
                libraryList.SelectedIndex = selectLibrary;
            }
            GraphicsLibraryManager.GraphicsLibraryName = null;

            libraryList.DoubleClick += new EventHandler(libraryList_DoubleClick);
        }

        void libraryList_DoubleClick(object sender, EventArgs e)
        {
            GraphicsLibraryManager.GraphicsLibraryName = libraryList.SelectedItem as string;
            Close();
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            GraphicsLibraryManager.GraphicsLibraryName = libraryList.SelectedItem as string;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
