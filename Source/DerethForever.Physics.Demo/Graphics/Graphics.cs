﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.Drawing;
using System.Windows.Forms;
using BulletSharp;

namespace DerethForever.Physics.Demo
{
    public abstract class Graphics : IDisposable
    {
        public DerethDemo Demo { get; protected set; }
        public Form Form { get; protected set; }

        public virtual float FarPlane { get; set; }
        public float FieldOfView { get; protected set; }

        public virtual float AspectRatio
        {
            get
            {
                Size clientSize = Form.ClientSize;
                return (float)clientSize.Width / (float)clientSize.Height;
            }
        }

        public virtual bool IsFullScreen { get; set; }
        public virtual bool CullingEnabled { get; set; }

        public string InfoText { get; set; } = "";

        public MeshFactory MeshFactory;

        public abstract IDebugDraw GetPhysicsDebugDrawer();

        protected Graphics(DerethDemo demo)
        {
            Demo = demo;
            FarPlane = 400.0f;
            FieldOfView = (float)Math.PI / 4;
        }

        public virtual void Initialize()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        public abstract void Run();
        public abstract void UpdateView();

        public virtual void SetFormText(string text)
        {
            Form.Text = text;
        }

        public virtual void SetInfoText(string text)
        {
        }
    }
}
