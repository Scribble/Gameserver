/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using BulletSharp;
using System;
using System.Windows.Forms;

namespace DerethForever.Physics.Demo
{
    public sealed class FreeLook
    {
        private Input _input;
        private MouseController _mouseController;
        private bool _doUpdate;
        private Matrix _yToUpTransform, _upToYTransform;
        private Vector3 _eye, _target, _up;

        public FreeLook(Input input)
        {
            _input = input;
            _mouseController = new MouseController(input);
            Target = Vector3.UnitX;
            Up = Vector3.UnitY;
        }

        public Vector3 Eye
        {
            get { return _eye; }
            set
            {
                _eye = value;
                UpdateMouseController();
            }
        }

        public Vector3 Target
        {
            get { return _target; }
            set
            {
                _target = value;
                UpdateMouseController();
            }
        }

        public Vector3 Up
        {
            get { return _up; }
            set
            {
                _up = value;

                // MouseController uses UnitY as the up-vector,
                // create transforms for converting between UnitY-up and Up-up
                _yToUpTransform = Matrix.RotationAxis(Vector3.Cross(Vector3.UnitY, _up), Angle(_up, Vector3.UnitY));
                _upToYTransform = Matrix.Invert(_yToUpTransform);
                UpdateMouseController();
            }
        }

        public bool Update(float frameDelta)
        {
            if (!_mouseController.Update() && _input.KeysDown.Count == 0)
            {
                if (!_doUpdate)
                    return false;
                _doUpdate = false;
            }

            Vector3 direction = Vector3.TransformCoordinate(-_mouseController.Vector, _yToUpTransform);

            if (_input.KeysDown.Count != 0)
            {
                Vector3 relDirection = frameDelta * direction;

                // eliminate the up/down component of travel
                relDirection.Y = 0;
                Vector3 upVector = frameDelta * new Vector3(0, 1, 0);

                float flySpeed = _input.KeysDown.Contains(Keys.ControlKey) ? 20 : 10;

                if (_input.KeysDown.Contains(Keys.W))
                {
                    _eye += flySpeed * relDirection;
                }
                if (_input.KeysDown.Contains(Keys.S))
                {
                    _eye -= flySpeed * relDirection;
                }

                if (_input.KeysDown.Contains(Keys.A))
                {
                    _eye += Vector3.Cross(flySpeed * relDirection, _up);
                }
                if (_input.KeysDown.Contains(Keys.D))
                {
                    _eye -= Vector3.Cross(flySpeed * relDirection, _up);
                }

                // use space to go up
                if (_input.KeysDown.Contains(Keys.Space))
                {
                    _eye += flySpeed * upVector;
                }
                // use shift to go up
                if (_input.KeysDown.Contains(Keys.ShiftKey))
                {
                    _eye -= flySpeed * upVector;
                }
            }
            _target = _eye + (_eye - _target).Length * direction;

            return true;
        }

        private void UpdateMouseController()
        {
            Vector3 direction = _eye - _target;
            direction.Normalize();
            _mouseController.Vector = Vector3.TransformCoordinate(direction, _upToYTransform);
            _doUpdate = true;
        }

        // vertices must be normalized
        private static float Angle(Vector3 v1, Vector3 v2)
        {
            return (float)Math.Acos(Vector3.Dot(v1, v2));
        }
    }
}
