/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using Newtonsoft.Json;

namespace DerethForever.Database.Tests.CachePwn
{
    public class GeneratorTable
    {
        [JsonProperty("delay")]
        public float Delay { get; set; }

        [JsonProperty("frame")]
        public Frame Frame { get; set; } = new Frame();

        [JsonProperty("initCreate")]
        public uint InitCreate { get; set; }

        [JsonProperty("maxNum")]
        public uint MaxNumber { get; set; }

        [JsonProperty("objcell_id")]
        public uint ObjectCell { get; set; }

        [JsonProperty("probability")]
        public double Probability { get; set; }

        [JsonProperty("ptid")]
        public uint PaletteId { get; set; }

        [JsonProperty("shade")]
        public float Shade { get; set; }

        [JsonProperty("slot")]
        public uint Slot { get; set; }

        [JsonProperty("stackSize")]
        public int StackSize { get; set; }

        [JsonProperty("type")]
        public uint WeenieClassId { get; set; }

        [JsonProperty("whenCreate")]
        public uint WhenCreate { get; set; }

        [JsonProperty("whereCreate")]
        public uint WhereCreate { get; set; }
    }
}
