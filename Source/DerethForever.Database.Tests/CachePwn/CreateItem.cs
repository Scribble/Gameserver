/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using DerethForever.Entity;
using Newtonsoft.Json;

namespace DerethForever.Database.Tests.CachePwn
{
    public class CreateItem
    {
        [JsonProperty("wcid")]
        public uint? WeenieClassId { get; set; }

        [JsonProperty("palette")]
        public uint? Palette { get; set; }

        [JsonProperty("shade")]
        public double? Shade { get; set; }

        [JsonProperty("destination")]
        public uint? Destination { get; set; }

        [JsonProperty("stack_size")]
        public int? StackSize { get; set; }

        [JsonProperty("try_to_bond")]
        public bool? TryToBond { get; set; }

        public CreationProfile Convert()
        {
            CreationProfile cp = new CreationProfile()
            {
                CreationProfileGuid = Guid.NewGuid(),
                Destination = Destination,
                Palette = Palette,
                Shade = Shade,
                StackSize = StackSize,
                TryToBond = TryToBond ?? false,
                WeenieClassId = WeenieClassId
            };

            return cp;
        }
    }
}
