/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Reflection;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using Newtonsoft.Json;

namespace DerethForever.Database.Tests.CachePwn
{
    public class CachePwnWeenie
    {
        [JsonProperty("wcid")]
        public uint WeenieId { get; set; }

        [JsonProperty("weenieType")]
        public int WeenieTypeId { get; set; }

        [JsonProperty("attributes")]
        public AttributeSet Attributes { get; set; }

        [JsonProperty("body")]
        public Body Body { get; set; }

        [JsonProperty("pageDataList")]
        public Book Book { get; set; }

        [JsonProperty("boolStats")]
        public List<BoolStat> BoolStats { get; set; } = new List<BoolStat>();

        [JsonProperty("intStats")]
        public List<IntStat> IntStats { get; set; } = new List<IntStat>();

        [JsonProperty("didStats")]
        public List<DidStat> DidStats { get; set; } = new List<DidStat>();

        [JsonProperty("floatStats")]
        public List<FloatStat> FloatStats { get; set; } = new List<FloatStat>();

        [JsonProperty("int64")]
        public List<Int64Stat> Int64Stats { get; set; } = new List<Int64Stat>();

        [JsonProperty("stringStats")]
        public List<StringStat> StringStats { get; set; } = new List<StringStat>();

        [JsonProperty("createList")]
        public List<CreateItem> CreateList { get; set; } = new List<CreateItem>();

        [JsonProperty("skills")]
        public List<SkillListing> Skills { get; set; } = new List<SkillListing>();

        [JsonProperty("emoteTable")]
        public List<EmoteCategoryListing> EmoteTable { get; set; } = new List<EmoteCategoryListing>();

        [JsonProperty("spellBook")]
        public List<SpellbookEntry> Spells { get; set; } = new List<SpellbookEntry>();

        [JsonProperty("posStats")]
        public List<PositionListing> Positions { get; set; } = new List<PositionListing>();

        [JsonProperty("generatorTable")]
        public List<GeneratorTable> GeneratorTable { get; set; } = new List<GeneratorTable>();


        public DataObject UpdateWeenie(DataObject targetDataWeenie)
        {
            DataObject w = targetDataWeenie;
            w.DataObjectId = WeenieId;
            w.WeenieClassId = WeenieId;

            // Clear any that already exist and preserve the ones not in the json.
            IntStats.ForEach(stat => w.IntProperties.Remove(w.IntProperties.Find(x => x.PropertyId == stat.Key)));

            IntStats?.ForEach(stat =>
            {
                w.IntProperties.Add(new DataObjectPropertiesInt()
                {
                    DataObjectId = WeenieId,
                    PropertyId = (uint)stat.Key,
                    PropertyValue = stat.Value
                });
            });

            // Clear any that already exist and preserve the ones not in the json.
            Int64Stats.ForEach(stat => w.Int64Properties.Remove(w.Int64Properties.Find(x => x.PropertyId == stat.Key)));

            Int64Stats.ForEach(stat =>
            {
                w.Int64Properties.Add(new DataObjectPropertiesInt64()
                {
                    DataObjectId = WeenieId,
                    PropertyId = (uint)stat.Key,
                    PropertyValue = (ulong)stat.Value
                });
            });

            // Clear any that already exist and preserve the ones not in the json.
            FloatStats.ForEach(stat => w.DoubleProperties.Remove(w.DoubleProperties.Find(x => x.PropertyId == stat.Key)));

            FloatStats.ForEach(stat =>
            {
                w.DoubleProperties.Add(new DataObjectPropertiesDouble()
                {
                    DataObjectId = WeenieId,
                    PropertyId = (ushort)stat.Key,
                    PropertyValue = stat.Value
                });
            });

            // Clear any that already exist and preserve the ones not in the json.
            DidStats.ForEach(stat => w.DataIdProperties.Remove(w.DataIdProperties.Find(x => x.PropertyId == stat.Key)));

            DidStats.ForEach(stat =>
            {
                w.DataIdProperties.Add(new DataObjectPropertiesDataId()
                {
                    DataObjectId = WeenieId,
                    PropertyId = (uint)stat.Key,
                    PropertyValue = stat.Value
                });
            });

            // Clear any that already exist and preserve the ones not in the json.
            BoolStats.ForEach(stat => w.BoolProperties.Remove(w.BoolProperties.Find(x => x.PropertyId == stat.Key)));

            BoolStats.ForEach(stat =>
            {
                w.BoolProperties.Add(new DataObjectPropertiesBool()
                {
                    DataObjectId = WeenieId,
                    PropertyId = (uint)stat.Key,
                    PropertyValue = stat.BoolValue
                });
            });

            // Clear any that already exist and preserve the ones not in the json.
            StringStats.ForEach(stat => w.StringProperties.Remove(w.StringProperties.Find(x => x.PropertyId == stat.Key)));

            StringStats.ForEach(stat =>
            {
                w.StringProperties.Add(new DataObjectPropertiesString()
                {
                    DataObjectId = WeenieId,
                    PropertyId = (ushort)stat.Key,
                    PropertyValue = stat.Value
                });
            });

            // Clear any that already exist if we have entries in json.
            if (Book?.Pages?.Count > 0)
                w.BookProperties.Clear();

            Book?.Pages?.ForEach(page =>
            {
                w.BookProperties.Add(new DataObjectPropertiesBook()
                {
                    DataObjectId = WeenieId,
                    AuthorAccount = page.AuthorAccount,
                    AuthorId = (uint)page.AuthorId,
                    AuthorName = page.AuthorName,
                    IgnoreAuthor = Convert.ToUInt32(page.IgnoreAuthor ?? false),
                    // they look 0 based in the database - I am removing the 1 + Coral Golem
                    Page = (uint)Book.Pages.IndexOf(page),
                    PageText = page.PageText
                });
            });

            if (Attributes?.Strength != null)
                PropertyCopier<Attribute, CreatureAbility>.Copy(Attributes.Strength, w.StrengthAbility);
            if (Attributes?.Endurance != null)
                PropertyCopier<Attribute, CreatureAbility>.Copy(Attributes.Endurance, w.EnduranceAbility);
            if (Attributes?.Coordination != null)
                PropertyCopier<Attribute, CreatureAbility>.Copy(Attributes.Coordination, w.CoordinationAbility);
            if (Attributes?.Quickness != null)
                PropertyCopier<Attribute, CreatureAbility>.Copy(Attributes.Quickness, w.QuicknessAbility);
            if (Attributes?.Focus != null)
                PropertyCopier<Attribute, CreatureAbility>.Copy(Attributes.Focus, w.FocusAbility);
            if (Attributes?.Self != null)
                PropertyCopier<Attribute, CreatureAbility>.Copy(Attributes.Self, w.SelfAbility);
            if (Attributes?.Health != null)
                PropertyCopier<Attribute, CreatureVital>.Copy(Attributes.Health, w.Health);
            if (Attributes?.Stamina != null)
                PropertyCopier<Attribute, CreatureVital>.Copy(Attributes.Stamina, w.Stamina);
            if (Attributes?.Mana != null)
                PropertyCopier<Attribute, CreatureVital>.Copy(Attributes.Mana, w.Mana);

            if (Skills?.Count > 0)
                w.DataObjectPropertiesSkills.Clear();

            Skills?.ForEach(skill =>
            {
                if (skill.SkillId != null && skill.Skill.Ranks != null && skill.Skill.XpInvested != null &&
                    skill.Skill.TrainedLevel != null)
                {
                    DataObjectPropertiesSkill tempSkill = new DataObjectPropertiesSkill
                    {
                        SkillId = (ushort)skill.SkillId,
                        SkillPoints = (ushort)skill.Skill.Ranks,
                        SkillXpSpent = (uint)skill.Skill.XpInvested,
                        SkillStatus = (ushort)skill.Skill.TrainedLevel
                    };
                    CreatureSkill x = new CreatureSkill(w, tempSkill);
                    w.DataObjectPropertiesSkills.Add(new CreatureSkill(w, tempSkill));
                }
            });


            if (Spells?.Count > 0)
                w.SpellIdProperties.Clear();

            Spells?.ForEach(spell =>
            {
                w.SpellIdProperties.Add(new DataObjectPropertiesSpell()
                {
                    DataObjectId = WeenieId,
                    SpellId = (uint)spell.SpellId,
                    Probability = (double)(spell.Stats.CastingChance ?? 0)
                });
            });

            if (Positions?.Count > 0)
                w.Positions.Clear();

            Positions?.ForEach(position =>
            {
                w.Positions.Add(new DataObjectPropertiesPosition()
                {
                    DataObjectId = WeenieId,
                    PositionX = position.Position.Frame.Position.X,
                    PositionY = position.Position.Frame.Position.Y,
                    PositionZ = position.Position.Frame.Position.Z,
                    RotationW = position.Position.Frame.Rotations.W,
                    RotationX = position.Position.Frame.Rotations.X,
                    RotationY = position.Position.Frame.Rotations.Y,
                    RotationZ = position.Position.Frame.Rotations.Z,
                    Cell = position.Position.LandCellId,
                    DbPositionType = (ushort)position.PositionType
                });
            });

            if (GeneratorTable?.Count > 0)
                w.GeneratorTable.Clear();

            GeneratorTable?.ForEach(gt =>
            {
                Entity.GeneratorTable ngt = new Entity.GeneratorTable
                {
                    Delay = gt.Delay,
                    InitCreate = gt.InitCreate,
                    MaxNumber = gt.MaxNumber,
                    ObjectCell = gt.ObjectCell,
                    PaletteId = gt.PaletteId,
                    Probability = gt.Probability,
                    Shade = gt.Shade,
                    Slot = gt.Slot,
                    StackSize = gt.StackSize,
                    WeenieClassId = gt.WeenieClassId,
                    WhenCreate = (RegenerationType)gt.WhenCreate,
                    WhereCreate = (RegenerationLocation)gt.WhereCreate
                };
                ngt.Frame.Angles.W = gt.Frame.Rotations.W;
                ngt.Frame.Angles.X = gt.Frame.Rotations.X;
                ngt.Frame.Angles.Y = gt.Frame.Rotations.Y;
                ngt.Frame.Angles.Z = gt.Frame.Rotations.Z;
                ngt.Frame.Origin.X = gt.Frame.Position.X;
                ngt.Frame.Origin.Y = gt.Frame.Position.Y;
                ngt.Frame.Origin.X = gt.Frame.Position.X;
                w.GeneratorTable.Add(ngt);
            });

            if (EmoteTable?.Count > 0)
                w.EmoteTable.Clear();

            uint esCtr = (uint)(EmoteTable?.Count);
            uint eCtr;
            uint holdCategory = 0;

            EmoteTable?.ForEach(category =>
            {
                if (category.EmoteCategoryId != holdCategory)
                {
                    holdCategory = (uint)category.EmoteCategoryId;

                    if (holdCategory != 0)
                        esCtr = esCtr - 1;
                }

                category.Emotes.ForEach(e =>
                {
                    EmoteSet es = new EmoteSet()
                    {
                        WeenieClassId = WeenieId,
                        EmoteSetGuid = Guid.NewGuid(),
                        ClassId = e.ClassId,
                        EmoteCategoryId = e.Category,
                        MaxHealth = e.MaxHealth,
                        MinHealth = e.MinHealth,
                        Probability = (e.Probability ?? 0),
                        Quest = e.Quest,
                        Style = e.Style,
                        SubStyle = e.SubStyle,
                        SortOrder = esCtr,
                        VendorType = e.VendorType
                    };

                    eCtr = 0;
                    e.Emotes.ForEach(ea =>
                    {
                        es.Emotes.Add(new Entity.Emote()
                        {
                            Amount = ea.Amount,
                            Amount64 = ea.Amount64,
                            CreationProfile = ea?.Item?.Convert(),
                            Delay = ea.Delay ?? 0f,
                            Display = ea.Display,
                            EmoteTypeId = ea.EmoteActionType,
                            EmoteGuid = Guid.NewGuid(),
                            EmoteSetGuid = es.EmoteSetGuid,
                            Extent = ea.Extent ?? 0f,
                            HeroXp64 = ea.HeroXp64,
                            Maximum = ea.Max,
                            Maximum64 = ea.Maximum64,
                            MaximumFloat = ea.FMax,
                            Message = ea.Message,
                            Minimum = ea.Min,
                            Minimum64 = ea.Minimum64,
                            MinimumFloat = ea.FMin,
                            Motion = ea.Motion,
                            Percent = ea.Percent,
                            PhysicsScript = ea.PScript,
                            PositionLandBlockId = null, // no source
                            PositionX = ea.Frame?.Position.X,
                            PositionY = ea.Frame?.Position.Y,
                            PositionZ = ea.Frame?.Position.Z,
                            RotationW = ea.Frame?.Rotations.W,
                            RotationX = ea.Frame?.Rotations.X,
                            RotationY = ea.Frame?.Rotations.Y,
                            RotationZ = ea.Frame?.Rotations.Z,
                            Sound = ea.Sound,
                            SpellId = ea.SpellId,
                            Stat = ea.Stat,
                            TestString = ea.TestString,
                            TreasureClass = ea.treasure_class,
                            TreasureType = (uint?)ea.treasure_type,
                            SortOrder = eCtr,
                            WealthRating = ea.Wealth_Rating
                        });
                        eCtr = eCtr + 1;
                    });

                    w.EmoteTable.Add(es);
                });

            });



            if (CreateList?.Count > 0)
                w.CreateList.Clear();

            CreateList?.ForEach(ci =>
            {
                w.CreateList.Add(ci.Convert());
            });

            if (Body?.BodyParts.Count > 0)
                w.BodyParts.Clear();
            Body?.BodyParts.ForEach(bp =>
            {
                Entity.BodyPart myBp = new Entity.BodyPart();
                myBp.ArmorValues.Acid = (int)bp.BodyPart.ArmorValues.ArmorVsAcid;
                myBp.ArmorValues.Base = (int)bp.BodyPart.ArmorValues.BaseArmor;
                myBp.ArmorValues.Bludgeon = (int)bp.BodyPart.ArmorValues.ArmorVsBludgeon;
                myBp.ArmorValues.Cold = (int)bp.BodyPart.ArmorValues.ArmorVsCold;
                myBp.ArmorValues.Electric = (int)bp.BodyPart.ArmorValues.ArmorVsElectric;
                myBp.ArmorValues.Fire = (int)bp.BodyPart.ArmorValues.ArmorVsFire;
                myBp.ArmorValues.Nether = (int)bp.BodyPart.ArmorValues.ArmorVsNether;
                myBp.ArmorValues.Pierce = (int)bp.BodyPart.ArmorValues.ArmorVsPierce;
                myBp.ArmorValues.Slash = (int)bp.BodyPart.ArmorValues.ArmorVsSlash;
                myBp.BodyHeight = (int)bp.BodyPart.BH;
                myBp.Damage = (int)bp.BodyPart.DVal;
                myBp.DamageVariance = bp.BodyPart.DVar;
                myBp.DamageType = (int)bp.BodyPart.DType;
                if (bp.BodyPart.SD != null)
                {
                    if (bp.BodyPart.SD.HLB != null)
                        myBp.TargetingData.HighLeftBack = (double)bp.BodyPart.SD.HLB;
                    if (bp.BodyPart.SD.HLF != null)
                        myBp.TargetingData.HighLeftFront = (double)bp.BodyPart.SD.HLF;
                    if (bp.BodyPart.SD.HRB != null)
                        myBp.TargetingData.HighRightBack = (double)bp.BodyPart.SD.HRB;
                    if (bp.BodyPart.SD.HRF != null)
                        myBp.TargetingData.HighRightFront = (double)bp.BodyPart.SD.HRF;
                    if (bp.BodyPart.SD.LLB != null)
                        myBp.TargetingData.LowLeftBack = (double)bp.BodyPart.SD.LLB;
                    if (bp.BodyPart.SD.LLF != null)
                        myBp.TargetingData.LowLeftFront = (double)bp.BodyPart.SD.LLF;
                    if (bp.BodyPart.SD.LRB != null)
                        myBp.TargetingData.LowRightBack = (double)bp.BodyPart.SD.LRB;
                    if (bp.BodyPart.SD.LRF != null)
                        myBp.TargetingData.LowRightFront = (double)bp.BodyPart.SD.LRF;
                    if (bp.BodyPart.SD.MLB != null)
                        myBp.TargetingData.MidLeftBack = (double)bp.BodyPart.SD.MLB;
                    if (bp.BodyPart.SD.MLF != null)
                        myBp.TargetingData.MidLeftFront = (double)bp.BodyPart.SD.MLF;
                    if (bp.BodyPart.SD.MRB != null)
                        myBp.TargetingData.MidRightBack = (double)bp.BodyPart.SD.MRB;
                    if (bp.BodyPart.SD.MRF != null)
                        myBp.TargetingData.MidRightFront = (double)bp.BodyPart.SD.MRF;
                }
                myBp.BodyPartType = (BodyPartType)bp.Key;
                myBp.WeenieClassId = WeenieId;
                w.BodyParts.Add(myBp);
            });
            return w;
        }
    }
}
