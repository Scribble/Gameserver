/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using DerethForever.Entity;
using DerethForever.Entity.Enum;

namespace DerethForever.Database
{
    public interface IWorldDatabase
    {
        List<TeleportLocation> GetPointsOfInterest();

        uint GetCurrentId(uint min, uint max);

        // *********************************************************************************************************
        // TODO: these top 2 methods are not data access functions - they don't belong here.
        // *********************************************************************************************************

        /// <summary>
        /// this method absolutely does not belong here.  the current caller should pull the list of weenies and
        /// then create the proper ace objects from them rather than having the data layer do business logic
        /// </summary>
        List<DataObject> GetWeenieInstancesByLandblock(ushort landblock);

        /// <summary>
        /// this method absolutely does not belong here.  the current caller should pull the list of weenies and
        /// then create the proper ace objects from them rather than having the data layer do business logic
        /// </summary>
        DataObject GetDataObjectByWeenie(uint weenieClassId);

        List<Recipe> GetAllRecipes();

        List<Recipe> SearchRecipes(SearchRecipesCriteria criteria);

        List<ushort> SearchSpawnMaps(SearchSpawnMapsCriteria criteria);

        Recipe GetRecipe(Guid recipeGuid);

        void CreateRecipe(Recipe recipe);

        void UpdateRecipe(Recipe recipe);

        void DeleteRecipe(Guid recipeGuid);

        List<Content> GetAllContent();

        void CreateContent(Content content);

        void UpdateContent(Content content);

        void DeleteContent(Guid contentGuid);

        List<WeenieObjectInstance> GetSpawnMap(ushort landblockId);

        WeenieObjectInstance GetInstance(ushort spawnMapId, int instanceId);

        void UpdateInstance(ushort landblockId, WeenieObjectInstance instance);

        void RemoveInstance(ushort landblockId, WeenieObjectInstance instance);


        /// <summary>
        /// gets any matching weenie objects, only very shallowly populated.  this is not
        /// a full object.  to get the full object, call GetWeenie on the resulting weenie id.
        /// </summary>
        List<WeenieSearchResult> SearchWeenies(SearchWeeniesCriteria criteria, int max);

        bool UserModifiedFlagPresent();

        /// <summary>
        /// exports changes in the database (UserModified = true).  returns a list of the files created
        /// </summary>
        List<string> ExportChangesToDisk(string rootPath);

        /// <summary>
        /// resets all the UserModified = true flags in the db.  returns the number of rows affected.
        /// </summary>
        int ResetUserModifiedFlags();

        /// <summary>
        /// does a full object replacement, deleting all properties prior to insertion
        /// </summary>
        bool ReplaceWeenie(DataObject weenie);

        List<DataObject> GetWeeniesByLandblock(ushort landblock);

        bool SaveWeenie(DataObject weenie);

        /// <summary>
        /// replaces the cached weenie only.  will not persist the change to the underlying structure.
        /// </summary>
        bool CacheOnlySaveWeenie(DataObject weenie);

        bool DeleteWeenie(DataObject weenie);

        DataObject GetWeenie(uint weenieClassId);
    }
}
