/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Permissions;

using DerethForever.Common;
using DerethForever.Entity.Enum;
using System.Text;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace DerethForever.Database
{
    /// <summary>
    /// Remote Content sync is used to download content from the a remote Api.
    /// </summary>
    public static class Redeploy
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static Database CurrentDb { get; set; }

        public static bool RedeploymentActive { get; private set; } = false;

        public static bool RedeploymentSuccessful { get; private set; } = false;

        public static string CurrentToken { get; private set; } = string.Empty;

        public static bool DownloadFailure { get; private set; } = false;

        /// <summary>
        /// External user agent used when connecting to a remote Api, or another location.
        /// </summary>
        public static string ApiUserAgent { get; set; } = "Dereth Forever";

        /// <summary>
        /// The amount of calls left before being rate limited.
        /// </summary>
        public static int TotalApiCallsAvailable { get; set; } = 60;

        /// <summary>
        /// The amount of calls left before being rate limited.
        /// </summary>
        public static int RemaingApiCalls { get; set; } = 60;

        /// <summary>
        /// The time when the a remote API will accept a new request.
        /// </summary>
        public static DateTime? ApiResetTime { get; set; } = DateTime.Today.AddYears(1);

        /// <summary>
        /// Default Authentication database name.
        /// </summary>
        public static readonly string DefaultAuthenticationDatabaseName = "df_auth";

        /// <summary>
        /// Default Shard database name.
        /// </summary>
        public static readonly string DefaultShardDatabaseName = "df_shard";

        /// <summary>
        /// Default World database name.
        /// </summary>
        public static readonly string DefaultWorldDatabaseName = "df_world";

        /// <summary>
        /// Downloads for the Authentication Database.
        /// </summary>
        private static RemoteResourceList AuthenticationDownloads { get; set; } = new RemoteResourceList();

        /// <summary>
        /// Downloads for the Shard Database.
        /// </summary>
        private static RemoteResourceList ShardDownloads { get; set; } = new RemoteResourceList();

        /// <summary>
        /// Downloads for the World Database.
        /// </summary>
        private static RemoteResourceList WorldDownloads { get; set; } = new RemoteResourceList();

        public static string BackupFileNameTimestampFormat = "-MMM_dd_yyyy_HH-mm-ss";

        /// <summary>
        /// Setups the databse functions and default database specific resource lists.
        /// </summary>
        private static void Initialize()
        {
            CurrentDb = new Database();
            CurrentDb.Initialize(ConfigManager.Config.Database.Shard.Host,
                          ConfigManager.Config.Database.Shard.Port,
                          ConfigManager.Config.Database.Shard.Username,
                          ConfigManager.Config.Database.Shard.Password,
                          DatabaseSelectionOption.None,
                          false);
            AuthenticationDownloads.DefaultDatabaseName = DefaultAuthenticationDatabaseName;
            AuthenticationDownloads.ConfigDatabaseName = ConfigManager.Config.Database.Authentication.Database;
            ShardDownloads.DefaultDatabaseName = DefaultShardDatabaseName;
            ShardDownloads.ConfigDatabaseName = ConfigManager.Config.Database.Shard.Database;
            WorldDownloads.DefaultDatabaseName = DefaultWorldDatabaseName;
            WorldDownloads.ConfigDatabaseName = DefaultWorldDatabaseName;
            SetToken();
        }

        /// <summary>
        /// Changes the database.
        /// </summary>
        private static void ResetDatabaseConnection(string newDatabase)
        {
            CurrentDb.ResetConnectionString(ConfigManager.Config.Database.Shard.Host,
                          ConfigManager.Config.Database.Shard.Port,
                          ConfigManager.Config.Database.Shard.Username,
                          ConfigManager.Config.Database.Shard.Password,
                          newDatabase,
                          false);
        }

        /// <summary>
        /// Sets the authentication token
        /// </summary>
        public static void SetToken()
        {
            if (RemoteSyncTokenAvailable)
            {
                CurrentToken = ConfigManager.Config.ContentServer.RemoteSyncToken;
            }
        }

        /// <summary>
        /// Triggers a download failure
        /// </summary>
        public static void ToggleDownloadFailure()
        {
            DownloadFailure = !DownloadFailure;
        }

        /// <summary>
        /// Captures the Rate Limit Values from the Response Header
        /// </summary>
        public static void CaptureWebHeaderData(WebHeaderCollection headers)
        {
            if (headers?.Count > 0)
            {
                int tmpInt = 0;
                double rateLimitEpoch = 0;

                // Gitlab Example :
                //  RateLimit-Limit: 600
                //  RateLimit-Observed: 2
                //  RateLimit-Remaining: 598
                //  RateLimit-Reset: 1515994026
                //  RateLimit-ResetTime: Tue, 15 Jan 2017 05:27:06 GMT

                // Gitlab Specific
                if (int.TryParse(headers.Get("RateLimit-Limit"), out tmpInt))
                {
                    TotalApiCallsAvailable = tmpInt;
                }
                // capture the remaining api calls
                if (int.TryParse(headers.Get("RateLimit-Remaining"), out tmpInt))
                {
                    RemaingApiCalls = tmpInt;
                }
                // capture the timestamp for rate limite reset
                if (double.TryParse(headers.Get("RateLimit-Reset"), out rateLimitEpoch))
                {
                    ApiResetTime = ConvertFromUnixTimestamp(rateLimitEpoch);
                }
            }
        }

        /// <summary>
        /// Checks if a Path exists and creates one if it doesn't.
        /// </summary>
        public static bool CheckLocalDataPath(string dataPath)
        {
            // Check to verify config has a valid path:
            if (dataPath?.Length > 0)
            {
                var currentPath = Path.GetFullPath(dataPath);
                // Check too see if path exists and create if does not exist.
                if (!Directory.Exists(currentPath))
                {
                    try
                    {
                        Directory.CreateDirectory(currentPath);
                    }
                    catch
                    {
                        log.Error($"Could not create directory: {dataPath}");
                        return false;
                    }
                }
                PermissionSet perms = new PermissionSet(PermissionState.None);
                FileIOPermission writePermission = new FileIOPermission(FileIOPermissionAccess.Write, currentPath);
                perms.AddPermission(writePermission);
                if (!perms.IsSubsetOf(AppDomain.CurrentDomain.PermissionSet))
                {
                    // You don't have write permissions
                    log.Error($"Write permissions missing in: {dataPath}");
                    return false;
                }
                // All checks pass, so the directory is good to use.
                return true;
            }
            log.Error($"Configuration error, missing datapath!");
            return false;
        }

        /// <summary>
        /// Moves downloads to the appropriate resource list.
        /// </summary>
        private static void ParseDownloads(List<RemoteResource> list)
        {
            if (AuthenticationDownloads.Downloads.Count > 0)
            {
                AuthenticationDownloads.Downloads.Clear();
            }

            if (ShardDownloads.Downloads.Count > 0)
            {
                ShardDownloads.Downloads.Clear();
            }

            if (WorldDownloads.Downloads.Count > 0)
            {
                WorldDownloads.Downloads.Clear();
            }

            foreach (var download in list)
            {
                // Filter database files
                if (download.Type == RemoteResourceType.SqlBaseFile ||
                    download.Type == RemoteResourceType.WorldReleaseSqlFile ||
                    download.Type == RemoteResourceType.WorldReleaseJsonFile ||
                    download.Type == RemoteResourceType.SqlUpdateFile)
                {
                    if (download.DatabaseName == DefaultAuthenticationDatabaseName)
                    {
                        AuthenticationDownloads.Downloads.Add(download);
                    }
                    else if (download.DatabaseName == DefaultShardDatabaseName)
                    {
                        ShardDownloads.Downloads.Add(download);
                    }
                    else if (download.DatabaseName == DefaultWorldDatabaseName)
                    {
                        WorldDownloads.Downloads.Add(download);
                    }
                }
            }
        }

        /// <summary>
        /// Parses a file path too determine the correct database.
        /// </summary>
        public static Tuple<string, RemoteResourceType> GetDatabaseNameAndResourceType(string filePath, string fileName)
        {
            var localType = RemoteResourceType.Unknown;
            var databaseName = string.Empty;

            if (fileName.Contains(".zip"))
            {
                if (fileName.Contains("json-"))
                {
                    localType = RemoteResourceType.WorldReleaseJsonFile;
                    databaseName = DefaultWorldDatabaseName;
                } else
                    localType = RemoteResourceType.Archive;
            }

            if (fileName.Contains(".txt"))
            {
                localType = RemoteResourceType.TextFile;
            }
            else if (filePath.Contains("/Base"))
            {
                localType = RemoteResourceType.SqlBaseFile;
                if (fileName.Contains("AuthenticationBase"))
                {
                    databaseName = DefaultAuthenticationDatabaseName;
                }
                else if (fileName.Contains("ShardBase"))
                {
                    databaseName = DefaultShardDatabaseName;
                }
                else if (fileName.Contains("WorldBase"))
                {
                    databaseName = DefaultWorldDatabaseName;
                }
            }
            else if (filePath.Contains("/Updates"))
            {
                localType = RemoteResourceType.SqlUpdateFile;
                if (filePath.Contains("/Authentication"))
                {
                    databaseName = DefaultAuthenticationDatabaseName;
                }
                else if (filePath.Contains("/Shard"))
                {
                    databaseName = DefaultShardDatabaseName;
                }
            }
            else if (filePath.Contains("json-releases"))
            {
                databaseName = DefaultWorldDatabaseName;
                localType = RemoteResourceType.WorldReleaseJsonFile;
            }
            else if (filePath.Contains("releases"))
            {
                databaseName = DefaultWorldDatabaseName;
                localType = RemoteResourceType.WorldReleaseSqlFile;
            }
            else if (fileName.Contains(".sql"))
            {
                localType = RemoteResourceType.SqlFile;
            }

            // Explictitly Ignore all backup files:
            if (fileName.ToLowerInvariant().Contains("backup"))
            {
                localType = RemoteResourceType.BackupFile;
            }

            if (localType == RemoteResourceType.Unknown)
            {
                if (fileName.Contains(".json"))
                {
                    localType = RemoteResourceType.JsonFile;

                    if (fileName.Contains(@"current_release.json"))
                    {
                        localType = RemoteResourceType.ReleaseInfoFile;
                    }
                }
            }

            return Tuple.Create<string, RemoteResourceType>(databaseName, localType);
        }

        public static void BackupContentFolder()
        {
            var contentFolderPath = Path.GetFullPath(ConfigManager.Config.ContentServer.LocalContentPath);
            var backupPath = Path.GetFullPath(ConfigManager.Config.ContentServer.LocalBackupPath + "Content-Backup" + DateTime.UtcNow.ToString(Redeploy.BackupFileNameTimestampFormat) + ".zip");
            CreateZipFromPath(contentFolderPath, backupPath);
        }

        /// <summary>
        /// Converts Content returned by the Gitlab API to a String.
        /// </summary>
        public static string DecodeBase64String(string contentString)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(contentString));
        }

        /// <summary>
        /// Deletes and re-creates a database.
        /// </summary>
        private static void ResetDatabase(string databaseName)
        {
            ResetDatabaseConnection(string.Empty);
            // Delete Database, to clear everything including stored procs and views.
            var dropResult = CurrentDb.DropDatabase(databaseName);
            if (dropResult != null)
            {
                log.Error($"Error dropping database: {dropResult}");
            }

            // Create Database
            var createResult = CurrentDb.CreateDatabase(databaseName);
            if (createResult != null)
            {
                log.Error($"Error creating database: {createResult}");
            }
        }

        /// <summary>
        /// Clears a content folder of files and directories.
        /// </summary>
        public static void DeleteFolderContents(string folderPath)
        {
            foreach (var file in Directory.GetFiles(folderPath))
            {
                File.Delete(file);
            }
            foreach (var dir in Directory.GetDirectories(folderPath))
            {
                Directory.Delete(dir, true);
            }
        }

        /// <summary>
        /// Retreives the content from the source provided.
        /// </summary>
        /// <param name="source">Gitlab or LocalDisk must be used</param>
        /// <returns>A list of file resources for use with the database</returns>
        public static List<RemoteResource> GetResources(ResourceSelectionOption source)
        {
            var updateMessage = $"Retreiving files from {Enum.GetName(typeof(ResourceSelectionOption), source)}";
            log.Info(updateMessage);
            // Local database download Path
            var localDataPath = Path.GetFullPath(Path.Combine(ConfigManager.Config.ContentServer.LocalContentPath));

            // Check the data path and create if needed.
            if (CheckLocalDataPath(localDataPath))
            {
                // Retreives resource files from the selection source and returns a list of Remote Resources:
                return ResourceFactory.CreateResource(source).SyncFileList();
            }
            return null;
        }

        /// <summary>
        /// Extracts a zip archive into the WorldDb Content folder.
        /// </summary>
        public static void ExtractWorldDb(string sourcePath)
        {
            var contentFolderPath = Path.GetFullPath(ConfigManager.Config.ContentServer.LocalContentPath);
            var worldFolder = Path.Combine(ConfigManager.Config.Database.World.Folder);
            var localSourcePath = Path.GetFullPath(Path.Combine(contentFolderPath, sourcePath));
            var storedReleaseFileName = Path.GetFileName(localSourcePath);
            var storedContentPath = Path.Combine(contentFolderPath, storedReleaseFileName);

            var tmpPath = Path.GetDirectoryName(localSourcePath) + "\\tmp\\";

            var extractionError = ExtractZip(localSourcePath, tmpPath);
            if (extractionError?.Length > 0)
            {
                log.Error($"Could not extract release {extractionError}");
                return;
            }

            var files = from file in Directory.EnumerateFiles(tmpPath, "*.*", SearchOption.AllDirectories) select new { Info = new FileInfo(file) };

            if (files.Count() > 0)
            {
                foreach (var file in files)
                {
                    var newPath = file.Info.Directory.Name?.Length > 0 ? Path.Combine(worldFolder, file.Info.Directory.Name, file.Info.Name) : Path.Combine(worldFolder, file.Info.Name);

                    if (File.Exists(newPath))
                        File.Delete(newPath);

                    // Useful for parseing downloads:
                    // // Warning, function too calculate hash on the end:
                    // resources.Add(new RemoteResource()
                    // {
                    //    DatabaseName = Redeploy.DefaultWorldDatabaseName,
                    //    SourceUri = sourceUrl,
                    //    SourcePath = sourcePath,
                    //    FileName = file.Info.Name,
                    //    FilePath = newPath,
                    //    Type = RemoteResourceType.WorldReleaseJsonFile,
                    //    FileSize = file.Info.Length,
                    //    Hash = Redeploy.CalculateFileChecksum(file.Info.FullName)
                    // });

                    // Move the extracted file.
                    try
                    {
                        // This invalidates the info path
                        File.Move(file.Info.FullName, newPath);
                    }
                    catch (Exception e)
                    {
                        log.Error($"error: {e.Message}");
                    }
                }
            }
            else
            {
                log.Error("Not enough files were extracted from the world release.");
                if (Directory.Exists(tmpPath))
                    Directory.Delete(tmpPath, true);
                return;
            }

            // always delete tmp
            Directory.Delete(tmpPath, true);

            // Move zip to root of the content folder (stored)
            File.Move(localSourcePath, storedContentPath);

            // Remove parent path if it is not the content directory
            if (Path.GetFullPath(new FileInfo(localSourcePath).Directory.FullName) != Path.GetFullPath(new FileInfo(contentFolderPath).Directory.FullName))
                Directory.Delete(Path.GetDirectoryName(localSourcePath));
        }

        /// <summary>
        /// Attempts to download and redeploy data from a data source too the specified database(s). WARNING: CAN CAUSE LOSS OF DATA IF USED IMPROPERLY!
        /// </summary>
        public static string RedeployDatabaseFromSource(DatabaseSelectionOption databaseSelection, ResourceSelectionOption dataSource)
        {
            if (RedeploymentActive)
                return "There is already an active redeployment in progress...";
            if (databaseSelection == DatabaseSelectionOption.None)
                return "You must select a database other than 0 (None)..";
            if (dataSource == ResourceSelectionOption.None)
                return "You must select a source option other than 0 (None)..";
            // Determine if the config settings appear valid:
            if (ConfigManager.Config.ContentServer == null)
                return "ContentServer configration missing from config! Please edit the config!";
            if (ConfigManager.Config.ContentServer.LocalContentPath?.Length <= 0)
                return $"Data path missing from the ContentServer config! Please edit the config! {ConfigManager.Config.ContentServer.LocalContentPath}";

            log.Info($"A Redeployment has been initiated for {Enum.GetName(typeof(DatabaseSelectionOption), databaseSelection)}!");

            RedeploymentActive = true;

            // Setup the database requirements.
            Initialize();
            List<RemoteResource> databaseFiles = null;

            databaseFiles = GetResources(dataSource);

            if (databaseFiles == null)
            {
                if (dataSource == ResourceSelectionOption.Gitlab)
                {
                    RedeploymentActive = false;
                    var couldNotDownload = RemaingApiCalls == 0 ? $"API limit reached, please wait until {ApiResetTime.ToString()} and then try again." : "Unknown issue downloading, check the logfile for more details.";
                    log.Info(couldNotDownload);
                    return couldNotDownload;
                }
            }

            if (databaseFiles?.Count > 0)
            {
                List<RemoteResourceList> resources = new List<RemoteResourceList>();

                ParseDownloads(databaseFiles);

                // Change all databases!
                if (databaseSelection == DatabaseSelectionOption.All)
                {
                    resources.Add(AuthenticationDownloads);
                    resources.Add(ShardDownloads);
                    resources.Add(WorldDownloads);
                }
                else
                {
                    // Filter too a specific database
                    switch (databaseSelection)
                    {
                        case DatabaseSelectionOption.Authentication:
                            {
                                if (AuthenticationDownloads.Downloads?.Count > 0)
                                    resources.Add(AuthenticationDownloads);
                                else
                                {
                                    RedeploymentActive = false;
                                    var errorMessage = $"Downloads were missing for {DatabaseSelectionOption.Authentication.ToString()}";
                                    log.Error(errorMessage);
                                    return errorMessage;
                                }
                                break;
                            }
                        case DatabaseSelectionOption.Shard:
                            {
                                if (ShardDownloads.Downloads?.Count > 0)
                                    resources.Add(ShardDownloads);
                                else
                                {
                                    RedeploymentActive = false;
                                    var errorMessage = $"Downloads were missing for {DatabaseSelectionOption.Shard.ToString()}";
                                    log.Error(errorMessage);
                                    return errorMessage;
                                }
                                break;
                            }
                        case DatabaseSelectionOption.World:
                            {
                                if (WorldDownloads.Downloads?.Count > 0)
                                    resources.Add(WorldDownloads);
                                else
                                {
                                    RedeploymentActive = false;
                                    var errorMessage = $"Downloads were missing for {DatabaseSelectionOption.World.ToString()}";
                                    log.Error(errorMessage);
                                    return errorMessage;
                                }
                                break;
                            }
                    }
                }

                foreach (var resource in resources)
                {
                    if (resource.Downloads.Count == 0) continue;

                    var baseFile = string.Empty;
                    List<string> updates = new List<string>();
                    var worldFile = string.Empty;

                    // Seporate base files from updates
                    foreach (var download in resource.Downloads)
                    {
                        if (download.Type == RemoteResourceType.SqlBaseFile)
                        {
                            if (databaseSelection != DatabaseSelectionOption.World)
                                baseFile = download.FilePath;
                        }
                        if (download.Type == RemoteResourceType.SqlUpdateFile)
                        {
                            updates.Add(download.FilePath);
                        }
                        if (download.Type == RemoteResourceType.WorldReleaseJsonFile || download.Type == RemoteResourceType.WorldReleaseSqlFile)
                        {
                            worldFile = download.FilePath;
                        }
                    }

                    try
                    {
                        // First sequence, load the auth and shard base, we will skip world as worldbase is now part of the world release file.
                        if (resource.DefaultDatabaseName != DefaultWorldDatabaseName)
                        {
                            // reset the database
                            ResetDatabase(resource.ConfigDatabaseName);

                            if (File.Exists(baseFile))
                            {
                                var returnString = ReadAndLoadScript(baseFile, resource.ConfigDatabaseName, resource.DefaultDatabaseName);
                                if (returnString?.Length > 0)
                                {
                                    log.Error($"There was an error returned from the database when loading your script {baseFile}:{resource.ConfigDatabaseName}:{resource.DefaultDatabaseName}::{returnString}");
                                }
                            }
                            else
                            {
                                var errorMessage = $"There was an error locating the base file {baseFile} for {resource.DefaultDatabaseName}!";
                                log.Error(errorMessage);
                            }
                        }

                        // Last, if this is the world database, we will load the Game World
                        if (resource.DefaultDatabaseName == DefaultWorldDatabaseName)
                        {
                            if (File.Exists(worldFile))
                            {
                                if (worldFile.Contains(".zip"))
                                {
                                    log.Debug($"Begining worlddb extraction: {worldFile}");
                                    ExtractWorldDb(worldFile);
                                } else
                                {
                                    var returnString = ReadAndLoadScript(worldFile, resource.ConfigDatabaseName, resource.DefaultDatabaseName);
                                    if (returnString?.Length > 0)
                                    {
                                        log.Error($"There was an error returned from the database when loading your script {worldFile}:{resource.ConfigDatabaseName}:{resource.DefaultDatabaseName}::{returnString}");
                                    }
                                }
                            }
                            else
                            {
                                var errorMessage = $"There was an error locating the base file {worldFile} for {resource.DefaultDatabaseName}!";
                                log.Error(errorMessage);
                            }
                        }

                        // run all updates
                        if (updates.Count() > 0)
                        {
                            foreach (var file in updates)
                            {
                                var errorResult = ReadAndLoadScript(file, resource.ConfigDatabaseName, resource.DefaultDatabaseName);
                                if (errorResult?.Length > 0)
                                    log.Error($"Issues loading script: {errorResult}");
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        var errorMessage = error.Message;
                        if (error.InnerException != null)
                        {
                            errorMessage += " Inner: " + error.InnerException.Message;
                        }
                        log.Error(errorMessage);
                        RedeploymentActive = false;
                        return errorMessage;
                    }
                }
                RedeploymentActive = false;
                // Save success state
                RedeploymentSuccessful = true;
                // Success
                return null;
            }
            RedeploymentActive = false;
            // Could not find configuration or error in function.
            var configErrorMessage = "No data files were found on local disk or an unknown error has occurred.";
            log.Error(configErrorMessage);
            return configErrorMessage;
        }

        /// <summary>
        /// Reads a file and updates database names from the config, then loads file the file content into the database.
        /// </summary>
        private static string ReadAndLoadScript(string sqlFile, string databaseName, string defaultDatabase)
        {
            log.Info($"Database: {databaseName} Loading {sqlFile}...");
            // open file into string
            string sqlInputFile = File.ReadAllText(sqlFile);
            if (databaseName != defaultDatabase)
            {
                sqlInputFile = sqlInputFile.Replace(defaultDatabase, databaseName);
            }
            ResetDatabaseConnection(databaseName);
            return CurrentDb.ExecuteSqlQueryOrScript(sqlInputFile, databaseName, true);
        }

        /// <summary>
        /// Attempts to extract a file from a directory, into a relative path.
        /// </summary>
        public static string ExtractZip(string filePath, string destinationPath)
        {
            // $"Extracting Zip {filePath}...";
            if (!Directory.Exists(destinationPath))
                Directory.CreateDirectory(destinationPath);
            else
                DeleteFolderContents(destinationPath);

            if (!File.Exists(filePath))
            {
                return "ERROR: Zip missing!";
            }

            log.Info($"Extracting archive {filePath}");
            try
            {
                ZipFile.ExtractToDirectory(filePath, destinationPath);
            }
            catch (Exception error)
            {
                return error.Message;
            }
            return null;
        }

        /// <summary>
        /// Attempts to create a zip from a directory or file, into a relative path.
        /// </summary>
        public static string CreateZipFromPath(string filePath, string destinationPath)
        {
            if (!Directory.Exists(Path.GetDirectoryName(destinationPath)))
                Directory.CreateDirectory(destinationPath);

            if (!Directory.Exists(filePath))
            {
                return "ERROR: Content folder is missing!";
            }

            log.Info($"Creating archive from {filePath}");

            try
            {
                ZipFile.CreateFromDirectory(filePath, destinationPath);
            }
            catch (Exception error)
            {
                return error.Message;
            }
            return null;
        }

        /// <summary>
        /// Checks to make sure the config variables will sustain downloads.
        /// </summary>
        public static bool RemoteSyncTokenAvailable
        {
            get
            {
                if (ConfigManager.Config.ContentServer.RemoteSyncToken?.Length > 0)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Checks to make sure the config variables contain the correct information for deployment.
        /// </summary>
        public static bool CanDeploy
        {
            get
            {
                if (ConfigManager.Config.ContentServer.LocalContentPath?.Length > 0)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Converts a double to epoch time.
        /// </summary>
        public static DateTime ConvertFromUnixTimestamp(double unixTimeStamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddSeconds(unixTimeStamp).ToLocalTime();
        }

        public static string CalculateFileChecksum(string file)
        {
            using (FileStream stream = File.OpenRead(file))
            {
                var hash = new System.Security.Cryptography.SHA256Managed();
                byte[] sum = hash.ComputeHash(stream);
                // Remove hyphens
                return BitConverter.ToString(sum).Replace("-", String.Empty);
            }
        }
    }
}
