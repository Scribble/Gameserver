/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity.Enum;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.Database
{
    public class ResourceFactory
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Creates an interface for the intended ResourceSelectionOption
        /// </summary>
        /// <returns>IRedeploySource or null</returns>
        public static IRedeploySource CreateResource(ResourceSelectionOption redeploySelectionSource)
        {
            switch (redeploySelectionSource)
            {
                case (ResourceSelectionOption.Gitlab):
                    return new GitlabResource();
                case (ResourceSelectionOption.LocalDisk):
                    return new LocalResource();
                case (ResourceSelectionOption.Website):
                    return new WebsiteResource();
            }
            throw new ArgumentException("redeploySelectionSource");
        }
    }
}
