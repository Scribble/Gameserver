/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.DatExplorer
{
    public class DatTreeNode : IDatTreeNode
    {
        private string nodeName;
        private int nodeDatId;
        private string nodeDatFilePath;
        private uint nodeDatFileId;
        private int nodeLevel;
        private bool nodeExpanded;
        private int nodeStep;
        internal List<IDatTreeNode> nodeChildren;

        public DatTreeNode(string name, int datId, string datPath, uint datFileId, int level, bool expanded)
        {
            nodeName = name;
            nodeDatId = datId;
            nodeDatFilePath = datPath ?? string.Empty;
            nodeDatFileId = datFileId;
            nodeLevel = level;
            nodeExpanded = expanded;
        }

        public int CountChildren
        {
            get { return nodeChildren.Count; }
        }

        public List<IDatTreeNode> Children
        {
            get { return nodeChildren; }
        }

        public string Name
        {
            get { return nodeName; }
            set { nodeName = value; }
        }

        public bool Expanded
        {
            get { return nodeExpanded; }
            set { nodeExpanded = value; }
        }

        public int Level
        {
            get { return nodeLevel; }
            set { nodeLevel = value; }
        }

        public string DatFilePath
        {
            get { return nodeDatFilePath; }
            set { nodeDatFilePath = value; }
        }

        public int DatId
        {
            get { return nodeDatId; }
            set { nodeDatId = value; }
        }

        public uint DatFileId
        {
            get { return nodeDatFileId; }
            set { nodeDatFileId = value; }
        }

        public int Step
        {
            get { return nodeStep; }
            set { nodeStep = value; }
        }

        public void AddChild(IDatTreeNode child)
        {
            nodeChildren.Add(child);
        }
    }
}
