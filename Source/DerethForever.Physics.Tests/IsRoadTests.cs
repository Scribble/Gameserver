/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using DerethForever.DatLoader.FileTypes;
using DerethForever.DatLoader;
using DerethForever.Common;

namespace DerethForever.Physics.Tests
{
    [TestClass]
    public class IsRoadTests
    {
        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            File.Copy(Path.Combine(System.Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"), ".\\Config.json", true);
            ConfigManager.Initialize();
            DatManager.Initialize();
        }

        private void AssertIsRoad(uint landblockId, float x, float y)
        {
            CellLandblock block = CellLandblock.ReadFromDat(landblockId);
            bool result = DerethPhysics.IsRoadAtPoint(block, x, y);
            Assert.IsTrue(result, $"{landblockId:X} ({x}, {y}) was supposed to be a road, but was not.");
        }

        private void AssertIsNotRoad(uint landblockId, float x, float y)
        {
            CellLandblock block = CellLandblock.ReadFromDat(landblockId);
            bool result = DerethPhysics.IsRoadAtPoint(block, x, y);
            Assert.IsFalse(result, $"{landblockId:X} ({x}, {y}) was not supposed to be a road, but was.");
        }

        [TestMethod]
        public void Roads_Intersection_IsRoad_1()
        {
            // this is a problem point in rithwic, road side;
            AssertIsRoad(0xC88C0008, 19.5f, 182.7f);
        }

        [TestMethod]
        public void Roads_Intersection_IsRoad_2()
        {
            // this is a problem point on the road from rithwic to eastham
            AssertIsRoad(0xCA8D0035, 154.7f, 106.0f);
        }

        [TestMethod]
        public void Roads_Intersection_IsRoad_3()
        {
            // this is a problem point on the road from rithwic to eastham
            AssertIsRoad(0xCA8D003E, 186.98f, 136.4f);
        }
        
        [TestMethod]
        public void Roads_Intersection_IsRoad_4()
        {
            // this is a problem point on the road from rithwic to eastham
            AssertIsRoad(0xCA8D003E, 179.6f, 135.5f);
        }
        
        [TestMethod]
        public void Roads_Intersection_IsNotRoad_1()
        {
            // this is a problem point in rithwic, not road side
            AssertIsNotRoad(0xC88C0008, 17.5f, 181.2f);
        }
        
        [TestMethod]
        public void Roads_Intersection_IsNotRoad_2()
        {
            // this is a problem point on the road from rithwic to eastham
            AssertIsNotRoad(0xCA8D003E, 175.0f, 138.0f);
        }
    }
}
