/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.Physics.Core;
using System.Threading;
using DerethForever.Common;
using DerethForever.DatLoader;

namespace DerethForever.Physics.Standalone
{
    public class Program
    {
        private static bool _run = true;
        private static DerethPhysics _physics;
        private static Thread _physicsThread;

        static void Main(string[] args)
        {
            // copy config over to this project
            File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"), ".\\Config.json", true);
            ConfigManager.Initialize();
            DatManager.Initialize();

            _physics = new DerethPhysics();
            _physics.Start();
            _physicsThread = new Thread(Run);
            _physicsThread.Start();

            // load up rithwic

            for (uint x = 0xC688; x < 0xC695; x++)
                _physics.LoadLandlock(x << 16);

            for (uint x = 0xC788; x < 0xC795; x++)
                _physics.LoadLandlock(x << 16);

            for (uint x = 0xC888; x < 0xC895; x++)
                _physics.LoadLandlock(x << 16);

            for (uint x = 0xC988; x < 0xC995; x++)
                _physics.LoadLandlock(x << 16);

            for (uint x = 0xCA88; x < 0xCA95; x++)
                _physics.LoadLandlock(x << 16);

            _physics.LaunchViewer();
            _physics.MoveCamera(0xC98C0000);

            Console.WriteLine("Starting physics simulation.  Type 'exit' to terminate.");
            string command;
            
            while (_run)
            {
                Console.Write("> ");
                command = Console.ReadLine();

                switch (command?.ToLower())
                {
                    case "open viewer":
                        _physics.LaunchViewer();
                        break;
                    case "close viewer":
                        _physics.CloseViewer();
                        break;
                    case "exit":
                        _run = false;
                        break;
                    case "reset-eye":
                        _physics.MoveCamera(0xC98C0000);
                        break;

                }

                // implement features here

            }

            // note, this doesn't actually appear to work
            _physicsThread.Join();
        }

        public static void Run()
        {
            int outputEvery = 1000;
            int iterations = 0;

            while (_run)
            {
                _physics.Step();
                iterations++;
            }

            _physics.Dispose();
        }
    }
}
