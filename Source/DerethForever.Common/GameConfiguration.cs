/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DerethForever.Common
{
    public class GameConfiguration
    {
        public string WorldName { get; set; }

        public string Description { get; set; }
        
        public string Welcome { get; set; }

        public NetworkSettings Network { get; set; }

        public AccountDefaults Accounts { get; set; }

        public string DatFilesDirectory { get; set; }

        /// <summary>
        /// The ammount of seconds to wait before turning off the server. Default value is 60 (for 1 minute).
        /// </summary>
        [DefaultValue(60)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public uint ShutdownInterval { get; set; }

        /// <summary>
        /// whether or not this server requires secure authentication.  for backwards compatibility, this is false
        /// by default.
        /// </summary>
        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public bool SecureAuthentication { get; set; }

        /// <summary>
        /// list of LoginServers that this server will accept tokens from.  If you are running
        /// your own Login Server, you will need to make sure it is in this list.
        /// </summary>
        public List<string> AllowedAuthServers { get; set; } = new List<string>();

        /// <summary>
        /// path to your world database.  when running from visual studio, the default path should work for most
        /// users that clone DerethForever.GameServer and Dereth to the same parent directory
        /// </summary>
        [DefaultValue("..\\..\\..\\..\\..\\..\\Dereth\\Database\\")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public string WorldDbDirectory { get; set; }
    }
}
