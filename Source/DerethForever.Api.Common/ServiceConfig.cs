/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;

using System.IO;

using DerethForever.Common;

namespace DerethForever.Api.Common
{
    public static class ServiceConfig
    {
        public static void LoadServiceConfig()
        {
            string debugPath = @"..\..\..\..\DerethForever\Config.json"; // default path for debug
            string path = "Config.json"; // default path for user installations

            if (!File.Exists(path) && File.Exists(debugPath))
                path = debugPath;

            var config = JsonConvert.DeserializeObject<MasterConfiguration>(File.ReadAllText(path));

            ConfigManager.Initialize(config);
        }
    }
}
