﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Network.Enum
{
     public enum ConfirmationType : uint
     {
         Undefined        = 0x00,
         SwearAllegiance  = 0x01,
         AlterSkill       = 0x02,
         AlterAttribute   = 0x03,
         Fellowship       = 0x04,
         CraftInteraction = 0x05,
         Augmentation     = 0x06,
         Yes_No           = 0x07
     }
}