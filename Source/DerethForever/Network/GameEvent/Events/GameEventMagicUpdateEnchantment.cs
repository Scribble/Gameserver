﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.DatLoader.Entity;

namespace DerethForever.Network.GameEvent.Events
{
    public class GameEventMagicUpdateEnchantment : GameEventMessage
    {
        public GameEventMagicUpdateEnchantment(Session session, SpellBase spellBase, uint layer, uint spellCategory, int cooldownId, uint enchantmentTypeFlag)
            : base(GameEventType.MagicUpdateEnchantment, GameMessageGroup.Group09, session)
        {
            const double startTime = 0;
            const double lastTimeDegraded = 0;
            const uint key = 0;
            const float val = 0;
            const uint spellSetId = 0;
            uint spellId = layer | spellCategory | (uint)cooldownId; // spellId is made up of these 3 components
            Writer.Write(spellId);
            Writer.Write(layer | spellCategory); // packed spell category
            Writer.Write(spellBase.Power);
            Writer.Write(startTime); // FIXME: this needs to be passed it.
            Writer.Write(spellBase.Duration);
            Writer.Write(session.Player.Guid.Full);
            Writer.Write(spellBase.DegradeModifier);
            Writer.Write(spellBase.DegradeLimit);
            Writer.Write(lastTimeDegraded);
            Writer.Write(enchantmentTypeFlag);

            // FIXME: These next 2 may be depreciated need more research.
            Writer.Write(key);
            Writer.Write(val);
            Writer.Write(spellSetId);

            Writer.Align();
        }
    }
}
