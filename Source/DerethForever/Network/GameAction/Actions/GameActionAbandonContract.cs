﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Network.GameAction.Actions
{
    /// <summary>
    /// This method handles the Game Action F7B1 - 0x0316 Abandon Contract.   This is sent to the server when the player
    /// selects a tracked quest from the quest panel and clicks on the abandon button.   We get the id of the contract we want to delete.
    /// We will respond with a F7B0 - 0x0315 message SendClientContractTracker passing the deleteContract flag set to true.   Og II
    /// </summary>
    public static class GameActionAbandonContract
    {
        [GameAction(GameActionType.AbandonContract)]
        public static void Handle(ClientMessage message, Session session)
        {
            // Read in the applicable data.
            uint contractId = message.Payload.ReadUInt32();
            session.Player.HandleActionAbandonContract(contractId);
        }
    }
}
