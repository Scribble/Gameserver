﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using DerethForever.Common.Extensions;

namespace DerethForever.Network.GameAction.Actions
{
    public static class GameActionQueryAge
    {
        [GameAction(GameActionType.QueryAge)]
        public static void Handle(ClientMessage message, Session session)
        {
            var target = message.Payload.ReadString16L();
            DateTime playerDOB = new DateTime();
            playerDOB = playerDOB.AddSeconds(session.Player.Age);
            TimeSpan tsAge = playerDOB - new DateTime();

            string age = "";

            if (tsAge.ToString("%d") != "0")
            {
                if (Convert.ToInt16(tsAge.ToString("%d")) > 0 && Convert.ToInt16(tsAge.ToString("%d")) <= 7)
                    age = age + tsAge.ToString("%d") + "d ";
                if (Convert.ToInt16(tsAge.ToString("%d")) > 7)
                {
                    int months  = 0;
                    int weeks   = 0;
                    int days    = 0;

                    for (int i = 0; i < tsAge.Days; i++)
                    {
                        days++;
                        if (days > 7)
                        {
                            weeks++;
                            days = 0;
                        }
                        if (weeks > 3)
                        {
                            months++;
                            weeks = 0;
                        }
                    }

                    if (months > 0)
                        age = age + months + "mo ";
                    if (weeks > 0)
                        age = age + weeks + "w ";
                    if (days > 0)
                        age = age + days + "d ";
                }
            }

            if (tsAge.ToString("%h") != "0")
                age = age + tsAge.ToString("%h") + "h ";

            if (tsAge.ToString("%m") != "0")
                age = age + tsAge.ToString("%m") + "m ";

            if (tsAge.ToString("%s") != "0")
                age = age + tsAge.ToString("%s") + "s";

            var ageEvent = new GameEvent.Events.GameEventQueryAgeResponse(session, "", age);

            session.Network.EnqueueSend(ageEvent);
        }
    }
}