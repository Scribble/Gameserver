/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

using DerethForever.Actors;
using DerethForever.Entity;

namespace DerethForever.Network.GameAction.Actions
{
    public static class GameActionBuyItems
    {
        [GameAction(GameActionType.Buy)]
        public static void Handle(ClientMessage message, Session session)
        {
            ObjectGuid vendorId = new ObjectGuid(message.Payload.ReadUInt32());

            uint itemcount = message.Payload.ReadUInt32();

            List<ItemProfile> items = new List<ItemProfile>();

            while (itemcount > 0)
            {
                itemcount--;
                ItemProfile item = new ItemProfile();
                item.Amount = message.Payload.ReadUInt32();
                // item.Amount = item.Amount & 0xFFFFFF;

                item.Guid = message.Payload.ReadGuid();
                items.Add(item);
            }
            
            // curancy 0 default, if else then currancy is set other then money
            uint i_alternateCurrencyID = message.Payload.ReadUInt32();

            // todo: take into account other currencyIds other then assuming default
            session.Player.HandleActionBuy(vendorId, items);
        }
    }
}
