﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Network
{
    public enum GameMessageGroup
    {
        InvalidQueue        = 0x00,
        EventQueue          = 0x01,
        ControlQueue        = 0x02,
        WeenieQueue         = 0x03,
        LoginQueue          = 0x04,
        DatabaseQueue       = 0x05,
        SecureControlQueue  = 0x06,
        SecureWeenieQueue   = 0x07, // Autonomous Position
        SecureLoginQueue    = 0x08,
        Group09             = 0x09, // TODO rename to UIQueue next PR Og II
        Group0A             = 0x0A, // TODO rename SmartboxQueue next PR Og II
        ObserverQueue       = 0x0B,
        QueueMax            = 0x0C
    }
}