/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using log4net;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using DerethForever.Actors.Actions;
using DerethForever.DatLoader.Entity;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Entity.Enum.Properties;
using DerethForever.Factories;
using DerethForever.Managers;
using DerethForever.Network;
using DerethForever.Network.GameEvent.Events;
using DerethForever.Network.GameMessages.Messages;
using DerethForever.Network.Motion;
using DerethForever.Network.Sequence;

namespace DerethForever.Actors
{
    public class Creature : Container
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// A list of the emotes on the weenie - they are broken out by category and type for processing.
        /// </summary>
        public List<EmoteSet> EmoteSets
        {
            get { return DataWeenie.EmoteTable; }
            set { DataWeenie.EmoteTable = value; }
        }

        public CreatureAbility Strength
        {
            get { return DataObject.StrengthAbility; }
            set { DataObject.StrengthAbility = value; }
        }

        public CombatMode CombatMode { get; private set; }

        public DataObject DataCorpse
        {
            get { return DataObject; }
        }

        public CreatureAbility Endurance
        {
            get { return DataObject.EnduranceAbility; }
            set { DataObject.EnduranceAbility = value; }
        }

        public CreatureAbility Coordination
        {
            get { return DataObject.CoordinationAbility; }
            set { DataObject.CoordinationAbility = value; }
        }

        public CreatureAbility Quickness
        {
            get { return DataObject.QuicknessAbility; }
            set { DataObject.QuicknessAbility = value; }
        }

        public CreatureAbility Focus
        {
            get { return DataObject.FocusAbility; }
            set { DataObject.FocusAbility = value; }
        }

        public CreatureAbility Self
        {
            get { return DataObject.SelfAbility; }
            set { DataObject.SelfAbility = value; }
        }

        public CreatureVital Health
        {
            get { return DataObject.Health; }
            set { DataObject.Health = value; }
        }

        public CreatureVital Stamina
        {
            get { return DataObject.Stamina; }
            set { DataObject.Stamina = value; }
        }

        public CreatureVital Mana
        {
            get { return DataObject.Mana; }
            set { DataObject.Mana = value; }
        }

        public Dictionary<Ability, CreatureVital> Vitals
        {
            get { return DataObject.DataObjectPropertiesAttributes2nd; }
        }

        public int? InquireIntStat(int stat)
        {
            return DataObject.GetIntProperty((PropertyInt)stat);
        }
        
        public string InquireStringStat(int stat)
        {
            return DataObject.GetStringProperty((PropertyString)stat);
        }
        
        public double? InquireFloatStat(int stat)
        {
            return DataObject.GetDoubleProperty((PropertyDouble)stat);
        }

        /// <summary>
        /// This will be false when creature is dead and waits for respawn
        /// </summary>
        public bool IsAlive { get; set; }

        public double RespawnTime { get; set; }

        protected void SetupVitals()
        {
            if (Health.Current != Health.MaxValue)
            {
                VitalTick(Health);
            }
            if (Stamina.Current != Stamina.MaxValue)
            {
                VitalTick(Stamina);
            }
            if (Mana.Current != Mana.MaxValue)
            {
                VitalTick(Mana);
            }
        }

        public Creature(DataObject baseObject, bool isPlayer = false)
            : base(baseObject)
        {
            if (!isPlayer)
                CreateItemList();

            if (Attackable)
                return;

            if (RadarColor != Entity.Enum.RadarColor.Yellow || RadarColor == Entity.Enum.RadarColor.Yellow && CreatureType == null)
                NpcLooksLikeObject = true;
        }

        public void UpdateAppearance(EquipMask equipMask = EquipMask.Clothing | EquipMask.Armor | EquipMask.Cloak)
        {
            ClearObjDesc();
            AddBaseModelData();

            IOrderedEnumerable<KeyValuePair<ObjectGuid, WorldObject>> sortedPriority = from entry in WieldedObjects orderby entry.Value.Priority descending select entry;
            List<uint> coverage = new List<uint>();

            foreach (KeyValuePair<ObjectGuid, WorldObject> w in sortedPriority)
            {
                // We can wield things that are not part of our model, only use those items that can cover our model.
                if ((w.Value.CurrentWieldedLocation & equipMask) == 0)
                    continue;

                ClothingTable item;
                if (w.Value.ClothingBase != null)
                    item = ClothingTable.ReadFromDat((uint)w.Value.ClothingBase);
                else
                    return;

                if (SetupTableId == null || !item.ClothingBaseEffects.ContainsKey((uint)SetupTableId))
                    continue;

                // Add the model and texture(s)
                ClothingBaseEffect clothingBaseEffect = item.ClothingBaseEffects[(uint)SetupTableId];
                foreach (CloObjectEffect t in clothingBaseEffect.CloObjectEffects)
                {
                    byte partNum = (byte)t.Index;
                    // If that part is already covered, well, don't cover it!
                    if (coverage.Contains(partNum))
                        continue;

                    AddModel((byte)t.Index, (ushort)t.ModelId);
                    coverage.Add(partNum);
                    foreach (CloTextureEffect t1 in t.CloTextureEffects)
                        AddTexture((byte)t.Index, (ushort)t1.OldTexture, (ushort)t1.NewTexture);
                }

                // We will use the PaletteTemplate / Shade system for palettes -- if there are any
                if (w.Value.PaletteTemplate != null)
                {
                    if (!item.ClothingSubPalEffects.ContainsKey((int)w.Value.PaletteTemplate))
                        continue;

                    CloSubPalEffect itemSubPal = item.ClothingSubPalEffects[(int)w.Value.PaletteTemplate];
                    foreach (CloSubPalette t1 in itemSubPal.CloSubPalettes)
                    {
                        PaletteSet itemPalSet = PaletteSet.ReadFromDat(t1.PaletteSet);

                        double shadeTmp;
                        if (w.Value.Shade == null)
                            shadeTmp = 0; // TODO - Is this the correct default shade?
                        else
                            shadeTmp = (double)w.Value.Shade;

                        ushort itemPal = (ushort)itemPalSet.GetPaletteID(shadeTmp);

                        foreach (CloSubPalleteRange t in t1.Ranges)
                        {
                            uint palOffset = t.Offset / 8;
                            uint numColors = t.NumColors / 8;
                            AddPalette(itemPal, (ushort)palOffset, (ushort)numColors);
                        }
                    }
                }
                else
                {
                    // This is currently a fall back and will ultimately be deleted once the data is compete - IronGolem
                    foreach (ModelPalette p in w.Value.GetPalettes)
                        AddPalette(p.PaletteId, p.Offset, p.Length);
                }
            }

            if (SetupTableId == null)
                return;

            SetupModel baseSetup = SetupModel.ReadFromDat((uint)SetupTableId);
            for (byte i = 0; i < baseSetup.Parts.Count; i++)
            {
                if (!coverage.Contains(i) && i != 0x10) // Don't add body parts for those that are already covered. Also don't add the head, that was already covered by AddCharacterBaseModelData()
                    AddModel(i, baseSetup.Parts[i]);
            }
        }

        /// <summary>
        /// This is called prior to SendSelf to load up the child list for wielded items that are held in a hand.
        /// </summary>
        public void SetChildren()
        {
            Children.Clear();

            foreach (WorldObject wieldedObject in WieldedObjects.Values)
            {
                WorldObject wo = wieldedObject;
                if ((wo.CurrentWieldedLocation != null) && (((EquipMask)wo.CurrentWieldedLocation & EquipMask.Selectable) != 0))
                    SetChild(this, wo, (int)wo.CurrentWieldedLocation, out _, out _);
                else
                    log.Debug($"Error - item set as child that should not be set - no currentWieldedLocation {wo.Name} - {wo.Guid.Full:X}");
            }
        }

        /// <summary>
        /// This method sets properties needed for items that will be child items.
        /// Items here are only items equipped in the hands.  This deals with the orientation
        /// and positioning for visual appearance of the child items held by the parent. Og II
        /// </summary>
        /// <param name="container">Who is the parent of this child item</param>
        /// <param name="item">The child item - we link them together</param>
        /// <param name="placement">Where is this on the parent - where is it equipped</param>
        /// <param name="placementId">out parameter - this deals with the orientation of the child item as it relates to parent model</param>
        /// <param name="childLocation">out parameter - this is another part of the orientation data for correct visual display</param>
        public bool SetChild(Container container, WorldObject item, int placement, out int placementId, out int childLocation)
        {
            placementId = 0;
            childLocation = 0;
            if (((EquipMask)placement & EquipMask.MeleeWeapon) != 0)
            {
                placementId = (int)Entity.Enum.Placement.RightHandCombat;
                childLocation = (int)ChildLocation.RightHand;
            }
            else if (((EquipMask)placement & EquipMask.MissileWeapon) != 0)
            {
                if (item.DefaultCombatStyle == CombatStyle.Atlatl ||
                    item.DefaultCombatStyle == CombatStyle.Bow ||
                    item.DefaultCombatStyle == CombatStyle.Crossbow)
                {
                    placementId = (int)Entity.Enum.Placement.LeftHand;
                    childLocation = (int)ChildLocation.LeftHand;
                }
                else
                {
                    placementId = (int)Entity.Enum.Placement.RightHandCombat;
                    childLocation = (int)ChildLocation.RightHand;
                }
            }
            else if (((EquipMask)placement & EquipMask.Shield) != 0)
            {
                if (item.ItemType == ItemType.Armor)
                {
                    placementId = (int)Entity.Enum.Placement.Shield;
                    childLocation = (int)ChildLocation.Shield;
                }
                else
                {
                    placementId = (int)Entity.Enum.Placement.RightHandCombat;
                    childLocation = (int)ChildLocation.LeftWeapon;
                }
            }
            else if (((EquipMask)placement & EquipMask.Held) != 0)
            {
                placementId = (int)Entity.Enum.Placement.RightHandCombat;
                childLocation = (int)ChildLocation.RightHand;
            }
            else if (((EquipMask)placement & EquipMask.MissileAmmo) != 0)
            {
                placementId = (int)Entity.Enum.Placement.Default;
                childLocation = (int)ChildLocation.Default;
            }
            else if (((EquipMask)placement & (EquipMask.Armor | EquipMask.Clothing)) != 0)
            {
                placementId = (int)Entity.Enum.Placement.Default;
                childLocation = (int)ChildLocation.Default;
                return false;
            }
            else
            {
                // Should not fall to here
                placementId = (int)Entity.Enum.Placement.Default;
                childLocation = (int)ChildLocation.Default;
                return false;
            }
            if (item.CurrentWieldedLocation != null)
                container.Children.Add(new HeldItem(item.Guid.Full, childLocation, (EquipMask)item.CurrentWieldedLocation));
            item.ParentLocation = childLocation;
            item.Location = Location;
            item.AnimationFrame = placementId;
            return true;
        }

        /// <summary>
        /// This method iterates over the create list and creates any world objects as needed.
        /// </summary>
        private void CreateItemList()
        {
            // TODO: This puts on our we need content added default robe.   Remove when complete.   Coral Golem.
            if (DataWeenie.CreateList.Count == 0 && CreatureType == Entity.Enum.CreatureType.Human)
            {
                DataWeenie.CreateList.Add(new CreationProfile
                {
                    Destination = (uint)DestinationType.Wield,
                    OwnerId = Guid.Full,
                    WeenieClassId = 26452,
                    Palette = 87,
                    Shade = 0,
                    StackSize = 0,
                    TryToBond = false
                });
            }

            foreach (CreationProfile creationProfile in DataWeenie.CreateList)
            {
                if (creationProfile?.WeenieClassId == null)
                    continue;

                WorldObject item = WorldObjectFactory.CreateNewWorldObject((uint)creationProfile.WeenieClassId);
                if (item == null)
                    continue;

                if (creationProfile.Palette != null)
                    item.PaletteTemplate = (int)creationProfile.Palette;

                item.Shade = creationProfile.Shade;
                if (creationProfile.StackSize != null)
                    item.StackSize = (ushort)creationProfile.StackSize;

                // TODO: right now, I am skipping anything that is not a wield item.   Need to come back and finish this up for other destinations.
                if (creationProfile.Destination == null || (DestinationType)creationProfile.Destination != DestinationType.Wield)
                    continue;
                // Unset container fields

                if (item.ValidLocations != null)
                {
                    // NOTE: SetChild needs to have the current wielded location set prior to calling.
                    item.CurrentWieldedLocation = item.ValidLocations;
                    SetChild(this, item, (int)item.ValidLocations, out int _, out int _);
                }
                item.SetParent(this);
                item.WielderId = Guid.Full;
                WieldedObjects.Add(item.Guid, item);
            }
            UpdateAppearance();
        }

        public virtual void DoOnKill(Session killerSession)
        {
            OnKillInternal(killerSession).EnqueueChain();
        }

        protected ActionChain OnKillInternal(Session killerSession)
        {
            // Will start death animation
            OnKill(killerSession);

            // Wait, then run kill animation
            ActionChain onKillChain = new ActionChain();
            onKillChain.AddDelaySeconds(2);
            onKillChain.AddChain(GetCreateCorpseChain());

            return onKillChain;
        }

        protected virtual float GetCorpseSpawnTime()
        {
            return 60;
        }

        public ActionChain GetCreateCorpseChain()
        {
            ActionChain createCorpseChain = new ActionChain(this, () =>
            {
                ////// Create Corspe and set a location on the ground
                ////// TODO: set text of killer in description and find a better computation for the location, some corpse could end up in the ground
                ////var corpse = CorpseObjectFactory.CreateCorpse(this, this.Location);
                ////// FIXME(ddevec): We don't have a real corpse yet, so these come in null -- this hack just stops them from crashing the game
                ////corpse.Location.PositionY -= (corpse.ObjScale ?? 0);
                ////corpse.Location.PositionZ -= (corpse.ObjScale ?? 0) / 2;

                ////// Corpses stay on the ground for 5 * player level but minimum 1 hour
                ////// corpse.DespawnTime = Math.Max((int)session.Player.PropertiesInt[Enum.Properties.PropertyInt.Level] * 5, 360) + WorldManager.PortalYearTicks; // as in live
                ////// corpse.DespawnTime = 20 + WorldManager.PortalYearTicks; // only for testing
                ////float despawnTime = GetCorpseSpawnTime();

                ////// Create corpse
                ////CurrentLandblock.AddWorldObject(corpse);
                ////// Create corpse decay
                ////ActionChain despawnChain = new ActionChain();
                ////despawnChain.AddDelaySeconds(despawnTime);
                ////despawnChain.AddAction(CurrentLandblock, () => corpse.CurrentLandblock.RemoveWorldObject(corpse.Guid, false));
                ////despawnChain.EnqueueChain();
            });
            return createCorpseChain;
        }

        private void OnKill(Session session)
        {
            IsAlive = false;
            // This will determine if the derived type is a player
            var isDerivedPlayer = Guid.IsPlayer();

            if (!isDerivedPlayer)
            {
                // Create and send the death notice
                string killMessage = $"{session.Player.Name} has killed {Name}.";
                var creatureDeathEvent = new GameEventDeathNotice(session, killMessage);
                session.Network.EnqueueSend(creatureDeathEvent);
            }

            // MovementEvent: (Hand-)Combat or in the case of smite: from Standing to Death
            // TODO: Check if the duration of the motion can somehow be computed
            UniversalMotion motionDeath = new UniversalMotion(MotionStance.Standing, new MotionItem(MotionCommand.Dead));
            CurrentLandblock.EnqueueBroadcastMotion(this, motionDeath);

            // If the object is a creature, Remove it from from Landblock
            if (!isDerivedPlayer)
            {
                CurrentLandblock.RemoveWorldObject(Guid, false);
            }
        }

        /// <summary>
        /// Updates a vital, returns true if vital is now < max
        /// </summary>
        public void UpdateVital(CreatureVital vital, uint newVal)
        {
            EnqueueAction(new ActionEventDelegate(() => UpdateVitalInternal(vital, newVal)));
        }

        public void DeltaVital(CreatureVital vital, long delta)
        {
            EnqueueAction(new ActionEventDelegate(() => DeltaVitalInternal(vital, delta)));
        }

        private void DeltaVitalInternal(CreatureVital vital, long delta)
        {
            uint absVal;

            if (delta < 0 && Math.Abs(delta) > vital.Current)
            {
                absVal = (uint)(-1 * vital.Current);
            }
            else if (delta + vital.Current > vital.MaxValue)
            {
                absVal = (uint)(vital.MaxValue - vital.Current);
            }
            else
            {
                absVal = (uint)(vital.Current + delta);
            }

            UpdateVitalInternal(vital, absVal);
        }

        private void VitalTick(CreatureVital vital)
        {
            double tickTime = vital.NextTickTime;
            if (double.IsNegativeInfinity(tickTime))
            {
                tickTime = vital.RegenRate;
            }
            else
            {
                tickTime -= WorldManager.PortalYearTicks;
            }

            // Set up our next tick
            ActionChain tickChain = new ActionChain();
            tickChain.AddDelayTicks(tickTime);
            tickChain.AddAction(this, () => VitalTickInternal(vital));
            tickChain.EnqueueChain();
        }

        protected virtual void VitalTickInternal(CreatureVital vital)
        {
            vital.Tick(WorldManager.PortalYearTicks);
            if (vital.Current != vital.MaxValue)
            {
                VitalTick(vital);
            }
        }

        /// <summary>
        /// This method checks to make sure we have a casting device equipped and if so, it sets
        /// the motion state and sends the messages to switch us to spellcasting state.   Og II
        /// </summary>
        public void HandleSwitchToMagicCombatMode()
        {
            HeldItem mEquipedWand = Children.Find(s => s.EquipMask == EquipMask.Held);
            if (mEquipedWand != null)
            {
                UniversalMotion mm = new UniversalMotion(MotionStance.Spellcasting);
                mm.MovementData.CurrentStyle = (ushort)((uint)MotionStance.Spellcasting & 0xFFFF);
                SetMotionState(this, mm);
                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessagePrivateUpdatePropertyInt(Sequences, PropertyInt.CombatMode, (int)CombatMode.Magic));
            }
            else
                log.InfoFormat("Changing combat mode for {0} - could not locate a wielded magic caster", Guid);
        }

        /// <summary>
        /// This method is called if we unwield missle ammo.   It will check to see if I have arrows wielded
        /// send the message to "hide" the arrow.
        /// </summary>
        /// <param name="oldCombatMode"></param>
        public void HandleUnloadMissileAmmo(CombatMode oldCombatMode)
        {
            // Before I can switch to any non missile mode, do I have missile ammo I need to remove?
            WorldObject ammo = null;
            HeldItem mEquipedAmmo = Children.Find(s => s.EquipMask == EquipMask.MissileAmmo);

            if (mEquipedAmmo != null)
                ammo = GetInventoryItem(new ObjectGuid(mEquipedAmmo.Guid));

            if (oldCombatMode == CombatMode.Missile)
            {
                if (ammo != null)
                {
                    ammo.Location = null;
                    CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessagePickupEvent(ammo));
                }
            }
        }

        /// <summary>
        /// GetWilded Items
        /// </summary>
        public virtual WorldObject GetWieldedItem(ObjectGuid objectGuid)
        {
            // check wielded objects
            if (WieldedObjects.ContainsKey(objectGuid))
            {
                WorldObject inventoryItem;
                if (WieldedObjects.TryGetValue(objectGuid, out inventoryItem))
                    return inventoryItem;
            }
            return null;
        }

        /// <summary>
        /// This method sets us into peace mode.   It checks our current state to see if we have missle ammo equipped
        /// it will make the call to hid the "ammo" as we switch to peace mode.   It will then send the message switch our stance. Og II
        /// </summary>
        /// <param name="oldCombatMode"></param>
        /// <param name="isAutonomous"></param>
        public void HandleSwitchToPeaceMode(CombatMode oldCombatMode, bool isAutonomous = false)
        {
            HandleUnloadMissileAmmo(oldCombatMode);

            // FIXME: (Og II)<this is a hack for now to be removed.> Placement has an issue we have not figured out.   It has to do with animation frame. Og II
            PositionFlag &= ~UpdatePositionFlag.Placement;
            // End hack
            CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessageUpdatePosition(this));
            UniversalMotion mm = new UniversalMotion(MotionStance.Standing);
            mm.MovementData.CurrentStyle = (ushort)((uint)MotionStance.Standing & 0xFFFF);
            SetMotionState(this, mm);
            var mEquipedAmmo = WieldedObjects.FirstOrDefault(s => s.Value.CurrentWieldedLocation == EquipMask.MissileAmmo).Value;
            CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessagePrivateUpdatePropertyInt(Sequences, PropertyInt.CombatMode, (int)CombatMode.NonCombat));
            if (mEquipedAmmo != null)
                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectGhostRange, new GameMessagePickupEvent(mEquipedAmmo));
        }

        public void HandleSwitchToMissileCombatMode(ActionChain combatModeChain)
        {
            // TODO and FIXME: GetInventoryItem doesn't work for this so this function is effectively broke
            HeldItem mEquipedMissile = Children.Find(s => s.EquipMask == EquipMask.MissileWeapon);
            if (mEquipedMissile?.Guid != null)
            {
                WorldObject missileWeapon = GetInventoryItem(new ObjectGuid(mEquipedMissile.Guid));
                if (missileWeapon == null)
                {
                    log.InfoFormat("Changing combat mode for {0} - could not locate wielded weapon {1}", Guid, mEquipedMissile.Guid);
                    return;
                }

                var mEquipedAmmo = WieldedObjects.First(s => s.Value.CurrentWieldedLocation == EquipMask.MissileAmmo).Value;

                MotionStance ms;
                CombatStyle cs;

                if (missileWeapon.DefaultCombatStyle != null)
                    cs = missileWeapon.DefaultCombatStyle.Value;
                else
                {
                    log.InfoFormat("Changing combat mode for {0} - wielded item {1} has not be assigned a default combat style", Guid, mEquipedMissile.Guid);
                    return;
                }

                switch (cs)
                {
                    case CombatStyle.Bow:
                        ms = MotionStance.BowAttack;
                        break;
                    case CombatStyle.Crossbow:
                        ms = MotionStance.CrossBowAttack;
                        break;
                    default:
                        ms = MotionStance.Invalid;
                        break;
                }

                UniversalMotion mm = new UniversalMotion(ms);
                mm.MovementData.CurrentStyle = (ushort)ms;
                if (mEquipedAmmo == null)
                {
                    CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessageUpdatePosition(this));
                    SetMotionState(this, mm);
                }
                else
                {
                    CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessageUpdatePosition(this));
                    SetMotionState(this, mm);
                    mm.MovementData.ForwardCommand = (uint)MotionCommand.Reload;
                    SetMotionState(this, mm);
                    CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessageUpdatePosition(this));
                    // FIXME: (Og II)<this is a hack for now to be removed. Need to pull delay from dat file
                    combatModeChain.AddDelaySeconds(0.25);
                    // System.Threading.Thread.Sleep(250); // used for debugging
                    mm.MovementData.ForwardCommand = (ushort)MotionCommand.Invalid;
                    SetMotionState(this, mm);
                    // FIXME: (Og II)<this is a hack for now to be removed. Need to pull delay from dat file
                    combatModeChain.AddDelaySeconds(0.40);
                    combatModeChain.AddAction(this, () => CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessageParentEvent(this, mEquipedAmmo, 1, 1)));
                    // CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessageParentEvent(this, ammo, 1, 1)); // used for debugging
                }
                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessagePrivateUpdatePropertyInt(Sequences, PropertyInt.CombatMode, (int)CombatMode.Missile));
            }
        }

        public void HandleSwitchToMeleeCombatMode(CombatMode olCombatMode)
        {
            // TODO and FIXME: GetInventoryItem doesn't work for this so this function is effectively broke
            bool shieldEquiped = false;
            bool weaponInShieldSlot = false;

            // Check to see if we were in missile combat and have an arrow hanging around we might need to remove.
            HandleUnloadMissileAmmo(olCombatMode);

            HeldItem mEquipedShieldSlot = Children.Find(s => s.EquipMask == EquipMask.Shield);
            if (mEquipedShieldSlot != null)
            {
                WorldObject itemInShieldSlot = GetInventoryItem(new ObjectGuid(mEquipedShieldSlot.Guid));
                if (itemInShieldSlot != null)
                {
                    if (itemInShieldSlot.ItemType == Entity.Enum.ItemType.Armor)
                        shieldEquiped = true;
                    else
                        weaponInShieldSlot = true;
                }
            }

            HeldItem mEquipedMelee = Children.Find(s => s.EquipMask == EquipMask.MeleeWeapon);
            HeldItem mEquipedTwoHanded = Children.Find(s => s.EquipMask == EquipMask.TwoHanded);
            MotionStance ms = MotionStance.Invalid;
            CombatStyle cs = CombatStyle.Undef;
            // are we unarmed?   If so, do we have a shield?
            if (mEquipedMelee == null && mEquipedTwoHanded == null && !weaponInShieldSlot)
            {
                if (!shieldEquiped)
                    ms = MotionStance.UaNoShieldAttack;
                else
                    ms = MotionStance.MeleeShieldAttack;
            }
            else if (weaponInShieldSlot)
                ms = MotionStance.DualWieldAttack;

            if (mEquipedTwoHanded != null)
            {
                WorldObject twoHandedWeapon = GetInventoryItem(new ObjectGuid(mEquipedTwoHanded.Guid));
                if (twoHandedWeapon.DefaultCombatStyle != null)
                {
                    cs = twoHandedWeapon.DefaultCombatStyle.Value;
                    // ms = MotionStance.TwoHandedSwordAttack;
                    // ms = MotionStance.TwoHandedStaffAttack; ?
                    switch (cs)
                    {
                        // case CombatStyle.???
                        // ms = MotionStance.TwoHandedStaffAttack;
                        // break;
                        default:
                            ms = MotionStance.TwoHandedSwordAttack;
                            break;
                    }
                }
            }

            // Let's see if we are melee single handed / two handed with our without shield as appropriate.
            if (mEquipedMelee?.Guid != null && ms != MotionStance.DualWieldAttack)
            {
                WorldObject meleeWeapon = GetInventoryItem(new ObjectGuid(mEquipedMelee.Guid));

                if (meleeWeapon == null)
                {
                    log.InfoFormat("Changing combat mode for {0} - could not locate wielded weapon {1}", Guid, mEquipedMelee.Guid);
                    return;
                }

                if (!shieldEquiped)
                {
                    if (meleeWeapon.DefaultCombatStyle != null)
                    {
                        cs = meleeWeapon.DefaultCombatStyle.Value;
                        switch (cs)
                        {
                            case CombatStyle.Atlatl:
                                ms = MotionStance.AtlatlCombat;
                                break;
                            case CombatStyle.Sling:
                                ms = MotionStance.SlingAttack;
                                break;
                            case CombatStyle.ThrownWeapon:
                                ms = MotionStance.ThrownWeaponAttack;
                                break;
                            default:
                                ms = MotionStance.MeleeNoShieldAttack;
                                break;
                        }
                    }
                }
                else
                {
                    switch (meleeWeapon.DefaultCombatStyle)
                    {
                        case CombatStyle.Unarmed:
                        case CombatStyle.OneHanded:
                        case CombatStyle.OneHandedAndShield:
                        case CombatStyle.TwoHanded:
                        case CombatStyle.DualWield:
                        case CombatStyle.Melee:
                            ms = MotionStance.MeleeShieldAttack;
                            break;
                        case CombatStyle.ThrownWeapon:
                            ms = MotionStance.ThrownShieldCombat;
                            break;
                        ////case CombatStyle.Unarmed:
                        ////    ms = MotionStance.MeleeShieldAttack;
                        ////    break;
                        default:
                            log.InfoFormat(
                                "Changing combat mode for {0} - unable to determine correct combat stance for weapon {1}", Guid, mEquipedMelee.Guid);
                            return;
                    }
                }
            }
            if (ms != MotionStance.Invalid)
            {
                UniversalMotion mm = new UniversalMotion(ms);
                mm.MovementData.CurrentStyle = (ushort)ms;
                SetMotionState(this, mm);
                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessagePrivateUpdatePropertyInt(Sequences, PropertyInt.CombatMode, (int)CombatMode.Melee));
            }
            else
                log.InfoFormat("Changing combat mode for {0} - wielded item {1} has not be assigned a default combat style", Guid, mEquipedMelee?.Guid ?? mEquipedTwoHanded?.Guid);
        }

        public void SetCombatMode(CombatMode newCombatMode)
        {
            log.InfoFormat("Changing combat mode for {0} to {1}", Guid, newCombatMode);

            ActionChain combatModeChain = new ActionChain();
            combatModeChain.AddAction(this, () =>
                {
                    CombatMode oldCombatMode = CombatMode;
                    CombatMode = newCombatMode;
                    switch (CombatMode)
                    {
                        case CombatMode.NonCombat:
                            HandleSwitchToPeaceMode(oldCombatMode);
                            break;
                        case CombatMode.Melee:
                            HandleSwitchToMeleeCombatMode(oldCombatMode);
                            break;
                        case CombatMode.Magic:
                            HandleSwitchToMagicCombatMode();
                            break;
                        case CombatMode.Missile:
                            HandleSwitchToMissileCombatMode(combatModeChain);
                            break;
                        default:
                            log.InfoFormat("Changing combat mode for {0} - something has gone wrong", Guid);
                            break;
                    }
                });
            combatModeChain.EnqueueChain();
        }

        public void SetMotionState(WorldObject obj, MotionState motionState)
        {
            CurrentMotionState = motionState;
            motionState.IsAutonomous = false;
            GameMessageUpdateMotion updateMotion = new GameMessageUpdateMotion(Guid, Sequences.GetCurrentSequence(SequenceType.ObjectInstance), obj.Sequences, motionState);
            CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, updateMotion);
        }

        protected virtual void UpdateVitalInternal(CreatureVital vital, uint newVal)
        {
            uint old = vital.Current;

            if (newVal > vital.MaxValue)
            {
                newVal = (vital.MaxValue - vital.Current);
            }

            vital.Current = newVal;

            // Check for amount
            if (old == vital.MaxValue && vital.Current != vital.MaxValue)
            {
                // Start up a vital ticker
                new ActionChain(this, () => VitalTickInternal(vital)).EnqueueChain();
            }
        }

        public override void SerializeIdentifyObjectResponse(BinaryWriter writer, bool success, IdentifyResponseFlags flags = IdentifyResponseFlags.None)
        {
            bool hideCreatureProfile = NpcLooksLikeObject ?? false;

            if (!hideCreatureProfile)
            {
                flags |= IdentifyResponseFlags.CreatureProfile;
            }

            base.SerializeIdentifyObjectResponse(writer, success, flags);

            if (!hideCreatureProfile)
            {
                WriteIdentifyObjectCreatureProfile(writer, this, success);
            }
        }

        protected static void WriteIdentifyObjectCreatureProfile(BinaryWriter writer, Creature obj, bool success)
        {
            uint header = 0;
            // TODO: for now, we are always succeeding - will need to set this to 0 header for failure.   Og II
            if (success)
                header = 8;
            writer.Write(header);
            writer.Write(obj.Health.Current);
            writer.Write(obj.Health.MaxValue);
            if (header == 0)
            {
                for (int i = 0; i < 10; i++)
                {
                    writer.Write(0u);
                }
            }
            else
            {
                // TODO: we probably need buffed values here  it may be set my the last flag I don't understand yet. - will need to revisit. Og II
                writer.Write(obj.Strength.UnbuffedValue);
                writer.Write(obj.Endurance.UnbuffedValue);
                writer.Write(obj.Quickness.UnbuffedValue);
                writer.Write(obj.Coordination.UnbuffedValue);
                writer.Write(obj.Focus.UnbuffedValue);
                writer.Write(obj.Self.UnbuffedValue);
                writer.Write(obj.Stamina.UnbuffedValue);
                writer.Write(obj.Mana.UnbuffedValue);
                writer.Write(obj.Stamina.MaxValue);
                writer.Write(obj.Mana.MaxValue);
                // this only gets sent if the header can be masked with 1
                // Writer.Write(0u);
            }
        }

        public void HandleActionWorldBroadcast(string message, ChatMessageType messageType)
        {
            ActionChain chain = new ActionChain();
            chain.AddAction(this, () => DoWorldBroadcast(message, messageType));
            chain.EnqueueChain();
        }

        public void DoWorldBroadcast(string message, ChatMessageType messageType)
        {
            GameMessageSystemChat sysMessage = new GameMessageSystemChat(message, messageType);

            WorldManager.BroadcastToAll(sysMessage);
        }

        /// <summary>
        /// This signature services MoveToObject and TurnToObject
        /// Update Position prior to start, start them moving or turning, set statemachine to moving.
        /// Moved from player - we need to be able to move creatures as well.   Og II
        /// </summary>
        /// <param name="worldObjectPosition">Position in the world</param>
        /// <param name="sequence">Sequence for the object getting the message.</param>
        /// <param name="movementType">What type of movement are we about to execute</param>
        /// <param name="targetGuid">Who are we moving or turning toward</param>
        /// <returns>MovementStates</returns>
        public void OnAutonomousMove(Position worldObjectPosition, SequenceManager sequence, MovementTypes movementType, ObjectGuid targetGuid)
        {
            UniversalMotion newMotion = new UniversalMotion(MotionStance.Standing, worldObjectPosition, targetGuid);
            newMotion.DistanceFrom = 0.60f;
            newMotion.MovementTypes = movementType;
            CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessageUpdatePosition(this));
            CurrentLandblock.EnqueueBroadcastMotion(this, newMotion);
        }

        /// <summary>
        /// This is the OnUse method.   This is just an initial implemention.   I have put in the turn to action at this point.
        /// If we are out of use radius, move to the object.   Once in range, let's turn the creature toward us and get started.
        /// Note - we may need to make an NPC class vs monster as using a monster does not make them turn towrad you as I recall. Og II
        ///  Also, once we are reading in the emotes table by weenie - this will automatically customize the behavior for creatures.
        /// </summary>
        /// <param name="playerId">Identity of the player we are interacting with</param>
        public override void ActOnUse(ObjectGuid playerId)
        {
            if (!(CurrentLandblock.GetObject(playerId) is Player player))
            {
                return;
            }

            if (!player.IsWithinUseRadiusOf(this))
            {
                player.DoMoveTo(this);
            }
            else
            {
                EmoteManager.OnUse(player, this);
                GameEventUseDone sendUseDoneEvent = new GameEventUseDone(player.Session);
                player.Session.Network.EnqueueSend(sendUseDoneEvent);
            }
        }

        /// <summary>
        /// This is handling the giving of an item.
        /// </summary>
        /// <param name="item">The item given</param>
        /// <param name="playerGuid">Guid of the player giving us the item</param>
        /// <param name="amount">How many of the item are we giving?</param>
        public bool HandleReceiveItem(WorldObject item, ObjectGuid playerGuid, uint amount)
        {
            if (!(CurrentLandblock.GetObject(playerGuid) is Player player))
            {
                return false;
            }

            EmoteManager.ReceiveItem(player, this, item, amount);
            return true;
        }

        public List<CreatureSkill> Skills
        {
            get { return DataObject.DataObjectPropertiesSkills; }
        }

        public CreatureSkill GetSkill(Skill skill)
        {
            return DataObject.GetSkillProperty(skill);
        }

        protected float GetRunSpeed()
        {
            float loadMod = 1f; // EncumbranceSystem::LoadMod(load);  // multiplier, as in 0 = full stop

            float fRunSkill = GetSkill(Skill.Run).ActiveValue;
            float modelScale = 1f; // should load from the model

            if (fRunSkill >= 800.0)
                return 4.5f; // Special case, fastest
            else
                return ((loadMod * (fRunSkill / (fRunSkill + 200.0f) * 11.0f) + 4.0f) / modelScale) / 4.0f;
        }
    }
}
