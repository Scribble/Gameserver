/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Managers;

namespace DerethForever.Actors.Actions
{
    public class ActionChain
    {
        public class ChainElement
        {
            public IAction Action { get; private set; }
            public IActor Actor { get; private set; }

            public ChainElement(IActor actor, IAction action)
            {
                this.Actor = actor;
                this.Action = action;
            }
        }

        public ChainElement FirstElement { get; private set; }
        private ChainElement lastElement;

        public ActionChain()
        {
            FirstElement = null;
            lastElement = null;
        }

        public ActionChain(IActor firstActor, Action firstAction)
        {
            FirstElement = new ChainElement(firstActor, new ActionEventDelegate(firstAction));
            lastElement = FirstElement;
        }

        public ActionChain(IActor firstActor, IAction firstAction)
        {
            FirstElement = new ChainElement(firstActor, firstAction);
            lastElement = FirstElement;
        }

        public ActionChain(ChainElement elm)
        {
            FirstElement = elm;
            lastElement = FirstElement;
        }

        public void AddAction(IActor actor, Action action)
        {
            ChainElement newElm = new ChainElement(actor, new ActionEventDelegate(action));
            AddAction(newElm);
        }

        public void AddAction(IActor actor, IAction action)
        {
            ChainElement newElm = new ChainElement(actor, action);
            AddAction(newElm);
        }

        public void AddAction(ChainElement elm)
        {
            if (FirstElement == null)
            {
                FirstElement = elm;
                lastElement = elm;
            }
            else
            {
                lastElement.Action.RunOnFinish(elm.Actor, elm.Action);
                lastElement = elm;
            }
        }

        public void AddChain(ActionChain chain)
        {
            // If we have a chain of our own
            if (lastElement != null)
            {
                lastElement.Action.RunOnFinish(chain.FirstElement.Actor, chain.FirstElement.Action);
                lastElement = chain.lastElement;
            }
            // If we're uninit'd, take their data
            else
            {
                FirstElement = chain.FirstElement;
                lastElement = chain.lastElement;
            }
        }

        public void AddBranch(IActor conditionActor, Func<bool> condition, ChainElement trueBranch, ChainElement falseBranch)
        {
            AddBranch(conditionActor, condition, new ActionChain(trueBranch), new ActionChain(falseBranch));
        }

        public void AddBranch(IActor conditionActor, Func<bool> condition, ActionChain trueBranch, ActionChain falseBranch)
        {
            AddAction(new ChainElement(conditionActor, new ConditionalAction(condition, trueBranch, falseBranch)));
        }

        public void AddLoop(IActor conditionActor, Func<bool> condition, ActionChain body)
        {
            AddAction(new ChainElement(conditionActor, new LoopAction(conditionActor, condition, body)));
        }

        public void AddDelaySeconds(double timeInSeconds)
        {
            AddAction(WorldManager.DelayManager, new DelayAction(WorldManager.SecondsToTicks(timeInSeconds)));
        }

        public void AddDelayTicks(double timeInTicks)
        {
            AddAction(WorldManager.DelayManager, new DelayAction(timeInTicks));
        }

        public void EnqueueChain()
        {
            if (FirstElement != null)
                FirstElement.Actor.EnqueueAction(FirstElement.Action);
        }
    }
}
