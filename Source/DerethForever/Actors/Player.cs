/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using log4net;
using System.Diagnostics;
using DerethForever.Database;
using DerethForever.DatLoader.Entity;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Actors.Actions;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Entity.Enum.Properties;
using DerethForever.Factories;
using DerethForever.Managers;
using DerethForever.Network;
using DerethForever.Network.GameEvent.Events;
using DerethForever.Network.GameMessages;
using DerethForever.Network.GameMessages.Messages;
using DerethForever.Network.Motion;
using DerethForever.Network.Sequence;
using DerethForever.Network.Enum;

namespace DerethForever.Actors
{
    public sealed class Player : Creature, IPlayer
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Enum used for the DoEatOrDrink() method
        /// </summary>
        public enum ConsumableBuffType : uint
        {
            Spell   = 0,
            Health  = 2,
            Stamina = 4,
            Mana    = 6
        }

        // TODO: link to Town Network marketplace portal destination in db, when db for that is finalized and implemented.
        private static readonly Position MarketplaceDrop = new Position(23855548, 49.206f, -31.935f, 0.005f, 0f, 0f, -0.7071068f, 0.7071068f); // PCAP verified drop

        private const float PickUpDistance = .75f;

        public Session Session { get; }

        /// <summary>
        /// This will be false when in portal space
        /// </summary>
        public bool InWorld { get; set; }

        /// <summary>
        /// Different than InWorld which is false when in portal space
        /// </summary>
        public bool IsOnline { get; private set; }

        /// <summary>
        /// ObjectId of the currently selected Target (only players and creatures)
        /// </summary>
        private ObjectGuid selectedTarget = ObjectGuid.Invalid;

        /// <summary>
        /// Temp tracked Objects of vendors / trade / containers.. needed for id / maybe more.
        /// </summary>
        private readonly Dictionary<ObjectGuid, WorldObject> interactiveWorldObjects = new Dictionary<ObjectGuid, WorldObject>();

        /// <summary>
        /// This tracks the contract tracker objects
        /// </summary>
        public Dictionary<uint, ContractTracker> TrackedContracts { get; set; }

        /// <summary>
        /// This dictionary is used to keep track of the last use of any item that implemented shared cooldown.
        /// It is session specific.   I think (could be wrong) cooldowns reset if you logged out and back in.
        /// This is a different mechanic than quest repeat timers and rare item use timers.
        /// example - contacts have a shared cooldown key value 100 so each time a player uses an item that has
        /// a shared cooldown we just add to the dictionary 100, datetime.now()   The check becomes trivial at that
        /// point if on a subsequent use, now() minus the last use value from the dictionary
        /// is greater than or equal to the cooldown, we can do the use - if not you must wait message.   Og II
        /// </summary>
        public Dictionary<int, DateTime> LastUseTracker { get; set; }

        /// <summary>
        /// Level of the player
        /// </summary>
        public int Level
        {
            get { return Character.Level; }
        }
        
        public int TotalSkillCredits
        {
            get { return Character.TotalSkillCredits; }
            set { Character.TotalSkillCredits = value; }
        }

        public int AvailableSkillCredits
        {
            get { return Character.AvailableSkillCredits; }
            set { Character.AvailableSkillCredits = value; }
        }

        /// <summary>
        ///  The fellowship that this player belongs to
        /// </summary>
        public Fellowship Fellowship = null;

        /// <summary>
        /// Creates a new fellowship based upon the options defined by the leader
        /// </summary>
        /// <param name="fellowshipName">Name of fellowship</param>
        /// <param name="shareXP">Whether fellowship shares experience</param>
        /// <param name="shareLoot">Whether the fellowship allows open looting for all members</param>
        public void CreateFellowship(string fellowshipName, bool shareXP)
        {
            bool shareLoot = GetCharacterOption(CharacterOption.ShareFellowshipLoot);

            if (Fellowship == null)
                Fellowship = new Fellowship(this, fellowshipName, shareXP, shareLoot);
        }

        /// <summary>
        /// Recruit a new member to the fellowship
        /// </summary>
        /// <param name="newRecruit">Player being recruited</param>
        public void RecruitFellowshipMember(Player newRecruit)
        {
            if (newRecruit.Fellowship != null)
            {
                SendSystemMessage($"{newRecruit.Name} is already in a fellowship", ChatMessageType.Fellowship);
                return;
            }

            if (Fellowship.Fellows.Count == Fellowship.FellowMaxMembers)
            {
                SendSystemMessage($"{newRecruit.Name} cannot join as fellowship is full", ChatMessageType.Fellowship);
                return;
            }

            if (newRecruit.GetCharacterOption(CharacterOption.AutomaticallyAcceptFellowshipRequests))
            {
                Fellowship.AddFellowshipMember(this, newRecruit);
            }
            else if (newRecruit.GetCharacterOption(CharacterOption.IgnoreFellowshipRequests))
            {
                SendSystemMessage($"{newRecruit.Name} is not accepting fellowship requests.", ChatMessageType.Fellowship);
            }
            else
            {
                Confirmation recruitConfirm = new Confirmation(ConfirmationType.Fellowship, $"{Name} invites you to join a fellowship",
                    Guid.Full, newRecruit.Guid.Full);
                ConfirmationManager.AddConfirmation(recruitConfirm);
                newRecruit.Session.Network.EnqueueSend(new GameEventConfirmationRequest(newRecruit.Session,
                    (uint)ConfirmationType.Fellowship, recruitConfirm.Context, recruitConfirm.Message));
            }
        }

        private DataCharacter Character { get { return DataObject as DataCharacter; } }

        public List<DataObjectPropertiesSpellBarPositions> SpellsInSpellBars
        {
            get
            {
                return DataObject.SpellsInSpellBars;
            }
            set
            {
                DataObject.SpellsInSpellBars = value;
            }
        }

        public void SetCharacterOptions1(int options1)
        {
            Character.CharacterOptions1Mapping = options1;
        }

        public void SetCharacterOptions2(int options2)
        {
            Character.CharacterOptions2Mapping = options2;
        }

        private readonly object clientObjectMutex = new object();

        /// <summary>
        /// FIXME(ddevec): This is the only object that need be locked in the player under the new model.
        ///   It must be locked because of how we handle object updates -- We can clean this up in the future
        /// </summary>
        private readonly Dictionary<ObjectGuid, double> clientObjectList = new Dictionary<ObjectGuid, double>();

        public Dictionary<PositionType, Position> Positions
        {
            get { return DataObject.Positions.ToDictionary(p => (PositionType)p.DbPositionType, p => p.Position); }
        }

        private Position PositionSanctuary
        {
            get { return DataObject.GetPosition(PositionType.Sanctuary); }
            set { DataObject.SetPosition(PositionType.Sanctuary, value); }
        }

        private Position PositionLastPortal
        {
            get { return DataObject.GetPosition(PositionType.LastPortal); }
            set { DataObject.SetPosition(PositionType.LastPortal, value); }
        }

        public bool UnknownSpell(uint spellId)
        {
            return !(DataObject.SpellIdProperties.Exists(x => x.SpellId == spellId));
        }

        /// <summary>
        /// This method is used to give items from one player to another player or a creature.   This is WIP and not complete. Coral Golem
        /// </summary>
        /// <param name="targetGuid">Who are we giving the item to?</param>
        /// <param name="objectGuid">What item are we giving</param>
        /// <param name="amount">How much of the item did we give.</param>
        public void HandleActionGiveObject(ObjectGuid targetGuid, ObjectGuid objectGuid, uint amount)
        {
            new ActionChain(this, () =>
            {
                WorldObject iwo = GetInventoryItem(objectGuid);

                if (CurrentLandblock != null && iwo != null)
                {
                    // Just forward our action to the appropriate user...
                    // TODO I need to check object type
                    // TODO I need to make this a virtual method on WorldObject and override - creatures and players will deal with this differently
                    // Creatures will respond via emote, players I need to check give status and other items.
                    Creature co = (Creature)CurrentLandblock.GetObject(targetGuid);
                    if (co != null)
                    {
                        bool result = co.HandleReceiveItem(iwo, this.Guid, amount);
                        if (result)
                            RemoveItemFromPlayerAndNotify(objectGuid, out iwo);
                    }
                }
            }).EnqueueChain();
        }

        public void HandleActionMagicRemoveSpellId(uint spellId)
        {
            ActionChain unlearnSpellChain = new ActionChain();
            unlearnSpellChain.AddAction(
                this,
                () =>
                {
                    if (!DataObject.SpellIdProperties.Exists(x => x.SpellId == spellId))
                    {
                        log.Error("Invalid spellId passed to Player.RemoveSpellFromSpellBook");
                        return;
                    }

                    DataObject.SpellIdProperties.RemoveAt(DataObject.SpellIdProperties.FindIndex(x => x.SpellId == spellId));
                    GameEventMagicRemoveSpellId removeSpellEvent = new GameEventMagicRemoveSpellId(Session, spellId);
                    Session.Network.EnqueueSend(removeSpellEvent);
                });
            unlearnSpellChain.EnqueueChain();
        }

        public void HandleActionLearnSpell(uint spellId)
        {
            ActionChain learnSpellChain = new ActionChain();
            SpellTable spells = SpellTable.ReadFromDat();
            if (!spells.Spells.ContainsKey(spellId))
            {
                GameMessageSystemChat errorMessage = new GameMessageSystemChat("SpellID not found in Spell Table", ChatMessageType.Broadcast);
                Session.Network.EnqueueSend(errorMessage);
                return;
            }
            learnSpellChain.AddAction(this,
                () =>
                {
                    if (!UnknownSpell(spellId))
                    {
                        GameMessageSystemChat errorMessage = new GameMessageSystemChat("That spell is already known", ChatMessageType.Broadcast);
                        Session.Network.EnqueueSend(errorMessage);
                        return;
                    }
                    DataObjectPropertiesSpell newSpell = new DataObjectPropertiesSpell
                    {
                        DataObjectId = this.Guid.Full,
                        SpellId = spellId
                    };
                    DataObject.SpellIdProperties.Add(newSpell);
                    GameEventMagicUpdateSpell updateSpellEvent = new GameEventMagicUpdateSpell(Session, spellId);
                    Session.Network.EnqueueSend(updateSpellEvent);

                    // Always seems to be this SkillUpPurple effect
                    Session.Player.HandleActionApplyVisualEffect(Entity.Enum.PlayScript.SkillUpPurple);

                    string spellName = spells.Spells[spellId].Name;
                    string message = "You learn the " + spellName + " spell.\n";
                    GameMessageSystemChat learnMessage = new GameMessageSystemChat(message, ChatMessageType.Broadcast);
                    Session.Network.EnqueueSend(learnMessage);
                });
            learnSpellChain.EnqueueChain();
        }

        /// <summary>
        /// This method implements player spell bar management for - adding a spell to a specific spell bar (0 based) at a specific slot (0 based).
        /// </summary>
        /// <param name="spellId"></param>
        /// <param name="spellBarPositionId"></param>
        /// <param name="spellBarId"></param>
        public void HandleActionAddSpellToSpellBar(uint spellId, uint spellBarPositionId, uint spellBarId)
        {
            // The spell bar magic happens here. First, let's mind our race conditions....
            ActionChain addSpellBarChain = new ActionChain();
            addSpellBarChain.AddAction(this, () =>
            {
                SpellsInSpellBars.Add(new DataObjectPropertiesSpellBarPositions()
                {
                    DataObjectId = DataObject.DataObjectId,
                    SpellId = spellId,
                    SpellBarId = spellBarId,
                    SpellBarPositionId = spellBarPositionId
                });
            });
            addSpellBarChain.EnqueueChain();
        }

        /// <summary>
        /// This method implements player spell bar management for - removing a spell to a specific spell bar (0 based)
        /// </summary>
        /// <param name="spellId"></param>
        /// <param name="spellBarId"></param>
        public void HandleActionRemoveSpellToSpellBar(uint spellId, uint spellBarId)
        {
            // More spell bar magic happens here. First, let's mind our race conditions....
            ActionChain removeSpellBarChain = new ActionChain();
            removeSpellBarChain.AddAction(this, () =>
            {
                SpellsInSpellBars.Remove(SpellsInSpellBars.Single(x => x.SpellBarId == spellBarId && x.SpellId == spellId));
                // Now I have to reorder
                IOrderedEnumerable<DataObjectPropertiesSpellBarPositions> sorted = SpellsInSpellBars.FindAll(x => x.DataObjectId == DataObject.DataObjectId && x.SpellBarId == spellBarId).OrderBy(s => s.SpellBarPositionId);
                uint newSpellBarPosition = 0;
                foreach (DataObjectPropertiesSpellBarPositions spells in sorted)
                {
                    spells.SpellBarPositionId = newSpellBarPosition;
                    newSpellBarPosition++;
                }
            });
            removeSpellBarChain.EnqueueChain();
        }

        public ReadOnlyDictionary<CharacterOption, bool> CharacterOptions
        {
            get { return Character.CharacterOptions; }
        }

        public ReadOnlyCollection<Friend> Friends
        {
            get { return Character.Friends; }
        }

        public bool IsAdmin
        {
            get { return Character.IsAdmin; }
            set { Character.IsAdmin = value; }
        }

        public bool IsEnvoy
        {
            get { return Character.IsEnvoy; }
            set { Character.IsEnvoy = value; }
        }

        public bool IsArch
        {
            get { return Character.IsArch; }
            set { Character.IsArch = value; }
        }

        public bool IsPsr
        {
            get { return Character.IsPsr; }
            set { Character.IsPsr = value; }
        }

        public int TotalLogins
        {
            get { return Character.TotalLogins; }
            set { Character.TotalLogins = value; }
        }

        public Player(Session session, DataCharacter character)
            : base(character, true)
        {
            Session = session;

            // This is the default send upon log in and the most common.   Anything with a velocity will need to add that flag.
            PositionFlag |= UpdatePositionFlag.ZeroQx | UpdatePositionFlag.ZeroQy | UpdatePositionFlag.Contact | UpdatePositionFlag.Placement;

            Player = true;

            // Admin = true; // Uncomment to enable Admin flag on Player objects. I would expect this would go in Admin.cs, replacing Player = true,
            // I don't believe both were on at the same time. -Ripley

            IgnoreCollision = true; Gravity = true; Hidden = true; EdgeSlide = true;

            // apply defaults.  "Load" should be overwriting these with values specific to the character
            // TODO: Load from database should be loading player data - including inventroy and positions
            CurrentMotionState = new UniversalMotion(MotionStance.Standing);

            // radius for object updates
            ListeningRadius = 5f;

            TrackedContracts = new Dictionary<uint, ContractTracker>();
            // Load the persisted tracked contracts into the working dictionary on player object.
            foreach (KeyValuePair<uint, DataContractTracker> trackedContract in DataObject.TrackedContracts)
            {
                ContractTracker loadContract = new ContractTracker(trackedContract.Value.ContractId, Guid.Full)
                {
                    DeleteContract       = trackedContract.Value.DeleteContract,
                    SetAsDisplayContract = trackedContract.Value.SetAsDisplayContract,
                    Stage                = trackedContract.Value.Stage,
                    TimeWhenDone         = trackedContract.Value.TimeWhenDone,
                    TimeWhenRepeats      = trackedContract.Value.TimeWhenRepeats
                };

                TrackedContracts.Add(trackedContract.Key, loadContract);
            }

            LastUseTracker = new Dictionary<int, DateTime>();
        }

        /// <summary>
        ///  Gets a list of Tracked Objects.
        /// </summary>
        public List<ObjectGuid> GetTrackedObjectGuids()
        {
            lock (clientObjectList)
            {
                return clientObjectList.Select(x => x.Key).ToList();
            }
        }

        public bool FirstEnterWorldDone = false;

        public int Age
        { get { return Character.Age; } }

        public uint CreationTimestamp
        { get { return (uint)Character.CreationTimestamp; } }

        public DataObject GetDataObject()
        {
            return Character;
        }

        // FIXME(ddevec): This should eventually be removed, with most of its contents making its way into the Player() constructor
        public void Load(DataCharacter character)
        {
            // This is a hack, but works well.   Because we cache the landblocks a call to create a landblock
            // will only create it if it does not exist.   This lets us fix first time loading issues and
            // has no performance hit if the landblock is already active.
            // TODO: we still need to find out why landblock loading is so slow.

            Landblock x;
            if (character.Location != null)
                x = new Landblock(character.Location.LandblockId);

            if (Common.ConfigManager.Config.Server.Accounts.OverrideCharacterPermissions)
            {
                if (Session.AccessLevel == AccessLevel.Admin)
                    character.IsAdmin = true;
                if (Session.AccessLevel == AccessLevel.Developer)
                    character.IsArch = true;
                if (Session.AccessLevel == AccessLevel.Envoy)
                    character.IsEnvoy = true;
                // TODO: Need to setup and account properly for IsSentinel and IsAdvocate.
                // if (Session.AccessLevel == AccessLevel.Sentinel)
                //    character.IsSentinel = true;
                // if (Session.AccessLevel == AccessLevel.Advocate)
                //    character.IsAdvocate= true;
            }

            FirstEnterWorldDone = false;

            IsAlive = true;
            IsOnline = true;

            SetupVitals();
            ////// Start vital ticking, if they need it
            ////if (Health.Current != Health.MaxValue)
            ////{
            ////    VitalTickInternal(Health);
            ////}

            ////if (Stamina.Current != Stamina.MaxValue)
            ////{
            ////    VitalTickInternal(Stamina);
            ////}

            ////if (Mana.Current != Mana.MaxValue)
            ////{
            ////    VitalTickInternal(Mana);
            ////}

            AddBaseModelData();

            UpdateAppearance(this);
            // Burden = UpdateBurden();

            // Save the the LoginTimestamp
            Character.SetDoubleTimestamp(PropertyDouble.LoginTimestamp);

            TotalLogins++;
            Sequences.AddOrSetSequence(SequenceType.ObjectInstance, new UShortSequence((ushort)TotalLogins));

            // SendSelf will trigger the entrance into portal space
            SendSelf();

            SendFriendStatusUpdates();

            // Init the client with the chat channel ID's, and then notify the player that they've choined the associated channels.
            GameEventSetTurbineChatChannels setTurbineChatChannels = new GameEventSetTurbineChatChannels(Session, 0, 1, 2, 3, 4, 6, 7, 0, 0, 0); // TODO these are hardcoded right now
            GameEventDisplayParameterizedStatusMessage general = new GameEventDisplayParameterizedStatusMessage(Session, StatusMessageType2.YouHaveEnteredThe_Channel, "General");
            GameEventDisplayParameterizedStatusMessage trade = new GameEventDisplayParameterizedStatusMessage(Session, StatusMessageType2.YouHaveEnteredThe_Channel, "Trade");
            GameEventDisplayParameterizedStatusMessage lfg = new GameEventDisplayParameterizedStatusMessage(Session, StatusMessageType2.YouHaveEnteredThe_Channel, "LFG");
            GameEventDisplayParameterizedStatusMessage roleplay = new GameEventDisplayParameterizedStatusMessage(Session, StatusMessageType2.YouHaveEnteredThe_Channel, "Roleplay");
            Session.Network.EnqueueSend(setTurbineChatChannels, general, trade, lfg, roleplay);

            FirstEnterWorldDone = true;
        }

        public DataObject GetSavableCharacter()
        {
            // Clone Character
            DataObject obj = (DataObject)Character.Clone();

            // These don't usually get saved back to the object so setting here for now.
            // Realistically speaking, I think it will be possible to eliminate WeenieHeaderFlags and PhysicsDescriptionFlag from the datbase
            // ObjectDescriptionFlag possibly could be eliminated as well... -Ripley
            // actually we do use those without creating a wo - so it would be needed to keep them in the database Og II
            obj.WeenieHeaderFlags = (uint)WeenieFlags;
            obj.PhysicsDescriptionFlag = (uint)PhysicsDescriptionFlag;

            return obj;
        }
        
        /// <summary>
        /// This will grant skill credits equal to the amount read from the Amount field in the weenie editor.
        /// </summary>
        /// <param name="amount"></param>
        public void GrantSkillCredits(int amount)
        {
            AvailableSkillCredits += amount;
            TotalSkillCredits += amount;
            GameMessagePrivateUpdatePropertyInt currentCredits = new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.AvailableSkillCredits, Character.AvailableSkillCredits);
            Session.Network.EnqueueSend(currentCredits);
            GameMessageSystemChat message = new GameMessageSystemChat($"You've earned {amount:n0} skill credits.", ChatMessageType.Broadcast);
            Session.Network.EnqueueSend(message);
        }

        /// <summary>
        /// Raise the available XP by a specified amount
        /// </summary>
        /// <param name="amount">A unsigned long containing the desired XP amount to raise</param>
        /// <param name="shared">If this xp is to be shared with fellowship</param>
        /// <param name="bonus">If this xp is not privvy to bonus fellowship. e.g. XP item turnin</param>
        public void GrantXp(ulong amount, bool shared = true, bool bonus = true)
        {
            if (Fellowship != null && shared)
            {
                Fellowship.DistributeXp(amount, this, bonus);
            }
            else
            {
                // until we are max level we must make sure that we send
                IncreaseXpTotals(amount);
                GameMessageSystemChat message = new GameMessageSystemChat($"You've earned {amount:n0} experience.", ChatMessageType.Broadcast);
                Session.Network.EnqueueSend(message);
            }
        }

        private void IncreaseXpTotals(ulong amount)
        {
            XpTable xpTable = XpTable.ReadFromDat();
            LevelingChart chart = xpTable.LevelingXpChart;
            CharacterLevel maxLevel = chart.Levels.Last();
            if (Character.Level != maxLevel.Level)
            {
                ulong amountLeftToEnd = maxLevel.TotalXp - Character.TotalExperience;
                if (amount > amountLeftToEnd)
                {
                    amount = amountLeftToEnd;
                }
                Character.GrantXp(amount);
                CheckForLevelup();

                GameMessagePrivateUpdatePropertyInt64 xpTotalUpdate = new GameMessagePrivateUpdatePropertyInt64(Session, PropertyInt64.TotalExperience, Character.TotalExperience);
                GameMessagePrivateUpdatePropertyInt64 xpAvailUpdate = new GameMessagePrivateUpdatePropertyInt64(Session, PropertyInt64.AvailableExperience, Character.AvailableExperience);
                Session.Network.EnqueueSend(xpTotalUpdate, xpAvailUpdate);
                // TODO Put in code for passup xp.   Coral Golem
            }
        }

        public void DistributeXP(ulong amount)
        {
            IncreaseXpTotals(amount);
        }

        /// <summary>
        /// Public method for adding a new skill by spending skill credits.
        /// </summary>
        /// <remarks>
        ///  The client will throw up more then one train skill dialog and the user has the chance to spend twice.
        /// </remarks>
        /// <param name="skill"></param>
        /// <param name="creditsSpent"></param>
        public void TrainSkill(Skill skill, int creditsSpent)
        {
            if (Character.AvailableSkillCredits >= creditsSpent)
            {
                // attempt to train the specified skill
                bool trainNewSkill = Character.TrainSkill(skill, creditsSpent);

                // create an update to send to the client
                GameMessagePrivateUpdatePropertyInt currentCredits = new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.AvailableSkillCredits, Character.AvailableSkillCredits);

                // as long as the skill is sent, the train new triangle button on the client will not lock up.
                // Sending Skill.None with status untrained worked in test
                GameMessagePrivateUpdateSkill trainSkillUpdate = new GameMessagePrivateUpdateSkill(Session, Skill.None, SkillStatus.Untrained, 0, 0, 0);
                // create a string placeholder for the correct after
                string trainSkillMessageText = "";

                // if the skill has already been trained or we do not have enough credits, then trainNewSkill be set false
                if (trainNewSkill)
                {
                    // replace the trainSkillUpdate message with the correct skill assignment:
                    trainSkillUpdate = new GameMessagePrivateUpdateSkill(Session, skill, SkillStatus.Trained, 0, 0, 0);
                    trainSkillMessageText = $"{SkillExtensions.ToSentence(skill)} trained. You now have {Character.AvailableSkillCredits} credits available.";
                }
                else
                {
                    trainSkillMessageText = $"Failed to train {SkillExtensions.ToSentence(skill)}! You now have {Character.AvailableSkillCredits} credits available.";
                }

                // create the final game message and send to the client
                GameMessageSystemChat message = new GameMessageSystemChat(trainSkillMessageText, ChatMessageType.Advancement);
                Session.Network.EnqueueSend(trainSkillUpdate, currentCredits, message);
            }
        }

        /// <summary>
        /// Determines if the player has advanced a level
        /// </summary>
        /// <remarks>
        /// Known issues:
        ///         1. XP updates from outside of the grantxp command have not been done yet.
        /// </remarks>
        private void CheckForLevelup()
        {
            // Question: Where do *we* call CheckForLevelup()? :
            //      From within the player.cs file, the options might be:
            //           GrantXp()
            //      From outside of the player.cs file, we may call CheckForLevelup() durring? :
            //           XP Updates?
            int startingLevel = Character.Level;
            XpTable xpTable = XpTable.ReadFromDat();
            LevelingChart chart = xpTable.LevelingXpChart;
            CharacterLevel maxLevel = chart.Levels.Last();
            bool creditEarned = false;
            if (Character.Level == maxLevel.Level) return;

            // increases until the correct level is found
            while (chart.Levels[Convert.ToInt32(Character.Level)].TotalXp <= Character.TotalExperience)
            {
                Character.Level++;
                CharacterLevel newLevel = chart.Levels.FirstOrDefault(item => item.Level == Character.Level);
                // increase the skill credits if the chart allows this level to grant a credit
                if (newLevel.GrantsSkillPoint)
                {
                    Character.AvailableSkillCredits++;
                    Character.TotalSkillCredits++;
                    creditEarned = true;
                }
                // break if we reach max
                if (Character.Level == maxLevel.Level)
                {
                    PlayParticleEffect(Entity.Enum.PlayScript.WeddingBliss, Guid);
                    break;
                }
            }

            if (Character.Level > startingLevel)
            {
                string level = $"{Character.Level}";
                string skillCredits = $"{Character.AvailableSkillCredits}";
                string xpAvailable = $"{Character.AvailableExperience:#,###0}";
                GameMessagePrivateUpdatePropertyInt levelUp = new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.Level, Character.Level);
                string levelUpMessageText = (Character.Level == maxLevel.Level) ? $"You have reached the maximum level of {level}!" : $"You are now level {level}!";
                GameMessageSystemChat levelUpMessage = new GameMessageSystemChat(levelUpMessageText, ChatMessageType.Advancement);
                string xpUpdateText = (Character.AvailableSkillCredits > 0) ? $"You have {xpAvailable} experience points and {skillCredits} skill credits available to raise skills and attributes." : $"You have {xpAvailable} experience points available to raise skills and attributes.";
                GameMessageSystemChat xpUpdateMessage = new GameMessageSystemChat(xpUpdateText, ChatMessageType.Advancement);
                GameMessagePrivateUpdatePropertyInt currentCredits = new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.AvailableSkillCredits, Character.AvailableSkillCredits);
                if (Character.Level != maxLevel.Level && !creditEarned)
                {
                    string nextCreditAtText = $"You will earn another skill credit at {chart.Levels.Where(item => item.Level > Character.Level).OrderBy(item => item.Level).First(item => item.GrantsSkillPoint).Level}";
                    GameMessageSystemChat nextCreditMessage = new GameMessageSystemChat(nextCreditAtText, ChatMessageType.Advancement);
                    Session.Network.EnqueueSend(levelUp, levelUpMessage, xpUpdateMessage, currentCredits, nextCreditMessage);
                }
                else
                {
                    Session.Network.EnqueueSend(levelUp, levelUpMessage, xpUpdateMessage, currentCredits);
                }
                // play level up effect
                PlayParticleEffect(Entity.Enum.PlayScript.LevelUp, Guid);
                if (Fellowship != null)
                    Fellowship.SendStatUpdate(this);
            }
        }

        public void SpendXp(Ability ability, uint amount)
        {
            bool isSecondary = false;
            ICreatureXpSpendableStat creatureStat;
            CreatureAbility creatureAbility;
            bool success = DataObject.DataObjectPropertiesAttributes.TryGetValue(ability, out creatureAbility);
            if (success)
            {
                creatureStat = creatureAbility;
            }
            else
            {
                CreatureVital v;
                success = DataObject.DataObjectPropertiesAttributes2nd.TryGetValue(ability, out v);

                // Invalid ability
                if (success)
                {
                    creatureStat = v;
                }
                else
                {
                    log.Error("Invalid ability passed to Player.SpendXp");
                    return;
                }
                isSecondary = true;
            }
            uint baseValue = creatureStat.Base;
            uint result = SpendAbilityXp(creatureStat, amount);
            uint ranks = creatureStat.Ranks;
            uint newValue = creatureStat.UnbuffedValue;
            string messageText = "";
            if (result > 0u)
            {
                GameMessage abilityUpdate;
                if (!isSecondary)
                {
                    abilityUpdate = new GameMessagePrivateUpdateAbility(Session, ability, ranks, baseValue, result);
                }
                else
                {
                    abilityUpdate = new GameMessagePrivateUpdateVital(Session, ability, ranks, baseValue, result, creatureStat.Current);
                }

                // checks if max rank is achieved and plays fireworks w/ special text
                if (IsAbilityMaxRank(ranks, isSecondary))
                {
                    // fireworks
                    PlayParticleEffect(Entity.Enum.PlayScript.WeddingBliss, Guid);
                    messageText = $"Your base {ability} is now {newValue} and has reached its upper limit!";
                }
                else
                {
                    messageText = $"Your base {ability} is now {newValue}!";
                }
                GameMessagePrivateUpdatePropertyInt64 xpUpdate = new GameMessagePrivateUpdatePropertyInt64(Session, PropertyInt64.AvailableExperience, Character.AvailableExperience);
                GameMessageSound soundEvent = new GameMessageSound(this.Guid, Sound.RaiseTrait, 1f);
                GameMessageSystemChat message = new GameMessageSystemChat(messageText, ChatMessageType.Advancement);

                // This seems to be needed to keep health up to date properly.
                // Needed when increasing health and endurance.
                if (ability == Entity.Enum.Ability.endurance)
                {
                    GameMessagePrivateUpdateVital healthUpdate = new GameMessagePrivateUpdateVital(Session, Entity.Enum.Ability.health, Health.Ranks, Health.Base, Health.ExperienceSpent, Health.Current);
                    Session.Network.EnqueueSend(abilityUpdate, xpUpdate, soundEvent, message, healthUpdate);
                }
                else if (ability == Entity.Enum.Ability.self)
                {
                    GameMessagePrivateUpdateVital manaUpdate = new GameMessagePrivateUpdateVital(Session, Entity.Enum.Ability.mana, Mana.Ranks, Mana.Base, Mana.ExperienceSpent, Mana.Current);
                    Session.Network.EnqueueSend(abilityUpdate, xpUpdate, soundEvent, message, manaUpdate);
                }
                else
                {
                    Session.Network.EnqueueSend(abilityUpdate, xpUpdate, soundEvent, message);
                }
                if (Fellowship != null)
                    Fellowship.SendStatUpdate(this);
            }
            else
            {
                ChatPacket.SendServerMessage(Session, $"Your attempt to raise {ability} has failed.", ChatMessageType.Broadcast);
            }
        }

        /// <summary>
        /// spends the xp on this ability.
        /// </summary>
        /// <returns>0 if it failed, total investment of the next rank if successful</returns>
        private uint SpendAbilityXp(ICreatureXpSpendableStat ability, uint amount)
        {
            uint result = 0;
            ExperienceExpenditureChart chart;
            XpTable xpTable = XpTable.ReadFromDat();
            switch (ability.Ability)
            {
                case Ability.health:
                case Ability.stamina:
                case Ability.mana:
                    chart = xpTable.VitalXpChart;
                    break;
                default:
                    chart = xpTable.AbilityXpChart;
                    break;
            }

            // do not advance if we cannot spend xp to rank up our skill by 1 point
            if (ability.Ranks >= (chart.Ranks.Count - 1))
                return result;

            uint rankUps = 0u;
            uint currentXp = chart.Ranks[Convert.ToInt32(ability.Ranks)].TotalXp;
            uint rank1 = chart.Ranks[Convert.ToInt32(ability.Ranks) + 1].XpFromPreviousRank;
            uint rank10 = 0u;
            int rank10Offset = 0;

            if (ability.Ranks + 10 >= (chart.Ranks.Count))
            {
                rank10Offset = 10 - (Convert.ToInt32(ability.Ranks + 10) - (chart.Ranks.Count - 1));
                rank10 = chart.Ranks[Convert.ToInt32(ability.Ranks) + rank10Offset].TotalXp - chart.Ranks[Convert.ToInt32(ability.Ranks)].TotalXp;
            }
            else
            {
                rank10 = chart.Ranks[Convert.ToInt32(ability.Ranks) + 10].TotalXp - chart.Ranks[Convert.ToInt32(ability.Ranks)].TotalXp;
            }

            if (amount == rank1)
                rankUps = 1u;
            else if (amount == rank10)
            {
                if (rank10Offset > 0u)
                {
                    rankUps = Convert.ToUInt32(rank10Offset);
                }
                else
                {
                    rankUps = 10u;
                }
            }

            if (rankUps > 0)
            {
                // FIXME(ddevec):
                //      Really AddRank() should probably be a method of CreatureAbility/CreatureVital
                ability.Ranks += rankUps;
                ability.ExperienceSpent += amount;
                this.Character.SpendXp(amount);
                result = ability.ExperienceSpent;
                if (Fellowship != null)
                    Fellowship.SendStatUpdate(this);
            }

            return result;
        }

        /// <summary>
        /// Check a rank against the ability charts too determine if the skill is at max
        /// </summary>
        /// <returns>Returns true if ability is max rank; false if ability is below max rank</returns>
        private bool IsAbilityMaxRank(uint rank, bool isAbilityVitals)
        {
            ExperienceExpenditureChart xpChart = new ExperienceExpenditureChart();
            XpTable xpTable = XpTable.ReadFromDat();

            if (isAbilityVitals)
                xpChart = xpTable.VitalXpChart;
            else
                xpChart = xpTable.AbilityXpChart;

            if (rank == (xpChart.Ranks.Count - 1))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Check a rank against the skill charts too determine if the skill is at max
        /// </summary>
        /// <returns>Returns true if skill is max rank; false if skill is below max rank</returns>
        private bool IsSkillMaxRank(uint rank, SkillStatus status)
        {
            ExperienceExpenditureChart xpChart = new ExperienceExpenditureChart();
            XpTable xpTable = XpTable.ReadFromDat();

            if (status == SkillStatus.Trained)
                xpChart = xpTable.TrainedSkillXpChart;
            else if (status == SkillStatus.Specialized)
                xpChart = xpTable.SpecializedSkillXpChart;

            if (rank == (xpChart.Ranks.Count - 1))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Spend xp Skill ranks
        /// </summary>
        public void SpendXp(Skill skill, uint amount)
        {
            uint baseValue = 0;
            CreatureSkill creatureSkill = GetSkill(skill);
            uint result = SpendSkillXp(creatureSkill, amount);

            uint ranks = creatureSkill.Ranks;
            uint newValue = creatureSkill.UnbuffedValue;
            SkillStatus status = creatureSkill.Status;
            GameMessagePrivateUpdatePropertyInt64 xpUpdate = new GameMessagePrivateUpdatePropertyInt64(Session, PropertyInt64.AvailableExperience, Character.AvailableExperience);
            GameMessagePrivateUpdateSkill skillUpdate = new GameMessagePrivateUpdateSkill(Session, skill, status, ranks, baseValue, result);
            GameMessageSound soundEvent = new GameMessageSound(this.Guid, Sound.RaiseTrait, 1f);
            string messageText = "";

            if (result > 0u)
            {
                // if the skill ranks out at the top of our xp chart
                // then we will start fireworks effects and have special text!
                if (IsSkillMaxRank(ranks, status))
                {
                    // fireworks on rank up is 0x8D
                    PlayParticleEffect(Entity.Enum.PlayScript.WeddingBliss, Guid);
                    messageText = $"Your base {skill} is now {newValue} and has reached its upper limit!";
                }
                else
                {
                    messageText = $"Your base {skill} is now {newValue}!";
                }
            }
            else
            {
                messageText = $"Your attempt to raise {skill} has failed!";
            }
            GameMessageSystemChat message = new GameMessageSystemChat(messageText, ChatMessageType.Advancement);
            Session.Network.EnqueueSend(xpUpdate, skillUpdate, soundEvent, message);
        }

        public override void DoOnKill(Session killerSession)
        {
            // First do on-kill
            OnKill(killerSession);
            // Then get onKill from our parent
            ActionChain killChain = base.OnKillInternal(killerSession);

            // Send the teleport out after we animate death
            killChain.AddAction(this, () =>
            {
                // teleport to sanctuary or best location
                Position newPosition = PositionSanctuary ?? PositionLastPortal ?? Location;

                // Enqueue a teleport action, followed by Stand-up
                // Queue the teleport to lifestone
                ActionChain teleportChain = GetTeleportChain(newPosition);

                teleportChain.AddAction(this, () =>
                {
                    // Regenerate/ressurect?
                    UpdateVitalInternal(Health, 5);

                    // Stand back up
                    DoMotion(new UniversalMotion(MotionStance.Standing));

                    // add a Corpse at the current location via the ActionQueue to honor the motion and teleport delays
                    // QueuedGameAction addCorpse = new QueuedGameAction(this.Guid.Full, corpse, true, GameActionType.ObjectCreate);
                    // AddToActionQueue(addCorpse);
                    // If the player is outside of the landblock we just died in, then reboadcast the death for
                    // the players at the lifestone.
                    if (Positions.ContainsKey(PositionType.LastOutsideDeath) && Positions[PositionType.LastOutsideDeath].Cell != newPosition.Cell)
                    {
                        string currentDeathMessage = $"died to {killerSession.Player.Name}.";
                        ActionBroadcastKill($"{Name} has {currentDeathMessage}", Guid, killerSession.Player.Guid);
                    }
                });
                teleportChain.EnqueueChain();
            });
            killChain.EnqueueChain();
        }

        /// <summary>
        /// Player Death/Kill, use this to kill a session's player
        /// </summary>
        /// <remarks>
        ///     TODO:
        ///         1. Find the best vitae formula and add vitae
        ///         2. Generate the correct death message, or have it passed in as a parameter.
        ///         3. Find the correct player death noise based on the player model and play on death.
        ///         4. Determine if we need to Send Queued Action for Lifestone Materialize, after Action Location.
        ///         5. Find the health after death formula and Set the correct health
        /// </remarks>
        private void OnKill(Session killerSession)
        {
            ObjectGuid killerId = killerSession.Player.Guid;

            IsAlive = false;
            Health.Current = 0; // Set the health to zero
            Character.NumDeaths++; // Increase the NumDeaths counter
            Character.DeathLevel++; // Increase the DeathLevel

            // TODO: Find correct vitae formula/value
            Character.VitaeCpPool = 0; // Set vitae

            // TODO: Generate a death message based on the damage type to pass in to each death message:
            string currentDeathMessage = $"died to {killerSession.Player.Name}.";

            // Send Vicitim Notification, or "Your Death" event to the client:
            // create and send the client death event, GameEventYourDeath
            GameEventYourDeath msgYourDeath = new GameEventYourDeath(Session, $"You have {currentDeathMessage}");
            GameMessagePrivateUpdateAttribute2ndLevel msgHealthUpdate = new GameMessagePrivateUpdateAttribute2ndLevel(Session, Vital.Health, Health.Current);
            GameMessagePrivateUpdatePropertyInt msgNumDeaths = new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.NumDeaths, Character.NumDeaths);
            GameMessagePrivateUpdatePropertyInt msgDeathLevel = new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.DeathLevel, Character.DeathLevel);
            GameMessagePrivateUpdatePropertyInt msgVitaeCpPool = new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.VitaeCpPool, Character.VitaeCpPool);
            GameEventPurgeAllEnchantments msgPurgeEnchantments = new GameEventPurgeAllEnchantments(Session);
            // var msgDeathSound = new GameMessageSound(Guid, Sound.Death1, 1.0f);

            // Send first death message group
            Session.Network.EnqueueSend(msgHealthUpdate, msgYourDeath, msgNumDeaths, msgDeathLevel, msgVitaeCpPool, msgPurgeEnchantments);

            // Broadcast the 019E: Player Killed GameMessage
            ActionBroadcastKill($"{Name} has {currentDeathMessage}", Guid, killerId);
        }

        public void HandleActionExamination(ObjectGuid examinationId)
        {
            // TODO: Throttle this request?. The live servers did this, likely for a very good reason, so we should, too.
            ActionChain examineChain = new ActionChain();

            if (examinationId.Full == 0)
            {
                // Deselect the formerly selected Target
                // selectedTarget = ObjectGuid.Invalid;
                return;
            }

            // The object can be in two spots... on the player or on the landblock
            // First check the player
            examineChain.AddAction(this, () =>
            {
                // search packs
                WorldObject wo = GetInventoryItem(examinationId);

                // search wielded items
                if (wo == null)
                    wo = GetWieldedItem(examinationId);

                // search interactive objects
                if (wo == null)
                {
                    if (interactiveWorldObjects.ContainsKey(examinationId))
                        wo = interactiveWorldObjects[examinationId];
                }

                // if its local examine it
                if (wo != null)
                {
                    wo.Examine(Session);
                }
                else
                {
                    // examine item on land block
                    CurrentLandblock.GetObject(examinationId).Examine(Session);
                }
            });
            examineChain.EnqueueChain();
        }

        public void HandleActionQueryHealth(ObjectGuid queryId)
        {
            ActionChain chain = new ActionChain();
            chain.AddAction(this, () =>
            {
                if (queryId.Full == 0)
                {
                    // Deselect the formerly selected Target
                    selectedTarget = ObjectGuid.Invalid;
                    return;
                }

                // Remember the selected Target
                selectedTarget = queryId;
                CurrentLandblock.GetObject(queryId).QueryHealth(Session);
            });
            chain.EnqueueChain();
        }
        public void HandleActionQueryItemMana(ObjectGuid queryId)
        {
            if (queryId.Full == 0)
            {
                // Do nothing if the queryID is 0
                return;
            }

            ActionChain chain = new ActionChain();
            chain.AddAction(this, () =>
            {
                // the object could be in the world or on the player, first check player
                WorldObject wo = GetInventoryItem(queryId);
                if (wo != null)
                {
                    wo.QueryItemMana(Session);
                }

                else
                {
                    // We could be wielded - let's check that next.
                    if (WieldedObjects.TryGetValue(queryId, out wo))
                    {
                        wo.QueryItemMana(Session);
                    }
                    else
                    {
                        // todo replace with interactive items.... this creates crashing!
                        // ActionChain idChain = new ActionChain();
                        // CurrentLandblock.ChainOnObject(idChain, queryId, (WorldObject cwo) =>
                        // {
                        //    cwo.QueryItemMana(Session);
                        // });
                        // idChain.EnqueueChain();
                    }
                }
            });
            chain.EnqueueChain();
        }

        public void HandleActionReadBookPage(ObjectGuid bookId, uint pageNum)
        {
            // TODO: Do we want to throttle this request, like appraisals?
            ActionChain bookChain = new ActionChain();

            // The object can be in two spots... on the player or on the landblock
            // First check the player
            bookChain.AddAction(this, () =>
            {
                WorldObject wo = GetInventoryItem(bookId);
                // book is in the player's inventory...
                if (wo != null)
                {
                    wo.ReadBookPage(Session, pageNum);
                }
                else
                {
                    CurrentLandblock.GetObject(bookId).ReadBookPage(Session, pageNum);
                }
            });
            bookChain.EnqueueChain();
        }

        #region VendorTransactions

        /// <summary>
        /// Sends updated network packets to client / vendor item list.
        /// </summary>
        public void HandleActionApproachVendor(Vendor vendor, List<WorldObject> itemsForSale)
        {
            new ActionChain(this, () =>
            {
                Session.Network.EnqueueSend(new GameEventApproachVendor(Session, vendor, itemsForSale));
                SendUseDoneEvent();
            }).EnqueueChain();
        }

        /// <summary>
        /// Fired from the client / client is sending us a Buy transaction to vendor
        /// </summary>
        /// <param name="vendorId"></param>
        /// <param name="items"></param>
        public void HandleActionBuy(ObjectGuid vendorId, List<ItemProfile> items)
        {
            new ActionChain(this, () =>
            {
                Vendor vendor = (CurrentLandblock.GetObject(vendorId) as Vendor);
                vendor.BuyValidateTransaction(vendorId, items, this);
            }).EnqueueChain();
        }

        /// <summary>
        /// Vendor has validated the transactions and sent a list of items for processing.
        /// </summary>
        /// <param name="vendor"></param>
        /// <param name="purchaselist"></param>
        /// <param name="valid"></param>
        /// <param name="goldcost"></param>
        public void FinalizeBuyTransaction(Vendor vendor, List<WorldObject> uqlist, List<WorldObject> genlist, bool valid, uint goldcost)
        {
            // todo research packets more for both buy and sell. ripley thinks buy is update..
            // vendor accepted the transaction
            if (valid)
            {
                if (SpendCurrency(goldcost, WeenieType.Coin))
                {
                    foreach (WorldObject wo in uqlist)
                    {
                        wo.ContainerId = Guid.Full;
                        wo.Placement = 0;
                        AddToInventory(wo);
                        Session.Network.EnqueueSend(new GameMessageCreateObject(wo));
                        Session.Network.EnqueueSend(new GameMessagePutObjectInContainer(Session, Guid, wo, 0));
                        Session.Network.EnqueueSend(new GameMessageUpdateInstanceId(Guid, wo.Guid, PropertyInstanceId.Container));
                    }
                    HandleAddNewWorldObjectsToInventory(genlist);
                }
                else // not enough cash.
                {
                    valid = false;
                }
            }

            vendor.BuyItemsFinalTransaction(this, uqlist, valid);
        }

        /// <summary>
        /// Client Calls this when Sell is clicked.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="vendorId"></param>
        public void HandleActionSell(List<ItemProfile> itemprofiles, ObjectGuid vendorId)
        {
            ActionChain chain = new ActionChain();

            chain.AddAction(this, () =>
            {
                List<WorldObject> purchaselist = new List<WorldObject>();
                foreach (ItemProfile profile in itemprofiles)
                {
                    // check packs of item.
                    WorldObject item = GetInventoryItem(profile.Guid);
                    if (item == null)
                    {
                        // check to see if this item is wielded
                        item = GetWieldedItem(profile.Guid);
                        if (item != null)
                        {
                            RemoveFromWieldedObjects(item.Guid);
                            UpdateAppearance(this);
                            Session.Network.EnqueueSend(
                               new GameMessageSound(Guid, Sound.WieldObject, (float)1.0),
                               new GameMessageObjDescEvent(this),
                               new GameMessageUpdateInstanceId(Guid, new ObjectGuid(0), PropertyInstanceId.Wielder),
                               new GameMessagePublicUpdatePropertyInt(Sequences, item.Guid, PropertyInt.CurrentWieldedLocation, 0));
                        }
                    }
                    else
                    {
                        // remove item from inventory.
                        RemoveWorldObjectFromInventory(profile.Guid);
                    }

                    Session.Network.EnqueueSend(new GameMessageUpdateInstanceId(profile.Guid, new ObjectGuid(0), PropertyInstanceId.Container));

                    SetInventoryForVendor(item);

                    // clean up the shard database.
                    DatabaseManager.Shard.DeleteObject(item.SnapShotOfDataObject(), null);

                    Session.Network.EnqueueSend(new GameMessageRemoveObject(item));
                    purchaselist.Add(item);
                }

                Vendor vendor = CurrentLandblock.GetObject(vendorId) as Vendor;
                vendor.SellItemsValidateTransaction(this, purchaselist);
            });
            chain.EnqueueChain();
        }

        public void FinalizeSellTransaction(WorldObject vendor, bool valid, List<WorldObject> purchaselist, uint payout)
        {
            // pay player in voinds
            if (valid)
            {
                CreateCurrency(WeenieType.Coin, payout);
            }
        }
        #endregion

        public bool CreateCurrency(WeenieType type, uint amount)
        {
            // todo: we need to look up this object to understand it by its weenie id.
            // todo: support more then hard coded coin.
            const uint coinWeenieId = 273;
            WorldObject wochk = WorldObjectFactory.CreateNewWorldObject(coinWeenieId);
            ushort maxstacksize = wochk.MaxStackSize.Value;
            wochk = null;

            List<WorldObject> payout = new List<WorldObject>();

            while (amount > 0)
            {
                WorldObject currancystack = WorldObjectFactory.CreateNewWorldObject(coinWeenieId);
                // payment contains a max stack
                if (maxstacksize <= amount)
                {
                    currancystack.StackSize = maxstacksize;
                    payout.Add(currancystack);
                    amount = amount - maxstacksize;
                }
                else // not a full stack
                {
                    currancystack.StackSize = (ushort)amount;
                    payout.Add(currancystack);
                    amount = amount - amount;
                }
            }

            // add money to player inventory.
            foreach (WorldObject wo in payout)
            {
                HandleAddNewWorldObjectToInventory(wo);
            }
            UpdateCurrencyClientCalculations(WeenieType.Coin);
            return true;
        }

        // todo re-think how this works..
        private void UpdateCurrencyClientCalculations(WeenieType type)
        {
            int coins = 0;
            List<WorldObject> currency = new List<WorldObject>();
            currency.AddRange(GetInventoryItemsOfTypeWeenieType(type));
            foreach (WorldObject wo in currency)
            {
                if (wo.WeenieType == WeenieType.Coin)
                    coins += wo.StackSize.Value;
            }
            // send packet to client letthing them know
            CoinValue = coins;
        }

        private bool SpendCurrency(uint amount, WeenieType type)
        {
            if (CoinValue - amount >= 0)
            {
                List<WorldObject> currency = new List<WorldObject>();
                currency.AddRange(GetInventoryItemsOfTypeWeenieType(type));
                currency = currency.OrderBy(o => o.Value).ToList();

                List<WorldObject> cost = new List<WorldObject>();
                uint payment = 0;

                WorldObject changeobj = WorldObjectFactory.CreateNewWorldObject(273);
                uint change = 0;

                foreach (WorldObject wo in currency)
                {
                    if (payment + wo.StackSize.Value <= amount)
                    {
                        // add to payment
                        payment = payment + wo.StackSize.Value;
                        cost.Add(wo);
                    }
                    else if (payment + wo.StackSize.Value > amount)
                    {
                        // add payment
                        payment = payment + wo.StackSize.Value;
                        cost.Add(wo);
                        // calculate change
                        if (payment > amount)
                        {
                            change = payment - amount;
                            // add new change object.
                            changeobj.StackSize = (ushort)change;
                        }
                        break;
                    }
                    else if (payment == amount)
                        break;
                }

                // destroy all stacks of currency required / sale
                foreach (WorldObject wo in cost)
                {
                    RemoveWorldObjectFromInventory(wo.Guid);
                    ObjectGuid clearContainer = new ObjectGuid(0);
                    Session.Network.EnqueueSend(new GameMessageUpdateInstanceId(wo.Guid, clearContainer, PropertyInstanceId.Container));

                    // clean up the shard database.
                    DatabaseManager.Shard.DeleteObject(wo.SnapShotOfDataObject(), null);
                    Session.Network.EnqueueSend(new GameMessageRemoveObject(wo));
                }

                // if there is change - readd - do this at the end to try to prevent exploiting
                if (change > 0)
                {
                    HandleAddNewWorldObjectToInventory(changeobj);
                }

                UpdateCurrencyClientCalculations(WeenieType.Coin);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Call this to add any new World Objects to inventory
        /// </summary>
        /// <param name="wo"></param>
        public void HandleAddNewWorldObjectsToInventory(List<WorldObject> wolist)
        {
            foreach (WorldObject wo in wolist)
            {
                HandleAddNewWorldObjectToInventory(wo);
            }
        }

        /// <summary>
        /// Add New WorldObject to Inventory
        /// </summary>
        /// <param name="wo"></param>
        public void HandleAddNewWorldObjectToInventory(WorldObject wo)
        {
            // Get Next Avalibale Pack Location.
            // uint packid = GetCreatureInventoryFreePack();

            // default player until I get above code to work!
            uint packid = Guid.Full;

            if (packid != 0)
            {
                wo.ContainerId = packid;
                AddToInventory(wo);
                Session.Network.EnqueueSend(new GameMessageCreateObject(wo));
                if (wo.WeenieType == WeenieType.Container)
                    Session.Network.EnqueueSend(new GameEventViewContents(Session, wo.SnapShotOfDataObject()));
            }
        }

        /// <summary>
        /// Adds a new object to the 's inventory of the specified weenie class.  intended use case: giving items to players
        /// while they are plaplayerying.  this calls all the necessary helper functions to have the item be tracked and sent to the client.
        /// </summary>
        /// <returns>the object created</returns>
        public WorldObject AddNewItemToInventory(uint weenieClassId)
        {
            WorldObject wo = Factories.WorldObjectFactory.CreateNewWorldObject(weenieClassId);
            wo.ContainerId = Guid.Full;
            wo.Placement = 0;
            AddToInventory(wo);
            TrackObject(wo);
            return wo;
        }

        /// <summary>
        /// Sends a death message broadcast all players on the landblock? that a killer has a victim
        /// </summary>
        /// <remarks>
        /// TODO:
        ///     1. Figure out who all receives death messages, on what landblock and at what order -
        ///         Example: Does the players around the victim receive the message or do the players at the lifestone receieve the message, or both?
        /// </remarks>
        /// <param name="deathMessage">What are we going to say about this death</param>
        /// <param name="victimId">Who was killed?</param>
        /// <param name="killerId">Who did the killing</param>
        public void ActionBroadcastKill(string deathMessage, ObjectGuid victimId, ObjectGuid killerId)
        {
            GameMessagePlayerKilled deathBroadcast = new GameMessagePlayerKilled(deathMessage, victimId, killerId);
            CurrentLandblock.EnqueueBroadcast(Location, Landblock.OutdoorChatRange, deathBroadcast);
        }

        // Play a sound
        public void PlaySound(Sound sound, ObjectGuid targetId)
        {
            Session.Network.EnqueueSend(new GameMessageSound(targetId, sound, 1f));
        }

        // plays particle effect like spell casting or bleed etc..
        public void PlayParticleEffect(PlayScript effectId, ObjectGuid targetId, float scale = 1f)
        {
            if (CurrentLandblock != null)
            {
                GameMessageScript effectEvent = new GameMessageScript(targetId, effectId, scale);
                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, effectEvent);
            }
        }

        /// <summary>
        /// spends the xp on this skill.
        /// </summary>
        /// <remarks>
        ///     Known Issues:
        ///         1. Not checking and accounting for XP gained from skill usage.
        /// </remarks>
        /// <returns>0 if it failed, total investment of the next rank if successful</returns>
        private uint SpendSkillXp(CreatureSkill skill, uint amount)
        {
            uint result = 0u;
            ExperienceExpenditureChart chart;
            XpTable xpTable = XpTable.ReadFromDat();

            if (skill.Status == SkillStatus.Trained)
                chart = xpTable.TrainedSkillXpChart;
            else if (skill.Status == SkillStatus.Specialized)
                chart = xpTable.SpecializedSkillXpChart;
            else
                return result;

            // do not advance if we cannot spend xp to rank up our skill by 1 point
            if (skill.Ranks >= (chart.Ranks.Count - 1))
                return result;

            uint rankUps = 0u;
            uint currentXp = chart.Ranks[Convert.ToInt32(skill.Ranks)].TotalXp;
            uint rank1 = chart.Ranks[Convert.ToInt32(skill.Ranks) + 1].XpFromPreviousRank;
            uint rank10 = 0u;
            int rank10Offset = 0;

            if (skill.Ranks + 10 >= (chart.Ranks.Count))
            {
                rank10Offset = 10 - (Convert.ToInt32(skill.Ranks + 10) - (chart.Ranks.Count - 1));
                rank10 = chart.Ranks[Convert.ToInt32(skill.Ranks) + rank10Offset].TotalXp - chart.Ranks[Convert.ToInt32(skill.Ranks)].TotalXp;
            }
            else
            {
                rank10 = chart.Ranks[Convert.ToInt32(skill.Ranks) + 10].TotalXp - chart.Ranks[Convert.ToInt32(skill.Ranks)].TotalXp;
            }

            if (amount == rank1)
                rankUps = 1u;
            else if (amount == rank10)
            {
                if (rank10Offset > 0u)
                {
                    rankUps = Convert.ToUInt32(rank10Offset);
                }
                else
                {
                    rankUps = 10u;
                }
            }

            if (rankUps > 0)
            {
                skill.Ranks += rankUps;
                skill.ExperienceSpent += amount;
                this.Character.SpendXp(amount);
                result = skill.ExperienceSpent;
            }

            return result;
        }

        /// <summary>
        /// Will send out GameEventFriendsListUpdate packets to everyone online that has this player as a friend.
        /// </summary>
        private void SendFriendStatusUpdates()
        {
            List<Session> inverseFriends = WorldManager.FindInverseFriends(Guid);

            if (inverseFriends.Count > 0)
            {
                Friend playerFriend = new Friend();
                playerFriend.Id = Guid;
                playerFriend.Name = Name;
                foreach (Session friendSession in inverseFriends)
                {
                    friendSession.Network.EnqueueSend(new GameEventFriendsListUpdate(friendSession, GameEventFriendsListUpdate.FriendsUpdateTypeFlag.FriendStatusChanged, playerFriend, true, GetVirtualOnlineStatus()));
                }
            }
        }

        /// <summary>
        /// Adds a friend and updates the database.
        /// </summary>
        /// <param name="friendName">The name of the friend that is being added.</param>
        public void AddFriend(string friendName)
        {
            if (string.Equals(friendName, Name, StringComparison.CurrentCultureIgnoreCase))
                ChatPacket.SendServerMessage(Session, "Sorry, but you can't be friends with yourself.", ChatMessageType.Broadcast);

            // Check if friend exists
            if (Character.Friends.SingleOrDefault(f => string.Equals(f.Name, friendName, StringComparison.CurrentCultureIgnoreCase)) != null)
                ChatPacket.SendServerMessage(Session, "That character is already in your friends list", ChatMessageType.Broadcast);

            // TODO: check if player is online first to avoid database hit??
            // Get character record from DB
            DatabaseManager.Shard.GetObjectInfoByName(friendName, ((ObjectInfo friendInfo) =>
            {
                if (friendInfo == null)
                {
                    ChatPacket.SendServerMessage(Session, "That character does not exist", ChatMessageType.Broadcast);
                    return;
                }

                Friend newFriend = new Friend();
                newFriend.Name = friendInfo.Name;
                newFriend.Id = new ObjectGuid(friendInfo.Guid);

                // Save to DB, assume success
                DatabaseManager.Shard.AddFriend(Guid.Low, newFriend.Id.Low, (() =>
                {
                    // Add to character object
                    Character.AddFriend(newFriend);

                    // Send packet
                    Session.Network.EnqueueSend(new GameEventFriendsListUpdate(Session, GameEventFriendsListUpdate.FriendsUpdateTypeFlag.FriendAdded, newFriend));
                }));
            }));
        }

        /// <summary>
        /// Remove a single friend and update the database.
        /// </summary>
        /// <param name="friendId">The ObjectGuid of the friend that is being removed</param>
        public void RemoveFriend(ObjectGuid friendId)
        {
            Friend friendToRemove = Character.Friends.SingleOrDefault(f => f.Id.Low == friendId.Low);

            // Not in friend list
            if (friendToRemove == null)
            {
                ChatPacket.SendServerMessage(Session, "That character is not in your friends list!", ChatMessageType.Broadcast);
                return;
            }

            // Remove from DB
            DatabaseManager.Shard.DeleteFriend(Guid.Low, friendId.Low, (() =>
            {
                // Remove from character object
                Character.RemoveFriend(friendId.Low);

                // Send packet
                Session.Network.EnqueueSend(new GameEventFriendsListUpdate(Session, GameEventFriendsListUpdate.FriendsUpdateTypeFlag.FriendRemoved, friendToRemove));
            }));
        }

        /// <summary>
        /// Delete all friends and update the database.
        /// </summary>
        public void RemoveAllFriends()
        {
            // Remove all from DB
            DatabaseManager.Shard.RemoveAllFriends(Guid.Low, null);

            // Remove from character object
            Character.RemoveAllFriends();
        }

        /// <summary>
        /// Set the AppearOffline option to the provided value.  It will also send out an update to all online clients that have this player as a friend. This option does not save to the database.
        /// </summary>
        public void AppearOffline(bool appearOffline)
        {
            SetCharacterOption(CharacterOption.AppearOffline, appearOffline);
            SendFriendStatusUpdates();
        }

        /// <summary>
        /// Set a single character option to the provided value. This does not save to the database.
        /// </summary>
        public void SetCharacterOption(CharacterOption option, bool value)
        {
            Character.SetCharacterOption(option, value);
        }

        public bool GetCharacterOption(CharacterOption option)
        {
            return Character.GetCharacterOption(option);
        }

        /// <summary>
        /// Set the currently position of the character, to later save in the database.
        /// </summary>
        public void SetPhysicalCharacterPosition()
        {
            // Saves the current player position after converting from a Position Object, to a CharacterPosition object
            SetCharacterPosition(PositionType.Location, Session.Player.Location);
        }

        /// <summary>
        /// Saves a CharacterPosition to the character position dictionary
        /// </summary>
        public void SetCharacterPosition(PositionType type, Position newPosition)
        {
            // reset the landblock id
            if (newPosition.LandblockId.Landblock == 0 && newPosition.Cell > 0)
            {
                newPosition.LandblockId = new LandblockId(newPosition.Cell);
            }

            Positions[type] = newPosition;
        }

        // Just preps the character to save
        public void HandleActionSaveCharacter()
        {
            GetSaveChain().EnqueueChain();
        }

        // Gets the ActionChain to save a character
        public ActionChain GetSaveChain()
        {
            return new ActionChain(this, SaveCharacter);
        }

        /// <summary>
        /// This method is used to clear the wielded items list ( the list of objects used to save wielded items ) and loads it with a snapshot
        /// of the WorldObjects from the current list of wielded world objects. Og II
        /// </summary>
        public void SnapshotWieldedItems(bool clearDirtyFlags = false)
        {
            WieldedItems.Clear();
            foreach (KeyValuePair<ObjectGuid, WorldObject> wo in WieldedObjects)
            {
                WieldedItems.Add(wo.Value.Guid, wo.Value.SnapShotOfDataObject(clearDirtyFlags));
            }
        }

        /// <summary>
        /// This method is used to clear the inventory lists of all containers. ( the list of objects used to save inventory items items ) and loads each with a snapshot
        /// of the WorldObjects from the current list of inventory world objects by container. Og II
        /// </summary>
        public void SnapshotInventoryItems(bool clearDirtyFlags = false)
        {
            Inventory.Clear();
            foreach (KeyValuePair<ObjectGuid, WorldObject> wo in InventoryObjects)
            {
                Inventory.Add(wo.Value.Guid, wo.Value.SnapShotOfDataObject(clearDirtyFlags));
                if (wo.Value.WeenieType == WeenieType.Container)
                {
                    wo.Value.Inventory.Clear();
                    foreach (KeyValuePair<ObjectGuid, WorldObject> item in wo.Value.InventoryObjects)
                    {
                        wo.Value.Inventory.Add(item.Value.Guid, item.Value.SnapShotOfDataObject(clearDirtyFlags));
                    }
                }
            }
        }

        public void SnapShotTrackedContracts()
        {
            foreach (KeyValuePair<uint, ContractTracker> tc in TrackedContracts)
            {
                DataObject.SetTrackedContract(tc.Key, tc.Value.SnapShotOfDataContractTracker());
            }
        }

        /// <summary>
        /// Internal save character functionality
        /// Saves the character to the persistent database. Includes Stats, Position, Skills, etc.
        /// </summary>
        private void SaveCharacter()
        {
            if (Character != null)
            {
                // Save the current position to persistent storage, only during the server update interval
                SetPhysicalCharacterPosition();

                // Let's get a snapshot of our object lists prior to save.
                SnapshotWieldedItems();
                SnapshotInventoryItems();
                SnapShotTrackedContracts();

                DatabaseManager.Shard.SaveObject(GetSavableCharacter(), null);

                DataObject.ClearDirtyFlags();

                // FIXME : the issue is here - I still have the inventory in two dictionaries after clone.   I am missing something Og II
#if DEBUG
                if (Session.Player != null)
                {
                    Session.Network.EnqueueSend(new GameMessageSystemChat($"{Session.Player.Name} has been saved.", ChatMessageType.Broadcast));
                }
#endif
            }
        }

        public void UpdateAge()
        {
            if (Character != null)
                Character.Age++;
        }

        public void SendAgeInt()
        {
            try
            {
                Session.Network.EnqueueSend(new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.Age, Character.Age));
            }
            catch (NullReferenceException)
            {
                // Do Nothing since player data hasn't loaded in
            }
        }

        /// <summary>
        /// Returns false if the player has chosen to Appear Offline.  Otherwise it will return their actual online status.
        /// </summary>
        public bool GetVirtualOnlineStatus()
        {
            if (Character.CharacterOptions[CharacterOption.AppearOffline] == true)
                return false;

            return IsOnline;
        }

        private void SendSelf()
        {
            GameEventPlayerDescription player = new GameEventPlayerDescription(Session);
            GameEventCharacterTitle title = new GameEventCharacterTitle(Session);
            GameEventFriendsListUpdate friends = new GameEventFriendsListUpdate(Session);

            Session.Network.EnqueueSend(player, title, friends);

            SetChildren();

            Session.Network.EnqueueSend(new GameMessagePlayerCreate(Guid),
                                        new GameMessageCreateObject(this));

            SendInventoryAndWieldedItems(Session);

            SendContractTrackerTable();
        }

        public void Teleport(Position newPosition)
        {
            ActionChain chain = GetTeleportChain(newPosition);
            chain.EnqueueChain();
        }

        private ActionChain GetTeleportChain(Position newPosition)
        {
            ActionChain teleportChain = new ActionChain();

            teleportChain.AddAction(this, () => TeleportInternal(newPosition));

            teleportChain.AddDelaySeconds(3);
            // Once back in world we can start listening to the game's request for positions
            teleportChain.AddAction(this, () =>
            {
                InWorld = true;
            });

            return teleportChain;
        }

        private void TeleportInternal(Position newPosition)
        {
            if (!InWorld)
                return;

            Hidden = true;
            IgnoreCollision = true;
            ReportCollision = false;
            EnqueueBroadcastPhysicsState();
            ExternalUpdatePosition(newPosition);
            InWorld = false;

            Session.Network.EnqueueSend(new GameMessagePlayerTeleport(this));

            lock (clientObjectList)
            {
                clientObjectList.Clear();
            }
        }

        public void RequestUpdatePosition(Position pos)
        {
            new ActionChain(this, () => ExternalUpdatePosition(pos)).EnqueueChain();
        }

        public void RequestUpdateMotion(HoldKey holdKey, MovementData md, MotionItem[] commands)
        {
            // Update our current style
            if ((md.MovementStateFlag & MovementStateFlag.CurrentStyle) != 0)
            {
                MotionStance newStance = (MotionStance)md.CurrentStyle;
                if (newStance != Stance)
                {
                    Stance = (MotionStance)md.CurrentStyle;
                }
            }

            CurrentMovement = md = md.ConvertToClientAccepted(holdKey, GetSkill(Skill.Run));

            UniversalMotion newMotion = new UniversalMotion(Stance, md);

            // This is a hack to make walking work correctly.   Og II
            if (holdKey != 0 || (md.ForwardCommand == (ushort)((uint)MotionCommand.WalkForward & 0xFFFF)))
                newMotion.IsAutonomous = true;

            newMotion.Commands.AddRange(commands);
            CurrentMotion = newMotion;
        }

        private void ExternalUpdatePosition(Position newPosition)
        {
            if (InWorld)
            {
                PrepUpdatePosition(newPosition);
            }
        }

        public void SetTitle(uint title)
        {
            GameEventUpdateTitle updateTitle = new GameEventUpdateTitle(Session, title);
            GameMessageSystemChat message = new GameMessageSystemChat($"Your title is now {title}!", ChatMessageType.Broadcast);
            Session.Network.EnqueueSend(updateTitle, message);
        }

        /// <summary>
        /// returns a list of the ObjectGuids of all known creatures
        /// </summary>
        private List<ObjectGuid> GetKnownCreatures()
        {
            lock (clientObjectList)
            {
                return (List<ObjectGuid>)clientObjectList.Select(x => x.Key).Where(o => o.IsCreature()).ToList();
            }
        }

        /// <summary>
        /// Tracks Interactive world object you are have interacted with recently.  this should be
        /// called from the context of an action chain being executed by the landblock loop.
        /// </summary>
        public void TrackInteractiveObjects(List<WorldObject> worldObjects)
        {
            // todo: figure out a way to expire objects.. objects clearly not in range of interaction /etc
            foreach (WorldObject wo in worldObjects)
            {
                if (interactiveWorldObjects.ContainsKey(wo.Guid))
                    interactiveWorldObjects[wo.Guid] = wo;
                else
                    interactiveWorldObjects.Add(wo.Guid, wo);
            }
        }

        /// <summary>
        /// forces either an update or a create object to be sent to the client
        /// </summary>
        public void TrackObject(WorldObject worldObject)
        {
            bool sendUpdate = true;

            if (worldObject.Guid == this.Guid)
                return;

            lock (clientObjectList)
            {
                sendUpdate = clientObjectList.ContainsKey(worldObject.Guid);

                if (!sendUpdate)
                {
                    clientObjectList.Add(worldObject.Guid, WorldManager.PortalYearTicks);
                    worldObject.PlayScript(this.Session);
                }
                else
                {
                    clientObjectList[worldObject.Guid] = WorldManager.PortalYearTicks;
                }
            }

            log.Debug($"Telling {Name} about {worldObject.Name} - {worldObject.Guid.Full:X}");

            if (sendUpdate)
            {
                Session.Network.EnqueueSend(new GameMessageUpdateObject(worldObject));
                // TODO: Better handling of sending updates to client. The above line is causing much more problems then it is solving until we get proper movement.
                // Add this or something else back in when we handle movement better, until then, just send the create object once and move on.
            }
            else
            {
                Session.Network.EnqueueSend(new GameMessageCreateObject(worldObject));
                if (worldObject.DefaultScriptId != null)
                    Session.Network.EnqueueSend(new GameMessageScript(worldObject.Guid, (PlayScript)worldObject.DefaultScriptId));
            }
        }

        public void HandleActionLogout(bool clientSessionTerminatedAbruptly = false)
        {
            GetLogoutChain().EnqueueChain();
        }

        public ActionChain GetLogoutChain(bool clientSessionTerminatedAbruptly = false)
        {
            ActionChain logoutChain = new ActionChain(this, () => LogoutInternal(clientSessionTerminatedAbruptly));

            Debug.Assert(MotionTableId != null, "MotionTableId != null");
            float logoutAnimationLength = MotionTable.GetAnimationLength((uint)MotionTableId, MotionCommand.LogOut);
            logoutChain.AddDelaySeconds(logoutAnimationLength);

            // remove the player from landblock management -- after the animation has run
            logoutChain.AddChain(CurrentLandblock.GetRemoveWorldObjectChain(Guid, false));

            return logoutChain;
        }

        /// <summary>
        /// Do the player log out work.<para />
        /// If you want to force a player to logout, use Session.LogOffPlayer().
        /// </summary>
        private void LogoutInternal(bool clientSessionTerminatedAbruptly)
        {
            if (Fellowship != null)
                Fellowship.QuitFellowship(this, false);
            if (!IsOnline)
                return;

            InWorld = false;
            IsOnline = false;

            SendFriendStatusUpdates();

            if (!clientSessionTerminatedAbruptly)
            {
                UniversalMotion logout = new UniversalMotion(MotionStance.Standing, new MotionItem(MotionCommand.LogOut));
                CurrentLandblock.EnqueueBroadcastMotion(this, logout);

                EnqueueBroadcastPhysicsState();

                // Thie retail server sends a ChatRoomTracker 0x0295 first, then the status message, 0x028B. It does them one at a time for each individual channel.
                // The ChatRoomTracker message doesn't seem to change at all.
                // For the purpose of DerethForever, we simplify this process.
                GameEventDisplayParameterizedStatusMessage general = new GameEventDisplayParameterizedStatusMessage(Session, StatusMessageType2.YouHaveLeftThe_Channel, "General");
                GameEventDisplayParameterizedStatusMessage trade = new GameEventDisplayParameterizedStatusMessage(Session, StatusMessageType2.YouHaveLeftThe_Channel, "Trade");
                GameEventDisplayParameterizedStatusMessage lfg = new GameEventDisplayParameterizedStatusMessage(Session, StatusMessageType2.YouHaveLeftThe_Channel, "LFG");
                GameEventDisplayParameterizedStatusMessage roleplay = new GameEventDisplayParameterizedStatusMessage(Session, StatusMessageType2.YouHaveLeftThe_Channel, "Roleplay");
                Session.Network.EnqueueSend(general, trade, lfg, roleplay);
            }
        }

        public void StopTrackingObject(WorldObject worldObject, bool remove)
        {
            bool sendUpdate = true;
            lock (clientObjectList)
            {
                sendUpdate = clientObjectList.ContainsKey(worldObject.Guid);
                if (sendUpdate)
                {
                    clientObjectList.Remove(worldObject.Guid);
                }
            }

            // Don't remove it if it went into our inventory...
            if (sendUpdate && remove)
            {
                Session.Network.EnqueueSend(new GameMessageRemoveObject(worldObject));
            }
        }

        public void HandleMRT()
        {
            ActionChain mrtChain = new ActionChain();

            // Handle MRT Toggle internal must decide what to do next...
            mrtChain.AddAction(this, new ActionEventDelegate(() => HandleMRTToggleInternal()));

            mrtChain.EnqueueChain();
        }

        private void HandleMRTToggleInternal()
        {
            // This requires the Admin flag set on ObjectDescriptionFlags
            // I would expect this flag to be set in Admin.cs which would be a subclass of Player
            // FIXME: maybe move to Admin class?
            // TODO: reevaluate class location

            if (!ImmuneCellRestrictions)
                ImmuneCellRestrictions = true;
            else
                ImmuneCellRestrictions = false;

            // The EnqueueBroadcastUpdateObject below sends the player back into teleport. I assume at this point, this was never done to players
            // EnqueueBroadcastUpdateObject();

            // The private message below worked as expected, but it only broadcast to the player. This would be a problem with for others in range seeing something try to
            // pass through a barrier but not being allowed.
            // var updateBool = new GameMessagePrivateUpdatePropertyBool(Session, PropertyBool.IgnoreHouseBarriers, ImmuneCellRestrictions);
            // Session.Network.EnqueueSend(updateBool);

            CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessageUpdatePropertyBool(this, PropertyBool.IgnoreHouseBarriers, ImmuneCellRestrictions));

            Session.Network.EnqueueSend(new GameMessageSystemChat($"Bypass Housing Barriers now set to: {ImmuneCellRestrictions}", ChatMessageType.Broadcast));
        }

        public void SendAutonomousPosition()
        {
            // Session.Network.EnqueueSend(new GameMessageAutonomousPosition(this));
        }

        public void HandleActionTeleToPosition(PositionType position, Action onError = null)
        {
            GetTeleToPositionChain(position, onError).EnqueueChain();
        }

        public ActionChain GetTeleToPositionChain(PositionType position, Action onError)
        {
            ActionChain teleChain = new ActionChain();
            teleChain.AddAction(this, () =>
            {
                if (Positions.ContainsKey(position))
                {
                    Position dest = Positions[position];
                    Teleport(dest);
                }
                else
                {
                    if (onError != null)
                    {
                        onError();
                    }
                }
            });
            return teleChain;
        }

        public void HandleActionTeleToLifestone()
        {
            ActionChain lifestoneChain = new ActionChain();

            // Handle lifestone internal must decide what to do next...
            lifestoneChain.AddAction(this, new ActionEventDelegate(HandleActionTeleToLifestoneInternal));

            lifestoneChain.EnqueueChain();
        }

        private void HandleActionTeleToLifestoneInternal()
        {
            if (Positions.ContainsKey(PositionType.Sanctuary))
            {
                // FIXME(ddevec): I should probably make a better interface for this
                UpdateVitalInternal(Mana, Mana.Current / 2);

                if (CombatMode != CombatMode.NonCombat)
                {
                    GameMessagePrivateUpdatePropertyInt updateCombatMode = new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.CombatMode, (int)CombatMode.NonCombat);
                    Session.Network.EnqueueSend(updateCombatMode);
                }

                UniversalMotion motionLifestoneRecall = new UniversalMotion(MotionStance.Standing, new MotionItem(MotionCommand.LifestoneRecall));
                // TODO: This needs to be changed to broadcast sysChatMessage to only those in local chat hearing range
                CurrentLandblock.EnqueueBroadcastSystemChat(this, $"{Name} is recalling to the lifestone.", ChatMessageType.Recall);
                // FIX: Recall text isn't being broadcast yet, need to address
                DoMotion(motionLifestoneRecall);

                // Wait for animation
                ActionChain lifestoneChain = new ActionChain();
                Debug.Assert(MotionTableId != null, "MotionTableId != null");
                float lifestoneAnimationLength = MotionTable.GetAnimationLength((uint)MotionTableId, MotionCommand.LifestoneRecall);

                // Then do teleport
                lifestoneChain.AddDelaySeconds(lifestoneAnimationLength);
                lifestoneChain.AddChain(GetTeleportChain(Positions[PositionType.Sanctuary]));

                lifestoneChain.EnqueueChain();
            }
            else
            {
                ChatPacket.SendServerMessage(Session, "Your spirit has not been attuned to a sanctuary location.", ChatMessageType.Broadcast);
            }
        }

        public void HandleActionTeleToMarketplace()
        {
            ActionChain mpChain = new ActionChain();
            mpChain.AddAction(this, HandleActionTeleToMarketplaceInternal);

            // TODO: (OptimShi): Actual animation length is longer than in retail. 18.4s
            // float mpAnimationLength = MotionTable.GetAnimationLength((uint)MotionTableId, MotionCommand.MarketplaceRecall);
            // mpChain.AddDelaySeconds(mpAnimationLength);
            mpChain.AddDelaySeconds(14);

            // Then do teleport
            mpChain.AddChain(GetTeleportChain(MarketplaceDrop));

            // Set the chain to run
            mpChain.EnqueueChain();
        }

        private void HandleActionTeleToMarketplaceInternal()
        {
            string message = $"{Name} is recalling to the marketplace.";

            GameMessagePrivateUpdatePropertyInt updateCombatMode = new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.CombatMode, (int)CombatMode.NonCombat);

            UniversalMotion motionMarketplaceRecall = new UniversalMotion(MotionStance.Standing, new MotionItem(MotionCommand.MarketplaceRecall));

            GameMessageUpdateMotion animationEvent = new GameMessageUpdateMotion(Guid,
                                                             Sequences.GetCurrentSequence(SequenceType.ObjectInstance),
                                                             Sequences, motionMarketplaceRecall);

            // TODO: This needs to be changed to broadcast sysChatMessage to only those in local chat hearing range
            // FIX: Recall text isn't being broadcast yet, need to address
            CurrentLandblock.EnqueueBroadcastSystemChat(this, message, ChatMessageType.Recall);
            Session.Network.EnqueueSend(updateCombatMode);
            DoMotion(motionMarketplaceRecall);
        }

        public void HandleActionFinishBarber(ClientMessage message)
        {
            ActionChain chain = new ActionChain();
            chain.AddAction(this, () => DoFinishBarber(message));
            chain.EnqueueChain();
        }

        public void DoFinishBarber(ClientMessage message)
        {
            // Read the payload sent from the client...
            PaletteBaseId = message.Payload.ReadUInt32();
            Character.HeadObject = message.Payload.ReadUInt32();
            Character.HairTexture = message.Payload.ReadUInt32();
            Character.DefaultHairTexture = message.Payload.ReadUInt32();
            Character.EyesTexture = message.Payload.ReadUInt32();
            Character.DefaultEyesTexture = message.Payload.ReadUInt32();
            Character.NoseTexture = message.Payload.ReadUInt32();
            Character.DefaultNoseTexture = message.Payload.ReadUInt32();
            Character.MouthTexture = message.Payload.ReadUInt32();
            Character.DefaultMouthTexture = message.Payload.ReadUInt32();
            Character.SkinPalette = message.Payload.ReadUInt32();
            Character.HairPalette = message.Payload.ReadUInt32();
            Character.EyesPalette = message.Payload.ReadUInt32();
            Character.SetupTableId = message.Payload.ReadUInt32();

            uint option_bound = message.Payload.ReadUInt32(); // Supress Levitation - Empyrean Only
            uint option_unk = message.Payload.ReadUInt32(); // Unknown - Possibly set aside for future use?

            // Check if Character is Empyrean, and if we need to set/change/send new motion table
            if (Character.Heritage == 9)
            {
                // These are the motion tables for Empyrean float and not-float (one for each gender). They are hard-coded into the client.
                const uint EmpyreanMaleFloatMotionDID = 0x0900020Bu;
                const uint EmpyreanFemaleFloatMotionDID = 0x0900020Au;
                const uint EmpyreanMaleMotionDID = 0x0900020Eu;
                const uint EmpyreanFemaleMotionDID = 0x0900020Du;

                // Check for the Levitation option for Empyrean. Shadow crown and Undead flames are handled by client.
                if (Character.Gender == 1) // Male
                {
                    if (option_bound == 1 && Character.MotionTableId != EmpyreanMaleMotionDID)
                    {
                        Character.MotionTableId = EmpyreanMaleMotionDID;
                        Session.Network.EnqueueSend(new GameMessagePrivateUpdateDataID(Session, PropertyDataId.MotionTable, (uint)Character.MotionTableId));
                    }
                    else if (option_bound == 0 && Character.MotionTableId != EmpyreanMaleFloatMotionDID)
                    {
                        Character.MotionTableId = EmpyreanMaleFloatMotionDID;
                        Session.Network.EnqueueSend(new GameMessagePrivateUpdateDataID(Session, PropertyDataId.MotionTable, (uint)Character.MotionTableId));
                    }
                }
                else // Female
                {
                    if (option_bound == 1 && Character.MotionTableId != EmpyreanFemaleMotionDID)
                    {
                        Character.MotionTableId = EmpyreanFemaleMotionDID;
                        Session.Network.EnqueueSend(new GameMessagePrivateUpdateDataID(Session, PropertyDataId.MotionTable, (uint)Character.MotionTableId));
                    }
                    else if (option_bound == 0 && Character.MotionTableId != EmpyreanFemaleFloatMotionDID)
                    {
                        Character.MotionTableId = EmpyreanFemaleFloatMotionDID;
                        Session.Network.EnqueueSend(new GameMessagePrivateUpdateDataID(Session, PropertyDataId.MotionTable, (uint)Character.MotionTableId));
                    }
                }
            }

            UpdateAppearance(this);

            // Broadcast updated character appearance
            CurrentLandblock.EnqueueBroadcast(
                Location,
                Landblock.MaxObjectRange,
                new GameMessageObjDescEvent(this));
        }

        public void HandleActionMotion(UniversalMotion motion)
        {
            if (CurrentLandblock != null)
            {
                DoMotion(motion);
            }
        }

        private void DoMotion(UniversalMotion motion)
        {
            CurrentLandblock.EnqueueBroadcastMotion(this, motion);
        }

        protected override void SendUpdatePosition()
        {
            base.SendUpdatePosition();
            GameMessage msg = new GameMessageUpdatePosition(this);
            Session.Network.EnqueueSend(msg);
        }

        /// <summary>
        /// This method removes an item from Inventory and adds it to wielded items.
        /// It also clears all properties used when an object is contained and sets the needed properties to be wielded Og II
        /// </summary>
        /// <param name="item">The world object we are wielding</param>
        /// <param name="wielder">Who is wielding the item</param>
        /// <param name="currentWieldedLocation">What wield location are we going into</param>
        private void AddToWieldedObjects(ref WorldObject item, WorldObject wielder, EquipMask currentWieldedLocation)
        {
            // Unset container fields
            item.Placement = null;
            item.ContainerId = null;
            // Set fields needed to be wielded.
            item.WielderId = wielder.Guid.Full;
            item.CurrentWieldedLocation = currentWieldedLocation;

            if (!wielder.WieldedObjects.ContainsKey(item.Guid))
            {
                wielder.WieldedObjects.Add(item.Guid, item);

                Burden += item.Burden;
            }
        }

        /// <summary>
        /// This method is used to remove an item from the Wielded Objects dictionary.
        /// It does not add it to inventory as you could be unwielding to the ground or a chest. Og II
        /// </summary>
        /// <param name="itemGuid">Guid of the item to be unwielded.</param>
        public void RemoveFromWieldedObjects(ObjectGuid itemGuid)
        {
            if (WieldedObjects.ContainsKey(itemGuid))
            {
                Burden -= WieldedObjects[itemGuid].Burden;
                WieldedObjects.Remove(itemGuid);
            }
        }

        /// <summary>
        /// This method iterates through your main pack, any packs and finds all the items contained
        /// It also iterates over your wielded items - it sends create object messages needed by the login process
        /// it is called from SendSelf as part of the login message traffic.   Og II
        /// </summary>
        /// <param name="session"></param>
        public void SendInventoryAndWieldedItems(Session session)
        {
            foreach (WorldObject invItem in InventoryObjects.Values)
            {
                session.Network.EnqueueSend(new GameMessageCreateObject(invItem));
                // Was the item I just send a container?   If so, we need to send the items in the container as well. Og II
                if (invItem.WeenieType != WeenieType.Container)
                    continue;

                Session.Network.EnqueueSend(new GameEventViewContents(Session, invItem.SnapShotOfDataObject()));
                foreach (WorldObject itemsInContainer in invItem.InventoryObjects.Values)
                {
                    session.Network.EnqueueSend(new GameMessageCreateObject(itemsInContainer));
                }
            }

            foreach (WorldObject wieldedObject in WieldedObjects.Values)
            {
                WorldObject item = wieldedObject;
                if ((item.CurrentWieldedLocation != null) && (((EquipMask)item.CurrentWieldedLocation & EquipMask.Selectable) != 0))
                {
                    int placementId;
                    int childLocation;
                    session.Player.SetChild(this, item, (int)item.CurrentWieldedLocation, out placementId, out childLocation);
                }
                session.Network.EnqueueSend(new GameMessageCreateObject(item));
            }
        }

        /// <summary>
        /// This method is used to take our persisted tracked contracts and send them on to the client. Pg II
        /// </summary>
        public void SendContractTrackerTable()
        {
            Session.Network.EnqueueSend(new GameEventSendClientContractTrackerTable(Session, TrackedContracts.Select(x => x.Value).ToList()));
        }

        /// <summary>
        /// This method is called in response to a put item in container message.  It is used when the item going
        /// into a container was wielded.   It sets the appropriate properties, sends out response messages
        /// and handles switching stances - for example if you have a bow wielded and are in bow combat stance,
        /// when you unwield the bow, this also sends the messages needed to go into unarmed combat mode. Og II
        /// </summary>
        /// <param name="container"></param>
        /// <param name="item"></param>
        /// <param name="placement"></param>
        /// <param name="inContainerChain"></param>
        private void HandleUnwieldItem(Container container, WorldObject item, int placement, ActionChain inContainerChain)
        {
            EquipMask? oldLocation = item.CurrentWieldedLocation;

            item.ContainerId = container.Guid.Full;
            SetInventoryForContainer(item, placement);

            RemoveFromWieldedObjects(item.Guid);
            // We will always be updating the player appearance
            UpdateAppearance(this);

            if ((oldLocation & EquipMask.Selectable) != 0)
            {
                // We are coming from a hand shield slot.
                Children.Remove(Children.Find(s => s.Guid == item.Guid.Full));
            }

            // Set the container stuff
            item.ContainerId = container.Guid.Full;
            item.Placement = placement;

            inContainerChain.AddAction(this, () =>
            {
                if (container.Guid != Guid)
                {
                    container.AddToInventory(item, placement);
                    Burden += item.Burden;
                }
                else
                    AddToInventory(item, placement);
            });

            CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange,
                                            new GameMessageUpdateInstanceId(Session.Player.Sequences, item.Guid, PropertyInstanceId.Wielder, new ObjectGuid(0)),
                                            new GameMessagePublicUpdatePropertyInt(Session.Player.Sequences, item.Guid, PropertyInt.CurrentWieldedLocation, 0),
                                            new GameMessageUpdateInstanceId(Session.Player.Sequences, item.Guid, PropertyInstanceId.Container, container.Guid),
                                            new GameMessagePickupEvent(item),
                                            new GameMessageSound(Guid, Sound.UnwieldObject, (float)1.0),
                                            new GameMessagePutObjectInContainer(Session, container.Guid, item, placement),
                                            new GameMessageObjDescEvent(this));

            if ((oldLocation != EquipMask.MissileWeapon && oldLocation != EquipMask.Held && oldLocation != EquipMask.MeleeWeapon) || ((CombatMode & CombatMode.CombatCombat) == 0))
                return;
            HandleSwitchToPeaceMode(CombatMode);
            HandleSwitchToMeleeCombatMode(CombatMode);
        }

        /// <summary>
        /// Method is called in response to put item in container message.   This use case is we are just
        /// reorganizing our items.   It is either a in pack slot to slot move, or we could be going from one
        /// pack (container) to another. This method is called from an action chain.  Og II
        /// </summary>
        /// <param name="item">the item we are moving</param>
        /// <param name="container">what container are we going in</param>
        /// <param name="placement">what is my slot position within that container</param>
        private void HandleMove(ref WorldObject item, Container container, int placement)
        {
            RemoveWorldObjectFromInventory(item.Guid);

            item.ContainerId = container.Guid.Full;
            item.Placement = placement;

            container.AddToInventory(item, placement);

            if (item.ContainerId != Guid.Full)
            {
                Burden += item.Burden ?? 0;
                if (item.WeenieType == WeenieType.Coin)
                    UpdateCurrencyClientCalculations(WeenieType.Coin);
            }
            Session.Network.EnqueueSend(
                new GameMessagePutObjectInContainer(Session, container.Guid, item, placement),
                new GameMessageUpdateInstanceId(Session.Player.Sequences, item.Guid, PropertyInstanceId.Container,
                    container.Guid));

            Session.SaveSession();
        }

        /// <summary>
        /// This method is used to split a stack of any item that is stackable - arrows, tapers, pyreal etc.
        /// It creates the new object and sets the burden of the new item, adjusts the count and burden of the splitting
        /// item. Og II
        /// </summary>
        /// <param name="stackId">This is the guild of the item we are spliting</param>
        /// <param name="containerId">The guid of the container</param>
        /// <param name="place">Place is the slot in the container we are spliting into.   Range 0-MaxCapacity</param>
        /// <param name="amount">The amount of the stack we are spliting from that we are moving to a new stack.</param>
        /// <returns></returns>

        public void HandleActionStackableSplitToContainer(uint stackId, uint containerId, int place, ushort amount)
        {
            // TODO: add the complementary method to combine items Og II
            ActionChain splitItemsChain = new ActionChain();
            splitItemsChain.AddAction(this, () =>
                {
                    Container container;
                    if (containerId == Guid.Full)
                    {
                        container = this;
                    }
                    else
                    {
                        container = (Container)GetInventoryItem(new ObjectGuid(containerId));
                    }

                    if (container == null)
                    {
                        log.InfoFormat("Asked to split stack {0} in container {1} - the container was not found",
                            stackId,
                            containerId);
                        return;
                    }
                    WorldObject stack = container.GetInventoryItem(new ObjectGuid(stackId));
                    if (stack == null)
                    {
                        log.InfoFormat("Asked to split stack {0} in container {1} - the stack item was not found",
                            stackId,
                            containerId);
                        return;
                    }
                    if (stack.Value == null || stack.StackSize < amount || stack.StackSize == 0)
                    {
                        log.InfoFormat(
                            "Asked to split stack {0} in container {1} - with amount of {2} but there is not enough to split",
                            stackId, containerId, amount);
                        return;
                    }

                    // Ok we are in business

                    WorldObject newStack = WorldObjectFactory.CreateWorldObject(stack.NewDataObjectFromCopy()); // Fix suggested by Mogwai and Og II
                    container.AddToInventory(newStack);

                    ushort oldStackSize = (ushort)stack.StackSize;
                    stack.StackSize = (ushort)(oldStackSize - amount);

                    newStack.StackSize = amount;

                    // Update burden
                    stack.Burden = (ushort)(StackUnitBurden * stack.StackSize ?? 1);
                    newStack.Burden = (ushort)(StackUnitBurden * newStack.StackSize ?? 1);

                    // Update value
                    stack.Value = (ushort)(StackUnitValue * stack.StackSize ?? 1);
                    newStack.Burden = (ushort)(StackUnitValue * newStack.StackSize ?? 1);

                    GameMessagePutObjectInContainer msgPutObjectInContainer =
                        new GameMessagePutObjectInContainer(Session, container.Guid, newStack, place);
                    GameMessageSetStackSize msgAdjustOldStackSize = new GameMessageSetStackSize(stack.Sequences,
                        stack.Guid, (int)stack.StackSize, (int)stack.Value);
                    GameMessageCreateObject msgNewStack = new GameMessageCreateObject(newStack);

                    CurrentLandblock.EnqueueBroadcast(Location, MaxObjectTrackingRange,
                        msgPutObjectInContainer, msgAdjustOldStackSize, msgNewStack);

                    if (stack.WeenieType == WeenieType.Coin)
                        UpdateCurrencyClientCalculations(WeenieType.Coin);
                });
            splitItemsChain.EnqueueChain();
        }

        /// <summary>
        /// This method is part of the contract tracking functions.   This is used to remove or abandon a contract.
        /// The method validates the id passed from the client against the portal.dat file, then sends the appropriate
        /// response to the client to remove the item from the quest panel. Og II
        /// </summary>
        /// <param name="contractId">This is the contract id passed to us from the client that we want to remove.</param>
        public void HandleActionAbandonContract(uint contractId)
        {
            ActionChain abandonContractChain = new ActionChain();
            abandonContractChain.AddAction(this, () =>
            {
                ContractTracker contractTracker = new ContractTracker(contractId, Guid.Full)
                {
                    Stage = 0,
                    TimeWhenDone = 0,
                    TimeWhenRepeats = 0,
                    DeleteContract = 1,
                    SetAsDisplayContract = 0
                };

                GameEventSendClientContractTracker contractMsg = new GameEventSendClientContractTracker(Session, contractTracker);

                DataContractTracker contract = new DataContractTracker();
                if (TrackedContracts.ContainsKey(contractId))
                    contract = TrackedContracts[contractId].SnapShotOfDataContractTracker();

                TrackedContracts.Remove(contractId);
                LastUseTracker.Remove((int)contractId);
                DataObject.TrackedContracts.Remove(contractId);

                DatabaseManager.Shard.DeleteContract(contract, deleteSuccess =>
               {
                   if (deleteSuccess)
                       log.Info($"ContractId {contractId:X} successfully deleted");
                   else
                       log.Error($"Unable to delete contractId {contractId:X} ");
               });

                Session.Network.EnqueueSend(contractMsg);
            });
            abandonContractChain.EnqueueChain();
        }

        /// <summary>
        /// This method is used to remove X number of items from a stack, including
        /// If amount to remove is greater or equal to the current stacksize, item will be removed.
        /// </summary>
        /// <param name="stackId">Guid.Full of the stacked item</param>
        /// <param name="containerId">Guid.Full of the container that contains the item</param>
        /// <param name="amount">Amount taken out of the stack</param>
        public void HandleActionRemoveItemFromInventory(uint stackId, uint containerId, ushort amount)
        {
            // FIXME: This method has been morphed into doing a few things we either need to rename it or
            // something.   This may or may not remove item from inventory.
            ActionChain removeItemsChain = new ActionChain();
            removeItemsChain.AddAction(this, () =>
            {
                Container container;
                if (containerId == Guid.Full)
                    container = this;
                else
                    container = (Container)GetInventoryItem(new ObjectGuid(containerId));

                if (container == null)
                {
                    log.InfoFormat("Asked to remove an item {0} in container {1} - the container was not found",
                        stackId,
                        containerId);
                    return;
                }

                WorldObject item = container.GetInventoryItem(new ObjectGuid(stackId));
                if (item == null)
                {
                    log.InfoFormat("Asked to remove an item {0} in container {1} - the item was not found",
                        stackId,
                        containerId);
                    return;
                }

                if (amount >= item.StackSize)
                    amount = (ushort)item.StackSize;

                ushort oldStackSize = (ushort)item.StackSize;
                ushort newStackSize = (ushort)(oldStackSize - amount);

                if (newStackSize < 1)
                    newStackSize = 0;

                item.StackSize = newStackSize;

                Session.Network.EnqueueSend(new GameMessageSetStackSize(item.Sequences, item.Guid, (int)item.StackSize, (int)item.Value));

                if (newStackSize < 1)
                {
                    // Remove item from inventory
                    DestroyInventoryItem(item);
                    // Clean up the shard database.
                    DatabaseManager.Shard.DeleteObject(item.SnapShotOfDataObject(), null);
                }
                else
                    Burden = (ushort)(Burden - (item.StackUnitBurden * amount));
            });
            removeItemsChain.EnqueueChain();
        }

        /// <summary>
        /// This method is used to pick items off the world - out of 3D space and into our inventory or to a wielded slot.
        /// It checks the use case needed, sends the appropriate response messages.   In addition, it will move to objects
        /// that are out of range in the attemp to pick them up.   It will call update apperiance if needed and you have
        /// wielded an item from the ground. Og II
        /// </summary>
        /// <param name="container"></param>
        /// <param name="itemGuid"></param>
        /// <param name="placement"></param>
        /// <param name="iidPropertyId"></param>
        private void HandlePickupItem(Container container, ObjectGuid itemGuid, int placement, PropertyInstanceId iidPropertyId)
        {
            // Logical operations:
            // !! FIXME: How to handle repeat on condition?
            // while (!objectInRange)
            //   try Move to object
            // !! FIXME: How to handle conditional
            // Try acquire from landblock
            // if acquire successful:
            //   add to container
            ActionChain pickUpItemChain = new ActionChain();

            // Move to the object
            pickUpItemChain.AddChain(CreateMoveToChain(itemGuid, PickUpDistance));

            // Pick up the object
            // Start pickup animation
            pickUpItemChain.AddAction(this, () =>
            {
                UniversalMotion motion = new UniversalMotion(MotionStance.Standing);
                motion.MovementData.ForwardCommand = (uint)MotionCommand.Pickup;
                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange,
                    new GameMessageUpdatePosition(this),
                    new GameMessageUpdateMotion(Guid,
                        Sequences.GetCurrentSequence(SequenceType.ObjectInstance),
                        Sequences, motion));
            });
            // Wait for animation to progress
            float pickupAnimationLength = MotionTable.GetAnimationLength((uint)MotionTableId, MotionCommand.Pickup);
            pickUpItemChain.AddDelaySeconds(pickupAnimationLength);

            // Ask landblock to transfer item
            // pickUpItemChain.AddAction(CurrentLandblock, () => CurrentLandblock.TransferItem(itemGuid, containerGuid));
            if (container.Guid.IsPlayer())
                CurrentLandblock.QueueItemTransfer(pickUpItemChain, itemGuid, container.Guid);
            else
                CurrentLandblock.ScheduleItemTransferInContainer(pickUpItemChain, itemGuid, (Container)GetInventoryItem(container.Guid));

            // Finish pickup animation
            pickUpItemChain.AddAction(this, () =>
            {
                // If success, the item is in our inventory:
                WorldObject item = GetInventoryItem(itemGuid);

                if (item.ContainerId != Guid.Full)
                {
                    Burden += item.Burden ?? 0;

                    if (item.WeenieType == WeenieType.Coin)
                    {
                        UpdateCurrencyClientCalculations(WeenieType.Coin);
                    }
                }

                if (item.WeenieType == WeenieType.Container)
                {
                    Session.Network.EnqueueSend(new GameEventViewContents(Session, item.SnapShotOfDataObject()));
                    foreach (KeyValuePair<ObjectGuid, WorldObject> packItem in item.InventoryObjects)
                    {
                        Session.Network.EnqueueSend(new GameMessageCreateObject(packItem.Value));
                        UpdateCurrencyClientCalculations(WeenieType.Coin);
                    }
                }

                // Update all our stuff if we succeeded
                if (item != null)
                {
                    SetInventoryForContainer(item, placement);
                    // FIXME(ddevec): I'm not 100% sure which of these need to be broadcasts, and which are local sends...
                    UniversalMotion motion = new UniversalMotion(MotionStance.Standing);
                    if (iidPropertyId == PropertyInstanceId.Container)
                    {
                        Session.Network.EnqueueSend(
                            ////new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.EncumbranceVal, UpdateBurden()),
                            new GameMessageSound(Guid, Sound.PickUpItem, 1.0f),
                            new GameMessageUpdateInstanceId(itemGuid, container.Guid, iidPropertyId),
                            new GameMessagePutObjectInContainer(Session, container.Guid, item, placement));
                    }
                    else
                    {
                        AddToWieldedObjects(ref item, container, (EquipMask)placement);
                        UpdateAppearance(container);
                        Session.Network.EnqueueSend(new GameMessageSound(Guid, Sound.WieldObject, (float)1.0),
                                                    new GameMessageObjDescEvent(this),
                                                    new GameMessageUpdateInstanceId(container.Guid, itemGuid, PropertyInstanceId.Wielder),
                                                    new GameEventWieldItem(Session, itemGuid.Full, placement));
                    }

                    CurrentLandblock.EnqueueBroadcast(
                        Location,
                        Landblock.MaxObjectRange,
                        new GameMessageUpdateMotion(
                            Guid,
                            Sequences.GetCurrentSequence(SequenceType.ObjectInstance),
                            Sequences,
                            motion),
                        new GameMessagePickupEvent(item));

                    if (iidPropertyId == PropertyInstanceId.Wielder)
                        CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, new GameMessageObjDescEvent(this));

                    // TODO: Og II - check this later to see if it is still required.
                    Session.Network.EnqueueSend(new GameMessageUpdateObject(item));
                }
                // If we didn't succeed, just stand up and be ashamed of ourself
                else
                {
                    UniversalMotion motion = new UniversalMotion(MotionStance.Standing);

                    CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange,
                        new GameMessageUpdateMotion(Guid,
                            Sequences.GetCurrentSequence(SequenceType.ObjectInstance),
                            Sequences, motion));
                    // CurrentLandblock.EnqueueBroadcast(self shame);
                }
            });
            // Set chain to run
            pickUpItemChain.EnqueueChain();
        }

        /// <summary>
        /// This method was developed by OptimShi.   It looks at currently wielded items and does the appropriate
        /// model replacements.   It then sends all uncovered  body parts. Og II
        /// </summary>
        /// <param name="container"></param>
        public void UpdateAppearance(Container container)
        {
            EquipMask equipMask = EquipMask.Clothing | EquipMask.Armor | EquipMask.Cloak;
            // Check if the character is displaying headgear and cloak
            if ((Character.CharacterOptions2Mapping & (int)CharacterOptions2.ShowYourCloak) == 0)
                equipMask &= ~EquipMask.Cloak;
            if ((Character.CharacterOptions2Mapping & (int)CharacterOptions2.ShowYourHelmOrHeadGear) == 0)
                equipMask &= ~EquipMask.HeadWear;

            UpdateAppearance(equipMask);
        }

        public void HandleActionWieldItem(Container container, uint itemId, int placement)
        {
            ActionChain wieldChain = new ActionChain();
            wieldChain.AddAction(this, () =>
                {
                    ObjectGuid itemGuid = new ObjectGuid(itemId);
                    WorldObject item = GetInventoryItem(itemGuid);
                    WorldObject packItem;

                    if (item != null)
                    {
                        ObjectGuid containerGuid = ObjectGuid.Invalid;
                        List<KeyValuePair<ObjectGuid, WorldObject>> containers = InventoryObjects.Where(wo => wo.Value.WeenieType == WeenieType.Container).ToList();
                        foreach (KeyValuePair<ObjectGuid, WorldObject> subpack in containers)
                        {
                            if (subpack.Value.InventoryObjects.TryGetValue(itemGuid, out packItem))
                            {
                                containerGuid = subpack.Value.Guid;
                                break;
                            }
                        }

                        Container pack;
                        if (item != null && containerGuid != ObjectGuid.Invalid)
                        {
                            pack = (Container)GetInventoryItem(containerGuid);

                            RemoveWorldObjectFromInventory(itemGuid);
                        }
                        else
                        {
                            if (item != null)
                                RemoveWorldObjectFromInventory(itemGuid);
                        }

                        AddToWieldedObjects(ref item, container, (EquipMask)placement);

                        if ((EquipMask)placement == EquipMask.MissileAmmo)
                            Session.Network.EnqueueSend(new GameEventWieldItem(Session, itemGuid.Full, placement),
                                                        new GameMessageSound(Guid, Sound.WieldObject, (float)1.0));
                        else
                        {
                            if (((EquipMask)placement & EquipMask.Selectable) != 0)
                            {
                                SetChild(container, item, placement, out int placementId, out int childLocation);

                                UpdateAppearance(container);

                                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange,
                                                                new GameMessageParentEvent(Session.Player, item, childLocation, placementId),
                                                                new GameEventWieldItem(Session, itemGuid.Full, placement),
                                                                new GameMessageSound(Guid, Sound.WieldObject, (float)1.0),
                                                                new GameMessageUpdateInstanceId(Session.Player.Sequences, item.Guid, PropertyInstanceId.Container, new ObjectGuid(0)),
                                                                new GameMessageUpdateInstanceId(Session.Player.Sequences, item.Guid, PropertyInstanceId.Wielder, container.Guid),
                                                                new GameMessagePublicUpdatePropertyInt(Session.Player.Sequences, item.Guid, PropertyInt.CurrentWieldedLocation, placement));

                                if (CombatMode == CombatMode.NonCombat || CombatMode == CombatMode.Undef)
                                    return;
                                switch ((EquipMask)placement)
                                {
                                    case EquipMask.MissileWeapon:
                                        SetCombatMode(CombatMode.Missile);
                                        break;
                                    case EquipMask.Held:
                                        SetCombatMode(CombatMode.Magic);
                                        break;
                                    default:
                                        SetCombatMode(CombatMode.Melee);
                                        break;
                                }
                            }
                            else
                            {
                                UpdateAppearance(container);

                                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange,
                                                            new GameEventWieldItem(Session, itemGuid.Full, placement),
                                                            new GameMessageSound(Guid, Sound.WieldObject, (float)1.0),
                                                            new GameMessageUpdateInstanceId(Session.Player.Sequences, item.Guid, PropertyInstanceId.Container, new ObjectGuid(0)),
                                                            new GameMessageUpdateInstanceId(Session.Player.Sequences, item.Guid, PropertyInstanceId.Wielder, container.Guid),
                                                            new GameMessagePublicUpdatePropertyInt(Session.Player.Sequences, item.Guid, PropertyInt.CurrentWieldedLocation, placement),
                                                            new GameMessageObjDescEvent(container));
                            }
                        }
                    }
                    else
                    {
                        HandlePickupItem(container, itemGuid, placement, PropertyInstanceId.Wielder);
                    }
                });
            wieldChain.EnqueueChain();
        }

        public void HandleActionPutItemInContainer(ObjectGuid itemGuid, ObjectGuid containerGuid, int placement = 0)
        {
            ActionChain inContainerChain = new ActionChain();
            inContainerChain.AddAction(
                this,
                () =>
                    {
                        Container container;

                        if (containerGuid.IsPlayer())
                            container = this;
                        else
                        {
                            // Ok I am going into player pack - not the main pack.

                            // TODO pick up here - I have a generic object for a container, need to find out why.
                            container = (Container)GetInventoryItem(containerGuid);
                        }

                        // is this something I already have? If not, it has to be a pickup - do the pickup and out.
                        if (!HasItem(itemGuid))
                        {
                            // This is a pickup into our main pack.
                            HandlePickupItem(container, itemGuid, placement, PropertyInstanceId.Container);
                            return;
                        }

                        // Ok, I know my container and I know I must have the item so let's get it.
                        WorldObject item = GetInventoryItem(itemGuid);

                        // check wilded.
                        if (item == null)
                            item = GetWieldedItem(itemGuid);

                        // Was I equiped?   If so, lets take care of that and unequip
                        if (item.WielderId != null)
                        {
                            HandleUnwieldItem(container, item, placement, inContainerChain);
                            return;
                        }

                        // if were are still here, this needs to do a pack pack or main pack move.
                        HandleMove(ref item, container, placement);
                    });
            inContainerChain.EnqueueChain();
        }

        /// <summary>
        /// Context: only call when in the player action loop
        /// </summary>
        public void DestroyInventoryItem(WorldObject wo)
        {
            RemoveWorldObjectFromInventory(wo.Guid);
            Session.Network.EnqueueSend(new GameMessageRemoveObject(wo));
            ////Session.Network.EnqueueSend(new GameMessagePrivateUpdatePropertyInt(Session.Player.Sequences, PropertyInt.EncumbranceVal, (uint)Burden));
        }

        public void RemoveItemFromPlayerAndNotify(ObjectGuid itemGuid, out WorldObject item)
        {
            // check packs of item.
            item = GetInventoryItem(itemGuid);
            if (item == null)
            {
                // check to see if this item is wielded
                item = GetWieldedItem(itemGuid);
                if (item != null)
                {
                    RemoveFromWieldedObjects(itemGuid);
                    UpdateAppearance(this);
                    Session.Network.EnqueueSend(
                        new GameMessageSound(Guid, Sound.WieldObject, (float)1.0),
                        new GameMessageObjDescEvent(this),
                        new GameMessageUpdateInstanceId(Guid, new ObjectGuid(0), PropertyInstanceId.Wielder));
                }
            }
            else
            {
                RemoveWorldObjectFromInventory(itemGuid);
                if (item.WeenieType == WeenieType.Coin || item.WeenieType == WeenieType.Container)
                    UpdateCurrencyClientCalculations(WeenieType.Coin);
            }
        }

        public void HandleActionDropItem(ObjectGuid itemGuid)
        {
            ActionChain dropChain = new ActionChain();

            // Goody Goody -- lets build  drop chain
            // First start drop animation
            dropChain.AddAction(this, () =>
            {
                RemoveItemFromPlayerAndNotify(itemGuid, out WorldObject item);

                SetInventoryForWorld(item);

                UniversalMotion motion = new UniversalMotion(MotionStance.Standing);
                motion.MovementData.ForwardCommand = (ushort)((uint)MotionCommand.Pickup & 0xFFFF);
                Session.Network.EnqueueSend(new GameMessageUpdateInstanceId(itemGuid, new ObjectGuid(0), PropertyInstanceId.Container));

                // Set drop motion
                CurrentLandblock.EnqueueBroadcastMotion(this, motion);

                // Now wait for Drop Motion to finish -- use ActionChain
                ActionChain chain = new ActionChain();

                // Wait for drop animation
                Debug.Assert(MotionTableId != null, "MotionTableId != null");
                float pickupAnimationLength = MotionTable.GetAnimationLength((uint)MotionTableId, MotionCommand.Pickup);
                chain.AddDelaySeconds(pickupAnimationLength);

                // Play drop sound
                // Put item on landblock
                chain.AddAction(this, () =>
                {
                motion = new UniversalMotion(MotionStance.Standing);
                CurrentLandblock.EnqueueBroadcastMotion(this, motion);
                Session.Network.EnqueueSend(new GameMessageSound(Guid, Sound.DropItem, (float)1.0),
                new GameMessagePutObjectIn3d(Session, this, itemGuid),
                new GameMessageUpdateInstanceId(itemGuid, new ObjectGuid(0), PropertyInstanceId.Container));

                // This is the sequence magic - adds back into 3d space seem to be treated like teleport.
                Debug.Assert(item != null, "item != null");
                item.Sequences.GetNextSequence(SequenceType.ObjectTeleport);
                item.Sequences.GetNextSequence(SequenceType.ObjectVector);

                CurrentLandblock.AddWorldObject(item);

                // Ok we have handed off to the landblock, let's clean up the shard database.
                DatabaseManager.Shard.DeleteObject(item.SnapShotOfDataObject(), null);

                Session.Network.EnqueueSend(new GameMessageUpdateObject(item));
            });

                chain.EnqueueChain();
                // Removed SaveSession - this was causing items that were dropped to not be removed
                // from inventory.   If this causes a problem with vendor, we need to fix vendor.  Og II
            });

            dropChain.EnqueueChain();
        }

        public void HandleActionUseOnTarget(ObjectGuid sourceObjectId, ObjectGuid targetObjectId)
        {
            ActionChain chain = new ActionChain(this, () =>
            {
                WorldObject invSource = GetInventoryItem(sourceObjectId);
                WorldObject invTarget = GetInventoryItem(targetObjectId);

                if (invTarget != null)
                {
                    // inventory on inventory, we can do this now
                    RecipeManager.UseObjectOnTarget(this, invSource, invTarget);
                }
                else if (invSource.WeenieType == WeenieType.Key)
                {
                    WorldObject theTarget = CurrentLandblock.GetObject(targetObjectId);
                    Key key = invSource as Key;
                    key.HandleActionUseOnTarget(this, theTarget);
                }
                else if (targetObjectId == Guid)
                {
                    // using something on ourselves
                    RecipeManager.UseObjectOnTarget(this, invSource, this);
                }
                else
                {
                    WorldObject theTarget = CurrentLandblock.GetObject(targetObjectId);
                    RecipeManager.UseObjectOnTarget(this, invSource, theTarget);
                }
            });
            chain.EnqueueChain();
        }

        public void HandleActionUse(ObjectGuid usedItemId)
        {
            new ActionChain(this, () =>
            {
                WorldObject iwo = GetInventoryItem(usedItemId);
                if (iwo != null)
                {
                    iwo.OnUse(Session);
                }
                else
                {
                    if (CurrentLandblock != null)
                    {
                        // Just forward our action to the appropriate user...
                        WorldObject wo = CurrentLandblock.GetObject(usedItemId);
                        if (wo != null)
                        {
                            wo.ActOnUse(Guid);
                        }
                    }
                }
            }).EnqueueChain();
        }

        /// <summary>
        /// This method is used to handle targeted spell casting.
        /// </summary>
        /// <param name="targetGuid">Target for the spell</param>
        /// <param name="spellId">What spell are we casting.</param>
        public void HandleActionCastTargetedSpell(ObjectGuid targetGuid, uint spellId)
        {
            ActionChain spellChain = new ActionChain();

            // Check if player is busy doing something else
            if (Busy)
            {
                GameEventDisplayStatusMessage tooBusy = new GameEventDisplayStatusMessage(Session, StatusMessageType1.Enum_001D);
                Session.Network.EnqueueSend(tooBusy);
                return;
            }

            // TODO - Set this on WorldObject (e.g. Doors, Containers, Monsters, etc can be "busy")
            Busy = true;

            SpellTable spellTable = SpellTable.ReadFromDat();
            if (!spellTable.Spells.ContainsKey(spellId))
            {
                // TODO - ABORT! Not a valid spell id. Let's do some sort of graceful error handling for these cheats.
                return;
            }
            SpellBase spell = spellTable.Spells[spellId];

            // TODO -- All of these checks are to see if the player can even cast the spell. Client handles them for now, but never trust the client--always verify
            // TODO Check if player has the skill trained/spec'd
            // TODO Check if player has the skill to successfully even attempt the spell
            // TODO Check if player is in spellcasting combat mode
            // TODO Check if player has a foci and proper comp formula (use spell.Formula or spell.FociFormula)
            // TODO Check if player has the comps to cast
            // TODO Check if player has the required mana to cast spell (include Mana Conversion formula, if player has this skill)
            // TODO Check if player within range (spell.BaseRangeConstant) of target to cast spell

            // Assuming all of those checks have passed...
            spellChain.AddAction(this, () =>
            {
                // TODO Adjust player's mana amount
                // TODO Award any XP if needed directly to the maigc skill, mana conversion (if applicable), and to overall character XP

                // Send spell words in chat
                CurrentLandblock.EnqueueBroadcastLocalMagic(this, spell.SpellWords);

                // Play wind-up animation, if any
                if (spell.WindUpMotions.Count > 0)
                {
                    UniversalMotion motionWindUp = new UniversalMotion(MotionStance.Spellcasting);
                    motionWindUp.MovementData.CurrentStyle = (ushort)((uint)MotionStance.Spellcasting & 0xFFFF);
                    motionWindUp.MovementData.ForwardCommand = (ushort)spell.WindUpMotions[0];
                    motionWindUp.MovementData.ForwardSpeed = 2;

                    if (spell.WindUpMotions.Count > 1)
                        for (int i = 1; i < spell.WindUpMotions.Count; i++)
                            motionWindUp.Commands.Add(new MotionItem((MotionCommand)(ushort)spell.WindUpMotions[i], 2));

                    DoMotion(motionWindUp);
                }

                // Play the spell cast motion
                UniversalMotion motionSpellCast = new UniversalMotion(MotionStance.Spellcasting);
                motionSpellCast.MovementData.CurrentStyle = (ushort)((uint)MotionStance.Spellcasting & 0xFFFF);
                motionSpellCast.MovementData.ForwardCommand = (ushort)spell.GestureMotion;
                motionSpellCast.MovementData.ForwardSpeed = 2;
                DoMotion(motionSpellCast);

                // Get the length of the wind up time
                float spellCastAnimationsLength = 0f;
                if (MotionTableId != null)
                {
                    MotionTable mt = MotionTable.ReadFromDat((uint)MotionTableId);
                    spellCastAnimationsLength += spell.WindUpMotions.Sum(t => mt.GetAnimationLength(MotionStance.Spellcasting, (MotionCommand)t));
                    // add the length for the spell gesture
                    spellCastAnimationsLength += (mt.GetAnimationLength(MotionStance.Spellcasting, (MotionCommand)spell.GestureMotion));
                }
                // Add the delay to let all of these animations run before we finish...
                spellChain.AddDelaySeconds(spellCastAnimationsLength / 2);

                Console.WriteLine(@"SpellCastAnimationLength: " + (spellCastAnimationsLength / 2) + @" vs " + spell.WindUpTime);

                // =1 - 1 / (1 + EXP(0.069375* (A1 - A2)))
                // TODO - Run a skill check on the player
                Random r = new Random();
                bool skillCheckSuccess = (r.Next(1, 100)) > 33; // 33% chance of fail
                // skillCheckSuccess = true;
                if (!skillCheckSuccess)
                {
                    // TODO - Check for comp burn - Send message if any have burned
                    if (CurrentLandblock != null)
                    {
                        // TODO - Add in a delay before sending the fizz message (would not display until the spell animation finished)

                        // Play Fizzle Effect (this also plays the sound)
                        spellChain.AddAction(this, () => {
                            HandleActionApplyVisualEffect(Entity.Enum.PlayScript.Fizzle);
                            GameEventDisplayStatusMessage fizz = new GameEventDisplayStatusMessage(Session, StatusMessageType1.YourSpellFizzled);
                            Session.Network.EnqueueSend(fizz);
                        });
                    }
                }
                else
                {
                    if (Guid == targetGuid)
                    {
                        // Play Spell Effect, if any (this also plays the sound)
                        if (spell.TargetEffect > 0)
                        {
                            spellChain.AddAction(this, () =>
                            {
                                // SUCCESSFUL SPELLCAST!
                                string castMessage = "You cast " + spell.Name + " on yourself";

                                // TODO - Add to player active enchantments
                                // TODO - If this spell is already active, or another in the same category, adjust the cast message
                                // ...refreshing {spellname}, et al
                                castMessage += "."; // If not refreshing/surpassing/less than active spell
                                GameMessageSystemChat spellCastMessage = new GameMessageSystemChat(castMessage, ChatMessageType.Magic);
                                Session.Network.EnqueueSend(spellCastMessage);

                                PlayParticleEffect((PlayScript)spell.TargetEffect, Guid, 0.2f); // 0.2f, 0.4f, 1.0f
                            });
                        }
                    }
                    else
                    {
                        // TODO - Apply this to the targetId
                    }
                }

                // Spell is done, just do some clean up
                spellChain.AddAction(this, () => {
                    UniversalMotion readyStance = new UniversalMotion(MotionStance.Spellcasting);
                    readyStance.MovementData.CurrentStyle = (ushort)((uint)MotionStance.Spellcasting & 0xFFFF);
                    DoMotion(readyStance);

                    // TODO - If any component burn, send text notice. ChatMessageType.Magic
                    // TODO - If any component burn, send stack updates

                    // Clear our "busy" state
                    Busy = false;
                    GameEventUseDone sendUseDoneEvent = new GameEventUseDone(Session);
                    Session.Network.EnqueueSend(sendUseDoneEvent);
                });
            });
            spellChain.EnqueueChain();
        }

        /// <summary>
        /// This method handles inscription.   If you remove the inscription, it will remove the data from the object and
        /// remove it from the shard database - all inscriptions are stored in ace_object_properties_string Og II
        /// </summary>
        /// <param name="itemGuid">This is the object that we are trying to inscribe</param>
        /// <param name="inscriptionText">This is our inscription</param>
        public void HandleActionSetInscription(ObjectGuid itemGuid, string inscriptionText)
        {
            new ActionChain(this, () =>
            {
                WorldObject iwo = GetInventoryItem(itemGuid);
                if (iwo == null)
                {
                    return;
                }

                if (iwo.Inscribable && iwo.ScribeName != "prewritten")
                {
                    if (iwo.ScribeName != null && iwo.ScribeName != this.Name)
                    {
                        ChatPacket.SendServerMessage(Session,
                            "Only the original scribe may alter this without the use of an uninscription stone.",
                            ChatMessageType.Broadcast);
                    }
                    else
                    {
                        if (inscriptionText != "")
                        {
                            iwo.Inscription = inscriptionText;
                            iwo.ScribeName = this.Name;
                            iwo.ScribeAccount = Session.SubscriptionId.ToString();
                            Session.Network.EnqueueSend(new GameEventInscriptionResponse(Session, iwo.Guid.Full,
                                iwo.Inscription, iwo.ScribeName, iwo.ScribeAccount));
                        }
                        else
                        {
                            iwo.Inscription = null;
                            iwo.ScribeName = null;
                            iwo.ScribeAccount = null;
                        }
                    }
                }
                else
                {
                    // Send some cool you cannot inscribe that item message.   Not sure how that was handled live,
                    // I could not find a pcap of a failed inscription. Og II
                    ChatPacket.SendServerMessage(Session, "Target item cannot be inscribed.", ChatMessageType.System);
                }
            }).EnqueueChain();
        }

        public void HandleActionApplySoundEffect(Sound sound)
        {
            new ActionChain(this, () => PlaySound(sound, Guid)).EnqueueChain();
        }

        public void HandleActionApplyVisualEffect(PlayScript effect)
        {
            new ActionChain(this, () => PlayParticleEffect(effect, Guid)).EnqueueChain();
        }

        public ActionChain CreateMoveToChain(ObjectGuid target, float distance)
        {
            ActionChain moveToChain = new ActionChain();
            // While !at(thing) moveToThing
            ActionChain moveToBody = new ActionChain();
            moveToChain.AddAction(this, () =>
            {
                Position dest = CurrentLandblock.GetPosition(target);
                if (dest == null)
                {
                    log.Error("FIXME: Need the ability to cancel actions on error");
                    return;
                }

                if (CurrentLandblock.GetWeenieType(target) == WeenieType.Portal)
                {
                    OnAutonomousMove(CurrentLandblock.GetPosition(target),
                                            Sequences, MovementTypes.MoveToPosition, target);
                }
                else
                {
                    OnAutonomousMove(CurrentLandblock.GetPosition(target),
                                            Sequences, MovementTypes.MoveToObject, target);
                }
            });

            // poll for arrival every .1 seconds
            moveToBody.AddDelaySeconds(.1);

            moveToChain.AddLoop(this, () =>
            {
                bool valid;
                float outdistance;
                // Break loop if CurrentLandblock == null (we portaled or logged out), or if we arrive at the item
                if (CurrentLandblock == null)
                {
                    return false;
                }

                bool ret = !CurrentLandblock.WithinUseRadius(Guid, target, out outdistance, out valid);
                if (!valid)
                {
                    // If one of the items isn't on a landblock
                    ret = false;
                }
                return ret;
            }, moveToBody);

            return moveToChain;
        }

        public void HandleActionSmiteAllNearby()
        {
            // Create smite action chain... then send it
            new ActionChain(this, () =>
            {
                if (CurrentLandblock == null)
                {
                    return;
                }

                foreach (ObjectGuid toSmite in GetKnownCreatures())
                {
                    Creature smitee = CurrentLandblock.GetObject(toSmite) as Creature;
                    if (smitee != null)
                    {
                        smitee.DoOnKill(Session);
                    }
                }
            }).EnqueueChain();
        }

        public void HandleActionSmiteSelected()
        {
            new ActionChain(this, () =>
            {
                if (selectedTarget != ObjectGuid.Invalid)
                {
                    ObjectGuid target = selectedTarget;
                    if (target.IsCreature() || target.IsPlayer())
                    {
                        HandleActionKill(target);
                    }
                }
                else
                {
                    ChatPacket.SendServerMessage(Session, "No target selected, use @smite all to kill all creatures in radar range.", ChatMessageType.Broadcast);
                }
            }).EnqueueChain();
        }

        public void TestWieldItem(Session session, uint modelId, int paletteTemplate, float shade = 0)
        {
            // ClothingTable item = ClothingTable.ReadFromDat(0x1000002C); // Olthoi Helm
            // ClothingTable item = ClothingTable.ReadFromDat(0x10000867); // Cloak
            // ClothingTable item = ClothingTable.ReadFromDat(0x10000008); // Gloves
            // ClothingTable item = ClothingTable.ReadFromDat(0x100000AD); // Heaume
            ClothingTable item = ClothingTable.ReadFromDat(modelId);

            int palCount = 0;

            List<uint> coverage = new List<uint>(); // we'll store our fake coverage items here
            ClearObjDesc();
            AddBaseModelData(); // Add back in the facial features, hair and skin palette

            if (item.ClothingBaseEffects.ContainsKey((uint)SetupTableId))
            {
                // Add the model and texture(s)
                ClothingBaseEffect clothingBaseEffec = item.ClothingBaseEffects[(uint)SetupTableId];
                foreach (CloObjectEffect t in clothingBaseEffec.CloObjectEffects)
                {
                    byte partNum = (byte)t.Index;
                    AddModel((byte)t.Index, (ushort)t.ModelId);
                    coverage.Add(partNum);
                    foreach (CloTextureEffect t1 in t.CloTextureEffects)
                        AddTexture((byte)t.Index, (ushort)t1.OldTexture, (ushort)t1.NewTexture);
                }

                // Apply an appropriate palette. We'll just pick a random one if not specificed--it's a surprise every time!
                // For actual equipment, these should just be stored in the ace_object palette_change table and loaded from there
                if (item.ClothingSubPalEffects.Count > 0)
                {
                    int size = item.ClothingSubPalEffects.Count;
                    palCount = size;

                    CloSubPalEffect itemSubPal;
                    // Generate a random index if one isn't provided
                    if (item.ClothingSubPalEffects.ContainsKey(paletteTemplate))
                    {
                        itemSubPal = item.ClothingSubPalEffects[paletteTemplate];
                    }
                    else
                    {
                        List<CloSubPalEffect> values = item.ClothingSubPalEffects.Values.ToList();
                        Random rand = new Random();
                        paletteTemplate = rand.Next(size);
                        itemSubPal = values[paletteTemplate];
                    }

                    foreach (CloSubPalette t in itemSubPal.CloSubPalettes)
                    {
                        PaletteSet itemPalSet = PaletteSet.ReadFromDat(t.PaletteSet);
                        ushort itemPal = (ushort)itemPalSet.GetPaletteID(shade);

                        foreach (CloSubPalleteRange t1 in t.Ranges)
                        {
                            uint palOffset = t1.Offset / 8;
                            uint numColors = t1.NumColors / 8;
                            AddPalette(itemPal, (ushort)palOffset, (ushort)numColors);
                        }
                    }
                }

                // Add the "naked" body parts. These are the ones not already covered.
                SetupModel baseSetup = SetupModel.ReadFromDat((uint)SetupTableId);
                for (byte i = 0; i < baseSetup.Parts.Count; i++)
                {
                    if (!coverage.Contains(i) && i != 0x10) // Don't add body parts for those that are already covered. Also don't add the head.
                        AddModel(i, baseSetup.Parts[i]);
                }

                GameMessageObjDescEvent objDescEvent = new GameMessageObjDescEvent(this);
                session.Network.EnqueueSend(objDescEvent);
                ChatPacket.SendServerMessage(session, "Equipping model " + modelId.ToString("X") +
                                                      ", Applying palette index " + paletteTemplate + " of " + palCount +
                                                      " with a shade value of " + shade + ".", ChatMessageType.Broadcast);
            }
            else
            {
                // Alert about the failure
                ChatPacket.SendServerMessage(session, "Could not match that item to your character model.", ChatMessageType.Broadcast);
            }
        }

        public void HandleActionTestCorpseDrop()
        {
            new ActionChain(this, () =>
            {
                if (selectedTarget != ObjectGuid.Invalid)
                {
                    // FIXME(ddevec): This is wrong
                    ObjectGuid target = selectedTarget;

                    if (target.IsCreature())
                    {
                        HandleActionKill(target);
                    }
                }
                else
                {
                    ChatPacket.SendServerMessage(Session, "No creature selected.", ChatMessageType.Broadcast);
                }
            }).EnqueueChain();
        }

        public void HandleActionKill(ObjectGuid toSmite)
        {
            // Create smite action chain... then send it
            new ActionChain(this, () =>
            {
                Creature c = CurrentLandblock.GetObject(toSmite) as Creature;
                if (c != null)
                {
                    c.DoOnKill(Session);
                }
            }).EnqueueChain();
        }

        // FIXME(ddevec): Copy pasted code for prototyping -- clean later
        protected override void VitalTickInternal(CreatureVital vital)
        {
            uint oldValue = vital.Current;
            base.VitalTickInternal(vital);

            if (vital.Current != oldValue)
            {
                // FIXME(ddevec): This is uglysauce.  CreatureVital should probably have it, but DerethForever.Entity doesn't seem to like importing DerethForever.Network.Enum
                Vital v;
                if (vital == Health)
                {
                    v = Vital.Health;
                }
                else if (vital == Stamina)
                {
                    v = Vital.Stamina;
                }
                else if (vital == Mana)
                {
                    v = Vital.Mana;
                }
                else
                {
                    log.Error("Unknown vital in UpdateVitalInternal: " + vital);
                    return;
                }

                Session.Network.EnqueueSend(new GameMessagePrivateUpdateAttribute2ndLevel(Session, v, vital.Current));
            }
        }

        protected override void UpdateVitalInternal(CreatureVital vital, uint newVal)
        {
            uint oldValue = vital.Current;
            base.UpdateVitalInternal(vital, newVal);

            if (vital.Current != oldValue)
            {
                // FIXME(ddevec): This is uglysauce.  CreatureVital should probably have it, but DerethForever.Entity doesn't seem to like importing DerethForever.Network.Enum
                Vital v;
                if (vital == Health)
                {
                    v = Vital.Health;
                }
                else if (vital == Stamina)
                {
                    v = Vital.Stamina;
                }
                else if (vital == Mana)
                {
                    v = Vital.Mana;
                }
                else
                {
                    log.Error("Unknown vital in UpdateVitalInternal: " + vital);
                    return;
                }

                Session.Network.EnqueueSend(new GameMessagePrivateUpdateAttribute2ndLevel(Session, v, vital.Current));
            }
        }

        public void HandleActionTalk(string message)
        {
            ActionChain chain = new ActionChain();
            chain.AddAction(this, () => DoTalk(message));
            chain.EnqueueChain();
        }

        public void DoTalk(string message)
        {
            CurrentLandblock.EnqueueBroadcastLocalChat(this, message);
        }

        public void HandleActionEmote(string message)
        {
            ActionChain chain = new ActionChain();
            chain.AddAction(this, () => DoEmote(message));
            chain.EnqueueChain();
        }

        public void DoEmote(string message)
        {
            CurrentLandblock.EnqueueBroadcastLocalChatEmote(this, message);
        }

        public void HandleActionSoulEmote(string message)
        {
            ActionChain chain = new ActionChain();
            chain.AddAction(this, () => DoSoulEmote(message));
            chain.EnqueueChain();
        }

        public void DoSoulEmote(string message)
        {
            CurrentLandblock.EnqueueBroadcastLocalChatSoulEmote(this, message);
        }

        public void DoMoveTo(WorldObject wo)
        {
            ActionChain moveToObjectChain = new ActionChain();

            moveToObjectChain.AddChain(CreateMoveToChain(wo.Guid, 0.2f));
            moveToObjectChain.AddDelaySeconds(0.50);

            moveToObjectChain.AddAction(wo, () => wo.ActOnUse(Guid));

            moveToObjectChain.EnqueueChain();
        }

        private const uint magicSkillCheckMargin = 50;

        public bool CanReadScroll(MagicSchool school, uint power)
        {
            bool ret = false;
            CreatureSkill creatureSkill;

            switch (school)
            {
                case MagicSchool.CreatureEnchantment:
                    creatureSkill = GetSkill(Skill.CreatureEnchantment);
                    break;
                case MagicSchool.WarMagic:
                    creatureSkill = GetSkill(Skill.WarMagic);
                    break;
                case MagicSchool.ItemEnchantment:
                    creatureSkill = GetSkill(Skill.ItemEnchantment);
                    break;
                case MagicSchool.LifeMagic:
                    creatureSkill = GetSkill(Skill.LifeMagic);
                    break;
                case MagicSchool.VoidMagic:
                    creatureSkill = GetSkill(Skill.VoidMagic);
                    break;
                default:
                    // Undefined magic school, something bad happened.
                    Debug.Assert((int)school > 5 || school <= 0, "Undefined magic school?");
                    return false;
            }

            if (creatureSkill.Status >= SkillStatus.Trained && creatureSkill.ActiveValue >= (power - magicSkillCheckMargin))
                ret = true;

            return ret;
        }

        public void SendUseDoneEvent()
        {
            Session.Network.EnqueueSend(new GameEventUseDone(Session));
        }

        private int coinValue = 0;
        public override int? CoinValue
        {
            get { return coinValue; }
            set
            {
                if (value != coinValue)
                {
                    base.CoinValue = value;
                    coinValue = (int)value;
                    if (FirstEnterWorldDone)
                        Session.Network.EnqueueSend(new GameMessagePrivateUpdatePropertyInt(Sequences, PropertyInt.CoinValue, coinValue));
                }
            }
        }

        private ushort burden = 0;
        public override ushort? Burden
        {
            get { return burden; }
            set
            {
                if (value != burden)
                {
                    base.Burden = value;
                    burden = (ushort)value;
                    if (FirstEnterWorldDone)
                        Session.Network.EnqueueSend(new GameMessagePrivateUpdatePropertyInt(Sequences, PropertyInt.EncumbranceVal, burden));
                }
            }
        }

        private int value = 0;
        public override int? Value
        {
            get { return value; }
            set { base.Value = 0; }
        }

        /// <summary>
        /// Sends a system message to player based upon the ChatMessageType
        /// </summary>
        /// <param name="message">String containing message to Player</param>
        /// <param name="chatMessageType">Enum from DerethForever.Entity.Enum.ChatMessageType </param>
        public void SendSystemMessage(string message, ChatMessageType chatMessageType)
        {
            Session.Network.EnqueueSend(new GameMessageSystemChat(message, chatMessageType));
        }

        /// <summary>
        /// Method used to perform the animation, sound, and vital update on consumption of food or potions
        /// </summary>
        /// <param name="consumableName">Name of the consumable</param>
        /// <param name="sound">Either Sound.Eat1 or Sound.Drink1</param>
        /// <param name="buffType">ConsumableBuffType.Spell,ConsumableBuffType.Health,ConsumableBuffType.Stamina,ConsumableBuffType.Mana</param>
        /// <param name="boostAmount">Amount the Vital is boosted by; can be null, if buffType = ConsumableBuffType.Spell</param>
        /// <param name="spellId">Id of the spell cast by the consumable; can be null, if buffType != ConsumableBuffType.Spell</param>
        public void ApplyComsumable(string consumableName, Entity.Enum.Sound sound, ConsumableBuffType buffType, uint? boostAmount, uint? spellDID)
        {
            GameMessageSystemChat buffMessage;
            MotionCommand motionCommand;

            if (sound == Sound.Eat1)
                motionCommand = MotionCommand.Eat;
            else
                motionCommand = MotionCommand.Drink;

            GameMessageSound soundEvent = new GameMessageSound(Guid, sound, 1.0f);
            UniversalMotion motion = new UniversalMotion(MotionStance.Standing, new MotionItem(motionCommand));

            DoMotion(motion);

            if (buffType == ConsumableBuffType.Spell)
            {
                // Null check for safety
                if (spellDID == null)
                    spellDID = 0;

                // TODO: Handle spell cast
                buffMessage = new GameMessageSystemChat($"Consuming {consumableName} not yet fully implemented.", ChatMessageType.System);
            }
            else
            {
                CreatureVital creatureVital;
                string vitalName;

                // Null check for safety
                if (boostAmount == null)
                    boostAmount = 0;

                switch (buffType)
                {
                    case ConsumableBuffType.Health:
                        creatureVital = Health;
                        vitalName = "Health";
                        break;
                    case ConsumableBuffType.Mana:
                        creatureVital = Mana;
                        vitalName = "Mana";
                        break;
                    default:
                        creatureVital = Stamina;
                        vitalName = "Stamina";
                        break;
                }

                uint updatedVitalAmount = creatureVital.Current + (uint)boostAmount;

                if (updatedVitalAmount > creatureVital.MaxValue)
                    updatedVitalAmount = creatureVital.MaxValue;

                boostAmount = updatedVitalAmount - creatureVital.Current;

                UpdateVital(creatureVital, updatedVitalAmount);

                buffMessage = new GameMessageSystemChat($"You regain {boostAmount} {vitalName}.", ChatMessageType.Craft);
            }

            Session.Network.EnqueueSend(soundEvent, buffMessage);

            // Wait for animation
            ActionChain motionChain = new ActionChain();
            float motionAnimationLength = MotionTable.GetAnimationLength((uint)MotionTableId, MotionCommand.Eat);
            motionChain.AddDelaySeconds(motionAnimationLength);

            // Return to standing position after the animation delay
            motionChain.AddAction(this, () => DoMotion(new UniversalMotion(MotionStance.Standing)));
            motionChain.EnqueueChain();
        }
    }
}
