/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// WeenieType.Coin

using System.Collections.Generic;
using System.IO;
using System.Linq;

using DerethForever.Entity.Enum;
using DerethForever.Entity.Enum.Properties;
using DerethForever.Entity;

namespace DerethForever.Actors
{
    public sealed class Coin : WorldObject
    {
        private const IdentifyResponseFlags idFlags = IdentifyResponseFlags.IntStatsTable;

        private ushort stackSize = 1;
        public override ushort? StackSize
        {
            get { return stackSize; }
            set
            {
                if (value != stackSize)
                {
                    base.StackSize = value;
                    stackSize = (ushort)value;
                    // Value = (Weenie.Value ?? 0) * (StackSize ?? 1);
                    Value = (StackUnitValue ?? 0) * (StackSize ?? 1);
                }
            }
        }

        public Coin(DataObject dataObject)
            : base(dataObject)
        {
            CoinPropertiesInt = PropertiesInt.Where(x => x.PropertyId == (uint)PropertyInt.Value
                                                          || x.PropertyId == (uint)PropertyInt.EncumbranceVal).ToList();

            StackSize = (base.StackSize ?? 1);
           
            // Value = (Weenie.Value ?? 0) * (StackSize ?? 1);
            Value = (StackUnitValue ?? 0) * (StackSize ?? 1);

            if (StackSize == null)
                StackSize = 1;
        }

        private List<DataObjectPropertiesInt> CoinPropertiesInt
        {
            get;
            set;
        }

        public override void SerializeIdentifyObjectResponse(BinaryWriter writer, bool success, IdentifyResponseFlags flags = IdentifyResponseFlags.None)
        {
            WriteIdentifyObjectHeader(writer, idFlags, true); // Always succeed in assessing a coin.
            WriteIdentifyObjectIntProperties(writer, idFlags, CoinPropertiesInt);
        }
    }
}
