/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

using DerethForever.DatLoader.FileTypes;
using DerethForever.Actors;
using DerethForever.Actors.Actions;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Entity.Enum.Properties;
using DerethForever.Network.GameMessages.Messages;
using DerethForever.Network.Motion;

namespace DerethForever.Managers
{
    public class RecipeManager
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static Random _random = new Random();
        private static RecipeCache _recipeCache = null;

        private static List<ObjectPropertyId> _updateStructure = new List<ObjectPropertyId>() { new ObjectPropertyId((uint)PropertyInt.Structure, ObjectPropertyType.PropertyInt) };

        public static void Initialize()
        {
            // build the cache
            var recipes = Database.DatabaseManager.World.GetAllRecipes();
            _recipeCache = new RecipeCache(recipes);
        }

        public static void UseObjectOnTarget(Player player, WorldObject source, WorldObject target)
        {
            Recipe recipe = _recipeCache.GetRecipe(source.WeenieClassId, target.WeenieClassId);

            if (recipe == null)
            {
                var message = new GameMessageSystemChat($"The {source.Name} cannot be used on the {target.Name}.", ChatMessageType.Craft);
                player.Session.Network.EnqueueSend(message);
                player.SendUseDoneEvent();
                return;
            }

            switch ((RecipeType)recipe.RecipeType)
            {
                case RecipeType.CreateItem:
                    HandleCreateItemRecipe(player, source, target, recipe);
                    break;
                case RecipeType.Dyeing:
                    HandleDyeRecipe(player, source, target, recipe);
                    break;
                case RecipeType.Healing:
                    HandleHealingRecipe(player, source, target, recipe);
                    break;
                case RecipeType.ManaStone:
                    HandleManaStoneRecipe(player, source, target, recipe);
                    break;
                case RecipeType.Tinkering:
                    break;
                case RecipeType.Unlocking:
                    break;
                case RecipeType.None:
                    return;
            }
        }

        private static void HandleCreateItemRecipe(Player player, WorldObject source, WorldObject target, Recipe recipe)
        {
            ActionChain craftChain = new ActionChain();
            CreatureSkill skill = null;
            bool skillSuccess = true; // assume success, unless there's a skill check
            double percentSuccess = 1;

            UniversalMotion motion = new UniversalMotion(MotionStance.Standing, new MotionItem(MotionCommand.ClapHands));
            craftChain.AddAction(player, () => player.HandleActionMotion(motion));
            float craftAnimationLength = MotionTable.GetAnimationLength((uint)player.MotionTableId, MotionCommand.ClapHands);
            craftChain.AddDelaySeconds(craftAnimationLength);

            craftChain.AddAction(player, () =>
            {
                if (recipe.SkillId != null && recipe.SkillDifficulty != null)
                {
                    // there's a skill associated with this
                    Skill skillId = (Skill)recipe.SkillId.Value;

                    // this shouldn't happen, but sanity check for unexpected nulls
                    if (!player.Skills.Any(s => s.Skill == skillId))
                    {
                        log.Warn("Unexpectedly missing skill in Recipe usage");
                        player.SendUseDoneEvent();
                        return;
                    }

                    skill = player.GetSkill(skillId);
                    percentSuccess = skill.GetPercentSuccess(recipe.SkillDifficulty.Value);
                }

                // straight skill check, if applciable
                if (skill != null)
                    skillSuccess = _random.NextDouble() < percentSuccess;

                if ((recipe.ResultFlags & (uint)RecipeResult.SourceItemDestroyed) > 0)
                    player.DestroyInventoryItem(source);

                if ((recipe.ResultFlags & (uint)RecipeResult.TargetItemDestroyed) > 0)
                    player.DestroyInventoryItem(target);

                if ((recipe.ResultFlags & (uint)RecipeResult.SourceItemUsesDecrement) > 0)
                {
                    if (source.Structure <= 1)
                        player.DestroyInventoryItem(source);
                    else
                    {
                        source.Structure--;
                        source.SendPartialUpdates(player.Session, _updateStructure);
                    }
                }

                if ((recipe.ResultFlags & (uint)RecipeResult.TargetItemUsesDecrement) > 0)
                {
                    if (target.Structure <= 1)
                        player.DestroyInventoryItem(target);
                    else
                    {
                        target.Structure--;
                        target.SendPartialUpdates(player.Session, _updateStructure);
                    }
                }

                if (skillSuccess)
                {
                    WorldObject newObject1 = null;
                    WorldObject newObject2 = null;

                    if ((recipe.ResultFlags & (uint)RecipeResult.SuccessItem1) > 0 && recipe.SuccessItem1Wcid != null)
                        newObject1 = player.AddNewItemToInventory(recipe.SuccessItem1Wcid.Value);

                    if ((recipe.ResultFlags & (uint)RecipeResult.SuccessItem2) > 0 && recipe.SuccessItem2Wcid != null)
                        newObject2 = player.AddNewItemToInventory(recipe.SuccessItem2Wcid.Value);

                    var text = string.Format(recipe.SuccessMessage, source.Name, target.Name, newObject1?.Name, newObject2?.Name);
                    var message = new GameMessageSystemChat(text, ChatMessageType.Craft);
                    player.Session.Network.EnqueueSend(message);
                }
                else
                {
                    WorldObject newObject1 = null;
                    WorldObject newObject2 = null;

                    if ((recipe.ResultFlags & (uint)RecipeResult.FailureItem1) > 0 && recipe.FailureItem1Wcid != null)
                        newObject1 = player.AddNewItemToInventory(recipe.FailureItem1Wcid.Value);

                    if ((recipe.ResultFlags & (uint)RecipeResult.FailureItem2) > 0 && recipe.FailureItem2Wcid != null)
                        newObject2 = player.AddNewItemToInventory(recipe.FailureItem2Wcid.Value);

                    var text = string.Format(recipe.FailMessage, source.Name, target.Name, newObject1?.Name, newObject2?.Name);
                    var message = new GameMessageSystemChat(text, ChatMessageType.Craft);
                    player.Session.Network.EnqueueSend(message);
                }

                player.SendUseDoneEvent();
            });

            craftChain.EnqueueChain();
        }

        private static void HandleHealingRecipe(Player player, WorldObject source, WorldObject target, Recipe recipe)
        {
            ActionChain chain = new ActionChain();

            // skill will be null since the difficulty is calculated manually
            if (recipe.SkillId == null)
            {
                log.Warn($"healing recipe has null skill id (should almost certainly be healing, but who knows).  recipe id {recipe.RecipeGuid}.");
                player.SendUseDoneEvent();
                return;
            }

            if (!(target is Player))
            {
                var message = new GameMessageSystemChat($"The {source.Name} cannot be used on {target.Name}.", ChatMessageType.Craft);
                player.Session.Network.EnqueueSend(message);
                player.SendUseDoneEvent();
                return;
            }

            Player targetPlayer = target as Player;
            Ability vital = (Ability?)recipe.HealingAttribute ?? Ability.health;

            // there's a skill associated with this
            Skill skillId = (Skill)recipe.SkillId.Value;

            // this shouldn't happen, but sanity check for unexpected nulls
            if (!player.Skills.Any(s => s.Skill == skillId))
            {
                log.Warn("Unexpectedly missing skill in Recipe usage");
                player.SendUseDoneEvent();
                return;
            }

            CreatureSkill skill = player.GetSkill(skillId);

            // at this point, we've validated that the target is a player, and the target is below max health

            if (target.Guid != player.Guid)
            {
                // TODO: validate range
            }

            MotionCommand cmd = MotionCommand.SkillHealSelf;

            if (target.Guid != player.Guid)
                cmd = MotionCommand.Woah; // guess?  nothing else stood out

            // everything pre-validatable is validated.  action will be attempted unless cancelled, so
            // queue up the animation and action
            UniversalMotion motion = new UniversalMotion(MotionStance.Standing, new MotionItem(cmd));
            chain.AddAction(player, () => player.HandleActionMotion(motion));
            chain.AddDelaySeconds(0.5);

            chain.AddAction(player, () =>
            {
                // TODO: revalidate range if other player (they could have moved)

                double difficulty = 2 * (targetPlayer.Vitals[vital].MaxValue - targetPlayer.Vitals[vital].Current);
                if (difficulty <= 0)
                {
                    // target is at max (or higher?) health, do nothing
                    var text = "You are already at full health.";

                    if (target.Guid != player.Guid)
                        text = $"{target.Name} is already at full health";

                    player.Session.Network.EnqueueSend(new GameMessageSystemChat(text, ChatMessageType.Craft));
                    player.SendUseDoneEvent();
                    return;
                }

                if (player.CombatMode != CombatMode.NonCombat && player.CombatMode != CombatMode.Undef)
                    difficulty *= 1.1;

                int boost = source.Boost ?? 0;
                double multiplier = source.HealkitMod ?? 1;

                double playerSkill = skill.ActiveValue + boost;
                if (skill.Status == SkillStatus.Trained)
                    playerSkill *= 1.1;
                else if (skill.Status == SkillStatus.Specialized)
                    playerSkill *= 1.5;

                // usage is inevitable at this point, consume the use
                if ((recipe.ResultFlags & (uint)RecipeResult.SourceItemUsesDecrement) > 0)
                {
                    if (source.Structure <= 1)
                        player.DestroyInventoryItem(source);
                    else
                    {
                        source.Structure--;
                        source.SendPartialUpdates(player.Session, _updateStructure);
                    }
                }

                double percentSuccess = CreatureSkill.GetPercentSuccess((uint)playerSkill, (uint)difficulty);

                if (_random.NextDouble() <= percentSuccess)
                {
                    string expertly = "";

                    if (_random.NextDouble() < 0.1d)
                    {
                        expertly = "expertly ";
                        multiplier *= 1.2;
                    }

                    // calculate amount restored
                    uint maxRestore = targetPlayer.Vitals[vital].MaxValue - targetPlayer.Vitals[vital].Current;

                    // TODO: get actual forumula for healing.  this is COMPLETELY wrong.  this is 60 + random(1-60).
                    double amountRestored = 60d + _random.Next(1, 61);
                    amountRestored *= multiplier;

                    uint actualRestored = (uint)Math.Min(maxRestore, amountRestored);
                    targetPlayer.Vitals[vital].Current += actualRestored;

                    var updateVital = new GameMessagePrivateUpdateAttribute2ndLevel(player.Session, vital.GetVital(), targetPlayer.Vitals[vital].Current);
                    player.Session.Network.EnqueueSend(updateVital);

                    if (targetPlayer.Guid != player.Guid)
                    {
                        // tell the other player they got healed
                        var updateVitalToTarget = new GameMessagePrivateUpdateAttribute2ndLevel(targetPlayer.Session, vital.GetVital(), targetPlayer.Vitals[vital].Current);
                        targetPlayer.Session.Network.EnqueueSend(updateVitalToTarget);
                    }

                    string name = "yourself";
                    if (targetPlayer.Guid != player.Guid)
                        name = targetPlayer.Name;

                    string vitalName = "Health";

                    if (vital == Ability.stamina)
                        vitalName = "Stamina";
                    else if (vital == Ability.mana)
                        vitalName = "Mana";

                    string uses = source.Structure == 1 ? "use" : "uses";

                    var text = string.Format(recipe.SuccessMessage, expertly, name, actualRestored, vitalName, source.Name, source.Structure, uses);
                    var message = new GameMessageSystemChat(text, ChatMessageType.Craft);
                    player.Session.Network.EnqueueSend(message);

                    if (targetPlayer.Guid != player.Guid)
                    {
                        // send text to the other player too
                        text = string.Format(recipe.AlternateMessage, player.Name, expertly, actualRestored, vitalName);
                        message = new GameMessageSystemChat(text, ChatMessageType.Craft);
                        targetPlayer.Session.Network.EnqueueSend(message);
                    }
                }

                player.SendUseDoneEvent();
            });

            chain.EnqueueChain();
        }

        private static void HandleDyeRecipe(Player player, WorldObject source, WorldObject target, Recipe recipe)
        {
            ActionChain craftChain = new ActionChain();
            CreatureSkill skill = null;
            bool skillSuccess = true; // assume success, unless there's a skill check
            double percentSuccess = 1;

            // Can only dye "dyable" items and if not equipped.
            if (target.Dyeable == true && (target.CurrentWieldedLocation == null || target.CurrentWieldedLocation == 0))
            {
                UniversalMotion motion = new UniversalMotion(MotionStance.Standing, new MotionItem(MotionCommand.ClapHands));
                craftChain.AddAction(player, () => player.HandleActionMotion(motion));
                float craftAnimationLength = MotionTable.GetAnimationLength((uint)player.MotionTableId, MotionCommand.ClapHands);
                craftChain.AddDelaySeconds(craftAnimationLength);

                craftChain.AddAction(player, () =>
                {
                    if (recipe.SkillId != null && recipe.SkillDifficulty != null) // This should always be true for an authentic Dyeing Recipe.
                    {
                        // there's a skill associated with this
                        Skill skillId = (Skill)recipe.SkillId.Value;

                        // this shouldn't happen, but sanity check for unexpected nulls
                        if (!player.Skills.Any(s => s.Skill == skillId))
                        {
                            log.Warn("Unexpectedly missing skill in Recipe usage");
                            player.SendUseDoneEvent();
                            return;
                        }

                        skill = player.GetSkill(skillId);
                        percentSuccess = skill.GetPercentSuccess(recipe.SkillDifficulty.Value);
                    }

                    // straight skill check, if applciable
                    if (skill != null)
                        skillSuccess = _random.NextDouble() < percentSuccess;
                    
                    if ((recipe.ResultFlags & (uint)RecipeResult.SourceItemDestroyed) > 0)
                        player.DestroyInventoryItem(source);

                    if ((recipe.ResultFlags & (uint)RecipeResult.SourceItemUsesDecrement) > 0)
                    {
                        if (source.Structure <= 1)
                            player.DestroyInventoryItem(source);
                        else
                        {
                            source.Structure--;
                            source.SendPartialUpdates(player.Session, _updateStructure);
                        }
                    }

                    if ((recipe.ResultFlags & (uint)RecipeResult.TargetItemUsesDecrement) > 0)
                    {
                        if (target.Structure <= 1)
                            player.DestroyInventoryItem(target);
                        else
                        {
                            target.Structure--;
                            target.SendPartialUpdates(player.Session, _updateStructure);
                        }
                    }

                    WorldObject newObject1 = null;
                    WorldObject newObject2 = null;

                    if (skillSuccess)
                    {
                        if ((recipe.ResultFlags & (uint)RecipeResult.SuccessItem1) > 0 && recipe.SuccessItem1Wcid != null)
                        {
                            newObject1 = player.AddNewItemToInventory(recipe.SuccessItem1Wcid.Value);

                            // adjust the stack size if we need to
                            if (recipe.SuccessItem1Quantity != null && recipe.SuccessItem1Quantity.Value > 1 && newObject1.MaxStackSize != null && newObject1.MaxStackSize > 1)
                                newObject1.StackSize = (ushort)recipe.SuccessItem1Quantity.Value;
                        }

                        if ((recipe.ResultFlags & (uint)RecipeResult.SuccessItem2) > 0 && recipe.SuccessItem2Wcid != null)
                        {
                            newObject2 = player.AddNewItemToInventory(recipe.SuccessItem2Wcid.Value);
                            // adjust the stack size if we need to
                            if (recipe.SuccessItem2Quantity != null && recipe.SuccessItem2Quantity.Value > 1 && newObject2.MaxStackSize != null && newObject2.MaxStackSize > 1)
                                newObject2.StackSize = (ushort)recipe.SuccessItem2Quantity.Value;
                        }

                        var text = string.Format(recipe.SuccessMessage, source.Name, target.Name, newObject1?.Name, newObject2?.Name);
                        var message = new GameMessageSystemChat(text, ChatMessageType.Craft);
                        player.Session.Network.EnqueueSend(message);

                        if (source.PaletteTemplate != null)
                        {
                            // Update INT.PalatteTemplate of Target
                            int paletteTemplate = (int)source.PaletteTemplate;
                            target.PaletteTemplate = paletteTemplate;
                            player.Session.Network.EnqueueSend(new GameMessagePublicUpdatePropertyInt(target.Sequences, target.Guid, PropertyInt.PaletteTemplate, paletteTemplate));

                            // Lookup ClothingBase Table entry to get icon options
                            if (target.ClothingBase != null)
                            {
                                ClothingTable ct = ClothingTable.ReadFromDat((uint)target.ClothingBase);
                                uint iconId = ct.GetIcon(paletteTemplate);
                                if (iconId != target.IconId)
                                {
                                // Update DID.Icon of Target
                                target.IconId = iconId;
                                    player.Session.Network.EnqueueSend(new GameMessagePublicUpdatePropertyDataID(target.Sequences, target.Guid, PropertyDataId.Icon, iconId));
                                }
                            }

                            target.EnqueueBroadcastUpdateObject();
                        }
                    }
                    else
                    {
                        if ((recipe.ResultFlags & (uint)RecipeResult.FailureItem1) > 0 && recipe.FailureItem1Wcid != null)
                            newObject1 = player.AddNewItemToInventory(recipe.FailureItem1Wcid.Value);

                        if ((recipe.ResultFlags & (uint)RecipeResult.FailureItem2) > 0 && recipe.FailureItem2Wcid != null)
                            newObject2 = player.AddNewItemToInventory(recipe.FailureItem2Wcid.Value);

                        var text = string.Format(recipe.FailMessage, source.Name, target.Name, newObject1?.Name, newObject2?.Name);
                        var message = new GameMessageSystemChat(text, ChatMessageType.Craft);
                        player.Session.Network.EnqueueSend(message);

                        // Update INT.PalatteTemplate of target with the botched PaletteTemplate
                        int paletteTemplate = (int)PaletteTemplate.DyeBotched;
                        target.PaletteTemplate = paletteTemplate;
                        player.Session.Network.EnqueueSend(new GameMessagePublicUpdatePropertyInt(target.Sequences, target.Guid, PropertyInt.PaletteTemplate, paletteTemplate));

                        // Lookup ClothingBase Table entry to get icon options
                        if (target.ClothingBase != null)
                        {
                            ClothingTable ct = ClothingTable.ReadFromDat((uint)target.ClothingBase);
                            uint iconId = ct.GetIcon(paletteTemplate);
                            if (iconId != target.IconId)
                            {
                                // Update DID.Icon of Target
                                target.IconId = iconId;
                                player.Session.Network.EnqueueSend(new GameMessagePublicUpdatePropertyDataID(target.Sequences, target.Guid, PropertyDataId.Icon, iconId));
                            }
                        }

                        // Check if the failure was just soooo terrible, that they not only "botched" the color, they broke the armor
                        // Calculating this at a 10% chance. These odds are just speculation and not based on any information I could find (which was none) - Iron Golem
                        if (_random.NextDouble() < 10 && target.ArmorLevel != null)
                        {
                            if (target.ArmorLevel < 20)
                                target.ArmorLevel = 0;
                            else
                                target.ArmorLevel -= 20;

                            player.Session.Network.EnqueueSend(new GameMessagePublicUpdatePropertyInt(target.Sequences, target.Guid, PropertyInt.ArmorLevel, (int)target.ArmorLevel));
                        }

                        target.EnqueueBroadcastUpdateObject();
                    }
                    player.SendUseDoneEvent();
                });
            }
            else
            {
                if (target.Dyeable == false)
                {
                    // TODO: Find out the propert error message. This message is not from any logs.
                    var message = new GameMessageSystemChat("That item cannot be dyed.", ChatMessageType.Craft);
                    player.Session.Network.EnqueueSend(message);
                }
                if (target.CurrentWieldedLocation != null && target.CurrentWieldedLocation > 0)
                {
                    // TODO: Find out the proper error message. This message is not from any logs.
                    var message = new GameMessageSystemChat("You must unequip this item to dye it.", ChatMessageType.Craft);
                    player.Session.Network.EnqueueSend(message);
                }
                player.SendUseDoneEvent();
            }
            craftChain.EnqueueChain();
        }

        private static void HandleManaStoneRecipe(Player player, WorldObject source, WorldObject target, Recipe recipe)
        {
            ActionChain craftChain = new ActionChain();

            craftChain.AddAction(player, () =>
            {
                // Using a mana stone/charge to add mana to an item
                if (source.ItemCurMana != null && source.ItemCurMana > 0)
                {
                    // is the target the player or a specific item
                    if (target is Player) // is a player
                    {
                        // TODO: if player
                        List<WorldObject> itemsNeedingMana = new List<WorldObject>();
                        foreach (WorldObject wieldedObject in player.WieldedObjects.Values)
                        {
                            string itemName = wieldedObject.Name;
                            // check if the item is not at max mana or has no mana
                            if (wieldedObject.ItemMaxMana != null && (wieldedObject.ItemCurMana == null || wieldedObject.ItemCurMana < wieldedObject.ItemMaxMana))
                                itemsNeedingMana.Add(wieldedObject);
                        }

                        if (itemsNeedingMana.Count > 0)
                        {
                            int manaRemaining = 0;
                            int manaPool = (int)source.ItemCurMana; // We'll remove from this as we add mana to the items
                            string manaMessageText = "The Mana Stone gives " + manaPool.ToString("N0") + " points of mana to the following items: ";
                            for (int i = 0; i < itemsNeedingMana.Count; i++)
                            {
                                int manaToAdd = manaPool / (itemsNeedingMana.Count - i); // find out how much to add to this one item. 
                                manaPool = manaPool - manaToAdd; // take away our newly added amount from the total pool.
                                manaRemaining += AddManaToItem(itemsNeedingMana[i], manaToAdd);
                                manaMessageText += itemsNeedingMana[i].Name + ", ";
                            }

                            // remove the extra, final comma seperator and space
                            manaMessageText = manaMessageText.TrimEnd(',', ' ');

                            // append how much more, if any, mana is needed to max out equipped items
                            if (manaRemaining > 0)
                                manaMessageText += "\nYou need " + manaRemaining.ToString("N0") + " more mana to fully charge your items.";
                            else
                                manaMessageText += "\nYour items are fully charged.";

                            var manaMessage = new GameMessageSystemChat(manaMessageText, ChatMessageType.Broadcast);
                            player.Session.Network.EnqueueSend(manaMessage);
                        }

                        // Check if the stone gets destroyed...
                        DestroyManaStone(player, source);
                    }
                    else if (target.ItemMaxMana != null) // Can the item even have mana added to it?
                    { 
                        // add mana to the item
                        int manaAdded = (int)source.ItemCurMana;
                        AddManaToItem(target, manaAdded);

                        // Send message about how much mana was added to the item(s)
                        string manaMessageText = "The Mana Stone gives " + manaAdded.ToString("N0") + " points of mana to the following items: " + target.Name + ".";
                        var manaMessage = new GameMessageSystemChat(manaMessageText, ChatMessageType.Broadcast);
                        player.Session.Network.EnqueueSend(manaMessage);

                        // Check if the stone gets destroyed...
                        DestroyManaStone(player, source);
                    }
                }
                else // destroying an item, using it's mana to fill the mana stone
                {
                    // check if "Retained" (Leather applied to the item means it cannot be destroyed with a mana stone)
                    if (target.Retained == true)
                    {
                        // This message is in the client and will automatically fire if we're trusting the client. But the client tells porky pies!
                        var message = new GameMessageSystemChat("You cannot drain the mana of this item because it is \"Retained\".", ChatMessageType.Broadcast);
                        player.Session.Network.EnqueueSend(message);
                    }
                    else
                    {
                        // TODO: Check the target is within the player's control - Client should help us out here, but never trust...you know the saying

                        // Check the target has mana
                        if (target.ItemCurMana > 0)
                        {
                            // Add the mana into the stone
                            int manaDrained = (int)(target.ItemCurMana * source.ItemEfficiency);
                            source.ItemCurMana = manaDrained;
                            source.ItemMaxMana = manaDrained;
                            // message the user with how much was drained into the stone

                            var message = new GameMessageSystemChat("The Mana Stone drains " + manaDrained.ToString("N0") + " points of mana from the " + target.Name + ".", ChatMessageType.Broadcast);
                            player.Session.Network.EnqueueSend(message);

                            // destroy the target (Inventory system will handle burden change)
                            player.DestroyInventoryItem(target);

                            // add UI_EFFECTS_INT = 1, to signify the stone is "charged", send Evt_Qualities__UpdateInt_ID message on the item
                            source.UiEffects = UiEffects.Magical;
                            player.Session.Network.EnqueueSend(new GameMessagePublicUpdatePropertyInt(source.Sequences, source.Guid, PropertyInt.UiEffects, (int)UiEffects.Magical));
                        }
                    }
                }

                player.SendUseDoneEvent();
            });

            if (craftChain.FirstElement != null)
                craftChain.EnqueueChain();
        }

        /// <summary>
        /// Quick little function to add mana to an item without going over to the maximum mana and returns how much more is needed to fill.
        /// This could be moved elsewhere, but I can't think of any instance where mana can be added to an item that would be managed by a "recipe".
        /// </summary>
        /// <returns>The amount of mana needed to fill the item to max.</returns>
        private static int AddManaToItem(WorldObject item, int manaToAdd)
        {
            int newMana = 0;
            if (item.ItemCurMana != null)
                newMana = (int)item.ItemCurMana + manaToAdd;
            else
                newMana = manaToAdd;
            if (item.ItemMaxMana != null && newMana > item.ItemMaxMana)
                newMana = (int)item.ItemMaxMana;

            if (newMana > 0)
                item.ItemCurMana = newMana;

            if (item.ItemCurMana != null)
                return ((int)item.ItemMaxMana - (int)item.ItemCurMana);
            else
                return (int)item.ItemMaxMana;
        }

        /// <summary>
        /// Checks to see if a mana stone get destroyed or not and sends the appropriate events & messages to the player.
        /// </summary>
        private static void DestroyManaStone(Player player, WorldObject manaStone)
        {
            // Check to see if we destroyed the mana stone
            bool destroyManaStone = false;
            if (manaStone.ManaStoneDestroyChance >= 1)
                destroyManaStone = true;
            else if (_random.NextDouble() < manaStone.ManaStoneDestroyChance)
                destroyManaStone = true;

            // Destroy Mana Charge / Stone
            if (destroyManaStone == true)
            {
                player.DestroyInventoryItem(manaStone);
                var destroyMessage = new GameMessageSystemChat("The Mana Stone is destroyed.", ChatMessageType.Broadcast);
                player.Session.Network.EnqueueSend(destroyMessage);
            }
            else
            {
                // If not destroy...
                // Set cur & max mana to null
                manaStone.ItemCurMana = null;
                manaStone.ItemMaxMana = null;
                // Set UI_EFFECTS_INT = null -- removes the "magical" glow on the stone
                manaStone.UiEffects = null;
                player.Session.Network.EnqueueSend(new GameMessagePublicUpdatePropertyInt(manaStone.Sequences, manaStone.Guid, PropertyInt.UiEffects, (int)UiEffects.Undef));
            }
        }
    }
}
