/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.Windows.Forms.VisualStyles;
using DerethForever.Database;
using DerethForever.Actors;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Managers;

namespace DerethForever.Factories
{
    public class WorldObjectFactory
    {
        public static List<WorldObject> CreateWorldObjects(List<DataObject> sourceObjects)
        {
            var results = new List<WorldObject>();

            foreach (var dataObject in sourceObjects)
            {
                if (dataObject.GeneratorStatus ?? false)  // Generator
                {
                    dataObject.Location = dataObject.Location.InFrontOf(-2.0);
                    dataObject.Location.PositionZ = dataObject.Location.PositionZ - 0.5f;
                    results.Add(new Generator(new ObjectGuid(dataObject.DataObjectId), dataObject));
                    dataObject.GeneratorEnteredWorld = true;
                    var objectList = GeneratorFactory.CreateWorldObjectsFromGenerator(dataObject) ?? new List<WorldObject>();
                    objectList.ForEach(o => results.Add(o));
                    continue;
                }

                if (dataObject.Location != null)
                {
                    WorldObject wo = CreateWorldObject(dataObject);
                    if (wo != null)
                        results.Add(wo);
                }
            }
            return results;
        }

        public static WorldObject CreateWorldObject(DataObject dataO)
        {
            WeenieType objWeenieType = (WeenieType?)dataO.WeenieType ?? WeenieType.Generic;

            switch (objWeenieType)
            {
                case WeenieType.LifeStone:
                    return new Lifestone(dataO);
                case WeenieType.Door:
                    return new Door(dataO);
                case WeenieType.Portal:
                    return new Portal(dataO);
                case WeenieType.Book:
                    return new Book(dataO);
                // case WeenieType.PKModifier:
                //    return new PKModifier(dataO);
                case WeenieType.Cow:
                    return new Cow(dataO);
                case WeenieType.Creature:
                    return new Creature(dataO);
                case WeenieType.Container:
                    return new Container(dataO);
                case WeenieType.Scroll:
                    return new Scroll(dataO);
                case WeenieType.Vendor:
                    return new Vendor(dataO);
                case WeenieType.Coin:
                    return new Coin(dataO);
                case WeenieType.Key:
                    return new Key(dataO);
                case WeenieType.Food:
                    return new Food(dataO);
                case WeenieType.Gem:
                    return new Gem(dataO);
                default:
                    return new GenericObject(dataO);
            }
        }

        public static WorldObject CreateWorldObject(uint weenieId, ObjectGuid guid)
        {
            DataObject dataObject = DatabaseManager.World.GetDataObjectByWeenie(weenieId);
            if (dataObject == null)
                return null;

            var clone = (DataObject)dataObject.Clone(guid.Full);
            clone.SetDirtyFlags();
            return CreateWorldObject(clone);
        }

        public static WorldObject CreateNewWorldObject(uint weenieId)
        {
            WorldObject wo = CreateWorldObject(weenieId, GuidManager.NewItemGuid());
            return wo;
        }
    }
}
