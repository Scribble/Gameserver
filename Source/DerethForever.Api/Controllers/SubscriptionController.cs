/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DerethForever.Api.Common;
using DerethForever.Entity;
using DerethForever.Database;
using DerethForever.Entity.Enum;
using System.Linq;
using DerethForever.Api.Models;

namespace DerethForever.Api.Controllers
{
    /// <summary>
    /// controller for all actions of Subscriptions
    /// </summary>
    public class SubscriptionController : BaseController
    {
        /// <summary>
        /// gets a list of all subscriptions for the authenticated user.
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<Subscription>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Get()
        {
            var principal = Request.GetRequestContext().Principal;
            Guid accountGuid = Guid.Parse(principal.Identity.Name);
            var subs = DatabaseManager.Authentication.GetSubscriptionsByAccount(accountGuid);
            return Request.CreateResponse(HttpStatusCode.OK, subs);
        }

        /// <summary>
        /// gets a list of all subscriptions for the authenticated user.
        /// </summary>
        [HttpGet]
        [ApiAuthorize(AccessLevel.Admin)]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<Subscription>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "invalid account guid")]
        [SwaggerResponse(HttpStatusCode.NotFound, "specified account not found")]
        public HttpResponseMessage GetByAccount(string accountGuid)
        {
            Guid actualGuid;
            if (!Guid.TryParse(accountGuid, out actualGuid))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Invalid account guid." });
            
            var subs = DatabaseManager.Authentication.GetSubscriptionsByAccount(actualGuid);

            if (subs == null || subs.Count == 0)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            return Request.CreateResponse(HttpStatusCode.OK, subs);
        }

        /// <summary>
        /// creates a new subscription for the authenticated user with "Player" access level.  elevations 
        /// above player must be done manually by the server operator
        /// </summary>
        [HttpPost]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Subscription))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Create(string subscriptionName)
        {
            var principal = Request.GetRequestContext().Principal;
            Guid accountGuid = Guid.Parse(principal.Identity.Name);
            Subscription s = new Subscription();
            s.AccountGuid = accountGuid;
            s.Name = subscriptionName;
            s.AccessLevel = Entity.Enum.AccessLevel.Player;
            DatabaseManager.Authentication.CreateSubscription(s);
            return Request.CreateResponse(HttpStatusCode.OK, s);
        }

        /// <summary>
        /// updates the permission level of the specified account.  auth token must have Admin permissions
        /// to perform this action.  subscription guid is optional if the specified account only has 1
        /// subscription.
        /// </summary>
        [HttpPost]
        [ApiAuthorize(AccessLevel.Admin)]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "not allowed.", Type = typeof(SimpleMessage))]
        public HttpResponseMessage UpdatePermissions([FromBody] UpdatePermissionsRequest request)
        {
            Guid subGuid = Guid.Empty;
            bool validGuid = Guid.TryParse(request.SubscriptionGuid, out subGuid);
            
            if (!validGuid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "invalid subscription guid provided." });

            var sub = DatabaseManager.Authentication.GetSubscriptionByGuid(subGuid);

            if (sub == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new { message = "invalid subscription guid provided." });

            DatabaseManager.Authentication.UpdateSubscriptionAccessLevel(sub.SubscriptionId, (AccessLevel)request.NewPermissions);

            return Request.CreateResponse(HttpStatusCode.OK, new { message = "permissions updated." });
        }

    }
}
