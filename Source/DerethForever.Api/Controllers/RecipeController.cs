/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Api.Common;
using DerethForever.Api.Models;
using DerethForever.Database;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace DerethForever.Api.Controllers
{
    /// <summary>
    /// controller for all Recipe methods in the API
    /// </summary>
    public class RecipeController : BaseController
    {
        /// <summary>
        /// gets the first 100 recipes matching the criteria
        /// </summary>
        [HttpPost]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<Recipe>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Search(SearchRecipesCriteria criteria)
        {
            var recipes = DatabaseManager.World.SearchRecipes(criteria).Take(100).ToList();
            return Request.CreateResponse(HttpStatusCode.OK, recipes);
        }

        /// <summary>
        /// gets a single recipe
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Recipe))]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Get(Guid recipeGuid)
        {
            var recipe = DatabaseManager.World.GetRecipe(recipeGuid);

            if (recipe != null)
                return Request.CreateResponse(HttpStatusCode.OK, recipe);
            else
                return Request.CreateResponse(HttpStatusCode.NotFound, new SimpleMessage("requested recipe not found."));
        }

        /// <summary>
        /// saves an existing recipe
        /// </summary>
        [HttpPost]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Update(Recipe recipe)
        {
            recipe.HasEverBeenSavedToDatabase = true;
            DatabaseManager.World.UpdateRecipe(recipe);

            return Request.CreateResponse(HttpStatusCode.OK, new SimpleMessage("Recipe updated!"));
        }

        /// <summary>
        /// creates a new recipe.  the guid on the recipe will be replaced.
        /// </summary>
        [HttpPost]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Create(Recipe recipe)
        {
            recipe.RecipeGuid = Guid.NewGuid();
            recipe.HasEverBeenSavedToDatabase = true;
            DatabaseManager.World.CreateRecipe(recipe);

            return Request.CreateResponse(HttpStatusCode.OK, new SimpleMessage("Recipe created!"));
        }

        /// creates a new recipe.  the guid on the recipe will be replaced.
        /// </summary>
        [HttpDelete]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "specified recipeGuid could not be found.")]
        public HttpResponseMessage Delete(Guid recipeGuid)
        {
            DatabaseManager.World.DeleteRecipe(recipeGuid);
            return Request.CreateResponse(HttpStatusCode.OK, new SimpleMessage("Recipe deleted."));
        }
    }
}
