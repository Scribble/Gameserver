/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Enum
{
    public enum BspNodeType
    {
        Number      = 589505315,  // 0x23232323, "####"

        BPIN        = 1112557902, // 0x4250494E, "BPIN"
        BPIn        = 1112557934, // 0x4250496E, "BPIn" - acclient.c 363229 - 32 more than "BPIN"
        BPnN        = 1112567374, // 0x42506E4E, "BPnN" - acclient.c 363235 - 9440 more than BPIn
        BPnn        = 1112567406, // 0x42506E6E, "BPnn"
        BpIN        = 1114655054, // 0x4270494E, "BpIN"
        BpnN        = 1114664526, // 0x42706E4E, "BpnN"

        Leaf        = 1279607110, // 0x4C454146, "LEAF"
        Port        = 1347375700, // 0x504F5254, "PORT"
    }
}
