/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

namespace DerethForever.ClientLib.Enum
{
    /// <summary>
    /// acclient.h 2856
    /// </summary>
    public enum MovementTypes
    {
        Invalid                = 0x0,
        RawCommand             = 0x1,
        InterpretedCommand     = 0x2,
        StopRawCommand         = 0x3,
        StopInterpretedCommand = 0x4,
        StopCompletely         = 0x5,
        MoveToObject           = 0x6,
        MoveToPosition         = 0x7,
        TurnToObject           = 0x8,
        TurnToHeading          = 0x9,
    }
}
