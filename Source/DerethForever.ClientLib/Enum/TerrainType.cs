/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

namespace DerethForever.ClientLib.Enum
{
    public enum TerrainType
    {
        BarrenRock           = 0x0,
        Grassland            = 0x1,
        Ice                  = 0x2,
        LushGrass            = 0x3,
        MarshSparseSwamp     = 0x4,
        MudRichDirt          = 0x5,
        ObsidianPlain        = 0x6,
        PackedDirt           = 0x7,
        PatchyDirt           = 0x8,
        PatchyGrassland      = 0x9,
        SandYellow           = 0xA,
        SandGrey             = 0xB,
        SandRockStrewn       = 0xC,
        SedimentaryRock      = 0xD,
        SemiBarrenRock       = 0xE,
        Snow                 = 0xF,
        WaterRunning         = 0x10,
        WaterStandingFresh   = 0x11,
        WaterShallowSea      = 0x12,
        WaterShallowStillSea = 0x13,
        WaterDeepSea         = 0x14,
        Reserved21           = 0x15,
        Reserved22           = 0x16,
        Reserved23           = 0x17,
        Reserved24           = 0x18,
        Reserved25           = 0x19,
        Reserved26           = 0x1A,
        Reserved27           = 0x1B,
        Reserved28           = 0x1C,
        Reserved29           = 0x1D,
        Reserved30           = 0x1E,
        Reserved31           = 0x1F,
        RoadType             = 0x20,
    }
}
