/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.ClientLib.Enum
{
    /// <summary>
    /// pulled from the PDB with the microsoft-pdb tool
    /// </summary>
    [Flags]
    public enum ObjectInfoState
    {
        Default         = 0x0000,
        Contact         = 0x0001,
        OnWalkable      = 0x0002,
        IsViewer        = 0x0004,
        PathClipped     = 0x0008,
        FreeRotate      = 0x0010,
        Missing         = 0x0020,
        PerfectClip     = 0x0040,
        IsImpenetrable  = 0x0080,
        IsPlayer        = 0x0100,
        EdgeSlide       = 0x0200,
        IgnoreCreatures = 0x0400,
        IsPK            = 0x0800,
        IsPkLite        = 0x1000
    }
}
