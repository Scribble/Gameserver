using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.DatUtil
{
    public enum DatTypes
    {
        Unknown = 0,
        Cell = 1,
        Portal = 2,
        Highres = 3,
        local_Lang = 4
    }
}
