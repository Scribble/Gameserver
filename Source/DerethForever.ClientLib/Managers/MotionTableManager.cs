/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System.Collections.Generic;
using DerethForever.ClientLib.Entity;

namespace DerethForever.ClientLib.Managers
{
    public class MotionTableManager
    {
        /// <summary>
        /// +0x000 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObj;

        // +0x004 table            : Ptr32 CMotionTable

        //+0x008 state            : MotionState

        /// <summary>
        /// +0x020 animation_counter : Int4B
        /// </summary>
        public uint AnimationCounter;

        /// <summary>
        /// +0x024 pending_animations : DLList<MotionTableManager::AnimNode>
        /// </summary>
        public List<AnimationNode> PendingAnimations;

        public void UseTime()
        {

        }
        /// <summary>
        /// ----- (0051BDD0) --------------------------------------------------------
        /// void __thiscall MotionTableManager::HandleEnterWorld(MotionTableManager*this, CSequence* seq)
        /// acclient.c 329949
        /// </summary>
        public void HandleEnterWorld(Sequence sequence)
        {
            // TODO: Finish this - Coral Golem
        }
    }
}
