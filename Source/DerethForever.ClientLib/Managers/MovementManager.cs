/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using DerethForever.ClientLib.Entity;

namespace DerethForever.ClientLib.Managers
{
    public class MovementManager
    {
        /// <summary>
        /// +0x000 motion_interpreter : Ptr32 CMotionInterp
        /// </summary>
        public MotionInterp MotionInterp;

        /// <summary>
        /// +0x004 moveto_manager   : Ptr32 MoveToManager
        /// </summary>
        public MoveToManager MoveToManager;

        /// <summary>
        /// +0x008 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObj;

        /// <summary>
        /// //+0x00c weenie_obj       : Ptr32 CWeenieObject
        /// </summary>
        public WeenieObject WeenieObj;


        // TODO: Coral Golem - pick up here 3/22/2018

        public void UseTime(PhysicsObject physicsObject)
        {
        }
    }
}
