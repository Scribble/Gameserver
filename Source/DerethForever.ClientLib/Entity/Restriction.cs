/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// no explicit client class - PackableHashTable(ulong, ulong)
    /// </summary>
    public class Restriction
    {
        /// <summary>
        /// landcell being restricted.  since interiors (building cells) are always their own cell, this
        /// is convenient in that you can restrict the entire thing.
        /// </summary>
        public uint LandCellId;

        /// <summary>
        /// id of the object controlling the cell
        /// </summary>
        public uint ControllerId;

        /// <summary>
        /// not an explicit client function, but inlined in CLandblockInfo::Unpack from lines 351222-351234
        /// </summary>
        public static Restriction Unpack(BinaryReader reader)
        {
            Restriction r = new Restriction();
            r.LandCellId = reader.ReadUInt32();
            r.ControllerId = reader.ReadUInt32();
            return r;
        }
    }
}
