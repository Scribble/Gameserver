/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CRegionDesc in the client
    /// </summary>
    public class RegionDescription : DatabaseObject
    {
        /// <summary>
        /// +0x038 region_number    : Uint4B
        /// </summary>
        public uint RegionNumber;

        /// <summary>
        /// +0x03c region_name      : AC1Legacy::PStringBase(char)
        /// </summary>
        public string RegionName;

        /// <summary>
        /// +0x040 version          : Uint4B
        /// </summary>
        public uint Version;

        /// <summary>
        /// +0x044 minimize_pal     : Int4B
        /// </summary>
        public int MinimizePalette;

        /// <summary>
        /// +0x048 parts_mask       : Uint4B
        /// </summary>
        public RegionPartsMask PartsMask;

        /// <summary>
        /// client source uknown
        /// </summary>
        public uint FileId { get; set; }

        /// <summary>
        /// +0x04c file_info        : Ptr32 FileNameDesc
        /// </summary>
        public FileNameDescription FileInfo;

        /// <summary>
        /// +0x050 sky_info         : Ptr32 SkyDesc
        /// </summary>
        public SkyDescription SkyInfo;

        /// <summary>
        /// +0x054 sound_info       : Ptr32 CSoundDesc
        /// </summary>
        public SoundDescription SoundInfo;

        /// <summary>
        /// +0x058 scene_info       : Ptr32 CSceneDesc
        /// </summary>
        public SceneDescription SceneInfo;

        /// <summary>
        /// +0x05c terrain_info     : Ptr32 CTerrainDesc
        /// </summary>
        public TerrainDescription TerrainInfo;

        /// <summary>
        /// +0x060 encounter_info   : Ptr32 CEncounterDesc
        /// </summary>
        public EncounterDescription EncounterInfo;

        /// <summary>
        /// +0x064 water_info       : Ptr32 WaterDesc
        /// </summary>
        public WaterDescription WaterInfo;

        /// <summary>
        /// +0x068 fog_info         : Ptr32 FogDesc
        /// </summary>
        public FogDescription FogInfo;

        /// <summary>
        /// +0x06c dist_fog_info    : Ptr32 DistanceFogDesc
        /// </summary>
        public DistanceFogDescription DistanceFogInfo;

        /// <summary>
        /// +0x070 region_map_info  : Ptr32 RegionMapDesc
        /// </summary>
        public RegionMapDescription RegionMapInfo;

        /// <summary>
        /// +0x074 region_misc      : Ptr32 RegionMisc
        /// </summary>
        public RegionMiscellaneous RegionMiscellaneous;
        
        /// <summary>
        /// //----- (004FF440) --------------------------------------------------------
        /// void **__thiscall CRegionDesc::UnPack(CRegionDesc *this, void **addr, unsigned int size)
        /// acclient.c 299776
        /// </summary>
        public static RegionDescription Unpack(BinaryReader reader)
        {
            RegionDescription reg = new RegionDescription();
            reg.FileId = reader.ReadUInt32();
            reg.RegionNumber = reader.ReadUInt32();
            reg.Version = reader.ReadUInt32();
            reg.RegionName = reader.ReadPString();
            
            // next 8 dwords (32 bytes) are (sorta) throw-away information.  they contain static information
            // that we presume to be constant for our purposes, so we're going to skip over them.
            var garbage = reader.ReadBytes(32);

            // not going to double buffer this, just assign to the static collection directly
            for (int i = 0; i < 256; i++)
                LandDefs.LandHeightTable[i] = reader.ReadSingle();
            
            GameTime.CurrentGameTime = GameTime.Unpack(reader);

            // client loads this into m_pNext, but it's clearly a mask for what elements to read next
            // plus, mp_Next is part of the base database object linked list structure.  probably decompiler
            // issues, as this function is littered with them.
            reg.PartsMask = (RegionPartsMask)reader.ReadUInt32();

            if (reg.PartsMask.HasFlag(RegionPartsMask.Sky))
                reg.SkyInfo = SkyDescription.Unpack(reader);

            if (reg.PartsMask.HasFlag(RegionPartsMask.Sound))
                reg.SoundInfo = SoundDescription.Unpack(reader);

            if (reg.PartsMask.HasFlag(RegionPartsMask.Scene))
            {
                reg.SceneInfo = new SceneDescription();

                uint numSceneTypes = reader.ReadUInt32();
                for (uint i = 0; i < numSceneTypes; i++)
                {
                    SceneType st = new SceneType();
                    int soundIndex = reader.ReadInt32();

                    if (soundIndex != -1)
                        st.AmbientSoundTable = reg.SoundInfo.SoundTables[soundIndex];

                    uint numScenes = reader.ReadUInt32();
                    for (int j = 0; j < numScenes; j++)
                        st.Scenes.Add(reader.ReadUInt32());

                    reg.SceneInfo.SceneTypes.Add(st);
                }
            }
            
            reg.TerrainInfo = new TerrainDescription();

            uint numTerrains = reader.ReadUInt32();
            for (int i = 0; i < numTerrains; i++)
            {
                TerrainType tt = new TerrainType();
                tt.TerrainName = reader.ReadPString();
                tt.TerrainColor = reader.ReadUInt32();

                uint numSceneTypes = reader.ReadUInt32();
                for (int j = 0; j < numSceneTypes; j++)
                {
                    int sceneIndex = reader.ReadInt32();
                    if (sceneIndex != -1)
                        tt.SceneTypes.Add(reg.SceneInfo.SceneTypes[sceneIndex]);

                    reg.TerrainInfo.TerrainTypes.Add(tt);
                }
            }

            reg.TerrainInfo.LandSurface = LandSurface.Unpack(reader);

            if (reg.PartsMask.HasFlag(RegionPartsMask.Misc))
                reg.RegionMiscellaneous = RegionMiscellaneous.Unpack(reader);

            return reg;
        }
    }
}
