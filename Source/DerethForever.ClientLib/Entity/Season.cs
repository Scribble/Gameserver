/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// Season in the client
    /// </summary>
    public class Season
    {
        /// <summary>
        /// +0x000 season_name      : AC1Legacy::PStringBase<char>
        /// </summary>
        public string Name;

        /// <summary>
        /// +0x004 begin            : Int4B
        /// </summary>
        public int Begin;

        /// <summary>
        /// //----- (005A6CE0) --------------------------------------------------------
        /// int __thiscall Season::UnPack(Season *this, void **addr, unsigned int *size)
        /// acclient.c 463882
        /// </summary>
        public static Season Unpack(BinaryReader reader)
        {
            Season s = new Season();
            s.Begin = reader.ReadInt32();
            s.Name = reader.ReadPString();
            return s;
        }
    }
}
