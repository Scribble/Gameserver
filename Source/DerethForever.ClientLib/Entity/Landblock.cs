/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using DerethForever.ClientLib.DatUtil;
using DerethForever.ClientLib.Enum.LandDefs;
using log4net;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CLandBlock in the client.  the client version of this implemented dual inheritance of
    /// DBObj and CLandblockStruct.  we've implemented LandblockStruct as an interface with
    /// extension methods in order to give the illusion of multiple inheritance here in C#
    /// </summary>
    public class Landblock : DatabaseObject, LandblockStruct
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// intentionally private.  Landblocks are only valid if loaded from the dat file via Unpack.
        /// </summary>
        private Landblock()
        {
        }

        /// <summary>
        /// one and only way to get landblocks.
        /// </summary>
        public static Landblock Load(uint landblockId)
        {
            uint fileId = ((landblockId & 0xFFFF0000) | 0x0000FFFF);
            Landblock lb = CellDatReader.Current.Unpack<Landblock>(fileId);

            lb.Generate();
            lb.InitializeStaticObjects();
            lb.InitializeBuildings();

            return lb;
        }

        /// <summary>
        /// land_uvs - static 4-element array
        /// </summary>
        public static UvMapping[] LandUvMapping = new UvMapping[4];

        /// <summary>
        /// =00000000`00844acc use_encounter_info : Int4B
        /// </summary>
        public static bool UseEncounterInfo;

        /// <summary>
        /// 0x038 vertex_lighting : Ptr32 RGBColor
        /// </summary>
        public List<RgbColor> VertexLighting { get; set; } = new List<RgbColor>();

        /// <summary>
        /// heading index into LandDefs.Direction array
        /// 0x03c trans_dir : LandDefs::Direction
        /// </summary>
        public Direction TransDirection { get; set; }

        /// <summary>
        /// 0x040 side_vertex_count
        /// </summary>
        public int SideVertexCount { get; set; } = 9;

        /// <summary>
        /// 0x044 side_polygon_count
        /// </summary>
        public int SidePolygonCount { get; set; }

        /// <summary>
        /// 0x048 side_cell_count
        /// </summary>
        public int SideCellCount { get; set; }

        /// <summary>
        /// 0x04C water_type
        /// </summary>
        public Enum.WaterType WaterType { get; set; }

        /// <summary>
        /// Heightmap.  index is x + y*row.
        /// 0x050 height : Ptr32 UChar
        /// </summary>
        public byte[,] HeightMap { get; set; } = new byte[9, 9];

        /// <summary>
        /// terrain map
        /// 0x054 terrain : Ptr32 Uint2B
        /// </summary>
        public ushort[,] Terrain { get; set; } = new ushort[9, 9];

        /// <summary>
        /// 0x058 vertex_array : CVertexArray
        /// </summary>
        public VertexArray VertexArray { get; set; } = new VertexArray();

        /// <summary>
        /// 0x080 polygons
        /// </summary>
        public List<Polygon> Polygons { get; set; } = new List<Polygon>();

        /// <summary>
        /// 0x084 num_surface_strips : Uint4B
        /// </summary>
        public uint NumSurfaceStrips => (uint)SurfaceStrips.Count;

        /// <summary>
        /// 0x088 surface_strips : Ptr32 CSurfaceTriStrips
        /// </summary>
        public List<SurfaceTriStrip> SurfaceStrips { get; set; } = new List<SurfaceTriStrip>();

        /// <summary>
        /// 0x08C block_surface_index : uint4B
        /// </summary>
        public uint BlockSurfaceIndex { get; set; }

        /// <summary>
        /// deviation from client: 2-D array instead of single array with calculated index
        /// 0x090 lcell : Ptr32 CLandCell
        /// </summary>
        public LandblockCell[,] LandblockCells { get; set; } = new LandblockCell[8, 8];

        /// <summary>
        /// direction of the triangle cut across this landblock.
        /// 0x094 SWtoNEcut : Ptr32 Int4B
        /// </summary>
        public bool[,] SouthWestToNorthEastCut { get; set; } = new bool[8, 8];

        /// <summary>
        /// 0x098 block_cord : SqCoord
        /// </summary>
        public SqCoord BlockCoord;

        /// <summary>
        /// 0x0a0 block_frame : Frame
        /// </summary>
        public Frame BlockFrame;

        /// <summary>
        /// 0x0e0 max_zval : Float
        /// </summary>
        public float MaxZ;

        /// <summary>
        /// 0x034 min_zval : Float
        /// </summary>
        public float MinZ;

        /// <summary>
        /// 0x0e8 dyn_objs_init_done : Int4B
        /// </summary>
        public bool DynamicObjectsInitialized;

        /// <summary>
        /// 0x0ec lbi_exists : Int4B
        /// </summary>
        public bool LandblockInfoExists;

        /// <summary>
        /// heading index into LandDefs.Direction array
        /// 0x0f0 dir : LandDefs::Direction
        /// </summary>
        public Direction Direction;

        /// <summary>
        /// 0x0f4 closest : SqCoord
        /// </summary>
        public SqCoord Closest;

        /// <summary>
        /// 0x0fc in_view : BoundingType
        /// </summary>
        public Enum.BoundingType InView;

        /// <summary>
        /// 0x100 lbi
        /// </summary>
        public LandblockInfo LandblockInfo;

        /// <summary>
        /// 0x104 num_static_objects : Uint4B
        /// </summary>
        public uint NumStaticObjects => (uint)StaticObjects.Count;

        /// <summary>
        /// 0x108 static_objects : DArray&lt;CPhysicsObj *&gt;
        /// </summary>
        public List<PhysicsObject> StaticObjects { get; set; } = new List<PhysicsObject>();

        /// <summary>
        /// 0x118 num_buildings : Uint4B
        /// </summary>
        public uint NumBuildings => (uint)Buildings.Count;

        /// <summary>
        /// 0x11c buildings : Ptr32 Ptr32 CBuildingObj
        /// </summary>
        public List<Building> Buildings { get; set; } = new List<Building>();

        /// <summary>
        /// 0x120 stab_num : Uint4B
        /// </summary>
        public uint StabNum => (uint)StabList.Count;

        /// <summary>
        /// 0x124 stablist : Ptr32 Uint4B
        /// </summary>
        public List<uint> StabList = new List<uint>();

        /// <summary>
        /// 0x128 draw_array : Ptr32 Ptr32 CLandCell
        /// </summary>
        public List<LandblockCell> DrawArray = new List<LandblockCell>();
        
        /// <summary>
        /// 0x128 draw_array : Ptr32 Ptr32 CLandCell
        /// </summary>
        public uint DrawArraySize => (uint)DrawArray.Count;

        /// <summary>
        /// //----- (0052F310) --------------------------------------------------------
        /// int __thiscall CLandBlock::UnPack(CLandBlock*this, void** addr, unsigned int size)
        /// acclient.c 351521
        /// </summary>
        public static Landblock Unpack(BinaryReader reader, DatReader datReader)
        {
            Landblock lb = new Landblock();

            lb.Id = reader.ReadUInt32();
            uint hasLbi = reader.ReadUInt32();
            lb.LandblockInfoExists = (hasLbi != 0);

            // this is an implied cast into LandblockStruct
            lb.Unpack(reader);

            if (lb.LandblockInfoExists)
            {
                uint infoFile = (lb.Id & 0xFFFF0000) | 0xFFFE;
                lb.LandblockInfo = datReader.Unpack<LandblockInfo>(infoFile);
            }
            return lb;

        }

        /// <summary>
        /// //----- (00530A40) --------------------------------------------------------
        /// void __thiscall CLandBlock::init_static_objs(CLandBlock *this, LongNIValHash(unsigned long) *hash)
        /// acclient.c 352787
        /// </summary>
        private void InitializeStaticObjects()
        {
            if (StaticObjects.Count > 0)
                return; // already initialized

            if (LandblockInfo == null)
                return;

            for (int i = 0; i < LandblockInfo.NumObjects; i++)
            {
                var obj = PhysicsObject.MakeObject(LandblockInfo.ObjectIds[i], 0, false);
                if (obj != null)
                {
                    Position p = new Position();
                    p.CellId = Id;
                    p.Frame = new Frame(LandblockInfo.ObjectFrames[i]); // we have classes here instead of a struct, have to new it up
                    
                    Vector3 loc = p.Frame.Origin;
                    uint cellId = Id;

                    bool mapped = LandDefs.AdjustToOutside(ref cellId, ref loc);
                    var cell = GetLandCell(mapped ? cellId : 0);

                    if (cell != null)
                    {
                        obj.AddObjectToCell(cell, new Frame(p.Frame));
                        AddStaticObject(obj);
                    }
                    else
                    {
                        throw new Exception($"Unable to place static object id {LandblockInfo.ObjectIds[i]}");
                    }
                }
                else
                {
                    throw new Exception($"Unable to load static object id {LandblockInfo.ObjectIds[i]}");
                }
            }

            for (int x = 0; x < SideCellCount; x++)
            {
                for (int y = 0; y < SideCellCount; y++)
                {
                    // apply restrictions ~ client line 352878
                }
            }

            // client checks a constant here, i'm skipping it
            GetLandScenes();
        }

        /// <summary>
        /// //----- (00530460) --------------------------------------------------------
        /// void __thiscall CLandBlock::get_land_scenes(CLandBlock *this)
        /// acclient.c 352530
        /// </summary>
        private void GetLandScenes()
        {
            // todo implment Landblock.GetLandScenes
        }
        
        /// <summary>
        /// //----- (0052FD80) --------------------------------------------------------
        /// void __thiscall CLandBlock::init_buildings(CLandBlock *this)
        /// acclient.c 352114
        /// </summary>
        private void InitializeBuildings()
        {
            uint maxStab = 0;

            // i THINK this is a basic dungeon check
            if (SideCellCount != 8)
                return;

            if (LandblockInfo == null)
                return;

            for (int i = 0; i < LandblockInfo.NumBuildings; i++)
            {
                var bInfo = LandblockInfo.Buildings[i];
                var b = Building.MakeBuilding(bInfo.Id, bInfo.Portals, bInfo.NumLeaves);
                Position p = new Position() { CellId = Id, Frame = new Frame(bInfo.Frame) };
                Vector3 loc = p.Frame.Origin;
                uint cellId = Id;
                bool adjusted = LandDefs.AdjustToOutside(ref cellId, ref loc);

                var cell = GetLandCell(adjusted ? cellId : 0);
                if (cell != null)
                {
                    p.CellId = cellId;
                    b.SetInitialFrame(p.Frame);
                    b.AddToCell((SortCell)cell);
                    Buildings.Add(b);
                    b.AddToStabList(StabList, ref maxStab);
                }
                else
                {
                    throw new Exception($"Unable to load or place building id {LandblockInfo.Buildings[i].Id}");
                }
            }
        }

        /// <summary>
        /// client says the return is an int, but it's really a pointer.  in practice, it is cast as
        /// an ObjectCell, so we'll return that for now.  suspect that it should be LandblockCell.
        /// //----- (0052FD20) --------------------------------------------------------
        /// signed int __thiscall CLandBlock::get_landcell(CLandBlock *this, unsigned int cell_id)
        /// acclient.c 352098
        /// </summary>
        public ObjectCell GetLandCell(uint cellId)
        {
            int x = 0;
            int y = 0;

            LandDefs.GidToLCoord(cellId, ref x, ref y);

            var cell = LandblockCells[x & 7, y & 7];

            if (cell.Id == cellId)
                return cell;

            return null;
        }

        /// <summary>
        /// //----- (0052F910) --------------------------------------------------------
        /// void __thiscall CLandBlock::add_static_object(CLandBlock *this, CPhysicsObj *object)
        /// acclient.c 351857
        /// </summary>
        public void AddStaticObject(PhysicsObject physicsObject)
        {
            StaticObjects.Add(physicsObject);
        }
    }
}
