/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.DatUtil;
using log4net;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CLandBlockInfo from the client
    /// </summary>
    public class LandblockInfo : DatabaseObject
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// intentionally private.  LandblockInfo objects must be generated from the dat files
        /// </summary>
        private LandblockInfo()
        {   
        }

        /// <summary>
        /// 0x038 num_objects : Uint4B
        /// </summary>
        public uint NumObjects => (uint)ObjectIds.Count;

        /// <summary>
        /// 0x03c object_ids : Ptr32 IDClass&lt;_tagDataID,32,0&gt;
        /// </summary>
        public List<uint> ObjectIds { get; set; } = new List<uint>();

        /// <summary>
        /// +0x040 object_frames    : Ptr32 Frame+0x040 object_frames    : Ptr32 Frame
        /// </summary>
        public List<Frame> ObjectFrames { get; set; } = new List<Frame>();

        /// <summary>
        /// +0x044 num_buildings    : Uint4B
        /// </summary>
        public uint NumBuildings => (uint)Buildings.Count;

        /// <summary>
        /// +0x048 buildings        : Ptr32 Ptr32 BuildInfo
        /// </summary>
        public List<BuildingInfo> Buildings = new List<BuildingInfo>();

        /// <summary>
        /// +0x04c restriction_table : Ptr32 PackableHashTable&lt;unsigned long,unsigned long&gt;
        /// </summary>
        public List<Restriction> RestrictionTable = new List<Restriction>();

        /// <summary>
        /// +0x050 cell_ownership   : Ptr32 PackableHashTable&lt;unsigned long,PackableList&lt;unsigned long&gt; &gt;
        /// </summary>
        public uint CellOwnership;

        /// <summary>
        /// normally this would just check the list length, but this list applies to dungeons which
        /// were traditionally lazy-loaded via streaming content
        /// +0x054 num_cells        : Uint4B
        /// </summary>
        public uint NumCells { get; set; }

        /// <summary>
        /// +0x058 cell_ids         : Ptr32 Uint4B
        /// </summary>
        public List<uint> CellIds = new List<uint>();

        /// <summary>
        /// +0x05c cells            : Ptr32 Ptr32 CEnvCell
        /// </summary>
        public List<EnvironmentCell> Cells { get; set; } = new List<EnvironmentCell>();

        /// <summary>
        /// //----- (0052ECA0) --------------------------------------------------------
        /// int __thiscall CLandBlockInfo::UnPack(CLandBlockInfo*this, void** addr, unsigned int size)
        /// acclient.c 351049
        /// </summary>
        public static LandblockInfo Unpack(BinaryReader reader)
        {
            LandblockInfo info = new LandblockInfo();
            info.Id = reader.ReadUInt32();
            
            info.NumCells = reader.ReadUInt32();
            uint numObjects = reader.ReadUInt32();

            for (uint i = 0; i < numObjects; i++)
            {
                info.ObjectIds.Add(reader.ReadUInt32());
                info.ObjectFrames.Add(Frame.Unpack(reader));
            }

            ushort numBuildings = reader.ReadUInt16();
            ushort buildingFlags = reader.ReadUInt16();

            for (ushort i = 0; i < numBuildings; i++)
            {
                BuildingInfo bi = new BuildingInfo();
                info.Buildings.Add(bi);

                bi.Id = reader.ReadUInt32();
                bi.Frame = Frame.Unpack(reader);
                bi.NumLeaves = reader.ReadUInt32();

                uint numPortals = reader.ReadUInt32();

                for (int j = 0; j < numPortals; j++)
                    bi.Portals.Add(BuildingPortal.Unpack(reader, info.Id & 0xFFFF0000));
            }

            if ((buildingFlags & 1) != 0)
            {
                ushort numRestrictions = reader.ReadUInt16();
                reader.ReadUInt16(); // static 8, line 351225

                for (int i = 0; i < numRestrictions; i++)
                    info.RestrictionTable.Add(Restriction.Unpack(reader));
            }

            // this part isn't technically part of unpack, but we want to force it
            // on the server ahead of time.  only problem is, things may be missing
            // so we wrap it in a try block
            for (uint i = 0; i < info.NumCells; i++)
            {
                uint cellId = (info.Id & 0xFFFF0000) + 0x0100u + i;
                try
                {
                    var cell = CellDatReader.Current.Unpack<EnvironmentCell>(cellId);
                    info.Cells.Add(cell);
                }
                catch
                {
                    log.Error($"Unable to load cell {cellId:X4}");
                }
            }
            return info;
        }
    }
}
