/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    public class CylinderSphere
    {
        /// <summary>
        /// +0x000 low_pt           : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Origin { get; set; }

        /// <summary>
        /// +0x00c height           : Float
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// +0x010 radius           : Float
        /// </summary>
        public float Radius { get; set; }

        public CylinderSphere(Vector3 origin, float radius, float height)
        {
            this.Origin = origin;
            this.Radius = radius;
            this.Height = height;
        }

        /// <summary>
        /// //----- (0053A960) --------------------------------------------------------
        /// int __thiscall CCylSphere::UnPack(CCylSphere*this, void** addr, unsigned int size)
        /// acclient.c 361548
        /// </summary>
        public static CylinderSphere Unpack(BinaryReader reader)
        {
            CylinderSphere cs = new CylinderSphere(reader.ReadVector(), reader.ReadSingle(), reader.ReadSingle());
            return cs;
        }
    }
}
