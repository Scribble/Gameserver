/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CWeenieObject in the client
    /// </summary>
    public class WeenieObject
    {
        /// <summary>
        /// +0x008 id               : Uint4B
        /// </summary>
        public uint Id { get; set; }

        /// <summary>
        /// +0x018 update_time      : Float
        /// </summary>
        public double UpdateTime { get; set; }

        public bool IsCreature()
        {
            return true;
            // TODO: Place holder until we wire this up with DataObject via interface Coral Golem
        }

        /// <summary>
        /// ----- (0058C560) --------------------------------------------------------
        /// bool __thiscall ACCWeenieObject::InqRunRate(ACCWeenieObject*this, float* rate)
        /// </summary>
        /// <returns></returns>
        public bool InqRunRate(ref float rate)
        {
            // TODO:  Once we have access to the DataObject we just need call the method to get this
            // Place Holder value for now.
            rate = Constants.DefaultSpeed;
            return true;
        }
    }
}
