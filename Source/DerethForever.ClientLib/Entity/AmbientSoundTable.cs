/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AmbientSTBDesc in the client
    /// </summary>
    public class AmbientSoundTable
    {
        /// <summary>
        /// private because unpack is the only valid source of this class
        /// </summary>
        private AmbientSoundTable()
        {
        }

        /// <summary>
        /// 0x000 stb_id : IDClass
        /// </summary>
        public uint SoundTableId;

        /// <summary>
        /// 0x004 stb_not_found : Int4B
        /// </summary>
        public bool NotFound;

        /// <summary>
        /// 0x008 ambient_sounds : AC1Legacy::SmartArray(AmbientSoundDesc *)
        /// </summary>
        public List<AmbientSound> AmbientSounds = new List<AmbientSound>();

        /// <summary>
        /// 0x014 sound_table : Ptr32 CSoundTable
        /// </summary>
        public uint SoundTable;

        /// <summary>
        /// 0x018 play_count : Uint4B
        /// </summary>
        public uint PlayCount;

        /// <summary>
        /// //----- (005518F0) --------------------------------------------------------
        /// int __thiscall AmbientSTBDesc::UnPack(AmbientSTBDesc *this, void **addr, unsigned int *size)
        /// acclient.c 384535
        /// </summary>
        public static AmbientSoundTable Unpack(BinaryReader reader)
        {
            AmbientSoundTable s = new AmbientSoundTable();

            s.SoundTableId = reader.ReadUInt32();

            uint num = reader.ReadUInt32();
            for (uint i = 0; i < num; i++)
                s.AmbientSounds.Add(AmbientSound.Unpack(reader));

            return s;
        }
    }
}
