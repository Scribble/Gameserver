/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AnimFrame in the client
    /// </summary>
    public class AnimationFrame
    {
        /// <summary>
        /// 0x000 frame : Ptr32 AFrame
        /// </summary>
        public List<Frame> Frames { get; set; } = new List<Frame>();

        /// <summary>
        /// 0x004 num_frame_hooks : Uint4B
        /// </summary>
        public uint NumFrameHooks => (uint)Frames.Count;

        /// <summary>
        /// 0x008 hooks : Ptr32 CAnimHook
        /// </summary>
        public List<AnimationHook> Hooks { get; set; } = new List<AnimationHook>();

        /// <summary>
        /// 0x00c num_parts : Uint4B
        /// </summary>
        public uint NumParts => (uint)Hooks.Count;

        /// <summary>
        /// //----- (0051F8E0) --------------------------------------------------------
        /// int __thiscall AnimFrame::UnPack(AnimFrame *this, unsigned int _num_parts, void **addr, unsigned int size)
        /// acclient.c 333870
        /// </summary>
        public static AnimationFrame Unpack(uint numParts, BinaryReader reader)
        {
            AnimationFrame a = new AnimationFrame();

            for (uint i = 0; i < numParts; i++)
                a.Frames.Add(Frame.Unpack(reader));

            uint numHooks = reader.ReadUInt32();

            for (uint i = 0; i < numHooks; i++)
                a.Hooks.Add(AnimationHook.Unpack(reader));

            return a;
        }
    }
}
