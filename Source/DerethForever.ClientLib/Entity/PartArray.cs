/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.Numerics;
using DerethForever.ClientLib.DatUtil;
using DerethForever.ClientLib.Enum;
using DerethForever.ClientLib.Managers;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CPartArray in the client
    /// </summary>
    public class PartArray
    {
        /// <summary>
        /// 0x000 pa_state : Uint4B
        /// </summary>
        public PhysicsState State { get; set; }

        /// <summary>
        /// 0x004 owner : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject Owner { get; set; }

        /// <summary>
        /// 0x008 sequence : CSequence
        /// </summary>
        public Sequence Sequence { get; set; } = new Sequence();

        /// <summary>
        /// 0x050 motion_table_manager : Ptr32 MotionTableManager
        /// </summary>
        public MotionTableManager MotionTableManager { get; set; }

        /// <summary>
        /// 0x054 setup : Ptr32 CSetup
        /// </summary>
        public SetupModel Setup { get; private set; }

        /// <summary>
        /// 0x058 num_parts : Uint4B
        /// </summary>
        public uint NumParts { get; set; }

        /// <summary>
        /// 0x05c parts : Ptr32 Ptr32 CPhysicsPart
        /// </summary>
        public List<PhysicsPart> Parts { get; set; } = new List<PhysicsPart>();

        /// <summary>
        /// 0x060 scale : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Scale = Vector3.One;

        /// <summary>
        /// 0x06c pals : Ptr32 Ptr32 Palette
        /// </summary>
        public List<Palette> Palettes = new List<Palette>();

        /// <summary>
        /// 0x070 lights : Ptr32 LIGHTLIST
        /// </summary>
        public List<object> Lights = new List<object>();

        /// <summary>
        /// 0x074 last_animframe : Ptr32 AnimFrame
        /// </summary>
        public PhysicsAnimationFrame LastAnimationFrame;

       public int GetNumSphere()
        {
            return this.Setup.Spheres.Count;
        }

        /// <summary>
        /// //----- (00517DB0) --------------------------------------------------------
        /// void __thiscall CPartArray::Update(CPartArray *this, float quantum, Frame* offset_frame)
        /// acclient.c 325140
        /// </summary>
        public void Update(double quantum, Frame position)
        {
            Sequence.Update(quantum, position);
        }

        /// <summary>
        /// //----- (00519310) --------------------------------------------------------
        /// void __thiscall CPartArray::SetFrame(CPartArray *this, Frame *frame)
        /// acclient.c 326766
        /// </summary>
        public void SetFrame(Frame loc)
        {
            UpdateParts(loc);

            // update lights in client.  skip it for server.
        }

        /// <summary>
        /// //----- (005193D0) --------------------------------------------------------
        /// int __thiscall CPartArray::SetPlacementFrame(CPartArray *this, unsigned int placement_id)
        /// acclient.c 326818
        /// </summary>
        public bool SetPlacementFrame(Placement placement)
        {
            if (!Setup.PlacementFrames.ContainsKey(placement))
                placement = Placement.Default;

            if (Setup.PlacementFrames.ContainsKey(placement))
            {
                var frame = Setup.PlacementFrames[placement];
                Sequence.SetPlacementFrame(frame.AnimFrame, placement);
                return true;
            }

            Sequence.SetPlacementFrame(null, Placement.Default);
            return false;
        }

        /// <summary>
        /// //----- (005190F0) --------------------------------------------------------
        /// void __thiscall CPartArray::UpdateParts(CPartArray *this, Frame *frame)
        /// acclient.c 326601
        /// </summary>
        public void UpdateParts(Frame frame)
        {
            var animFrame = Sequence.GetCurrentAnimationFrame();

            if (animFrame != null)
            {
                for (int i = 0; i < Parts.Count; i++)
                {
                    Parts[i].Position.Frame = Frame.Combine(frame, animFrame.Frames[i], Scale);
                }
            }
        }

        /// <summary>
        /// //----- (00517D60) --------------------------------------------------------
        /// void __thiscall CPartArray::HandleMovement(CPartArray *this)
        /// acclient.c 325106
        /// </summary>
        public void HandleMovement()
        {
            MotionTableManager.UseTime();
        }

        /// <summary>
        /// //----- (00518070) --------------------------------------------------------
        /// CSphere* __thiscall CPartArray::GetSphere(CPartArray*this)
        /// acclient.c 325364
        /// </summary>
        public List<Sphere> GetSpheres()
        {
            return Setup.Spheres;
        }

        /// <summary>
        /// //----- (005180C0) --------------------------------------------------------
        /// int __thiscall CPartArray::AllowsFreeHeading(CPartArray *this)
        /// acclient.c 325394
        /// </summary>
        public bool AllowsFreeHeading()
        {
            return Setup.Bitfield.HasFlag(SetupModelFlags.AllowFreeHeading);
        }

        /// <summary>
        /// //----- (005180D0) --------------------------------------------------------
        /// double __thiscall CPartArray::GetStepUpHeight(CPartArray *this)
        /// acclient.c 325400
        /// </summary>
        public float GetStepUpHeight()
        {
            return (Setup?.StepUpHeight ?? 0) * Scale.Z;
        }

        /// <summary>
        /// //----- (005180F0) --------------------------------------------------------
        /// double __thiscall CPartArray::GetStepDownHeight(CPartArray*this)
        /// acclient.c 325414
        /// </summary>
        public float GetStepDownHeight()
        {
            return (Setup?.StepDownHeight ?? 0) * Scale.Z;
        }

        /// <summary>
        /// //----- (00519150) --------------------------------------------------------
        /// int __thiscall CPartArray::SetMeshID(CPartArray *this, IDClass mesh_did)
        /// acclient.c 326635
        /// </summary>
        public bool SetMeshId(uint meshDid)
        {
            if (meshDid == 0)
                return false;

            Setup = SetupModel.MakeSimpleSetup(meshDid);
            if (Setup == null)
                return false;

            InitializeParts();
            return true;
        }

        /// <summary>
        /// //----- (00519640) --------------------------------------------------------
        /// CPartArray *__cdecl CPartArray::CreateMesh(CPhysicsObj *_owner, IDClass setup_did)
        /// acclient.c 326984
        /// </summary>
        public static PartArray CreateMesh(PhysicsObject owner, uint setupDid)
        {
            PartArray pa = new PartArray();
            pa.Owner = owner;
            pa.Sequence.SetObject(owner);

            if (!pa.SetMeshId(setupDid))
                return null;

            pa.SetPlacementFrame(Placement.Default);

            return pa;
        }

        /// <summary>
        /// //----- (005195A0) --------------------------------------------------------
        /// CPartArray *__cdecl CPartArray::CreateSetup(CPhysicsObj *_owner, IDClass setup_did, int bCreateParts)
        /// acclient.c 326947
        /// </summary>
        public static PartArray CreateSetup(PhysicsObject owner, uint setupId, bool createParts)
        {
            PartArray pa = new PartArray();
            pa.State = 0;
            pa.Owner = owner;
            pa.Sequence = new Sequence();
            pa.Sequence.SetObject(owner);

            if (!pa.SetSetupId(setupId, createParts))
                return null;

            pa.SetPlacementFrame(Placement.Default);

            return pa;
        }

        /// <summary>
        /// //----- (00519330) --------------------------------------------------------
        /// int __thiscall CPartArray::SetSetupID(CPartArray *this, IDClass setup_id, int bCreateParts)
        /// acclient.c 326779
        /// </summary>
        private bool SetSetupId(uint setupId, bool createParts)
        {
            if (Setup == null || Setup.Id != setupId)
            {
                // clear dependent data
                Palettes = new List<Palette>();
                Parts = new List<PhysicsPart>();
                Lights = new List<object>();

                // fetch the info from the dat
                Setup = PortalDatReader.Current.Unpack<SetupModel>(setupId);

                if (Setup == null)
                    return false;

                // initialize dependent data
                if (createParts)
                {
                    InitializeParts();
                }

                InitializeLights();
                InitializeDefaults();
            }

            return true;
        }

        /// <summary>
        /// //----- (00517F40) --------------------------------------------------------
        /// unsigned int __thiscall CPartArray::InitParts(CPartArray *this)
        /// acclient.c 325274
        /// </summary>
        private void InitializeParts()
        {
            Parts = new List<PhysicsPart>();

            Setup.PartIds.ForEach(pId => Parts.Add(PhysicsPart.MakePhysicsPart(pId)));

            for (int i = 0; i < Parts.Count; i++)
            {
                Parts[i].PhysicsObject = Owner;
                Parts[i].Index = i;

                if (i < Setup.DefaultScale.Count)
                    Parts[i].GraphicsObjectScale = Setup.DefaultScale[i];
            }
        }

        /// <summary>
        /// //----- (00518C00) --------------------------------------------------------
        /// int __thiscall CPartArray::InitLights(CPartArray *this)
        /// acclient.c 326321
        /// </summary>
        public void InitializeLights()
        {
            // probably don't care about lights for a server
        }

        /// <summary>
        /// //----- (00517DD0) --------------------------------------------------------
        /// void __thiscall CPartArray::SetCellID(CPartArray *this, const unsigned int cell_id)
        /// acclient.c 325146
        /// </summary>
        public void SetCellId(uint cellId)
        {
            Parts.ForEach(p => p.Position.CellId = cellId);
        }

        /// <summary>
        /// //----- (00518980) --------------------------------------------------------
        /// void __thiscall CPartArray::InitDefaults(CPartArray *this)
        /// acclient.c 326158
        /// </summary>
        private void InitializeDefaults()
        {
            if (Setup.DefaultAnimation != 0)
            {
                Sequence.ClearAnimations();
                AnimationData defaultAnim = new AnimationData();
                defaultAnim.AnimationId = Setup.DefaultAnimation;
                defaultAnim.LowFrame = 0;
                defaultAnim.HighFrame = -1;
                defaultAnim.Framerate = 30.0f;
                Sequence.AppendAnimation(defaultAnim);
            }

            Owner?.InitializeDefaults(Setup);
        }

        /// <summary>
        /// //----- (00518340) --------------------------------------------------------
        /// void __thiscall CPartArray::SetTranslucencyInternal(CPartArray *this, float _translucency)
        /// acclient.c 325638
        /// </summary>
        internal void SetTranslucencyInternal(float value)
        {
            if (Setup != null)
                Parts?.ForEach(p => p.SetTranslucency(value));
        }

        /// <summary>
        /// //----- (00518110) --------------------------------------------------------
        /// int __thiscall CPartArray::CacheHasPhysicsBSP(CPartArray *this)
        /// acclient.c 325428
        /// </summary>
        internal bool CacheHasPhysicsBsp()
        {
            foreach (var p in Parts)
            {
                if (p.GraphicsObjects[0].PhysicsBsp != null)
                {
                    State |= PhysicsState.HasPhysicsBsp;
                    return true;
                }
            }

            State &= ~PhysicsState.HasPhysicsBsp;
            return false;
        }

        /// <summary>
        /// ----- (005187F0) --------------------------------------------------------
        /// signed int __thiscall CPartArray::StopInterpretedMotion(CPartArray*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 326054
        /// </summary>
        public Sequence StopInterpretedMotion(MotionCommand motion, MovementParameters movementParameters)
        {
            // TODO: RE this method
            return null;
        }

        /// <summary>
        /// ----- (00517D70) --------------------------------------------------------
        /// void __thiscall CPartArray::HandleEnterWorld(CPartArray*this)
        /// acclient.c 325116
        /// </summary>
        public void HandleEnterWorld()
        {
            MotionTableManager?.HandleEnterWorld(Sequence);
        }

        public Sequence DoInterpretedMotion(MotionCommand motion, MovementParameters movementParameters)
        {
            return MotionTableManager == null ? new Sequence(7) : null;
            // TODO:: This is not finished yet.

        }
    }
}
