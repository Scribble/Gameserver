/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System.Collections.Generic;
using DerethForever.ClientLib.Enum;


namespace DerethForever.ClientLib.Entity
{
    public class RawMotionState
    {
        /// <summary>
        /// +0x004 actions          : LList<ActionNode>
        /// </summary>
        public List<ActionNode> Actions { get; set; }

        /// <summary>
        ///  +0x00c current_holdkey  : HoldKey
        /// </summary>
        public HoldKey CurrentHoldKey { get; set; }

        /// <summary>
        /// +0x010 current_style    : Uint4B
        /// </summary>
        public MotionCommand CurrentStyle { get; set; }

        /// <summary>
        /// +0x014 forward_command  : Uint4B
        /// </summary>
        public MotionCommand ForwardCommand { get; set; }

        /// <summary>
        /// +0x018 forward_holdkey  : HoldKey
        /// </summary>
        public HoldKey ForwardHoldKey { get; set; }

        /// <summary>
        /// +0x01c forward_speed    : Float
        /// </summary>
        public float ForwardSpeed { get; set; }

        /// <summary>
        /// +0x020 sidestep_command : Uint4B
        /// </summary>
        public MotionCommand SideStepCommand { get; set; }

        /// <summary>
        /// +0x024 sidestep_holdkey : HoldKey
        /// </summary>
        public HoldKey SideStepHoldKey { get; set; }

        /// <summary>
        /// +0x028 sidestep_speed   : Float
        /// </summary>
        public float SideStepSpeed { get; set; }

        /// <summary>
        /// +0x02c turn_command     : Uint4B
        /// </summary>
        public MotionCommand TurnCommand { get; set; }

        /// <summary>
        /// +0x030 turn_holdkey     : HoldKey
        /// </summary>
        public HoldKey TurnHoldKey { get; set; }

        /// <summary>
        /// +0x030 turn_holdkey     : HoldKey
        /// </summary>
        public float TurnSpeed { get; set; }

        /// <summary>
        /// ----- (0051E840) --------------------------------------------------------
        /// void __thiscall RawMotionState::AddAction(RawMotionState*this, unsigned int action, float speed, unsigned int stamp, int autonomous)
        /// </summary>
        public void AddAction(MotionCommand action, float speed, uint stamp, bool autonomous)
        {
            Actions.Add(new ActionNode(action, speed, stamp, autonomous));
        }

        /// <summary>
        /// ----- (0051EB60) --------------------------------------------------------
        /// void __thiscall RawMotionState::ApplyMotion(RawMotionState*this, unsigned int motion, MovementParameters*params)
        /// </summary>
        public void ApplyMotion(MotionCommand motion, MovementParameters movementParams)
        {
            switch (motion)
            {
                case MotionCommand.SideStepRight:
                case MotionCommand.SideStepLeft:
                    SideStepCommand = motion;
                    if (movementParams.SetHoldKey)
                    {
                        SideStepHoldKey = HoldKey.Invalid;
                        SideStepSpeed = movementParams.Speed;
                    }
                    else
                    {
                        SideStepHoldKey = movementParams.HoldKeyToApply;
                        SideStepSpeed = movementParams.Speed;
                    }
                    break;

                case MotionCommand.TurnRight:
                case MotionCommand.TurnLeft:
                    TurnCommand = motion;
                    if (movementParams.SetHoldKey)
                    {
                        TurnHoldKey = HoldKey.Invalid;
                        TurnSpeed = movementParams.Speed;
                    }
                    else
                    {
                        TurnHoldKey = movementParams.HoldKeyToApply;
                        TurnSpeed = movementParams.Speed;
                    }
                    break;

                default:
                    if (((uint)motion & 0x40000000) != 0)
                    {
                        if ((uint)motion != 0x44000007)
                        {
                            ForwardCommand = motion;
                            if (movementParams.SetHoldKey)
                            {
                                ForwardHoldKey = HoldKey.Invalid;
                                ForwardSpeed = movementParams.Speed;
                            }
                            else
                            {
                                ForwardHoldKey = movementParams.HoldKeyToApply;
                                ForwardSpeed = movementParams.Speed;
                            }
                        }
                    }
                    else if (((uint)motion & 0x80000000) != 0)
                    {
                        if (CurrentStyle != motion)
                        {
                            ForwardCommand = MotionCommand.Ready;
                            CurrentStyle = motion;
                        }
                    }
                    else if (((uint)motion & 0x10000000) != 0)
                    {
                        AddAction(motion, movementParams.Speed, movementParams.ActionStamp, movementParams.Autonomous);
                    }
                    break;
            }
        }

    }
}
