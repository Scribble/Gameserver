/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BSPNODE in the client
    /// </summary>
    public class BspNode
    {
        /// <summary>
        /// a field because of struct field vs property shenanigans
        /// 0x004 sphere : CSphere
        /// </summary>
        public Sphere Sphere;

        /// <summary>
        /// a field because of struct field vs property shenanigans
        /// 0x014 splitting_plane : Plane
        /// </summary>
        public Plane SplittingPlane;

        /// <summary>
        /// 0x024 type : Int4B
        /// </summary>
        public BspNodeType Type { get; set; }

        /// <summary>
        /// 0x028 num_polys : Uint4B
        /// </summary>
        public uint NumPolygons => (uint)InPolygons.Count;

        /// <summary>
        /// 0x02c in_polys : Ptr32 Ptr32 CPolygon
        /// </summary>
        public List<Polygon> InPolygons { get; set; } = new List<Polygon>();

        /// <summary>
        /// 0x030 pos_node : Ptr32 BSPNODE
        /// </summary>
        public BspNode PositiveNode { get; set; }

        /// <summary>
        /// 0x034 neg_node : Ptr32 BSPNODE
        /// </summary>
        public BspNode NegativeNode { get; set; }

        /// <summary>
        /// //----- (0053C770) --------------------------------------------------------
        /// int __cdecl BSPNODE::UnPackChild(BSPNODE **node, void **addr, unsigned int size)
        /// acclient.c 363344
        /// </summary>
        public static BspNode UnpackChild(BinaryReader reader, BspTreeType treeType, List<Polygon> polygons)
        {
            BspNode n;

            var nodeType = (BspNodeType)reader.ReadUInt32();

            if (nodeType == BspNodeType.Port)
            {
                n = new BspPortal();
                ((BspPortal)n).UnpackPort(reader, treeType, polygons);
            }
            else if (nodeType == BspNodeType.Leaf)
            {
                n = new BspLeaf();
                ((BspLeaf)n).UnpackLeaf(reader, treeType, polygons);
            }
            else
            {
                n = new BspNode();
                n.Type = nodeType;
                n.Unpack(reader, treeType, polygons);
            }

            return n;
        }

        /// <summary>
        /// //----- (0053C540) --------------------------------------------------------
        /// int __thiscall BSPNODE::UnPack(BSPNODE *this, void **addr, unsigned int size)
        /// acclient.c 363186
        /// </summary>
        public void Unpack(BinaryReader reader, BspTreeType treeType, List<Polygon> polygons)
        {
            SplittingPlane = reader.ReadPlane();

            // compiler optimizations were the devil here. just saying.

            switch (Type)
            {
                case BspNodeType.BpIN:
                case BspNodeType.BpnN:
                    NegativeNode = BspNode.UnpackChild(reader, treeType, polygons);
                    break;
                case BspNodeType.BPIn:
                case BspNodeType.BPnn:
                    PositiveNode = BspNode.UnpackChild(reader, treeType, polygons);
                    break;
                case BspNodeType.BPIN:
                case BspNodeType.BPnN:
                    PositiveNode = BspNode.UnpackChild(reader, treeType, polygons);
                    NegativeNode = BspNode.UnpackChild(reader, treeType, polygons);
                    break;
            }

            Sphere = reader.ReadSphere();

            if (treeType == BspTreeType.Physics)
                return;

            uint numPolys = reader.ReadUInt32();

            for (int i = 0; i < numPolys; i++)
            {
                ushort polyIndex = reader.ReadUInt16();
                InPolygons.Add(polygons[i]);
            }
        }
    }
}
