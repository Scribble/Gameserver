/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Numerics;
using DerethForever.ClientLib.Enum;
using DerethForever.ClientLib.Integration;
using DerethForever.ClientLib.Managers;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// corresponds to CPhysicsObject in the client. provides the base
    /// object for WorldObject from which all other Dereth Forever objects
    /// derive.  direct implementations of "PhysicsObject" will not be
    /// interactive with the main server logic and should be used for static
    /// objects only.
    /// </summary>
    public class PhysicsObject
    {
        /// <summary>
        /// 0x004 hash_next : Ptr32 HashBaseData(unsigned long)
        /// </summary>
        public object HashNext;

        /// <summary>
        /// 0x008 id : Uint4B
        /// </summary>
        public uint Id;

        /// <summary>
        /// 0x00c netblob_list : Ptr32 NIList(NetBlob *)
        /// </summary>
        public List<object> NetBlobList = new List<object>();

        /// <summary>
        /// 0x010 part_array : Ptr32 PartArray
        /// </summary>
        public PartArray PartArray { get; set; }

        /// <summary>
        /// +0x014 player_vector    : AC1Legacy::Vector3
        /// </summary>
        public Vector3 PlayerVector { get; set; }

        /// <summary>
        /// +0x020 player_distance  : Float
        /// </summary>
        public float PlayerDistance { get; set; }

        /// <summary>
        /// +0x024 CYpt             : Float
        /// </summary>
        public float CYpt { get; set; }

        //+0x028 sound_table      : Ptr32 CSoundTable


        /// <summary>
        /// +0x02c m_bExaminationObject : Bool
        /// </summary>
        public bool ExaminationObject { get; set; }

        // +0x030 script_manager   : Ptr32 ScriptManager
        // +0x034 physics_script_table : Ptr32 PhysicsScriptTable

        /// <summary>
        /// +0x038 default_script   : PScriptType
        /// </summary>
        public PlayScript DefaultScript { get; set; }

        /// <summary>
        /// +0x03c default_script_intensity : Float
        /// </summary>
        public float DefaultScriptIntensity { get; set; }

        /// <summary>
        /// 0x040 parent : Ptr32 CPhysicsObj
        /// </summary>
        public uint? ParentId = null;

        /// <summary>
        /// 0x044 children : Ptr32
        /// </summary>
        public ChildList Children = new ChildList();

        /// <summary>
        /// 0x048 m_position : Position
        /// </summary>
        public Position CurrentPosition = new Position();

        /// <summary>
        /// 0x090 cell : Ptr32 CObjCell
        /// </summary>
        public ObjectCell Cell;

        /// <summary>
        /// 0x0c4 movement_manager : Ptr32 MovementManager
        /// </summary>
        public MovementManager MovementManager = null;

        /// <summary>
        /// 0x0c8 position_manager : Ptr32 PositionManager
        /// </summary>
        public PositionManager PositionManager = null;

        /// <summary>
        /// 0x0d0 jumped_this_frame : Int4B
        /// </summary>
        public bool JumpedThisFrame { get; set; } = false;

        /// <summary>
        /// 0x011c detection_manager : Ptr32 DetectionManager
        /// </summary>
        public DetectionManager DetectionManager = null;

        /// <summary>
        /// 0x0a8 state : Uint4B
        /// </summary>
        public PhysicsState State { get; set; } = PhysicsState.Static;

        /// <summary>
        /// 0x0ac transient_state : Uint4B
        /// </summary>
        public TransientState TransientState { get; set; } = TransientState.Uninitialized;

        /// <summary>
        /// 0x0b0 elasticity : Float
        /// </summary>
        public float Elasticity { get; set; }

        /// <summary>
        /// 0x0b4 translucency : Float
        /// </summary>
        public float Translucency { get; set; }

        /// <summary>
        /// 0x0b4 translucencyOriginal : Float
        /// </summary>
        public float TranslucencyOriginal { get; set; }

        /// <summary>
        /// 0x0bc friction : Float
        /// </summary>
        public float Friction;

        /// <summary>
        /// 0x0d8 update_time      : Float
        /// </summary>
        public float UpdateTime;

        /// <summary>
        /// 0x0e0 m_velocityVector : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Velocity;

        /// <summary>
        /// 0x0ec mm_accelerationVector : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Acceleration;

        /// <summary>
        /// 0x0f8 m_omegaVector : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Omega;

        /// <summary>
        /// +0x108 anim_hooks       : AC1Legacy::SmartArray<CAnimHook *>
        /// </summary>
        public List<AnimationHook> AnimHooks;

        /// <summary>
        /// 0x114 m_scale : Float
        /// </summary>
        public float Scale { get; set; } = 1f;

        /// <summary>
        /// 0x124 target_manager : Ptr32 TargetManager
        /// </summary>
        public TargetManager TargetManager = null;

        /// <summary>
        /// 0x130 contact_plane : Plane
        /// </summary>
        public Plane ContactPlane;

        /// <summary>
        /// 0x140 contact_plane_cell_id : Uint4B
        /// </summary>
        public uint ContactPlaneCellId;

        /// <summary>
        /// 0x144 sliding_normal : AC1Legacy::Vector3
        /// </summary>
        public Vector3 SlidingNormal;

        /// <summary>
        /// 0x150 cached_velocity : AC1Legacy::Vector3
        /// </summary>
        public Vector3 CachedVelocity;

        private const float _maxVelocityMagSquared = 2500f;

        public bool HasLocation
        {
            get { return CurrentPosition != null && CurrentPosition.CellId > 0; }
        }

        public virtual IWorldObject Instance { get; }

        /// <summary>
        /// //----- (00515D10) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::update_object(CPhysicsObj*this)
        /// </summary>
        public virtual void UpdateObject(float timeToUse)
        {
            // things with parents are in containers.
            // things without locations don't have physics
            // and things that are frozen don't do physics either
            if (ParentId != null || !HasLocation || State.HasFlag(PhysicsState.Frozen))
            {
                TransientState &= ~TransientState.Active;
                return;
            }

            // client has a lot of additional code here for calculating the player's distance from this thing.
            // if it's > 96 away, it sets the transient state to inactive

            if (timeToUse > Constants.TOLERANCE && timeToUse < Constants.QUANTUM_ABSURDUM)
            {
                if (timeToUse > Constants.QUANTUM_MAX_97)
                {
                    // the client would do iterative UpdateObjectInternals here to play catchup.  We'll
                    // reintroduce that complexity later
                    timeToUse = Constants.QUANTUM_MAX_97;
                }

                UpdateObjectInternal(timeToUse);
            }
        }

        /// <summary>
        /// ----- (005156B0) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::UpdateObjectInternal(CPhysicsObj *this, float quantum)
        /// acclient.c 322718
        /// </summary>
        protected virtual void UpdateObjectInternal(float timeToUse)
        {
            Position newPos = new Position();

            if (CurrentPosition == null)
                return;

            if (TransientState.HasFlag(TransientState.Active))
            {
                if (TransientState.HasFlag(TransientState.CheckEthereal))
                    SetEthereal(false, false);

                JumpedThisFrame = false;

                newPos.CellId = CurrentPosition.CellId;
                UpdatePositionInternal(timeToUse, newPos.Frame);

                if (PartArray?.GetNumSphere() > 0)
                {
                    if (newPos == CurrentPosition)
                    {
                        newPos.Frame.Origin = CurrentPosition.Frame.Origin;

                        SetFrame(newPos.Frame);
                        CachedVelocity = new Vector3();
                    }
                    else
                    {
                        if (State.HasFlag(PhysicsState.AlignPath))
                        {
                            // acclient.c 322802
                            Vector3 offset = newPos.Frame.Origin - CurrentPosition.Frame.Origin;
                            offset.Normalize();
                            newPos.Frame.SetVectorHeading(offset);
                        }
                        else if (State.HasFlag(PhysicsState.Sledding))
                        {
                            if (!Velocity.IsInsignificant())
                            {
                                float heading = Velocity.GetHeading();
                                newPos.Frame.SetHeading(heading);
                            }
                        }

                        Transition trans = Transition(CurrentPosition, newPos);

                        if (trans != null)
                        {
                            Vector3 offset = this.CurrentPosition.GetOffset(newPos);
                            CachedVelocity = offset.CreateVelocity((float)timeToUse);
                            SetPositionInternal(trans);
                        }
                        else
                        {
                            // no transition allowed (?)
                            newPos.Frame.Origin = CurrentPosition.Frame.Origin;
                            CachedVelocity = Vector3.Zero;
                            SetInitialFrame(newPos.Frame);
                        }
                    }
                }
                else
                {
                    if (MovementManager == null)
                    {
                        if (TransientState.HasFlag(TransientState.Walkable))
                            TransientState &= ~TransientState.Active;
                    }

                    newPos.Frame.Origin = CurrentPosition.Frame.Origin;
                    SetFrame(newPos.Frame);
                    CachedVelocity = Vector3.Zero;
                }

                DetectionManager?.CheckDetection(this);

                TargetManager?.HandleTargeting(this);

                MovementManager?.UseTime(this);

                PartArray?.HandleMovement();

                PositionManager.UseTime(this);
            }

            // update particles - do we care?

            // update scripts
            UpdateScripts();
        }

        protected virtual void UpdateScripts()
        {
            // TODO implement UpdateScripts
        }

        /// <summary>
        /// //----- (00512C30) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::UpdatePositionInternal(CPhysicsObj*this, float quantum, Frame* o_newFrame)
        /// acclient.c 319989
        /// </summary>
        protected virtual void UpdatePositionInternal(float timeToUse, Frame newFrame)
        {
            Frame offsetFrame = new Frame();
            bool hidden = State.HasFlag(PhysicsState.Hidden);

            if (!hidden)
            {
                PartArray?.Update(timeToUse, offsetFrame);

                if (TransientState.HasFlag(TransientState.Walkable))
                {
                    offsetFrame.Origin.X *= Scale;
                    offsetFrame.Origin.Y *= Scale;
                    offsetFrame.Origin.Z *= Scale;
                }
                else
                {
                    offsetFrame.Origin.X = 0;
                    offsetFrame.Origin.Y = 0;
                    offsetFrame.Origin.Z = 0;
                }
            }

            PositionManager?.AdjustOffset(offsetFrame, timeToUse);
            newFrame = Frame.Combine(CurrentPosition.Frame, offsetFrame);

            if (!hidden)
                UpdatePhysicsInternal(timeToUse, newFrame);

            ProcessHooks();
        }

        /// <summary>
        /// //----- (00515330) --------------------------------------------------------
        /// int __thiscall CPhysicsObj::SetPositionInternal(CPhysicsObj*this, CTransition* transit)
        /// acclient.c 322504
        /// </summary>
        protected void SetPositionInternal(Transition transition)
        {
            // TODO implement SetPositionInternal
        }

        /// <summary>
        /// //----- (00514120) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::set_initial_frame(CPhysicsObj*this, Frame* frame)
        /// acclient.c 321356
        /// </summary>
        public void SetInitialFrame(Frame location)
        {
            CurrentPosition.Frame = location;

            if (!State.HasFlag(PhysicsState.ParticleEmitter))
            {
                PartArray?.SetFrame(location);
            }

            UpdateChildrenInternal();
        }

        /// <summary>
        /// ----- (00511C40) --------------------------------------------------------
        /// int __thiscall CPhysicsObj::set_ethereal(CPhysicsObj*this, int ethereal, int send_event)
        /// acclient.c 319046
        /// </summary>
        protected virtual bool SetEthereal(bool ethereal, bool sendEvent)
        {
            if (ethereal)
            {
                State |= PhysicsState.Ethereal;
                TransientState &= ~TransientState.CheckEthereal;
                return true;
            }

            State &= ~PhysicsState.Ethereal;
            if (ParentId != null || !HasLocation || !EtherealCollisions())
            {
                TransientState &= ~TransientState.CheckEthereal;
                return true;
            }

            State |= PhysicsState.Ethereal;
            TransientState |= TransientState.CheckEthereal;
            return false;
        }

        /// <summary>
        /// //----- (00514090) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::set_frame(CPhysicsObj*this, Frame* i_frame)
        /// acclient.c 321328
        /// </summary>
        protected virtual void SetFrame(Frame frame)
        {
            Frame copy = new Frame(frame);

            if (!copy.IsValid() && copy.IsValidExceptForHeading())
            {
                // zero out the heading
                copy.Quaternion = Quaternion.Identity;
            }

            CurrentPosition.Frame = copy;

            if (!State.HasFlag(PhysicsState.ParticleEmitter))
            {
                PartArray?.SetFrame(copy);
            }

            UpdateChildrenInternal();
        }

        /// <summary>
        /// //----- (0050F4F0) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::set_cell_id(CPhysicsObj *this, unsigned int new_cell_id)
        /// acclient.c 316474
        /// </summary>
        public void SetCellId(uint cellId)
        {
            CurrentPosition.CellId = cellId;

            if (!State.HasFlag(PhysicsState.ParticleEmitter))
                PartArray?.SetCellId(cellId);
        }

        /// <summary>
        /// //----- (00512DC0) --------------------------------------------------------
        /// CTransition* __thiscall CPhysicsObj::transition(CPhysicsObj*this, Position* old_pos, Position* new_pos, int admin_move)
        /// acclient.c 320060
        /// </summary>
        private Transition Transition(Position current, Position target, bool adminMove = false)
        {
            Transition trans = new Transition();
            var objectState = GetObjectInfoState(trans, adminMove);

            if (objectState == 0)
                return null;

            trans.InitializeObject(this, objectState);

            var spheres = PartArray?.GetSpheres()?.ToArray();

            if (spheres?.Length > 0)
                trans.InitializeSpheres(spheres, Scale);
            else
                trans.InitializeSpheres(new Sphere[] { Globals.DummySphere }, 1.0f);

            trans.InitializePath(Cell, current, target);

            if (TransientState.HasFlag(TransientState.StationaryStuck))
                trans.CollisionInfo.FramesStationaryFall = 3;
            else if (TransientState.HasFlag(TransientState.StationaryStop))
                trans.CollisionInfo.FramesStationaryFall = 2;
            else if (TransientState.HasFlag(TransientState.StationaryFall))
                trans.CollisionInfo.FramesStationaryFall = 1;

            if (trans.FindValidPosition())
                return trans;

            return null;
        }

        protected virtual void UpdateChildrenInternal()
        {
            // TODO implement UpdateChildrenInternal
        }

        protected virtual bool EtherealCollisions()
        {
            // TODO implement EtherealCollisions
            return false;
        }

        /// <summary>
        /// //----- (00510700) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::UpdatePhysicsInternal(CPhysicsObj *this, float quantum, Frame* offset_frame)
        /// acclient.c 317701
        /// </summary>
        private void UpdatePhysicsInternal(float quantum, Frame newFrame)
        {
            float velocityMagSquared = Velocity.LengthSquared();
            if (velocityMagSquared < 0f)
            {
                if (MovementManager != null && TransientState.HasFlag(TransientState.Walkable))
                    TransientState &= ~TransientState.Active;
            }
            else
            {
                // max velocity is 50, squared is 2500
                if (velocityMagSquared > _maxVelocityMagSquared)
                {
                    Velocity.Normalize();
                    Velocity *= 50f;
                }

                CalculateFriction(quantum, velocityMagSquared);

                if (velocityMagSquared < 0.0627) // client used "velocity_mag2 - 0.25 * 0.25 < 0.00019999999" - i simplified
                    Velocity = Vector3.Zero;

                newFrame.Origin += Velocity * quantum + Acceleration * 0.5f * quantum * quantum;
            }

            Velocity += (Acceleration * quantum);
            Vector3 w = Omega * quantum;
            newFrame.Rotate(w);
        }

        /// <summary>
        /// calculates friction based on the provided inputs, and adjusts velocity accordingly
        /// //----- (0050EE70) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::calc_friction(CPhysicsObj*this, float quantum, float velocity_mag2)
        /// acclient.c 316091
        /// </summary>
        private void CalculateFriction(float quantum, float velocityMagnitudeSquared)
        {
            double f;

            if (TransientState.HasFlag(TransientState.Walkable))
            {
                float dotProduct = Vector3.Dot(Velocity, ContactPlane.Normal);
                if (dotProduct >= 0.25)
                    return;

                Velocity.X -= ContactPlane.Normal.X * dotProduct;
                Velocity.Y -= ContactPlane.Normal.Y * dotProduct;
                Velocity.Z -= ContactPlane.Normal.Z * dotProduct;

                // mud figured out the sledding check from assembly.  hexrays barfed on this.
                if ((State & PhysicsState.Sledding) > 0)
                {
                    f = 0.2d;

                    if (velocityMagnitudeSquared >= 1.5625) // 1.25 ^ 2
                    {
                        // client used Math.Cos(0.1745329251994329).  i'd rather save the trig function.
                        double zLimit = 0.984807753d;
                        if (velocityMagnitudeSquared < 6.25 || zLimit <= ContactPlane.Normal.Z)
                            f = Friction;
                    }
                    else
                    {
                        f = 1f;
                    }
                }
                else
                {
                    f = Friction;
                }

                double scalar = Math.Pow((1 - f), quantum);
                Velocity *= (float)scalar;
            }
        }

        /// <summary>
        /// Aka get_object_info.  Almost certainly a misnamed function as it just gets a state
        /// //----- (00511CC0) --------------------------------------------------------
        /// signed int __thiscall CPhysicsObj::get_object_info(CPhysicsObj*this, CTransition* transit, int admin_move)
        /// acclient.c 319074
        /// </summary>
        private ObjectInfoState GetObjectInfoState(Transition trans, bool adminMove)
        {
            ObjectInfoState newState = 0;

            if (State.HasFlag(PhysicsState.EdgeSlide))
                newState = ObjectInfoState.EdgeSlide;

            if (!adminMove)
            {
                if (TransientState.HasFlag(TransientState.Contact))
                {
                    bool isWater = TransientState.HasFlag(TransientState.WaterContact);
                    bool isInContact = IsInContact();

                    trans.InitializeContactPlane(ContactPlaneCellId, ContactPlane, isWater, isInContact);

                    if (isInContact)
                    {
                        newState |= ObjectInfoState.Contact;

                        if (TransientState.HasFlag(TransientState.Walkable))
                            newState |= ObjectInfoState.OnWalkable;
                    }
                }

                if (TransientState.HasFlag(TransientState.Sliding))
                    trans.InitializeSlidingNormal(SlidingNormal);
            }

            if (PartArray.AllowsFreeHeading())
                newState |= ObjectInfoState.FreeRotate;

            if (State.HasFlag(PhysicsState.Missile))
                newState |= ObjectInfoState.PathClipped;

            return newState;
        }

        /// <summary>
        /// checks the contact flag and for some velocity.  odd function for 1 LOC.
        /// ----- (0050F5B0) --------------------------------------------------------
        /// int __thiscall CPhysicsObj::check_contact(CPhysicsObj*this, int contact)
        /// </summary>
        private int CheckContact(int contact)
        {
            return IsInContact() ? contact : 0;
        }

        /// <summary>
        /// replacement function for CheckContact
        /// </summary>
        /// <returns></returns>
        private bool IsInContact()
        {
            return !(TransientState.HasFlag(TransientState.Contact) && Velocity.LengthSquared() > Constants.TOLERANCE);
        }

        /// <summary>
        /// //----- (0050EA00) --------------------------------------------------------
        /// double __thiscall CPhysicsObj::GetStepUpHeight(CPhysicsObj*this)
        /// acclient.c 315701
        /// </summary>
        public float GetStepUpHeight()
        {
            return PartArray?.GetStepUpHeight() ?? 0;
        }

        /// <summary>
        /// //----- (0050EA20) --------------------------------------------------------
        /// double __thiscall CPhysicsObj::GetStepDownHeight(CPhysicsObj*this)
        /// acclient.c 315715
        /// </summary>
        public float GetStepDownHeight()
        {
            return PartArray?.GetStepDownHeight() ?? 0;
        }

        private void ProcessHooks()
        {
            // TODO implement ProcessHooks
        }

        /// <summary>
        /// //----- (0050FF80) --------------------------------------------------------
        /// int __thiscall CPhysicsObj::InitObjectBegin(CPhysicsObj *this, unsigned int object_iid, int bDynamic)
        /// acclient.c 317273
        /// </summary>
        public void InitObjectBegin(uint objectId, bool dynamic)
        {
            if (dynamic)
                State &= ~PhysicsState.Static;
            else
                State |= PhysicsState.Static;

            TransientState &= ~TransientState.Active;

            UpdateTime = Timer.CurrentTime;
        }

        /// <summary>
        /// //----- (005126B0) --------------------------------------------------------
        /// int __thiscall CPhysicsObj::InitPartArrayObject(CPhysicsObj *this, IDClass data_did, int bCreateParts)
        /// acclient.c 319646
        /// </summary>
        public void InitPartArray(uint dId, bool createParts)
        {
            // client goes through a lot of magic here in "DivineType".  it's way overkill
            // part arrays are only concerned with a few things:
            // * simple models      - 0x01......
            // * complex models     - 0x02......
            // * everything else we shove into...  a complex_model anyway.

            if (dId == 0u)
                return;

            uint dataType = (dId >> 24) & 0xFF;

            if (dataType == 0x01)
            {
                PartArray = PartArray.CreateMesh(this, dId);
            }
            else if (dataType == 0x02)
            {
                PartArray = PartArray.CreateSetup(this, dId, createParts);
            }
            else
            {
                if (dataType != 0)
                    return;

                uint id = 0x02000000 | dId; // force it into a 0x02 model
                if (!MakeAnimationObject(id, createParts))
                    return;

                TransientState = TransientState & ~TransientState.CheckEthereal;
                State |= PhysicsState.Ethereal | PhysicsState.IgnoreCollision;
                SetTranslucentyInternal(0.25f);
                return;
            }

            CacheHasPhysicsBsp();
        }

        /// <summary>
        /// //----- (0050FFE0) --------------------------------------------------------
        /// int __thiscall CPhysicsObj::InitObjectEnd(CPhysicsObj *this)
        /// acclient.c 317293
        /// </summary>
        public void InitObjectEnd()
        {
            if (PartArray == null)
                return;

            PartArray.SetPlacementFrame(Placement.Default);

            if (!State.HasFlag(PhysicsState.IgnoreCollision))
                PartArray.SetFrame(CurrentPosition.Frame);
        }

        /// <summary>
        /// //----- (00513970) --------------------------------------------------------
        /// CPhysicsObj *__cdecl CPhysicsObj::makeObject(IDClass data_did, unsigned int object_iid, int bDynamic)
        /// acclient.c 320829
        /// </summary>
        public static PhysicsObject MakeObject(uint dId, uint objectId, bool dynamic)
        {
            PhysicsObject po = new PhysicsObject();

            po.InitObjectBegin(objectId, dynamic);
            po.InitPartArray(dId, true);
            po.InitObjectEnd();

            return po;
        }

        /// <summary>
        /// //----- (0050E930) --------------------------------------------------------
        /// BOOL __thiscall CPhysicsObj::makeAnimObject(CPhysicsObj *this, IDClass(_tagDataID,32,0) setup_id, int bCreateParts)
        /// acclient.c 315621
        /// </summary>
        public bool MakeAnimationObject(uint setupId, bool createParts)
        {
            PartArray = PartArray.CreateSetup(this, setupId, createParts);
            return (PartArray != null);
        }

        /// <summary>
        /// //----- (005139D0) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::InitDefaults(CPhysicsObj *this, CSetup *setup)
        /// acclient.c 320854
        /// </summary>
        public void InitializeDefaults(SetupModel setup)
        {
            // TODO implement PhysicsObject.InitializeDefaults
        }

        /// <summary>
        /// //----- (0050F960) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::SetTranslucencyInternal(CPhysicsObj *this, float _translucency)
        /// acclient.c 316803
        /// </summary>
        private void SetTranslucentyInternal(float value)
        {
            // can't go below the original value

            if (value < TranslucencyOriginal)
                value = TranslucencyOriginal;

            Translucency = value;

            PartArray?.SetTranslucencyInternal(Translucency);
        }

        /// <summary>
        /// //----- (0050F570) --------------------------------------------------------
        /// int __thiscall CPhysicsObj::CacheHasPhysicsBSP(CPhysicsObj*this)
        /// acclient.c 316514
        /// </summary>
        private bool CacheHasPhysicsBsp()
        {
            if (PartArray != null && PartArray.CacheHasPhysicsBsp())
            {
                State |= PhysicsState.HasPhysicsBsp;
                return true;
            }
            else
            {
                State &= ~PhysicsState.HasPhysicsBsp;
                return false;
            }
        }

        /// <summary>
        /// //----- (005159E0) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::add_obj_to_cell(CPhysicsObj *this, CObjCell *new_cell, Frame *new_frame)
        /// acclient.c 322897
        /// </summary>
        public void AddObjectToCell(ObjectCell cell, Frame loc)
        {
            EnterCell(cell);
            Frame copy = new Frame(loc); // copy, because pointers
            CurrentPosition.Frame = copy;

            if (!State.HasFlag(PhysicsState.ParticleEmitter))
                SetFrame(copy);

            UpdateChildrenInternal();
            CalcCrossCellsStatic();
        }

        /// <summary>
        /// //----- (00510ED0) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::enter_cell(CPhysicsObj *this, CObjCell *new_cell)
        /// acclient.c 318202
        /// </summary>
        private void EnterCell(ObjectCell cell)
        {
            if (PartArray != null && PartArray.Parts.Count > 0)
            {
                cell.AddObject(this);
                Children.Objects.ForEach(c => c.EnterCell(cell));

                CurrentPosition.CellId = cell.Id;

                if (!State.HasFlag(PhysicsState.ParticleEmitter))
                    PartArray.SetCellId(cell.Id);

                Cell = cell;

                // call to PartArray.AddLightsToCell omitted
            }
        }

        /// <summary>
        /// //----- (00510950) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::calc_acceleration(CPhysicsObj *this)
        /// acclient.c 317787
        /// </summary>
        public void CalculateAcceleration()
        {
            if (TransientState.HasFlag(TransientState.Contact) && TransientState.HasFlag(TransientState.Walkable) && !State.HasFlag(PhysicsState.Sledding))
            {
                Acceleration = Vector3.Zero;
                Omega = Vector3.Zero;
            }
            else if (State.HasFlag(PhysicsState.Gravity))
            {
                Acceleration = Globals.Gravity;
            }
            else
            {
                Acceleration = Vector3.Zero;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public void CalcCrossCellsStatic()
        {
            // TODO: implement PhysicsObject.CalcCrossCellsStatic
        }

        public bool MovmentIsAutonomous()
        {
            throw new NotImplementedException();
            // TODO: implement MovementIsAutonomous
        }

        /// <summary>
        /// ----- (0050EA90) --------------------------------------------------------
        /// signed int __thiscall CPhysicsObj::StopInterpretedMotion(CPhysicsObj*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 315766
        /// </summary>
        public Sequence StopInterpretedMotion(MotionCommand motion, MovementParameters movementParams)
        {
            Sequence sequence = new Sequence();
            if (PartArray != null)
                sequence = PartArray.StopInterpretedMotion(motion, movementParams);
            return sequence;
        }

        /// <summary>
        /// ----- (0050FE20) --------------------------------------------------------
        /// void __thiscall CPhysicsObj::RemoveLinkAnimations(CPhysicsObj*this)
        /// acclient.c 317145
        /// </summary>
        public void RemoveLinkAnimations()
        {
            PartArray?.HandleEnterWorld();
        }

        /// <summary>
        /// ----- (0050EA70) --------------------------------------------------------
        /// signed int __thiscall CPhysicsObj::DoInterpretedMotion(CPhysicsObj*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 315753
        /// </summary>
        public Sequence DoInterpretedMotion(MotionCommand motion, MovementParameters movementParams)
        {
            return PartArray?.DoInterpretedMotion(motion, movementParams);
        }
    }
}
