/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    public class TabooTableEntry
    {
        /// <summary>
        /// private because all valid instances come from Unpack
        /// </summary>
        private TabooTableEntry()
        {
        }

        public ushort Unknown1 { get; set; }

        public ushort Unknown2 { get; set; }

        public ushort Unknown3 { get; set; }

        public ushort Unknown4 { get; set; }

        public ushort Unknown5 { get; set; }

        public List<string> WordList { get; set; } = new List<string>();

        /// <summary>
        /// no corresponding client function.  this one was just brute forced
        /// from data inspection
        /// </summary>
        public static TabooTableEntry Unpack(BinaryReader reader)
        {
            TabooTableEntry tte = new TabooTableEntry();
            tte.Unknown1 = reader.ReadUInt16();
            tte.Unknown2 = reader.ReadUInt16();
            tte.Unknown3 = reader.ReadUInt16();
            tte.Unknown4 = reader.ReadUInt16();
            tte.Unknown5 = reader.ReadUInt16();
            
            uint count = reader.ReadUInt32();

            for (int i = 0; i < count; i++)
                 tte.WordList.Add(reader.ReadTabooString());

            return tte;
        }
    }
}
