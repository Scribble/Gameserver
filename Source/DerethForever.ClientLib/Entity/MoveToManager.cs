/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.Collections.Generic;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class MoveToManager
    {
        /// <summary>
        /// +0x000 movement_type    : MovementTypes::Type
        /// </summary>
        public MovementTypes MovementType { get; set; }

        /// <summary>
        /// +0x004 sought_position  : Position
        /// </summary>
        public Position SoughtPosition { get; set; }

        /// <summary>
        /// +0x04c current_target_position : Position
        /// </summary>
        public Position CurrentTargetPosition { get; set; }

        /// <summary>
        /// +0x094 starting_position : Position
        /// </summary>
        public Position StartingPosition { get; set; }

        /// <summary>
        /// +0x0dc movement_params  : MovementParameters
        /// </summary>
        public MovementParameters MovementParams { get; set; }

        /// <summary>
        /// +0x108 previous_heading : Float
        /// </summary>
        public float PreviousHeading { get; set; }

        /// <summary>
        /// +0x10c previous_distance : Float
        /// </summary>
        public float PreviousDistance { get; set; }

        /// <summary>
        /// +0x110 previous_distance_time : Float
        /// </summary>
        public double PreviousDistanceTime { get; set; }

        /// <summary>
        /// +0x118 original_distance : Float
        /// </summary>
        public float OriginalDistance { get; set; }

        /// <summary>
        /// +0x120 original_distance_time : Float
        /// </summary>
        public double OriginalDistanceTime { get; set; }

        /// <summary>
        /// +0x128 fail_progress_count : Uint4B
        /// </summary>
        public uint FailProgressCount { get; set; }

        /// <summary>
        /// +0x12c sought_object_id : Uint4B
        /// </summary>
        public uint SoughtObjectId { get; set; }

        /// <summary>
        /// +0x130 top_level_object_id : Uint4B
        /// </summary>
        public uint TopLevelObjectId { get; set; }

        /// <summary>
        /// +0x134 sought_object_radius : Float
        /// </summary>
        public float SoughtObjectRadius { get; set; }

        /// <summary>
        /// +0x138 sought_object_height : Float
        /// </summary>
        public float SoughtObjectHeight { get; set; }

        /// <summary>
        /// +0x13c current_command  : Uint4B
        /// </summary>
        public uint CurrentCommand { get; set; }

        /// <summary>
        /// +0x13c current_command  : Uint4B
        /// </summary>
        public uint AuxCommand { get; set; }

        /// <summary>
        /// +0x144 moving_away      : Int4B
        /// </summary>
        public bool MovingAway { get; set; }

        /// <summary>
        /// +0x148 initialized      : Int4B
        /// </summary>
        public bool Initialized { get; set; }

        /// <summary>
        /// +0x14c pending_actions  : DLList<MoveToManager::MovementNode>
        /// </summary>
        public List<MovementNode> PendingActions { get; set; }

        /// <summary>
        /// +0x154 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObj { get; set; }

        /// <summary>
        /// +0x158 weenie_obj       : Ptr32 CWeenieObject
        /// </summary>
        public WeenieObject WeenieObj { get; set; }

        public MoveToManager()
        {
            InitializeLocalVars();
        }

        public MoveToManager(PhysicsObject obj, WeenieObject wobj)
        {
            PhysicsObj = obj;
            WeenieObj = wobj;
            InitializeLocalVars();
        }

        //TODO: These are kind of worthless - would it be better to just call the .add to PendingActions list?   Coral Golem

        /// <summary>
        /// ----- (00529580) --------------------------------------------------------
        /// void __thiscall MoveToManager::AddMoveToPositionNode(MoveToManager*this)
        /// acclient.c 345120
        /// </summary>
        public void AddMoveToPositionNode()
        {
            PendingActions.Add(new MovementNode(MovementTypes.MoveToPosition));
        }

        /// <summary>
        /// ----- (00529530) --------------------------------------------------------
        /// void __thiscall MoveToManager::AddTurnToHeadingNode(MoveToManager*this, float global_heading)
        /// acclient.c 345096
        /// </summary>
        public void AddTurnToHeadingNode(float heading)
        {
            PendingActions.Add(new MovementNode(MovementTypes.TurnToHeading, heading));
        }

        /// <summary>
        /// ----- (00529250) --------------------------------------------------------
        /// void __thiscall MoveToManager::InitializeLocalVariables(MoveToManager*this)
        /// acclient.c 344913
        /// </summary>
        public void InitializeLocalVars()
        {
            MovementType = MovementTypes.Invalid;

            MovementParams = new MovementParameters();

            PreviousDistanceTime = Timer.CurrentTime;
            OriginalDistanceTime = Timer.CurrentTime;

            PreviousHeading = 0.0f;

            FailProgressCount = 0;
            CurrentCommand = 0;
            AuxCommand = 0;
            MovingAway = false;
            Initialized = false;

            SoughtPosition = new Position();
            CurrentTargetPosition = new Position();

            SoughtObjectId = 0;
            TopLevelObjectId = 0;
            SoughtObjectRadius = 0;
            SoughtObjectHeight = 0;
        }

    }
}
