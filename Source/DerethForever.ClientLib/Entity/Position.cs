/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    public class Position
    {
        /// <summary>
        /// +0x004 objcell_id       : Uint4B
        /// </summary>
        public uint CellId { get; set; }

        /// <summary>
        /// +0x008 frame            : Frame
        /// </summary>
        public Frame Frame { get; set; } = new Frame();

        public static Position Unpack(BinaryReader reader)
        {
            Position p = new Position
            {
                CellId = reader.ReadUInt32(),
                Frame = Frame.Unpack(reader)
            };
            return p;
        }

        public static Position UnpackAsFrame(BinaryReader reader, uint cellId)
        {
            Position p = new Position
            {
                CellId = cellId,
                Frame = Frame.Unpack(reader)
            };
            return p;
        }

        /// <summary>
        /// //----- (00509F60) --------------------------------------------------------
        /// AC1Legacy::Vector3* __thiscall Position::get_offset(Position*this, AC1Legacy::Vector3* result, Position* p)
        /// acclient.c 311690
        /// </summary>
        /// <param name="target"></param>
        public Vector3 GetOffset(Position target)
        {
            Vector3 offset = Vector3.Zero;

            if (this.CellId != target.CellId)
                offset = GetBlockOffset(this.CellId, target.CellId);

            offset.X += target.Frame.Origin.X - this.Frame.Origin.X;
            offset.Y += target.Frame.Origin.Y - this.Frame.Origin.Y;
            offset.Z += target.Frame.Origin.Z - this.Frame.Origin.Z;

            return offset;
        }

        /// <summary>
        /// calculates distances between the specified landcells.  the client takes makes 2 functions
        /// out of this, one left shifting the result 3 bits and the other multiplying by 24.  instead,
        /// we will just multiply by 192 and save the shift operation.
        ///
        /// //----- (0043E630) --------------------------------------------------------
        /// AC1Legacy::Vector3* __cdecl LandDefs::get_block_offset(AC1Legacy::Vector3* result, unsigned int cell_from, unsigned int cell_to)
        /// acclient.c 123110
        /// </summary>
        public static Vector3 GetBlockOffset(uint originBlock, uint targetBlock)
        {
            uint originX = (originBlock & 0xFF000000) >> 24;
            uint originY = (originBlock & 0x00FF0000) >> 16;
            uint targetX = (targetBlock & 0xFF000000) >> 24;
            uint targetY = (targetBlock & 0x00FF0000) >> 16;
            float deltaX = (targetX - originX) * 192.0f;
            float deltaY = (targetY - originY) * 192.0f;

            return new Vector3(deltaX, deltaY, 0f);
        }

        public float Heading(Position position)
        {
            Vector3 dir = GetOffset(position);
            dir.Z = 0.0f;
            return 0.0f;
            // TODO not finished.   Coral Golem
        }

    }
}
