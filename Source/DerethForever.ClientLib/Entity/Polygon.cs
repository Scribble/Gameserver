/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CPolygon in the client
    /// </summary>
    public class Polygon
    {
        /// <summary>
        /// +0x000 vertices         : Ptr32 Ptr32 CVertex
        /// </summary>
        public List<Vector3> Vertices { get; set; } = new List<Vector3>();

        /// <summary>
        /// +0x004 vertex_ids       : Ptr32 Uint2B
        /// </summary>
        public List<short> VertexIds { get; set; } = new List<short>();

        /// <summary>
        /// +0x008 screen           : Ptr32 Ptr32 Vec2Dscreen
        /// </summary>
        public object Screen { get; set; }

        /// <summary>
        /// in practice, this is the index of this element in the parent Polygon array/list
        /// +0x00c poly_id          : Int2B
        /// </summary>
        public ushort PolygonId { get; set; }

        /// <summary>
        /// +0x00e num_pts          : UChar
        /// </summary>
        public byte NumPts { get; set; }

        /// <summary>
        /// Whether it has that textured/bumpiness to it
        /// +0x00f stippling        : Char
        /// </summary>
        public byte Stippling { get; set; }

        /// <summary>
        /// +0x010 sides_type       : Int4B
        /// </summary>
        public int SidesType { get; set; }

        /// <summary>
        /// +0x014 pos_uv_indices   : Ptr32 Char
        /// </summary>
        public List<byte> PosUVIndices { get; set; } = new List<byte>();

        /// <summary>
        /// +0x018 neg_uv_indices   : Ptr32 Char
        /// </summary>
        public List<byte> NegUVIndices { get; set; } = new List<byte>();

        /// <summary>
        /// +0x01c pos_surface      : Uint2B
        /// </summary>
        public ushort PosSurface { get; set; }

        /// <summary>
        /// +0x01e neg_surface      : Uint2B
        /// </summary>
        public ushort NegSurface { get; set; }

        /// <summary>
        /// field insted of property because struct shenanigans
        /// +0x020 plane            : Plane
        /// </summary>
        public Plane Plane;

        /// <summary>
        /// //----- (00538650) --------------------------------------------------------
        /// int __thiscall CPolygon::UnPack(CPolygon *this, void **addr, unsigned int size)
        /// </summary>
        public static Polygon Unpack(BinaryReader reader)
        {
            Polygon obj = new Polygon();

            obj.PolygonId = reader.ReadUInt16();
            obj.NumPts = reader.ReadByte();
            obj.Stippling = reader.ReadByte();
            obj.SidesType = reader.ReadInt32();
            obj.PosSurface = reader.ReadUInt16();
            obj.NegSurface = reader.ReadUInt16();

            for (short i = 0; i < obj.NumPts; i++)
                obj.VertexIds.Add(reader.ReadInt16());

            if ((obj.Stippling & 4) == 0)
            {
                for (short i = 0; i < obj.NumPts; i++)
                    obj.PosUVIndices.Add(reader.ReadByte());
            }

            if (obj.SidesType == 2 && ((obj.Stippling & 8) == 0))
            {
                for (short i = 0; i < obj.NumPts; i++)
                    obj.NegUVIndices.Add(reader.ReadByte());
            }

            if (obj.SidesType == 1)
            {
                obj.NegSurface = obj.PosSurface;
                obj.NegUVIndices = obj.PosUVIndices;
            }

            return obj;
        }

        /// <summary>
        /// //----- (005383D0) --------------------------------------------------------
        /// void __thiscall CPolygon::make_plane(CPolygon *this)
        /// acclient.c 359628
        /// </summary>
        public void MakePlane()
        {
            // takes 3 vertices to make a plane
            if (Vertices.Count < 3)
                return;
            
            Vector3 anchorV = Vertices[0];
            Vector3 normal = Vector3.Zero;

            for (int i = 1; i < Vertices.Count - 1; i++)
            {
                var v1 = Vertices[i] - anchorV;
                var v2 = Vertices[i + 1] - anchorV;

                normal = normal + Vector3.Cross(v1, v2);
            }

            normal.Normalize();

            float distance = 0.0f;
            Vertices.ForEach(v => distance += Vector3.Dot(normal, v));

            Plane.D = distance;
            Plane.Normal = normal;
        }
    }
}
