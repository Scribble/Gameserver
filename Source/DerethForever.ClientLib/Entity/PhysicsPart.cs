/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.DatUtil;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CPhysicsPart in the client
    /// </summary>
    public class PhysicsPart
    {
        /// <summary>
        /// 0x000 CYpt : Float
        /// </summary>
        public float CYpt { get; set; }

        /// <summary>
        /// 0x004 viewer_heading : AC1Legacy::Vector3
        /// </summary>
        public Vector3 ViewerHeading;

        /// <summary>
        /// probably not needed for the server
        /// 0x010 degrades : Ptr32 GfxObjDegradeInfo
        /// </summary>
        public GraphicsObjectDegradeInfo DegradeInfo;

        /// <summary>
        /// 0x014 deg_level : Uint4B
        /// </summary>
        public uint DegradeLevel { get; set; }

        /// <summary>
        /// 0x018 deg_mode : Int4B
        /// </summary>
        public int DegradeMode { get; set; }

        /// <summary>
        /// 0x020 gfxobj : Ptr32 Ptr32 CGfxObj
        /// </summary>
        public List<GraphicsObject> GraphicsObjects { get; set; } = new List<GraphicsObject>();

        /// <summary>
        /// 0x024 gfxobj_scale : AC1Legacy::Vector3
        /// </summary>
        public Vector3 GraphicsObjectScale = Vector3.One;

        /// <summary>
        /// 0x030 pos : Position
        /// </summary>
        public Position Position { get; set; } = new Position();

        /// <summary>
        /// 0x078 draw_pos : Position
        /// </summary>
        public Position DrawPosition { get; set; }

        /// <summary>
        /// 0x0c0 material : Ptr32 Material
        /// </summary>
        public object Material { get; set; }

        /// <summary>
        /// 0x0c4 surfaces : Ptr32 Ptr32 CSurface
        /// </summary>
        public List<Surface> Surfaces = new List<Surface>();

        /// <summary>
        /// 0x0c8 original_palette_id : IDClass
        /// </summary>
        public uint OriginalPaletteId { get; set; }

        /// <summary>
        /// 0x0cc curTranslucency : Float
        /// </summary>
        public float CurrentTranslucency { get; set; }

        /// <summary>
        /// 0x0d0 curDiffuse : Float
        /// </summary>
        public float CurrentDiffuse { get; set; }

        /// <summary>
        /// 0x0d4 curLuminosity : Float
        /// </summary>
        public float CurrentLuminosity { get; set; }

        /// <summary>
        /// 0x0d8 shiftPal : Ptr32 Palette
        /// </summary>
        public Palette ShiftPalette { get; set; }

        /// <summary>
        /// 0x0dc m_current_render_frame_num : Uint4B
        /// </summary>
        public uint CurrentRenderFrameNum { get; set; }

        /// <summary>
        /// 0x0e0 physobj : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObject { get; set; }

        /// <summary>
        /// 0x0e4 physobj_index : Int4B
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// //----- (0050E670) --------------------------------------------------------
        /// void __thiscall CPhysicsPart::SetTranslucency(CPhysicsPart *this, float _translucency)
        /// acclient.c 315488
        /// </summary>
        internal void SetTranslucency(float value)
        {
            // visual properties on a server?  pfffft.
        }

        /// <summary>
        /// //----- (0050E230) --------------------------------------------------------
        /// CPhysicsPart *__cdecl CPhysicsPart::makePhysicsPart(CPhysicsPart *pTemplate)
        /// acclient.c 315204
        /// </summary>
        public static PhysicsPart MakePhysicsPart(PhysicsPart template)
        {
            // todo implement PhysicsPart.MakePhysicsPart
            return null;
        }

        /// <summary>
        /// //----- (0050E760) --------------------------------------------------------
        /// CPhysicsPart *__cdecl CPhysicsPart::makePhysicsPart(IDClass gfxobj_id)
        /// acclient.c 315551
        /// </summary>
        public static PhysicsPart MakePhysicsPart(uint graphicsObjectId)
        {
            PhysicsPart pp = new PhysicsPart();
            if (pp.SetPart(graphicsObjectId))
                return pp;

            return null;
        }

        /// <summary>
        /// //----- (0050E700) --------------------------------------------------------
        /// int __thiscall CPhysicsPart::SetPart(CPhysicsPart *this, IDClass gfxobj_id)
        /// acclient.c 315525
        /// </summary>
        public bool SetPart(uint graphicsObjectId)
        {
            GraphicsObjectDegradeInfo degradeInfo;
            List<GraphicsObject> graphicsObjects;

            if (!LoadGraphics(graphicsObjectId, out degradeInfo, out graphicsObjects))
                return false;

            return SetGraphics(degradeInfo, graphicsObjects);
        }

        /// <summary>
        /// //----- (0050E2E0) --------------------------------------------------------
        /// void __thiscall CPhysicsPart::SetGfxObjArray(CPhysicsPart *this, GfxObjDegradeInfo *new_degrades, CGfxObj **new_gfxobj)
        /// acclient.c 315263
        /// </summary>
        private bool SetGraphics(GraphicsObjectDegradeInfo degradeInfo, List<GraphicsObject> graphicsObjects)
        {
            DegradeInfo = degradeInfo;
            GraphicsObjects = graphicsObjects;
            Surfaces = graphicsObjects[0].Surfaces;

            // there is a whole lot of other stuff going on here that i just don't think we care about
            return true;
        }

        /// <summary>
        /// //----- (0050DCF0) --------------------------------------------------------
        /// int __cdecl CPhysicsPart::LoadGfxObjArray(IDClass i_idRootObject, GfxObjDegradeInfo **new_degrades, CGfxObj ***new_gfxobj)
        /// acclient.c 314892
        /// </summary>
        private bool LoadGraphics(uint graphicsObjectId, out GraphicsObjectDegradeInfo degradeInfo, out List<GraphicsObject> graphicsObjects)
        {
            graphicsObjects = new List<GraphicsObject>();
            degradeInfo = null;

            GraphicsObject g = PortalDatReader.Current.Unpack<GraphicsObject>(graphicsObjectId);
            if (g == null)
                return false;

            if (false)
            {
                // loads qualified data id "26" - GraphicsObjectDegradeInfo
                degradeInfo = PortalDatReader.Current.Unpack<GraphicsObjectDegradeInfo>(0x11000000 | (g.DegradeDid & 0x00FFFFFF));

                foreach (var gfxId in degradeInfo.DegradeList)
                    graphicsObjects.Add(PortalDatReader.Current.Unpack<GraphicsObject>(gfxId.GraphicsObjectId));
            }

            // at the present time, we're not fooling with graphics degradation, so just return the 1 model
            graphicsObjects.Add(g);
            return true;
        }
    }
}
