/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// PalShiftTex in the client.  currently not used.
    /// </summary>
    public class PaletteShiftTexture
    {
        /// <summary>
        /// 0x000 tex_guid : IDClass
        /// </summary>
        public uint Id;

        /// <summary>
        /// 0x004 sub_pal : AC1Legacy::SmartArray(PalShiftSubPal *)
        /// </summary>
        public List<PaletteShiftSubPalette> SubPalettes = new List<PaletteShiftSubPalette>();

        /// <summary>
        /// 0x010 road_code : AC1Legacy::SmartArray(PalShiftRoadCode *)
        /// </summary>
        public List<PaletteShiftRoadCode> RoadCodes = new List<PaletteShiftRoadCode>();

        /// <summary>
        /// 0x01c terrain_pal : AC1Legacy::SmartArray(PalShiftTerrainPal *)
        /// </summary>
        public List<PaletteShiftTerrainPalette> TerrainPalette = new List<PaletteShiftTerrainPalette>();

        /// <summary>
        /// private because all valid instances come from Unpack
        /// </summary>
        private PaletteShiftTexture()
        {
        }

        /// <summary>
        /// //----- (005003F0) --------------------------------------------------------
        /// int __thiscall PalShiftTex::UnPack(PalShiftTex *this, void **addr, unsigned int *size)
        /// acclient.c 300987
        /// </summary>
        public static PaletteShiftTexture Unpack(BinaryReader reader)
        {
            throw new NotImplementedException();
        }
    }
}
