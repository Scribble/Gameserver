/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CAnimation in the client
    /// </summary>
    public class Animation : DatabaseObject
    {
        /// <summary>
        /// 0x038 pos_frames : Ptr32 AFrame
        /// </summary>
        public List<Frame> PosFrames { get; set; } = new List<Frame>();

        /// <summary>
        /// 0x03c part_frames : Ptr32 AnimFrame
        /// </summary>
        public List<AnimationFrame> PartFrames { get; set; } = new List<AnimationFrame>();

        /// <summary>
        /// 0x040 has_hooks : Int4B
        /// </summary>
        public bool HasHooks { get; set; }

        /// <summary>
        /// 0x044 num_parts : Uint4B
        /// </summary>
        public uint NumParts => (uint)PartFrames.Count;

        /// <summary>
        /// 0x048 num_frames : Uint4B
        /// </summary>
        public uint NumFrames => (uint)PosFrames.Count;
    }
}
