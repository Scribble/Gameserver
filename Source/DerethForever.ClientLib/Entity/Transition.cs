/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Entity;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib
{
    public class Transition
    {
        // TODO move offset/name/type to property summary

        /*
        0:000> dt acclient!CTransition
           =00841c5c transition_level : Int4B
           +0x000 object_info      : OBJECTINFO
           +0x020 sphere_path      : SPHEREPATH
           +0x270 collision_info   : COLLISIONINFO
           +0x2f4 cell_array       : CELLARRAY
           +0x310 new_cell_ptr     : Ptr32 CObjCell
        */
        public ObjectInfo ObjectInfo;
        public SpherePath SpherePath;
        public CollisionInfo CollisionInfo;
        public CellArray CellArray;
        // private CObjCell _newCell;

        public Transition()
        {
            Initialize();
        }

        /// <summary>
        /// //----- (00509DD0) --------------------------------------------------------
        /// void __thiscall CTransition::init(CTransition*this)
        /// acclient.c 311595
        /// </summary>
        private void Initialize()
        {
            SpherePath = new SpherePath();
            // TODO implement Transition.Initialize
        }

        /// <summary>
        /// this method required some assembly tracing to identify that it calls OBJECTINFO::init
        /// 
        /// this is the signature being called
        /// void __thiscall CTransition::init_object(CTransition *this, CPhysicsObj *object, int object_state); // idb
        /// 
        /// but because pointers are fun, and because the 0-offset of CTransition is also an OBJECT INFO, a direct jump
        /// to this method actually works.
        /// //----- (0050CF30) --------------------------------------------------------
        /// void __thiscall OBJECTINFO::init(OBJECTINFO* this, CPhysicsObj* _object, int object_state)
        /// acclient.c 314118
        /// </summary>
        public void InitializeObject(PhysicsObject physicsObject, ObjectInfoState state)
        {
            // TODO implement Transition.InitializeObject
        }

        /// <summary>
        /// //----- (0050E850) --------------------------------------------------------
        /// void __thiscall CTransition::init_contact_plane(CTransition*this, unsigned int cell_id, Plane* plane, int is_water)
        /// acclient.c 315598
        /// -- AND --
        /// //----- (0050E8E0) --------------------------------------------------------
        /// void __thiscall CTransition::init_last_known_contact_plane(CTransition* this, unsigned int cell_id, Plane* plane, int is_water)
        /// </summary>
        public void InitializeContactPlane(uint cellId, System.Numerics.Plane plane, bool isWater, bool lastKnownOnly = false)
        {
            CollisionInfo = new CollisionInfo(cellId, plane, isWater, lastKnownOnly);
        }

        /// <summary>
        /// //----- (0050FF20) --------------------------------------------------------
        /// void __thiscall CTransition::init_sliding_normal(CTransition*this, AC1Legacy::Vector3* normal)
        /// acclient.c 317253
        /// </summary>
        public void InitializeSlidingNormal(Vector3 normal)
        {
            CollisionInfo.SlidingNormalValid = true;
            CollisionInfo.SlidingNormal = normal;

            if (CollisionInfo.SlidingNormal.IsInsignificant())
                CollisionInfo.SlidingNormal.Normalize();
            else
                CollisionInfo.SlidingNormal = Vector3.Zero;
        }

        /// <summary>
        /// //----- (00509E50) --------------------------------------------------------
        /// void __thiscall CTransition::init_sphere(CTransition *this, const unsigned int num_sphere, CSphere *sphere, const float scale)
        /// acclient.c 311620
        /// </summary>
        public void InitializeSpheres(Sphere[] spheres, float scale)
        {
            SpherePath.InitializeSpheres(spheres, scale);
        }

        /// <summary>
        /// //----- (00509E60) --------------------------------------------------------
        /// void __thiscall CTransition::init_path(CTransition*this, CObjCell* begin_cell, Position* begin_pos, Position* end_pos)
        /// acclient.c 311626
        /// </summary>
        public void InitializePath(ObjectCell beginCell, Position beginPosition, Position endPosition)
        {
            SpherePath.InitializePath(beginCell, beginPosition, endPosition);
        }

        /// <summary>
        /// //----- (0050C310) --------------------------------------------------------
        /// int __thiscall CTransition::find_valid_position(CTransition*this)
        /// acclient.c 313419
        /// </summary>
        public bool FindValidPosition()
        {
            if (SpherePath.InsertType > 0)
                return FindPlacementPosition();

            return FindTransitionalPosition();
        }

        /// <summary>
        /// //----- (0050C170) --------------------------------------------------------
        /// int __thiscall CTransition::find_placement_position(CTransition*this)
        /// acclient.c 313341
        /// </summary>
        public bool FindPlacementPosition()
        {
            // copy current stuff to the check stuff
            SpherePath.CheckPosition.CellId = SpherePath.CurrentPosition.CellId;
            SpherePath.CheckPosition.Frame = new Frame(SpherePath.CurrentPosition.Frame);
            SpherePath.CheckCell = SpherePath.CurrentCell;

            // clear the flag, set the insert type
            SpherePath.CellArrayValid = false;
            SpherePath.InsertType = SpherePathInsertType.InitialPlacementInsert;

            // make the check
            if (SpherePath.CheckCell != null)
            {
                TransitionState ts = InsertIntoCell(SpherePath.CheckCell, 3);
                // very much not done here
            }

            // TODO finish Transition.FindPlacementPosition

            return false;
        }

        /// <summary>
        /// //----- (0050BDF0) --------------------------------------------------------
        /// int __thiscall CTransition::find_transitional_position(CTransition*this)
        /// acclient.c 313171
        /// </summary>
        public bool FindTransitionalPosition()
        {
            // TODO implement Transition.FindTransitionalPosition 2/13
            return false;
        }

        /// <summary>
        /// //----- (00509E70) --------------------------------------------------------
        /// int __thiscall CTransition::insert_into_cell(CTransition*this, CObjCell* cell, int num_insertion_attempts)
        /// acclient.c 311632
        /// </summary>
        private TransitionState InsertIntoCell(ObjectCell cell, int numAttempts)
        {
            if (cell == null)
                return TransitionState.Collided;

            var result = TransitionState.OK;

            for (int i = 0; i < numAttempts; i++)
            {
                result = cell.FindCollisions(this);

                if (result == TransitionState.OK || result == TransitionState.Collided)
                    return result;

                if (result == TransitionState.Slid)
                {
                    CollisionInfo.ContactPlaneValid = false;
                    CollisionInfo.ContactPlaneIsWater = false;
                }
            }

            return result;
        }
    }
}
