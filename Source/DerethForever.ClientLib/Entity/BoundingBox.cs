/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BBox in the client
    /// </summary>
    public struct BoundingBox
    {
        /// <summary>
        /// +0x000 m_vMin           : Vector3
        /// </summary>
        public Vector3 Minimum;

        /// <summary>
        /// +0x00c m_vMax           : Vector3
        /// </summary>
        public Vector3 Maximum;

        /// <summary>
        /// //----- (0053BF10) --------------------------------------------------------
        /// void __thiscall CVertexArray::BuildBoundingBox(CVertexArray *this)
        /// acclient.c 362715
        /// </summary>
        public static BoundingBox CreateBoundingBox(VertexArray va)
        {
            BoundingBox bb = new BoundingBox();
            va.Vertices.ToList().ForEach(v => bb.AdjustBox(v.Vector));
            return bb;
        }

        /// <summary>
        /// //----- (00534150) --------------------------------------------------------
        /// void __thiscall BBox::AdjustBBox(BBox *this, Vector3 *vc)
        /// acclient.c 356169
        /// </summary>
        public void AdjustBox(Vector3 v)
        {
            if (v.X < Minimum.X)
                Minimum.X = v.X;
            if (v.Y < Minimum.Y)
                Minimum.Y = v.Y;
            if (v.Z < Minimum.Z)
                Minimum.Z = v.Z;

            if (v.X > Maximum.X)
                Maximum.X = v.X;
            if (v.Y > Maximum.Y)
                Maximum.Y = v.Y;
            if (v.Z > Maximum.Z)
                Maximum.Z = v.Z;
        }
    }
}
