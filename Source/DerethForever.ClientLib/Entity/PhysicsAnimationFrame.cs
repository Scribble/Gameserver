/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AnimFrame in the client
    /// </summary>
    public class PhysicsAnimationFrame
    {
        /// <summary>
        /// 0x000 frame : Ptr32 AFrame
        /// </summary>
        public Frame Frame { get; set; }

        /// <summary>
        /// 0x003 num_frame_hooks : Uint4B
        /// </summary>
        public uint NumFrameHooks { get; set; }

        /// <summary>
        /// 0x008 hooks : Ptr32 : CAnimHook
        /// </summary>
        public List<AnimationHook> Hooks { get; set; } = new List<AnimationHook>();

        /// <summary>
        /// 0x00c num_parts : Uint4B
        /// </summary>
        public uint NumParts { get; set; }
    }
}
