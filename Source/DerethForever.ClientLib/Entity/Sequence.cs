/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class Sequence
    {
        // TODO: We are passing values on the create in the client but not sure how that works - this could be wrong Coral Golem
        /// <summary>
        /// +0x000 __VFN_table : Ptr32
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// +0x004 anim_list        : DLList<AnimSequenceNode>
        /// </summary>
        public LinkedList<AnimationSequenceNode> AnimationList { get; set; } = new LinkedList<AnimationSequenceNode>();

        /// <summary>
        /// +0x00c first_cyclic     : Ptr32 AnimSequenceNode
        /// </summary>
        public AnimationSequenceNode FirstCyclic { get; set; } = null;

        /// <summary>
        /// +0x010 velocity         : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Velocity { get; set; } = Vector3.Zero;

        /// <summary>
        /// 0x01c omega : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Omega { get; set; } = Vector3.Zero;

        /// <summary>
        /// 0x028 hook_obj : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject HookObject { get; set; } = null;

        /// <summary>
        /// +0x030 frame_number     : Float
        /// </summary>
        public float FrameNumber { get; set; }

        /// <summary>
        /// 0x038 curr_anim : Ptr32 AnimSequenceNode
        /// </summary>
        public AnimationSequenceNode CurrentAnimation { get; set; }

        /// <summary>
        /// 0x03c placement_frame : Ptr32 AnimFrame
        /// </summary>
        public AnimationFrame PlacementFrame { get; set; }

        /// <summary>
        /// 0x040 placement_frame_id : Uint4B
        /// </summary>
        public Placement PlacementFrameId { get; set; }

        public Sequence()
        {
            Velocity = Vector3.Zero;
            Omega = Vector3.Zero;
            AnimationList = new LinkedList<AnimationSequenceNode>();
        }

        public Sequence(uint id): this()
        {
            // We need to check this for sure
            // this->vfptr = (PackObjVtbl *)&CSequence::`vftable';
            Id = id;
        }

        /// <summary>
        /// //----- (005259D0) --------------------------------------------------------
        /// int __thiscall CSequence::UnPack(CSequence *this, void **addr, unsigned int size)
        /// acclient.c 340783
        /// </summary>
        public static Sequence Unpack(BinaryReader reader)
        {
            // TODO implement Sequence.Unpack
            return null;
        }

        /// <summary>
        /// //----- (00525B80) --------------------------------------------------------
        /// void __thiscall CSequence::update(CSequence *this, long double quantum, Frame* retval)
        /// acclient.c 340951
        /// </summary>
        public void Update(double quantum, Frame frame)
        {
            if (AnimationList?.First != null)
            {
                this.UpdateInternal(quantum, FirstCyclic, FrameNumber, ref frame);
                Apricot();
            }
            else if (frame != null)
            {
                ApplyPhysics(frame, quantum, quantum < 0);
            }
        }

        /// <summary>
        /// //----- (005249B0) --------------------------------------------------------
        /// void __thiscall CSequence::set_placement_frame(CSequence *this, AnimFrame *_placement_frame, unsigned int _id)
        /// acclient.c 339767
        /// </summary>
        public void SetPlacementFrame(AnimationFrame frame, Placement placement)
        {
            PlacementFrame = frame;
            PlacementFrameId = placement;
        }

        /// <summary>
        /// //----- (00524820) --------------------------------------------------------
        /// void __thiscall CSequence::set_object(CSequence *this, CPhysicsObj *_phys_obj)
        /// </summary>
        public void SetObject(PhysicsObject physicsObject)
        {
            HookObject = physicsObject;
        }

        /// <summary>
        /// //----- (00524DC0) --------------------------------------------------------
        /// void __thiscall CSequence::clear_animations(CSequence *this)
        /// acclient.c 340102
        /// </summary>
        public void ClearAnimations()
        {
            // TODO implement Sequence.ClearAnimations
        }

        /// <summary>
        /// //----- (00525510) --------------------------------------------------------
        /// void __thiscall CSequence::append_animation(CSequence *this, AnimData *new_data)
        /// acclient.c 340590
        /// </summary>
        public void AppendAnimation(AnimationData newData)
        {
            // TODO implement Sequence.AppendAnimation
        }

        /// <summary>
        /// //----- (005255D0) --------------------------------------------------------
        /// void __thiscall CSequence::update_internal(CSequence *this, long double quantum, AnimSequenceNode** _curr_anim, long double* _frame_number, Frame* retval)
        /// acclient.c 340659
        /// </summary>
        private void UpdateInternal(double quantum, AnimationSequenceNode animationNode, float frameNumber, ref Frame retVal)
        {
            // TODO implement UpdateInternal
        }

        /// <summary>
        /// //----- (00524B40) --------------------------------------------------------
        /// void __thiscall CSequence::apricot(CSequence *this)
        /// acclient.c 339893
        /// </summary>
        private void Apricot()
        {
            // TODO Implement Apricot
        }

        /// <summary>
        /// //----- (00524970) --------------------------------------------------------
        /// AnimFrame *__thiscall CSequence::get_curr_animframe(CSequence *this)
        /// acclient.c 00524970
        /// </summary>
        public AnimationFrame GetCurrentAnimationFrame()
        {
            if (CurrentAnimation == null)
                return PlacementFrame;

            return CurrentAnimation.GetPartFrame(FrameNumber);
        }

        /// <summary>
        /// //----- (00524AB0) --------------------------------------------------------
        /// void __thiscall CSequence::apply_physics(CSequence*this, Frame* frame, long double quantum, long double sign)
        /// acclient.c 339860
        /// </summary>
        private void ApplyPhysics(Frame frame, double quantum, bool forceNegative)
        {
            double scalar = Math.Abs(quantum);
            scalar *= forceNegative ? -1 : 1;

            frame.Origin.X *= (Velocity.X * (float)scalar);
            frame.Origin.Y *= (Velocity.Y * (float)scalar);
            frame.Origin.Z *= (Velocity.Z * (float)scalar);

            // TODO finish Sequence.ApplyPhysics
        }

    }
}
