/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// This was abstracted from :
    /// ----- (0051E840) --------------------------------------------------------
    /// void __thiscall RawMotionState::AddAction(RawMotionState*this, unsigned int action, float speed, unsigned int stamp, int autonomous)
    /// acclient.c 332582 
    /// </summary>
    public class ActionNode
    {
        /// <summary>
        /// +0x004 action           : Uint4B
        /// </summary>
        public MotionCommand Action { get; set; }

        /// <summary>
        /// +0x008 speed            : Float
        /// </summary>
        public float Speed { get; set; }

        /// <summary>
        ///  +0x00c stamp            : Uint4B
        /// </summary>
        public uint Stamp { get; set; }

        /// <summary>
        ///   +0x010 autonomous       : Int4B
        /// </summary>
        public bool Autonomous { get; set; } // was int in the client

        public ActionNode(MotionCommand action, float speed, uint stamp, bool autonomous)
        {
            Action = action;
            Speed = speed;
            Stamp = stamp;
            Autonomous = autonomous;
        }
    }
}
