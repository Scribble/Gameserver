/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;

namespace DerethForever.ClientLib.Entity.AnimationHooks
{
    public class TransparentHook : IHook
    {
        public float Start { get; private set; }

        public float End { get; private set; }

        public float Time { get; private set; }

        // Note: Unpack not found in client .   GetType and Execute exists
        public static TransparentHook Unpack(BinaryReader reader)
        {
            TransparentHook tp = new TransparentHook
            {
                Start = reader.ReadSingle(),
                End = reader.ReadSingle(),
                Time = reader.ReadSingle()
            };
            return tp;
        }
    }
}
