/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;

namespace DerethForever.ClientLib.Entity.AnimationHooks
{
    public class SoundTweakedHook : IHook
    {
        public uint SoundID { get; private set; }

        public float Priority { get; private set; }

        public float Probability { get; private set; }

        public float Volume { get; private set; }

        /// <summary>
        /// ----- (00527780) --------------------------------------------------------
        /// int __thiscall SoundTweakedHook::UnPack(SoundTweakedHook *this, void **addr, unsigned int size)
        /// acclient.c 343124
        /// </summary>
        public static SoundTweakedHook Unpack(BinaryReader reader)
        {
            SoundTweakedHook st = new SoundTweakedHook
            {
                SoundID = reader.ReadUInt32(),
                Priority = reader.ReadSingle(),
                Probability = reader.ReadSingle(),
                Volume = reader.ReadSingle()
            };
            return st;
        }
    }
}
