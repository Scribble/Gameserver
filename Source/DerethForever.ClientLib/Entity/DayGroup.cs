/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// DayGroup in the client
    /// </summary>
    public class DayGroup
    {
        /// <summary>
        /// private because Unpack is the only valid source of DayGroups
        /// </summary>
        private DayGroup()
        {
        }

        /// <summary>
        /// +0x000 day_name         : AC1Legacy::PStringBase<char>
        /// </summary>
        public string DayName;

        /// <summary>
        /// +0x004 chance_of_occur  : Float
        /// </summary>
        public float ChanceOfOccurance;

        /// <summary>
        /// +0x008 sky_time         : AC1Legacy::SmartArray<SkyTimeOfDay *>
        /// </summary>
        public List<SkyObject> SkyObjects = new List<SkyObject>();

        /// <summary>
        /// +0x014 sky_objects      : AC1Legacy::SmartArray<SkyObject*>
        /// </summary>
        public List<SkyTimeOfDay> SkyTimes = new List<SkyTimeOfDay>();

        /// <summary>
        /// //----- (00501A70) --------------------------------------------------------
        /// int __thiscall DayGroup::UnPack(DayGroup *this, void **addr, unsigned int *size)
        /// acclient.c 302642
        /// </summary>
        public static DayGroup Unpack(BinaryReader reader)
        {
            DayGroup dg = new DayGroup
            {
                ChanceOfOccurance = reader.ReadSingle(),
                DayName = reader.ReadPString()
            };

            uint numSkyObj = reader.ReadUInt32();
            for (uint i = 0; i < numSkyObj; i++)
                dg.SkyObjects.Add(SkyObject.Unpack(reader));

            uint numSkyTimes = reader.ReadUInt32();
            for (uint i = 0; i < numSkyTimes; i++)
                dg.SkyTimes.Add(SkyTimeOfDay.Unpack(reader));

            return dg;
        }
    }
}
