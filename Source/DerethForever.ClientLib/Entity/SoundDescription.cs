/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CSoundDesc in the client
    /// </summary>
    public class SoundDescription
    {
        /// <summary>
        /// private because Unpack is the only valid source of this class
        /// </summary>
        private SoundDescription()
        {   
        }

        /// <summary>
        /// 0x000 stb_desc : AC1Legacy::SmartArray(AmbientSTBDesc *)
        /// </summary>
        public List<AmbientSoundTable> SoundTables = new List<AmbientSoundTable>();

        /// <summary>
        /// //----- (005028D0) --------------------------------------------------------
        /// int __thiscall CSoundDesc::UnPack(CSoundDesc *this, void **addr, unsigned int *size)
        /// acclient.c 303694
        /// </summary>
        public static SoundDescription Unpack(BinaryReader reader)
        {
            SoundDescription sd = new SoundDescription();

            uint num = reader.ReadUInt32();
            for (uint i = 0; i < num; i++)
                sd.SoundTables.Add(AmbientSoundTable.Unpack(reader));

            return sd;
        }
    }
}
