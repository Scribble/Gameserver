/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class ObjectInfo
    {
        /// <summary>
        /// +0x000 object           : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObject;

        /// <summary>
        /// +0x004 state            : Int4B
        /// </summary>
        public ObjectInfoState ObjectInfoState;

        /// <summary>
        /// +0x008 scale            : Float
        /// </summary>
        public float Scale;

        /// <summary>
        /// +0x00c step_up_height   : Float
        /// </summary>
        public float StepUpHeight;

        /// <summary>
        /// +0x010 step_down_height : Float
        /// </summary>
        public float StepDownHeight;

        /// <summary>
        /// +0x014 ethereal         : Int4B
        /// </summary>
        public bool Ethereal; // int32 in client

        /// <summary>
        /// +0x018 step_down        : Int4B
        /// </summary>
        public bool StepDown; // int32 in client

        /// <summary>
        /// +0x01c targetID         : Uint4B
        /// </summary>
        public uint TargetId;

        /// <summary>
        /// this constructor is a substitute for the following method in the client:
        /// //----- (0050CF30) --------------------------------------------------------
        /// void __thiscall OBJECTINFO::init(OBJECTINFO* this, CPhysicsObj* _object, int object_state)
        /// acclient.c 314118
        /// </summary>
        public ObjectInfo(PhysicsObject physicsObject, ObjectInfoState state)
        {
            PhysicsObject = physicsObject;
            ObjectInfoState = state;
            Scale = physicsObject.Scale;
            StepUpHeight = physicsObject.GetStepUpHeight();
            StepDownHeight = physicsObject.GetStepDownHeight();
            Ethereal = physicsObject.State.HasFlag(PhysicsState.Ethereal);
            StepDown = !physicsObject.State.HasFlag(PhysicsState.Missile);

            if (physicsObject?.Instance != null)
            {
                if (physicsObject.Instance.IsImpenetrable())
                    ObjectInfoState |= ObjectInfoState.IsImpenetrable;

                if (physicsObject.Instance.IsPlayer())
                    ObjectInfoState |= ObjectInfoState.IsPlayer;

                if (physicsObject.Instance.IsPK())
                    ObjectInfoState |= ObjectInfoState.IsPK;

                if (physicsObject.Instance.IsPKLite())
                    ObjectInfoState |= ObjectInfoState.IsPkLite;
            }
        }
    }
}
