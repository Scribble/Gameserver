/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Numerics;
using DerethForever.ClientLib.Enum.LandDefs;
using log4net;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CLandBlockStruct in the client.  in the client, this class is the second of the types
    /// that CLandblock derives from.  since C# doesn't do multiple inheritance, this is being
    /// implemented as an interface WITHOUT THE I PREFIX.  I know, i know... bad code.
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1302:InterfaceNamesMustBeginWithI", Justification = "hack to make it look like multiple inheritance")]
    public interface LandblockStruct
    {
        /// <summary>
        /// this isn't necessarily part of LandblockStruct, but comes from DBObj in practice
        /// </summary>
        uint Id { get; }

        /// <summary>
        /// 0x000 vertex_lighting : Ptr32 RGBColor
        /// </summary>
        List<RgbColor> VertexLighting { get; set; }

        /// <summary>
        /// index into LandDefs.Direction array
        /// 
        /// 0x004 trans_dir : LandDefs::Direction
        /// </summary>
        Direction TransDirection { get; set; }

        /// <summary>
        /// 0x008 side_vertex_count : Int4B
        /// </summary>
        int SideVertexCount { get; set;  }

        /// <summary>
        /// 0x00c side_polygon_count : Int4B
        /// </summary>
        int SidePolygonCount { get; set;  }

        /// <summary>
        /// 0x010 side_cell_count : Int4B
        /// </summary>
        int SideCellCount { get; set; }

        /// <summary>
        /// 0x014 water_type : LandDefs::WaterType
        /// </summary>
        Enum.WaterType WaterType { get; set; }

        /// <summary>
        /// deviation from client: 2-D array instead of single array with calculated index
        /// 0x018 height : Ptr32 UChar
        /// </summary>
        byte[,] HeightMap { get; set; }

        /// <summary>
        /// deviation from client: 2-D array instead of single array with calculated index
        /// 0x01c herrain : Ptr32 Uint2B
        /// </summary>
        ushort[,] Terrain { get; set; }

        /// <summary>
        /// 0x020 vertex_array : CVertexArray
        /// </summary>
        VertexArray VertexArray { get; }

        /// <summary>
        /// 0x048 polygons : Ptr32 CPolygon
        /// </summary>
        List<Polygon> Polygons { get; set; }

        /// <summary>
        /// 0x04c num_surface_strips : Uint4B
        /// </summary>
        uint NumSurfaceStrips { get; }

        /// <summary>
        /// 0x050 surface_strips : Ptr32 CSurfaceTriStrips
        /// </summary>
        List<SurfaceTriStrip> SurfaceStrips { get; }

        /// <summary>
        /// 0x054 block_surface_index : uint4B
        /// </summary>
        uint BlockSurfaceIndex { get; }

        /// <summary>
        /// deviation from client: 2-D array instead of single array with calculated index
        /// 0x058 lcell : Ptr32 CLandCell
        /// </summary>
        LandblockCell[,] LandblockCells { get; }

        /// <summary>
        /// deviation from client: 2-D array instead of single array with calculated index
        /// direction of the triangle cut across this landblock.  index is x * 8 + y
        /// 0x05c SWtoNEcut : Ptr32 Int4B
        /// </summary>
        bool[,] SouthWestToNorthEastCut { get; }
    }

    /// <summary>
    /// this is our hack around multiple inheritance.  create an interface and add a bunch
    /// of extention methods
    /// </summary>
    public static class LandblockStructExtensions
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// SURFCHAR TERRAIN_SURF_CHAR_0 in the client
        /// </summary>
        private static readonly bool[] terrainWater = new bool[]
        {
            false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false,
            true,  true,  true,  true,  true, false, false, false,
            false, false, false, false, false, false, false, false,
        };

        /// <summary>
        /// //----- (00531FD0) --------------------------------------------------------
        /// int __thiscall CLandBlockStruct::UnPack(CLandBlockStruct*this, void** addr, unsigned int size)
        /// acclient.c 354109
        /// </summary>
        public static void Unpack(this LandblockStruct landblock, BinaryReader reader)
        {
            // devations from client: using 2d arrays for terrain and height

            // Read in the terrain. 9x9 so 81 records.
            for (int x = 0; x < 9; x++)
                for (int y = 0; y < 9; y++)
                    landblock.Terrain[x, y] = reader.ReadUInt16();

            // Read in the height. 9x9 so 81 records
            for (int x = 0; x < 9; x++)
                for (int y = 0; y < 9; y++)
                    landblock.HeightMap[x, y] = reader.ReadByte();
        }

        /// <summary>
        /// landblock Id was dropped because it's part of the LanblockStruct now
        /// //----- (00532BB0) --------------------------------------------------------
        /// int __thiscall CLandBlockStruct::generate(CLandBlockStruct *this, unsigned int block_id, unsigned int poly_size, LandDefs::Direction tdir)
        /// acclient.c 354762
        /// </summary>
        public static bool Generate(this LandblockStruct landblock, uint polygonSize = 1, Direction direction = Direction.InViewerBlock)
        {
            // afaict, polygonSize (poly_size) is always 1
            uint cellWidth = LandDefs.CellsPerBlock / polygonSize;

            if (landblock.SideCellCount != cellWidth || landblock.TransDirection != direction)
            {
                bool setSideStuff = false;

                if (landblock.SideCellCount != cellWidth)
                {
                    setSideStuff = true;
                    landblock.SideCellCount = (int)cellWidth;
                    landblock.SidePolygonCount = (int)cellWidth;
                    landblock.SideVertexCount = (int)cellWidth + 1;
                    landblock.InitializePolygonVertexArray();
                }

                landblock.TransDirection = direction;
                landblock.ConstructVerticesAndPolygons();

                // refactored into ConstructVerticesAndPolygons
                // landblock.ConstructVertices();
                // if (landblock.TransDirection != Direction.Default && landblock.SideCellCount > 1 && landblock.SideCellCount < 8)
                //     landblock.TransAdjust();

                if (setSideStuff)
                {
                    // refactored into ConstructVerticesAndPolygons
                    // landblock.ConstructPolygons(landblock.Id);

                    // note: this function is currently a no-op
                    landblock.ConstructUVs(true);
                }
                else
                {
                    // refactored into ConstructVerticesAndPolygons
                    // landblock.AdjustPlanes();
                }

                landblock.CalcWater();
            }

            return true;
        }

        /// <summary>
        /// ConstructVertices and ConstructPolygons were two halves of 1 whole.  this method
        /// is a refactoring of how the client did things to put it all in 1 place
        /// </summary>
        private static void ConstructVerticesAndPolygons(this LandblockStruct landblock)
        {
            float polySize = 24.0f;
            int vertCount = landblock.SideVertexCount;
            int polyCount = landblock.SidePolygonCount;
            uint blockX = 0;
            uint blockY = 0;
            ushort polyId = 0;

            if (landblock.Id > 0)
            {
                // the blockX assignment has the effect of being the raw X value left shift 3.
                // the primary (only) use for these values is in using the Prng stuff.
                blockX = (landblock.Id >> 21) & 0x7F8;
                blockY = ((landblock.Id >> 16) & 0xFF) * 8;
            }

            // build the cells
            for (uint x = 0; x < landblock.SideCellCount; x++)
            {
                for (uint y = 0; y < landblock.SideCellCount; y++)
                {
                    var cell = new LandblockCell();
                    cell.Id = landblock.Id & 0xFFFF0000 | (x * 8 + y + 1);

                    // client sets the cell.Position.Frame.Origin.X and Y to some really oddball values. the operating
                    // theory is that is setting the midpoint of the cell to some sensical value, though the use of it
                    // is (at time of writing this) unknown
                    cell.Position.Frame.Origin.X = (x + 0.5f) * polySize;
                    cell.Position.Frame.Origin.Y = (y + 0.5f) * polySize;

                    landblock.LandblockCells[x, y] = cell;
                }
            }

            landblock.VertexArray.Vertices = new Vertex[vertCount * vertCount];
            
            // build the vertices
            for (uint cellX = 0; cellX < vertCount; cellX++)
            {
                for (uint cellY = 0; cellY < vertCount; cellY++)
                {
                    float z = LandDefs.LandHeightTable[landblock.HeightMap[cellX, cellY]];
                    var v = new Vector3(cellX * polySize, cellY * polySize, z);
                    landblock.VertexArray.Vertices[cellX * vertCount + cellY] = new Vertex() { Vector = v };

                    if (cellY < polyCount && cellX < polyCount)
                    {
                        uint prngA = blockX + cellX;
                        uint prngB = blockY + cellY;
                        var rng = Prng.NeSwCut(prngA, prngB);

                        bool isNESW = (rng >= 0.5);
                        landblock.SouthWestToNorthEastCut[cellX, cellY] = isNESW;
                    }
                }
            }

            // apply transverse adjustments
            if (landblock.TransDirection != Direction.Default && landblock.SideCellCount > 1 && landblock.SideCellCount < 8)
                TransAdjust(landblock);

            var verts = landblock.VertexArray.Vertices;
            var s = landblock.SideVertexCount;
            landblock.Polygons = new List<Polygon>();

            // construct the polygons
            for (uint x = 0; x < polyCount; x++)
            {
                for (uint y = 0; y < polyCount; y++)
                {
                    Polygon p1, p2;

                    if (landblock.SouthWestToNorthEastCut[x, y])
                    {
                        p1 = landblock.AddPolygon(verts[x * s + y].Vector, verts[(x + 1) * s + y].Vector, verts[(x + 1) * s + y + 1].Vector, polyId++);
                        p2 = landblock.AddPolygon(verts[x * s + y].Vector, verts[(x + 1) * s + y + 1].Vector, verts[x * s + y + 1].Vector, polyId++);
                    }
                    else
                    {
                        p1 = landblock.AddPolygon(verts[x * s + y].Vector, verts[(x + 1) * s + y].Vector, verts[x * s + y + 1].Vector, polyId++);
                        p2 = landblock.AddPolygon(verts[(x + 1) * s + y].Vector, verts[(x + 1) * s + y + 1].Vector, verts[x * s + y + 1].Vector, polyId++);
                    }

                    p1.MakePlane();
                    p2.MakePlane();

                    // add them to the cell as well
                    landblock.LandblockCells[x, y].Polygons.Add(p1);
                    landblock.LandblockCells[x, y].Polygons.Add(p2);

                    // avoid an extra loop and do some cell work while we're here
                    
                    // client sets the cell.Position.Frame.Origin.X and Y to some really oddball values. the operating
                    // theory is that is setting the midpoint of the cell to some sensical value, though the use of it
                    // is (at time of writing this) unknown
                    landblock.LandblockCells[x, y].Position.Frame.Origin.X = (x + 0.5f) * polySize;
                    landblock.LandblockCells[x, y].Position.Frame.Origin.Y = (y + 0.5f) * polySize;
                }
            }
        }

        /// <summary>
        /// this is NOT the client landblock.  This is a new method as part of the Landblock.Generate
        /// optimization changes.
        /// </summary>
        private static Polygon AddPolygon(this LandblockStruct landblock, Vector3 v1, Vector3 v2, Vector3 v3, ushort polyId)
        {
            Polygon p = new Polygon();
            p.Vertices.Add(v1);
            p.Vertices.Add(v2);
            p.Vertices.Add(v3);
            p.NumPts = 3;
            p.PolygonId = polyId;
            p.SidesType = 0;
            landblock.Polygons.Add(p);
            return p;
        }

        /// <summary>
        /// this function (more or less) allocates all the memory for polygons, vertices, and cells, but
        /// doesn't appear to actually populate anything.  for us, it's a giant no-op.
        /// //----- (00530E90) --------------------------------------------------------
        /// void __thiscall CLandBlockStruct::InitPVArrays(CLandBlockStruct *this)
        /// acclient.c 353132
        /// </summary>
        private static void InitializePolygonVertexArray(this LandblockStruct landblock)
        {
            // client sets a bunch of UV stuff on the vertices, and allocates vertice memory in ways
            // that don't apply to us

            // client then allocates memory for triangles (polygons), and initializes them - we do this
            // in the new Add Polygon method

            // client allocates room for the SWtoNEcut array, we do it statically.

            // client then allocats and calls constructor for all the cells

            // long story short, this method is a no-op.  it's all done in the replacement function above.
        }

        /// <summary>
        /// adds the vertices to the VertexArray collection.  NOT USED.
        /// //----- (005328D0) --------------------------------------------------------
        /// void __thiscall CLandBlockStruct::ConstructVertices(CLandBlockStruct *this)
        /// acclient.c 354621
        /// </summary>
        private static void ConstructVertices(this LandblockStruct landblock)
        {
            throw new NotSupportedException("logic moved to ConstructVerticesAndPolygons");
        }
        
        /// <summary>
        /// //----- (005323D0) --------------------------------------------------------
        /// void __thiscall CLandBlockStruct::TransAdjust(CLandBlockStruct *this)
        /// acclient.c 354313
        /// </summary>
        private static void TransAdjust(this LandblockStruct landblock)
        {
            var td = landblock.TransDirection;
            uint vertexMax = (uint)landblock.SideVertexCount - 1;

            if (td == Direction.NorthWest || td == Direction.North || td == Direction.NorthEast)
            {
                for (uint x = 1; x < landblock.SidePolygonCount; x += 2)
                {
                    var v1 = landblock.VertexArray.GetGroundVertex(x, vertexMax);
                    var z0 = landblock.VertexArray.GetGroundVertex(x - 1, vertexMax).Z;
                    var z2 = landblock.VertexArray.GetGroundVertex(x + 1, vertexMax).Z;
                    v1.Z = (z0 + z2) / 2;
                }
            }

            if (td == Direction.West || td == Direction.NorthWest || td == Direction.SouthWest)
            {
                for (uint y = 1; y < landblock.SidePolygonCount; y += 2)
                {
                    var v1 = landblock.VertexArray.GetGroundVertex(0, y);
                    var z0 = landblock.VertexArray.GetGroundVertex(0, y + 1).Z;
                    var z2 = landblock.VertexArray.GetGroundVertex(0, y - 1).Z;
                    v1.Z = (z0 + z2) / 2;
                }
            }

            if (td == Direction.South || td == Direction.SouthWest || td == Direction.SouthEast)
            {
                for (uint x = 1; x < landblock.SidePolygonCount; x += 2)
                {
                    var v1 = landblock.VertexArray.GetGroundVertex(x, 0);
                    var z0 = landblock.VertexArray.GetGroundVertex(x - 1, 0).Z;
                    var z2 = landblock.VertexArray.GetGroundVertex(x + 1, 0).Z;
                    v1.Z = (z0 + z2) / 2;
                }
            }

            if (td == Direction.East || td == Direction.SouthEast || td == Direction.NorthEast)
            {
                for (uint y = 1; y < landblock.SidePolygonCount; y += 2)
                {
                    var v1 = landblock.VertexArray.GetGroundVertex(vertexMax, y);
                    var z0 = landblock.VertexArray.GetGroundVertex(vertexMax, y + 1).Z;
                    var z2 = landblock.VertexArray.GetGroundVertex(vertexMax, y - 1).Z;
                    v1.Z = (z0 + z2) / 2;
                }
            }

            // client had this.  not REing it unless necessary.
            if (landblock.SideCellCount == 4)
                log.Error($"SideCellCount was 4 for landblock {landblock.Id}.  RE work that was skipped needs to be completed.");
        }

        /// <summary>
        /// this has a dependency on ConstructVertices being done. NOT USED.
        /// //----- (00531D10) --------------------------------------------------------
        /// void __thiscall CLandBlockStruct::ConstructPolygons(CLandBlockStruct *this, unsigned int block_id)
        /// acclient.c 354001
        /// </summary>
        private static void ConstructPolygons(this LandblockStruct landblock, uint landblockId)
        {
            throw new NotSupportedException("logic moved to ConstructVerticesAndPolygons");
        }

        /// <summary>
        /// this function was only ever called during ground polygon generation.  it has been refactored to not
        /// be used during that process.  it now throws a not implemented exception, and this is purposeful.
        /// //----- (005310D0) --------------------------------------------------------
        /// int __thiscall CLandBlockStruct::AddPolygon(CLandBlockStruct *this, unsigned int pindex, unsigned int vindex0, unsigned int vindex1, unsigned int vindex2)
        /// acclient.c 353296
        /// </summary>
        public static Polygon AddPolygon(this LandblockStruct landblock, uint vIndex0, uint vIndex1, uint vIndex2)
        {
            throw new NotSupportedException("logic moved to ConstructVerticesAndPolygons");
        }

        /// <summary>
        /// //----- (005329A0) --------------------------------------------------------
        /// void __userpurge CLandBlockStruct::ConstructUVs(CLandBlockStruct *this@(ecx), __int16 a2@(bx), unsigned int block_id)
        /// acclient.c 354677
        /// </summary>
        public static void ConstructUVs(this LandblockStruct landblock, bool something)
        {
            // todo CONSIDER implementing LandblockStruct.ConstructUVs
        }

        /// <summary>
        /// //----- (005327E0) --------------------------------------------------------
        /// void __thiscall CLandBlockStruct::CalcWater(CLandBlockStruct *this)
        /// acclient.c 354566
        /// </summary>
        public static void CalcWater(this LandblockStruct landblock)
        {
            bool blockHasWater = false;
            bool blockAllWater = true;

            if (landblock.SideCellCount != 8)
            {
                landblock.WaterType = Enum.WaterType.NotWater;
                return;
            }

            for (int x = 0; x < landblock.SideCellCount; x++)
            {
                for (int y = 0; y < landblock.SideCellCount; y++)
                {
                    var cellHasWater = false;
                    var cellAllWater = true;

                    CalcCellWater(landblock, x, y, out cellHasWater, out cellAllWater);
                    if (cellHasWater)
                    {
                        blockHasWater = true;
                        if (cellAllWater)
                            landblock.LandblockCells[x, y].WaterType = Enum.WaterType.EntirelyWater;
                        else
                        {
                            blockAllWater = false;
                            landblock.LandblockCells[x, y].WaterType = Enum.WaterType.PartiallyWater;
                        }
                    }
                    else
                    {
                        blockAllWater = false;
                    }
                }
            }

            if (!blockHasWater)
                landblock.WaterType = Enum.WaterType.NotWater;
            else if (!blockAllWater)
                landblock.WaterType = Enum.WaterType.PartiallyWater;
            else
                landblock.WaterType = Enum.WaterType.EntirelyWater;
        }

        /// <summary>
        /// //----- (00531550) --------------------------------------------------------
        /// void __thiscall CLandBlockStruct::CalcCellWater(CLandBlockStruct *this, int cell_x, int cell_y, int *cell_has_water, int *cell_all_water)
        /// acclient.c 353608
        /// </summary>
        public static void CalcCellWater(this LandblockStruct landblock, int cellX, int cellY, out bool cellHasWater, out bool cellAllWater)
        {
            cellHasWater = false;
            cellAllWater = true;

            if (cellX == int.MaxValue || cellY == int.MaxValue)
                return;

            int offset = cellY + 9 * cellX;

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    var terrainIndex = offset + (9 * i) + j;
                    if (terrainIndex > 63)
                        continue;

                    var waterIndex = (landblock.Terrain[terrainIndex / 8, terrainIndex % 8] >> 2) & 0x1F;

                    if (terrainWater[waterIndex])
                        cellHasWater = true;
                    else
                        cellAllWater = false;
                }
            }
        }
        
        /// <summary>
        /// //----- (00531380) --------------------------------------------------------
        /// void __thiscall CLandBlockStruct::AdjPlanes(CLandBlockStruct *this)
        /// acclient.c 353453
        /// </summary>
        public static void AdjustPlanes(this LandblockStruct landblock)
        {
            landblock.Polygons.ForEach(p => p.MakePlane());
        }
    }
}
