/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CSceneType in the client
    /// </summary>
    public class SceneType
    {
        /// <summary>
        /// +0x000 scene_name       : PStringBase<char>
        /// </summary>
        public string SceneName;

        /// <summary>
        /// +0x004 scenes           : SmartArray<IDClass<_tagDataID,32,0>,1>
        /// </summary>
        public List<uint> Scenes = new List<uint>();

        /// <summary>
        /// +0x010 sound_table_desc : Ptr32 AmbientSTBDesc
        /// </summary>
        public AmbientSoundTable AmbientSoundTable;

        /// <summary>
        /// private because all valid instances come from unpack
        /// </summary>
        internal SceneType()
        {
        }

        /// <summary>
        /// //----- (005032C0) --------------------------------------------------------
        /// int __thiscall CSceneType::unpack(CSceneType *this, void **addr, unsigned int *size)
        /// acclient.c 304485
        /// </summary>
        public static SceneType Unpack(BinaryReader reader)
        {
            SceneType st = new SceneType();

            uint num = reader.ReadUInt32();
            for (uint i = 0; i < num; i++)
                st.Scenes.Add(reader.ReadUInt32());

            return st;
        }
    }
}
