/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// GraphicsResource in the client.  objects that inherit from GraphicsResource typically demonstrate
    /// multiple inheritance, so this is being implemented as an interface, with methods provided via 
    /// extension methods.
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1302:InterfaceNamesMustBeginWithI", Justification = "hack to make it look like multiple inheritance")]
    public interface GraphicsResource
    {
        /// <summary>
        /// +0x008 m_bIsLost : Bool
        /// </summary>
        bool IsLost { get; set; }

        /// <summary>
        /// +0x010 m_TimeUsed : Float
        /// </summary>
        float TimeUsed { get; set; }

        /// <summary>
        /// +0x018 m_FrameUsed : Uint4B
        /// </summary>
        uint FrameUsed { get; set; }

        /// <summary>
        /// +0x01c m_bIsThrashable : Bool
        /// </summary>
        bool IsThrashable { get; set; }

        /// <summary>
        /// +0x01d m_AutoRestore : Bool
        /// </summary>
        bool AutoRestore { get; set; }

        /// <summary>
        /// +0x020 m_nResourceSize
        /// </summary>
        uint ResourceSize { get; set; }

        /// <summary>
        /// +0x024 m_ListIndex
        /// </summary>
        uint ListIndex { get; set; }
    }
}
