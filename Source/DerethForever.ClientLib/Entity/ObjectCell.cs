/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CObjCell in the client
    /// </summary>
    public abstract class ObjectCell : DatabaseObject
    {
        /// <summary>
        /// +0x03c num_shadow_parts : Uint4B
        /// </summary>
        public uint NumShadowParts
        {
            // Not sure if this is right - please verify Coral Golem
            get { return (uint)(ShadowPartList?.Count ?? 0); }
        }

        /// <summary>
        /// 0x040 shadow_part_list : DArray(CShadowPart *)
        /// </summary>
        public List<ShadowPart> ShadowPartList { get; set; } = new List<ShadowPart>();

        /// <summary>
        /// +0x050 water_type       : LandDefs::WaterType
        /// </summary>
        public WaterType WaterType { get; set; }

        /// <summary>
        /// +0x054 pos              : Position
        /// </summary>
        public Position Position { get; set; } = new Position();

        /// <summary>
        /// +0x09c num_objects      : Uint4B
        /// </summary>
        public uint NumObjects { get; set; }

        /// <summary>
        /// +0x0a0 object_list      : DArray(CPhysicsObj *)
        /// </summary>
        public List<PhysicsObject> ObjectList { get; set; } = new List<PhysicsObject>();

        /// <summary>
        /// +0x0b0 num_lights       : Uint4B
        /// </summary>
        public uint NumLights
        {
            // Not sure if this is right - please verify Coral Golem
            get { return (uint)(LightList?.Count ?? 0); }
        }

        /// <summary>
        /// +0x0b4 light_list       : DArray(LIGHTOBJ const *)
        /// </summary>
        public List<LightObject> LightList { get; set; } = new List<LightObject>();

        /// <summary>
        /// +0x0c4 num_shadow_objects : Uint4B
        /// </summary>
        public uint NumShadowObjects
        {
            // Not sure if this is right - please verify Coral Golem
            get { return (uint)(ShadowObjectList?.Count ?? 0); }
        }

        /// <summary>
        /// +0x0c8 shadow_object_list : DArray(CShadowObj *)
        /// </summary>
        public List<ShadowObject> ShadowObjectList { get; set; } = new List<ShadowObject>();

        /// <summary>
        /// +0x0d8 restriction_obj  : Uint4B
        /// </summary>
        public uint RestrictionObject { get; set; } // pointer to something

        /// <summary>
        /// +0x0dc clip_planes      : Ptr32 Ptr32 ClipPlaneList
        /// </summary>
        public List<ClipPlane> ClipPlanes { get; set; } = new List<ClipPlane>(); // double pointer

        /// <summary>
        /// +0x0e0 num_stabs        : Uint4B
        /// </summary>
        public uint NumStabs
        {
            // Not sure if this is right - please verify Coral Golem
            get { return (uint)(Stabs?.Count ?? 0); }
        }

        /// <summary>
        /// +0x0e4 stab_list        : Ptr32 Uint4B
        /// </summary>
        public List<Stab> Stabs { get; set; } = new List<Stab>();

        /// <summary>
        /// +0x0e8 seen_outside     : Int4B
        /// </summary>
        public int SeenOutside { get; set; } // boolean?

        /// <summary>
        /// +0x0ec voyeur_table     : Ptr32 LongNIValHash(GlobalVoyeurInfo)
        /// </summary>
        public List<Voyeur> VoyeurTable { get; set; } = new List<Voyeur>();

        /// <summary>
        /// +0x0f0 myLandBlock_     : Ptr32 CLandBlock
        /// </summary>
        public Landblock MyLandblock { get; set; }

        /// <summary>
        /// doesn't actually exist on CObjCell because it's abstract and C++ is fun that way
        /// </summary>
        public abstract TransitionState FindCollisions(Transition trans);

        /// <summary>
        /// //----- (0052B750) --------------------------------------------------------
        /// signed int __thiscall CObjCell::find_obj_collisions(CObjCell*this, CTransition* transition)
        /// acclient.c 347142
        /// </summary>
        public virtual TransitionState FindObjectCollisions(Transition trans)
        {
            if (trans.SpherePath.InsertType != SpherePathInsertType.InitialPlacementInsert)
                return TransitionState.OK;

            foreach (var shadowObject in ShadowObjectList)
            {
                if (shadowObject is null)
                    continue;

                // TODO continue here after creating shadow object properties necessary to finish
            }

            return TransitionState.OK;
        }

        /// <summary>
        /// doesn't actually exist on CObjCell because it's abstract and C++ is fun that way
        /// </summary>
        public virtual TransitionState FindEnvironmentCollisions(Transition trans)
        {
            return TransitionState.Invalid;
        }

        /// <summary>
        /// //----- (0052BF60) --------------------------------------------------------
        /// void __thiscall CObjCell::add_object(CObjCell *this, CPhysicsObj *_object)
        /// acclient.c 347703
        /// </summary>
        public virtual void AddObject(PhysicsObject item)
        {
            ObjectList.Add(item);

            if (item.Id > 0 && item.ParentId == null && !item.State.HasFlag(PhysicsState.Hidden))
            {
                VoyeurTable.ForEach(v =>
                {
                    if (v.ObjectId != item.Id && v.ObjectId != 0)
                    {
                        // PhysicsObject voyeur = GetObjectA(v.ObjectId);
                        if (true) // if (voyeur != null)
                        {
                            DetectionInfo di = new DetectionInfo();
                            di.ObjectId = item.Id;
                            di.Status = DetectionType.Entered;

                            // todo ObjectCell.AddObject does broadcasting to voyeurs
                            // voyeur.ReceiveDetectionUpdate(di);
                        }
                    }
                });
            }
        }
    }
}
