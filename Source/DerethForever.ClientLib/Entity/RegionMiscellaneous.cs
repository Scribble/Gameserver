/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// RegionMisc in the client
    /// </summary>
    public class RegionMiscellaneous
    {
        /// <summary>
        /// 0x000 version : Uint4B
        /// </summary>
        public uint Version;

        /// <summary>
        /// 0x004 game_map : IDClass
        /// </summary>
        public uint GameMapId;

        /// <summary>
        /// 0x008 autotest_map : IDClass
        /// </summary>
        public uint AutoTestMapId;

        /// <summary>
        /// 0x00c autotest_map_size : Uint4B
        /// </summary>
        public uint AutoTestMapSize;

        /// <summary>
        /// 0x010 clear_cell : IDClass
        /// </summary>
        public uint ClearCellId;

        /// <summary>
        /// 0x014 clear_monster : IDClass
        /// </summary>
        public uint ClearMonsterId;

        /// <summary>
        /// private because all valid instances come from Unpack
        /// </summary>
        private RegionMiscellaneous()
        {
        }

        /// <summary>
        /// //----- (004FEBC0) --------------------------------------------------------
        /// int __thiscall RegionMisc::UnPack(RegionMisc *this, void **addr, unsigned int *size)
        /// acclient.c 299098
        /// </summary>
        public static RegionMiscellaneous Unpack(BinaryReader reader)
        {
            RegionMiscellaneous rm = new RegionMiscellaneous();

            rm.Version = reader.ReadUInt32();
            rm.GameMapId = reader.ReadUInt32();
            rm.AutoTestMapId = reader.ReadUInt32();
            rm.AutoTestMapSize = reader.ReadUInt32();
            rm.ClearCellId = reader.ReadUInt32();
            rm.ClearMonsterId = reader.ReadUInt32();

            return rm;
        }
    }
}
