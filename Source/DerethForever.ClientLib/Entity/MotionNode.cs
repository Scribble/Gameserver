/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class MotionNode
    {
        // Do we need to replicate this ? +0x000 llist_next       : Ptr32 LListData

        /// <summary>
        /// +0x004 context_id       : Uint4B
        /// </summary>
        public uint ContextId { get; set; }

        /// <summary>
        /// +0x008 motion           : Uint4B
        /// </summary>
        public MotionCommand Motion { get; set; }

        /// <summary>
        /// +0x00c jump_error_code  : Uint4B
        /// </summary>
        public uint JumpErrorCode { get; set; }


        /// <summary>
        /// This is created in-line
        /// acclient.c 343413 - 343419
        /// </summary>

        public MotionNode(uint contextId, MotionCommand motion, uint jumpErrorCode)
        {
            ContextId = contextId;
            Motion = motion;
            JumpErrorCode = jumpErrorCode;
        }
    }
}
