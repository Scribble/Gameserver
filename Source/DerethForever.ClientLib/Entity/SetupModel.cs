/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using DerethForever.ClientLib.DatUtil;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// These are client_portal.dat files starting with 0x02
    /// </summary>
    public class SetupModel : DatabaseObject
    {
        /// <summary>
        /// private because SetupModels ONLY come from the dat
        /// </summary>
        private SetupModel()
        {
        }

        /// <summary>
        /// not technically part of the client, but exists in the dat
        /// </summary>
        public SetupModelFlags Bitfield { get; set; }

        /// <summary>
        /// +0x038 num_parts        : Uint4B
        /// </summary>
        public uint NumParts
        {
            get { return (uint)(PartIds?.Count ?? 0); }
        }
        /// <summary>
        /// +0x03c parts            : Ptr32 IDClass
        /// </summary>
        public List<uint> PartIds { get; set; } = new List<uint>();

        /// <summary>
        /// +0x040 parent_index     : Ptr32 Uint4B
        /// </summary>
        public List<uint> ParentIndex { get; set; } = new List<uint>();

        /// <summary>
        /// +0x044 default_scale    : Ptr32 AC1Legacy::Vector3
        /// </summary>
        public List<Vector3> DefaultScale { get; set; } = new List<Vector3>();

        /// <summary>
        /// +0x048 num_cylsphere    : Uint4B
        /// </summary>
        public uint NumClySpheres
        {
            get { return (uint)(CylSpheres?.Count ?? 0); }
        }

        /// <summary>
        /// +0x04c cylsphere        : Ptr32 CCylSphere
        /// </summary>
        public List<CylinderSphere> CylSpheres { get; set; } = new List<CylinderSphere>();

        /// <summary>
        /// +0x050 num_sphere       : Uint4B
        /// </summary>
        public uint NumSpheres
        {
           get { return (uint)(Spheres?.Count ?? 0); }
        }

        /// <summary>
        ///  +0x054 sphere           : Ptr32 CSphere
        /// </summary>
        public List<Sphere> Spheres { get; set; } = new List<Sphere>();

        /// <summary>
        /// +0x058 has_physics_bsp  : Int4B
        /// </summary>
        public bool HasPhysicsBsp => Bitfield.HasFlag(SetupModelFlags.HasPhysicsBsp);

        /// <summary>
        /// +0x05c allow_free_heading : Int4B
        /// </summary>
        public bool AllowFreeHeading => Bitfield.HasFlag(SetupModelFlags.AllowFreeHeading);

        /// <summary>
        /// +0x060 height           : Float
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// +0x064 radius           : Float
        /// </summary>
        public float Radius { get; set; }

        /// <summary>
        /// +0x068 step_down_height : Float
        /// </summary>
        public float StepDownHeight { get; set; }

        /// <summary>
        /// +0x06c step_up_height   : Float
        /// </summary>
        public float StepUpHeight { get; set; }

        /// <summary>
        /// +0x070 sorting_sphere   : CSphere
        /// </summary>
        public Sphere SortingSphere { get; set; } = new Sphere();

        /// <summary>
        /// +0x080 selection_sphere : CSphere
        /// </summary>
        public Sphere SelectionSphere { get; set; } = new Sphere();
        
        /// <summary>
        /// +0x090 num_lights       : Uint4B
        /// </summary>
        public uint NumLights
        {
            get { return (uint)(Lights?.Count ?? 0); }
        }

        /// <summary>
        /// +0x094 lights           : Ptr32 LIGHTINFO
        /// </summary>
        public Dictionary<int, LightInfo> Lights { get; set; } = new Dictionary<int, LightInfo>();
        
        /// <summary>
        /// +0x0a4 holding_locations : Ptr32 LongHash(LocationType)
        /// </summary>
        public Dictionary<int, LocationType> HoldingLocations { get; set; } = new Dictionary<int, LocationType>();

        /// <summary>
        /// +0x0a8 connection_points : Ptr32 LongHash(LocationType)
        /// </summary>
        public Dictionary<int, LocationType> ConnectionPoints { get; set; } = new Dictionary<int, LocationType>();

        /// <summary>
        /// +0x0ac placement_frames : LongHash(PlacementType)
        /// </summary>
        public Dictionary<Placement, PlacementType> PlacementFrames { get; set; } = new Dictionary<Placement, PlacementType>();
        
        /// <summary>
        /// +0x0c4 default_anim_id  : IDClass
        /// </summary>
        public uint DefaultAnimation { get; set; }

        /// <summary>
        /// +0x0c8 default_script_id : IDClass
        /// </summary>
        public uint DefaultScript { get; set; }

        /// <summary>
        /// +0x0cc default_mtable_id : IDClass
        /// </summary>
        public uint DefaultMotionTable { get; set; }

        /// <summary>
        /// +0x0d0 default_stable_id : IDClass
        /// </summary>
        public uint DefaultSoundTable { get; set; }

        /// <summary>
        /// +0x0d4 default_phstable_id : IDClass
        /// </summary>
        public uint DefaultScriptTable { get; set; }

        /// <summary>
        /// //----- (00520C50) --------------------------------------------------------
        /// int __thiscall CSetup::UnPack(CSetup *this, void **addr, unsigned int size)
        /// acclient.c 335521
        /// </summary>
        public static SetupModel Unpack(BinaryReader reader)
        {
            SetupModel m = new SetupModel();

            m.Id = reader.ReadUInt32();
            m.Bitfield = (SetupModelFlags)reader.ReadUInt32();
            
            // Get all the GraphicsObjects in this SetupModel. These are all the 01-types.
            uint numParts = reader.ReadUInt32();
            for (int i = 0; i < numParts; i++)
            {
                m.PartIds.Add(reader.ReadUInt32());
            }

            if ((m.Bitfield & SetupModelFlags.HasParentIndex) > 0)
            {
                for (int i = 0; i < numParts; i++)
                {
                    m.ParentIndex.Add(reader.ReadUInt32());
                }
            }

            if ((m.Bitfield & SetupModelFlags.SpecifiesScale) > 0)
            {
                for (int i = 0; i < numParts; i++)
                {
                    m.DefaultScale.Add(reader.ReadVector());
                }
            }

            int numHoldingLocations = reader.ReadInt32();
            if (numHoldingLocations > 0)
                for (int i = 0; i < numHoldingLocations; i++)
                {
                    int key = reader.ReadInt32();
                    m.HoldingLocations.Add(key, LocationType.Unpack(reader));
                }

            int numConnectionPoints = reader.ReadInt32();
            if (numConnectionPoints > 0)
                for (int i = 0; i < numConnectionPoints; i++)
                {
                    int key = reader.ReadInt32();
                    m.ConnectionPoints.Add(key, LocationType.Unpack(reader));
                }

            int placementsCount = reader.ReadInt32();
            for (int i = 0; i < placementsCount; i++)
            {
                int key = reader.ReadInt32();
                m.PlacementFrames.Add((Placement)key, PlacementType.Unpack(numParts, reader));
            }

            int cylinderSphereCount = reader.ReadInt32();
            for (int i = 0; i < cylinderSphereCount; i++)
            {
                m.CylSpheres.Add(CylinderSphere.Unpack(reader));
            }

            int sphereCount = reader.ReadInt32();
            for (int i = 0; i < sphereCount; i++)
            {
                m.Spheres.Add(Sphere.Unpack(reader));
            }

            m.Height = reader.ReadSingle();
            m.Radius = reader.ReadSingle();
            m.StepDownHeight = reader.ReadSingle();
            m.StepUpHeight = reader.ReadSingle();

            m.SortingSphere = new Sphere(new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle()), reader.ReadSingle());
            m.SelectionSphere = new Sphere(new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle()), reader.ReadSingle());

            int numLights = reader.ReadInt32();
            if (numLights > 0)
            {
                for (int i = 0; i < numLights; i++)
                {
                    int key = reader.ReadInt32();
                    m.Lights.Add(key, LightInfo.Read(reader));
                }
            }

            m.DefaultAnimation = reader.ReadUInt32();
            m.DefaultScript = reader.ReadUInt32();
            m.DefaultMotionTable = reader.ReadUInt32();
            m.DefaultSoundTable = reader.ReadUInt32();
            m.DefaultScriptTable = reader.ReadUInt32();

            return m;
        }

        /// <summary>
        /// reads the model from the dat, sets the sorting sphere and default palcement
        /// //----- (00520090) --------------------------------------------------------
        /// CSetup *__cdecl CSetup::makeSimpleSetup(IDClass gfxobj_id)
        /// acclient.c 334456
        /// </summary>
        public static SetupModel MakeSimpleSetup(uint dId)
        {
            SetupModel sm = new SetupModel();

            sm.Id = dId;
            sm.PartIds.Add(dId);

            GraphicsObject g = PortalDatReader.Current.Unpack<GraphicsObject>(dId);

            if (g != null)
                sm.SortingSphere = g.PhysicsSphere ?? g.DrawingSphere ?? new Sphere();
            else
                return null;

            PlacementType pt = new PlacementType();
            pt.AnimFrame = new AnimationFrame();
            pt.AnimFrame.Frames.Add(new Frame());
            sm.PlacementFrames.Add(Placement.Default, pt);

            return sm;
        }
    }
}
