/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    public class CollisionInfo
    {
        /// <summary>
        /// +0x000 last_known_contact_plane_valid : Int4B
        /// </summary>
        public bool LastKnownContactPlaneValid; // int32 in client

        /// <summary>
        /// +0x004 last_known_contact_plane : Plane
        /// </summary>
        public Plane LastKnownContactPlane;

        /// <summary>
        ///  +0x014 last_known_contact_plane_is_water : Int4B
        /// </summary>
        public bool LastKnownContactPlaneIsWater;

        /// <summary>
        /// +0x018 contact_plane_valid : Int4B
        /// </summary>
        public bool ContactPlaneValid; // int32 in client

        /// <summary>
        /// +0x01c contact_plane    : Plane
        /// </summary>
        public Plane ContactPlane;

        /// <summary>
        /// +0x02c contact_plane_cell_id : Uint4B
        /// </summary>
        public uint ContactPlaneCellId;

        /// <summary>
        /// +0x030 last_known_contact_plane_cell_id : Uint4B
        /// </summary>
        public uint LastKnownContactPlaneCellId;

        /// <summary>
        /// +0x034 contact_plane_is_water : Int4B
        /// </summary>
        public bool ContactPlaneIsWater;

        /// <summary>
        /// +0x038 sliding_normal_valid : Int4B
        /// </summary>
        public bool SlidingNormalValid; // int32 in client

        /// <summary>
        /// +0x03c sliding_normal   : AC1Legacy::Vector3
        /// </summary>
        public Vector3 SlidingNormal;

        /// <summary>
        /// +0x048 collision_normal_valid : Int4B
        /// </summary>
        public bool CollisionNormalValid; // int32 in client

        /// <summary>
        /// +0x04c collision_normal : AC1Legacy::Vector3
        /// </summary>
        public Vector3 CollisionNormal;

        /// <summary>
        /// +0x058 adjust_offset    : AC1Legacy::Vector3
        /// </summary>
        public Vector3 AdjustOffset;

        /// <summary>
        /// +0x064 num_collide_object : Uint4B
        /// </summary>
        public int NumCollisionObjects // use list.Count instead?
        {
            get { return (CollisionObjects?.Count ?? 0); }
        }

        /// <summary>
        /// +0x068 collide_object   : DArray<CPhysicsObj const *>
        /// </summary>
        public List<PhysicsObject> CollisionObjects = new List<PhysicsObject>();

        /// <summary>
        /// +0x078 last_collided_object : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject LastCollidedObject;

        /// <summary>
        /// +0x07c collided_with_environment : Int4B
        /// </summary>
        public bool CollidedWithEnvironment; // int32 in client

        /// <summary>
        /// +0x080 frames_stationary_fall : Int4B
        /// </summary>
        public int FramesStationaryFall; // int32 in client

        /// <summary>
        /// combines 2 client init functions with the lastKnownOnly flag to differentiate.
        ///
        /// //----- (0050E850) --------------------------------------------------------
        /// void __thiscall CTransition::init_contact_plane(CTransition*this, unsigned int cell_id, Plane* plane, int is_water)
        /// acclient.c 315599
        /// -- AND --
        /// //----- (0050E8E0) --------------------------------------------------------
        /// void __thiscall CTransition::init_last_known_contact_plane(CTransition*this, unsigned int cell_id, Plane* plane, int is_water)
        /// </summary>
        public CollisionInfo(uint cellId, Plane plane, bool isWater, bool lastKnownOnly = false)
        {
            LastKnownContactPlaneValid = true;
            LastKnownContactPlane = plane;
            LastKnownContactPlaneIsWater = isWater;
            // this is a possible error in the client code - instead of setting LastKnownContactPlaneCellId,
            // it sets ContactPlaneCellId
            // This code was left to mirror the client - verified that it looks good.   Coral Golem
            ContactPlaneCellId = cellId;

            if (lastKnownOnly)
                return;
            LastKnownContactPlaneCellId = cellId;
            ContactPlaneValid = true;
            ContactPlane = plane;
            ContactPlaneIsWater = isWater;
        }
    }
}
