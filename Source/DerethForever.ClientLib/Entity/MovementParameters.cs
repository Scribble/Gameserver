/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class MovementParameters
    {
        /// <summary>
        /// +0x004 bitfield         : Uint4B
        /// </summary>
        public uint Bitfield { get; set; }

        /// <summary>
        /// +0x004 can_walk         : Pos 0, 1 Bit
        /// </summary>
        public bool CanWalk { get; set; }

        /// <summary>
        ///  +0x004 can_run          : Pos 1, 1 Bit
        /// </summary>
        public bool CanRun { get; set; }

        /// <summary>
        ///    +0x004 can_sidestep     : Pos 2, 1 Bit
        /// </summary>
        public bool CanSidestep { get; set; }

        /// <summary>
        /// +0x004 can_walk_backwards : Pos 3, 1 Bit
        /// </summary>
        public bool CanWalkBackwards { get; set; }

        /// <summary>
        /// +0x004 can_charge       : Pos 4, 1 Bit
        /// </summary>
        public bool CanCharge { get; set; }

        /// <summary>
        ///  +0x004 fail_walk        : Pos 5, 1 Bit
        /// </summary>
        public bool FailWalk { get; set; }

        /// <summary>
        /// +0x004 use_final_heading : Pos 6, 1 Bit
        /// </summary>
        public bool UseFinalHeading { get; set; }

        /// <summary>
        /// +0x004 sticky           : Pos 7, 1 Bit
        /// </summary>
        public bool Sticky { get; set; }

        /// <summary>
        ///  +0x004 move_away        : Pos 8, 1 Bit
        /// </summary>
        public bool MoveAway { get; set; }

        /// <summary>
        /// +0x004 move_towards     : Pos 9, 1 Bit
        /// </summary>
        public bool MoveTowards { get; set; }

        /// <summary>
        ///  +0x004 use_spheres      : Pos 10, 1 Bit
        /// </summary>
        public bool UseSpheres { get; set; }

        /// <summary>
        /// +0x004 set_hold_key     : Pos 11, 1 Bit
        /// </summary>
        public bool SetHoldKey { get; set; }

        /// <summary>
        /// +0x004 autonomous       : Pos 12, 1 Bit
        /// </summary>
        public bool Autonomous { get; set; }

        /// <summary>
        /// +0x004 modify_raw_state : Pos 13, 1 Bit
        /// </summary>
        public bool ModifyRawState { get; set; }

        /// <summary>
        ///   +0x004 modify_interpreted_state : Pos 14, 1 Bit
        /// </summary>
        public bool ModifyInterpretedState { get; set; }

        /// <summary>
        ///   +0x004 cancel_moveto    : Pos 15, 1 Bit
        /// </summary>
        public bool CancelMoveTo { get; set; }

        /// <summary>
        ///    +0x004 stop_completely  : Pos 16, 1 Bit
        /// </summary>
        public bool StopCompletely { get; set; }

        /// <summary>
        /// +0x004 disable_jump_during_link : Pos 17, 1 Bit
        /// </summary>
        public bool DisableJumpDuringLink { get; set; }

        /// <summary>
        /// +0x008 distance_to_object : Float
        /// </summary>
        public float DistanceToObject { get; set; }

        /// <summary>
        /// +0x00c min_distance     : Float
        /// </summary>
        public float MinDistance { get; set; }

        /// <summary>
        ///    +0x010 desired_heading  : Float
        /// </summary>
        public float DesiredHeading { get; set; }

        /// <summary>
        /// +0x014 speed            : Float
        /// </summary>
        public float Speed { get; set; }

        /// <summary>
        ///   +0x018 fail_distance    : Float
        /// </summary>
        public float FailDistance { get; set; }

        /// <summary>
        ///   +0x01c walk_run_threshhold : Float
        /// </summary>
        public float WalkRunThreshold { get; set; }

        /// <summary>
        ///   +0x020 context_id       : Uint4B
        /// </summary>
        public uint ContextId { get; set; }

        /// <summary>
        ///  +0x024 hold_key_to_apply : HoldKey
        /// </summary>
        public HoldKey HoldKeyToApply { get; set; }

        /// <summary>
        /// +0x028 action_stamp     : Uint4B
        /// </summary>
        public uint ActionStamp { get; set; }

        /// <summary>
        /// ----- (00524380) --------------------------------------------------------
        /// void __thiscall MovementParameters::MovementParameters(MovementParameters*this)
        /// acclient.c 339437
        /// </summary>
        public MovementParameters()
        {
            MinDistance = Constants.DefaultMinDistance;
            DistanceToObject = Constants.DefaultDistanceToObject;
            FailDistance = Constants.DefaultFailDistance;
            DesiredHeading = Constants.DefaultDesiredHeading;
            Speed = Constants.DefaultSpeed;
            WalkRunThreshold = Constants.DefaultWalkRunThreshold;
            HoldKeyToApply = HoldKey.Invalid;
            ContextId = Constants.DefaultContextId;
            ActionStamp = Constants.DefaultActionStamp;
            // TODO: Coral Golem - pick up here
        }

        /// <summary>
        /// ----- (0052AA00) --------------------------------------------------------
        /// void __thiscall MovementParameters::get_command(MovementParameters*this, float curr_distance, float curr_heading, unsigned int* command, HoldKey* key, int* moving_away)
        /// acclient.c 346176
        /// </summary>
        public void GetCommand(float dist, float heading, ref MotionCommand motion, ref HoldKey holdKey, ref bool movingAway)
        {
            if (MoveTowards || !MoveAway)
            {
                if (MoveAway)
                    TowardsAndAway(dist, heading, out motion, ref movingAway);
                else
                {
                    if (dist > DistanceToObject)
                    {
                        motion = MotionCommand.WalkForward;
                        movingAway = false;
                    }
                    else
                        motion = 0;
                }
            }
            else if (MoveAway)
            {
                if (dist < MinDistance)
                {
                    motion = MotionCommand.WalkForward;
                    movingAway = true;
                }
                else
                    motion = 0;
            }

            if (CanRun && (!CanWalk || dist - DistanceToObject > WalkRunThreshold))
                holdKey = HoldKey.Run;
            else
                holdKey = HoldKey.None;
        }

        /// <summary>
        /// ----- (0052AAD0) --------------------------------------------------------
        /// double __stdcall MovementParameters::get_desired_heading(unsigned int command, int moving_away)
        /// acclient.c 346225
        /// </summary>
        public float GetDesiredHeading(MotionCommand motion, bool movingAway)
        {
            switch (motion)
            {
                case MotionCommand.RunForward:
                case MotionCommand.WalkForward:
                    return movingAway ? 180.0f : 0.0f;
                case MotionCommand.WalkBackwards:
                    return movingAway ? 0.0f : 180.0f;
                default:
                    return 0.0f;
            }
        }
        /// <summary>
        /// ----- (0052A9A0) --------------------------------------------------------
        /// void __thiscall MovementParameters::towards_and_away(MovementParameters*this, float curr_distance, float curr_heading, unsigned int* command, int* moving_away)
        /// </summary>
        public void TowardsAndAway(float dist, float heading, out MotionCommand command, ref bool movingAway)
        {
            if (dist > DistanceToObject)
            {
                command = MotionCommand.WalkForward;
                movingAway = false;
            }
            else if (Math.Abs(dist - MinDistance) < Constants.TOLERANCE)
            {
                command = MotionCommand.WalkBackwards;
                movingAway = true;
            }
            else
                command = MotionCommand.Undef;
        }
    }
}
