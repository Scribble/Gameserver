/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class MovementNode
    {
        // +0x000 dllist_next      : Ptr32 DLListData
        // +0x004 dllist_prev      : Ptr32 DLListData

        /// <summary>
        /// +0x008 type             : MovementTypes::Type
        /// </summary>
        public MovementTypes Type { get; set; }

        /// <summary>
        /// +0x00c heading          : Float
        /// </summary>
        public float Heading { get; set; }


        public MovementNode(MovementTypes type, float heading = 0.0f)
        {
            Type = type;
            Heading = heading;
        }
    }
}
