/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AnimSequenceNode in the client
    /// </summary>
    public class AnimationSequenceNode
    {
        /// <summary>
        /// 0x004 dllist_next : Ptr32 DLListData
        /// </summary>
        public AnimationSequenceNode Next { get; set; }

        /// <summary>
        /// 0x008 dllist_prev : Ptr32 DLLIstData
        /// </summary>
        public AnimationSequenceNode Previous { get; set; }

        /// <summary>
        /// 0x00c anim : Ptr32 Animation
        /// </summary>
        public Animation Animation { get; set; }

        /// <summary>
        /// +0x010 framerate        : Float
        /// </summary>
        public float Framerate { get; set; }

        /// <summary>
        /// +0x014 low_frame        : Int4B
        /// </summary>
        public int LowFrame { get; set; }

        /// <summary>
        /// +0x018 high_frame       : Int4B
        /// </summary>
        public int HighFrame { get; set; }

        /// <summary>
        /// ----- (00525F40) --------------------------------------------------------
        /// int __thiscall AnimSequenceNode::UnPack(AnimSequenceNode*this, void** addr, unsigned int size)
        /// acclient.c 341257
        /// </summary>
        public static AnimationSequenceNode Unpack(BinaryReader reader, uint size)
        {
            const uint maxSize = 0x10;
            uint amimationID;

            // Behemoth - I just copied the code here - I was not quite sure about this so I stopped here

            // int result; // eax@2
            // char* v4; // edx@3
            // IDClass < _tagDataID,32,0 > v5; // esi@3
            // char* v6; // edx@3
            // char* v7; // edx@3

            // if (size >= 0x10)
            // {
            //    v4 = (char*)*addr + 4;
            //    v5.id = *(_DWORD*)*addr;
            //    *addr = v4;
            //    this->low_frame = *(_DWORD*)v4;
            //    v6 = (char*)*addr + 4;
            //    *addr = v6;
            //    this->high_frame = *(_DWORD*)v6;
            //    v7 = (char*)*addr + 4;
            //    *addr = v7;
            //    LODWORD(this->framerate) = *(_DWORD*)v7;
            //    *addr = (char*)*addr + 4;
            //    AnimSequenceNode::set_animation_id(this, v5);
            //    result = 1;
            // }
            // else
            // {
            //    result = 0;
            // }
            // return result;
            throw new NotImplementedException();

            if (size <= maxSize)
            {
                // put the real code here.
                return null;
            }
            return null;
        }

        /// <summary>
        /// //----- (00525C40) --------------------------------------------------------
        /// int __thiscall AnimSequenceNode::get_part_frame(AnimSequenceNode *this, int fn)
        /// acclient.c 340996
        /// </summary>
        public AnimationFrame GetPartFrame(float value)
        {
            if (value > 0 && value < Animation.PartFrames.Count)
                return Animation.PartFrames[(int)value];

            return null;
        }
    }
}
