/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    public class LightInfo
    {
        /// <summary>
        /// +0x044 viewerspace_location : AC1Legacy::Vector3
        /// </summary>
        public Frame ViewerspaceLocation { get; set; }

        /// <summary>
        /// +0x050 color            : RGBColor
        /// </summary>
        public uint Color { get; set; } // _RGB Color. Red is bytes 3-4, Green is bytes 5-6, Blue is bytes 7-8. Bytes 1-2 are always FF (?)

        /// <summary>
        /// +0x060 falloff          : Float
        /// </summary>
        public float Intensity { get; set; }

        /// <summary>
        /// +0x060 falloff          : Float
        /// </summary>
        public float Falloff { get; set; }

        /// <summary>
        /// +0x064 cone_angle       : Float
        /// </summary>
        public float ConeAngle { get; set; }

        public static LightInfo Read(BinaryReader datReader)
        {
            LightInfo obj = new LightInfo();

            obj.ViewerspaceLocation = Frame.Unpack(datReader);
            obj.Color = datReader.ReadUInt32();
            obj.Intensity = datReader.ReadSingle();
            obj.Falloff = datReader.ReadSingle();
            obj.ConeAngle = datReader.ReadSingle();
            return obj;
        }
    }
}
