/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// SkyTimeOfDay in the client
    /// </summary>
    public class SkyTimeOfDay
    {
        // TODO move offset/name/type to property summary

        /*
        0:000> dt acclient!SkyTimeOfDay
            +0x000 begin            : Float
            +0x004 dir_bright       : Float
            +0x008 dir_heading      : Float
            +0x00c dir_pitch        : Float
            +0x010 dir_color        : RGBAUnion
            +0x014 amb_bright       : Float
            +0x018 amb_color        : RGBAUnion
            +0x01c world_fog        : Int4B
            +0x020 min_world_fog    : Float
            +0x024 max_world_fog    : Float
            +0x028 world_fog_color  : RGBAUnion
            +0x02c sky_obj_replace  : AC1Legacy::SmartArray<SkyObjectReplace *>
        */

        /// <summary>
        /// private because Unpack is the only valid source for this class
        /// </summary>
        private SkyTimeOfDay()
        {
        }
        
        public float Begin;
        public float DirBright;
        public float DirHeading;
        public float DirPitch;

        /// <summary>
        /// RGBA union
        /// </summary>
        public uint DirColor;

        public float AmbBright;

        /// <summary>
        /// RGBA union
        /// </summary>
        public uint AmbColor;

        public int WorldFog;
        public float MinWorldFog;
        public float MaxWorldFog;

        /// <summary>
        /// RGBA union
        /// </summary>
        public uint WorldFogColor;

        public List<SkyObjectReplace> SkyObjReplace = new List<SkyObjectReplace>();

        /// <summary>
        /// //----- (005015B0) --------------------------------------------------------
        /// unsigned int __thiscall SkyTimeOfDay::UnPack(SkyTimeOfDay *this, void **addr, unsigned int *size)
        /// acclient.c 302257
        /// </summary>
        public static SkyTimeOfDay Unpack(BinaryReader reader)
        {
            SkyTimeOfDay s = new SkyTimeOfDay();

            s.Begin = reader.ReadSingle();
            s.DirBright = reader.ReadSingle();
            s.DirHeading = reader.ReadSingle();
            s.DirPitch = reader.ReadSingle();
            s.DirColor = reader.ReadUInt32();

            s.AmbBright = reader.ReadSingle();
            s.AmbColor = reader.ReadUInt32();

            s.MinWorldFog = reader.ReadSingle();
            s.MaxWorldFog = reader.ReadSingle();
            s.WorldFogColor = reader.ReadUInt32();
            s.WorldFog = reader.ReadInt32();

            uint numReplacements = reader.ReadUInt32();
            for (int i = 0; i < numReplacements; i++)
                s.SkyObjReplace.Add(SkyObjectReplace.Unpack(reader));

            return s;
        }
    }
}
