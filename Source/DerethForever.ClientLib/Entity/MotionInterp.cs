/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.Collections.Generic;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class MotionInterp
    {
        /// <summary>
        /// +0x000 initted          : Int4B
        /// </summary>
        public bool Initted { get; set; }

        /// <summary>
        /// +0x004 weenie_obj       : Ptr32 CWeenieObject
        /// </summary>
        public WeenieObject WeenieObj { get; set; }

        /// <summary>
        /// +0x008 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObj { get; set; }

        /// <summary>
        /// +0x00c raw_state        : RawMotionState
        /// </summary>
        public RawMotionState RawState { get; set; }

        /// <summary>
        /// +0x044 interpreted_state : InterpretedMotionState
        /// </summary>
        public InterpretedMotionState InterpretedState { get; set; }

        /// <summary>
        /// +0x06c current_speed_factor : Float
        /// </summary>
        public float CurrentSpeedFactor { get; set; }

        /// <summary>
        /// +0x070 standing_longjump : Int4B
        /// </summary>
        public bool StandingLongJump { get; set; }

        /// <summary>
        /// +0x074 jump_extent      : Float
        /// </summary>
        public float JumpExtent { get; set; }

        /// <summary>
        /// +0x078 server_action_stamp : Uint4B
        /// </summary>
        public int ServerActionStamp { get; set; }

        /// <summary>
        /// +0x07c my_run_rate      : Float
        /// </summary>
        public float MyRunRate { get; set; }

        /// <summary>
        /// +0x080 pending_motions  : LList<CMotionInterp::MotionNode>
        /// </summary>
        public List<MotionNode> PendingMotions { get; set; }

        // TODO: RE methods

        public MotionInterp(PhysicsObject physicsObj, WeenieObject weenieObj)
        {
            CurrentSpeedFactor = Constants.DefaultSpeed;
            MyRunRate = Constants.DefaultSpeed;

            SetPhysicsObject(physicsObj);
            SetWeenieObject(weenieObj);
        }

        /// <summary>
        /// ----- (00528870) --------------------------------------------------------
        /// void __thiscall CMotionInterp::apply_current_movement(CMotionInterp*this, int cancel_moveto, int disallow_jump)
        /// acclient.c 344302
        /// </summary>
        public void ApplyCurrentMovement(bool cancelMoveTo, bool disallowJump)
        {
            if (PhysicsObj == null || !Initted)
                return;

            if (WeenieObj == null || WeenieObj.IsCreature() && PhysicsObj.MovmentIsAutonomous())
                ApplyRawMovement(cancelMoveTo, disallowJump);
            else
                ApplyInterpretedMovement(cancelMoveTo, disallowJump);
        }

        /// <summary>
        /// ----- (005287E0) --------------------------------------------------------
        /// void __thiscall CMotionInterp::apply_raw_movement(CMotionInterp*this, int cancel_moveto, int disallow_jump)
        /// acclient.c 344260
        /// </summary>

        public void ApplyRawMovement(bool cancelMoveTo, bool disallowJump)
        {
            if (PhysicsObj == null)
                return;

            InterpretedState.CurrentStyle = RawState.CurrentStyle;
            InterpretedState.ForwardCommand = RawState.ForwardCommand;
            InterpretedState.ForwardSpeed = RawState.ForwardSpeed;
            InterpretedState.SideStepCommand = RawState.SideStepCommand;
            InterpretedState.SideStepSpeed = RawState.SideStepSpeed;
            InterpretedState.TurnCommand = RawState.TurnCommand;
            InterpretedState.TurnSpeed = RawState.TurnSpeed;

            AdjustMotion(InterpretedState.ForwardCommand, InterpretedState.ForwardSpeed, RawState.ForwardHoldKey);
            AdjustMotion(InterpretedState.SideStepCommand, InterpretedState.SideStepSpeed, RawState.SideStepHoldKey);
            AdjustMotion(InterpretedState.TurnCommand, InterpretedState.TurnSpeed, RawState.TurnHoldKey);

            ApplyInterpretedMovement(cancelMoveTo, disallowJump);
        }

        /// <summary>
        /// ----- (00528600) --------------------------------------------------------
        /// void __thiscall CMotionInterp::apply_interpreted_movement(CMotionInterp*this, int cancel_moveto, int disallow_jump)
        /// acclient.c 344148
        /// </summary>
        public void ApplyInterpretedMovement(bool cancelMoveTo, bool disallowJump)
        {
            if (PhysicsObj == null)
                return;

            // Double check this - I simplified it and it needs a second look.  Coral Golem
            MovementParameters movementParams = new MovementParameters
            {
                CancelMoveTo = cancelMoveTo,
                DisableJumpDuringLink = disallowJump
            };

            if (InterpretedState.ForwardCommand == MotionCommand.RunForward)
                MyRunRate = InterpretedState.ForwardSpeed;

            DoInterpretedMotion(InterpretedState.CurrentStyle, movementParams);

            if (ContactAllowsMove(InterpretedState.ForwardCommand))
            {
                if (!StandingLongJump)
                {
                    movementParams.Speed = InterpretedState.ForwardSpeed;
                    DoInterpretedMotion(InterpretedState.ForwardCommand, movementParams);

                    if (InterpretedState.SideStepCommand != 0)
                    {
                        movementParams.Speed = InterpretedState.SideStepSpeed;
                        DoInterpretedMotion(InterpretedState.SideStepCommand, movementParams);
                    }
                    else
                        StopInterpretedMotion(MotionCommand.SideStepRight, movementParams);
                }
                else
                {
                    movementParams.Speed = Constants.DefaultSpeed;
                    DoInterpretedMotion(MotionCommand.Ready, movementParams);
                    StopInterpretedMotion(MotionCommand.SideStepRight, movementParams);
                }
            }
            else
            {
                movementParams.Speed = Constants.DefaultSpeed;
                DoInterpretedMotion(MotionCommand.Falling, movementParams);
            }

            if (InterpretedState.TurnCommand != 0)
            {
                movementParams.Speed = InterpretedState.TurnSpeed;
                DoInterpretedMotion(InterpretedState.TurnCommand, movementParams);
            }
            else
            {
                if (StopInterpretedMotion(MotionCommand.TurnRight, movementParams) != null)
                {
                    // AddToQueue in the client
                    PendingMotions.Add(new MotionNode(movementParams.ContextId, MotionCommand.Ready, 0));

                    if (movementParams.ModifyInterpretedState)
                        InterpretedState.RemoveMotion(MotionCommand.TurnRight);
                }

                if (PhysicsObj.Cell == null)
                    PhysicsObj.RemoveLinkAnimations();
            }
        }

        /// <summary>
        /// ----- (00528360) --------------------------------------------------------
        /// signed int __thiscall CMotionInterp::DoInterpretedMotion(CMotionInterp*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 343976
        /// </summary>
        public Sequence DoInterpretedMotion(MotionCommand motion, MovementParameters movementParams)
        {
            if (PhysicsObj == null)
                return new Sequence(8);

            Sequence sequence = new Sequence();

            if (ContactAllowsMove(motion))
            {
                if (StandingLongJump && (motion == MotionCommand.WalkForward||
                    motion == MotionCommand.RunForward ||
                    motion == MotionCommand.SideStepRight))
                {
                    if (movementParams.ModifyInterpretedState)
                        InterpretedState.ApplyMotion(motion, movementParams);
                }
                else
                {
                    if (motion == MotionCommand.Dead)
                        PhysicsObj.RemoveLinkAnimations();

                    sequence = PhysicsObj.DoInterpretedMotion(motion, movementParams);

                    if (sequence == null)
                    {
                        uint jumpErrorCode = 0;

                        if (movementParams.DisableJumpDuringLink)
                            jumpErrorCode = 0x48;
                        else
                        {
                            jumpErrorCode = MotionAllowsJump(motion);

                            if (jumpErrorCode == 0 && (motion & (MotionCommand)0x10000000) != 0)
                                jumpErrorCode = MotionAllowsJump(InterpretedState.ForwardCommand);
                        }

                        // AddToQueue in the client
                        PendingMotions.Add(new MotionNode(movementParams.ContextId, motion, jumpErrorCode));

                        if (movementParams.ModifyInterpretedState)
                            InterpretedState.ApplyMotion(motion, movementParams);
                    }
                }
            }
            else
            {
                if ((motion & (MotionCommand)0x10000000) != 0)
                    sequence.Id = 0x24;

                else if (movementParams.ModifyInterpretedState)
                    InterpretedState.ApplyMotion(motion, movementParams);
            }

            if (PhysicsObj != null && PhysicsObj.Cell == null)
                PhysicsObj.RemoveLinkAnimations();

            return sequence;
        }

        /// <summary>
        /// ----- (00528010) --------------------------------------------------------
        /// void __thiscall CMotionInterp::adjust_motion(CMotionInterp*this, unsigned int* motion, float* speed, HoldKey key)
        /// acclient.c 343747
        /// </summary>
        public void AdjustMotion(MotionCommand motion, float speed, HoldKey holdKey)
        {
            if (WeenieObj != null && !WeenieObj.IsCreature())
                return;

            switch (motion)
            {
                case MotionCommand.RunForward:
                    return;

                case MotionCommand.WalkBackwards:
                    motion = MotionCommand.WalkForward;
                    speed *= -Constants.BackwardsFactor;
                    break;

                case MotionCommand.TurnLeft:
                    motion = MotionCommand.TurnRight;
                    speed *= -Constants.DefaultSpeed;
                    break;

                case MotionCommand.SideStepLeft:
                    motion = MotionCommand.SideStepRight;
                    speed *= -Constants.DefaultSpeed;
                    break;

                case MotionCommand.SideStepRight:
                    speed = speed * Constants.DefaultSideStepRightAdj;
                    break;
            }

            if (holdKey == HoldKey.Invalid)
                holdKey = RawState.CurrentHoldKey;

            if (holdKey == HoldKey.Run)
                ApplyRunToCommand(ref motion, ref speed);
        }

        /// <summary>
        /// ----- (00527BE0) --------------------------------------------------------
        /// void __thiscall CMotionInterp::apply_run_to_command(CMotionInterp*this, unsigned int* motion, float* speed)
        /// acclient.c 343440
        /// </summary>
        public void ApplyRunToCommand(ref MotionCommand motion, ref float speed)
        {
            float speedMod = 1.0f;

            if (WeenieObj != null)
            {
                float runFactor = 0.0f;
                speedMod = WeenieObj.InqRunRate(ref runFactor) ? runFactor : MyRunRate;
            }
            switch (motion)
            {
                case MotionCommand.WalkForward:
                    if (speed > 0.0f)
                        motion = MotionCommand.RunForward;

                    speed *= speedMod;
                    break;

                case MotionCommand.TurnRight:
                    speed *= Constants.RunTurnFactor;
                    break;

                case MotionCommand.SideStepRight:
                    speed *= speedMod;

                    if (Constants.MaxSidestepAnimRate < Math.Abs(speed))
                    {
                        if (speed > 0.0f)
                            speed = Constants.MaxSidestepAnimRate;
                        else
                            speed = -Constants.MaxSidestepAnimRate;
                    }
                    break;
            }
        }

        /// <summary>
        /// ----- (00528240) --------------------------------------------------------
        /// int __thiscall CMotionInterp::contact_allows_move(CMotionInterp*this, unsigned int motion)
        /// acclient.c 343883
        /// </summary>
        public bool ContactAllowsMove(MotionCommand motion)
        {
            if (PhysicsObj == null)
                return false;

            if (motion == MotionCommand.Dead || motion == MotionCommand.Falling || motion >= MotionCommand.TurnRight && motion <= MotionCommand.TurnLeft)
                return true;

            if (WeenieObj != null && !WeenieObj.IsCreature())
                return true;

            if (!PhysicsObj.State.HasFlag(PhysicsState.Gravity))
                return true;

            if (!PhysicsObj.TransientState.HasFlag(TransientState.Contact))
                return false;

            return PhysicsObj.TransientState.HasFlag(TransientState.Walkable);
        }

        /// <summary>
        /// ----- (00528470) --------------------------------------------------------
        /// signed int __thiscall CMotionInterp::StopInterpretedMotion(CMotionInterp*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 344035
        /// </summary>
        public Sequence StopInterpretedMotion(MotionCommand motion, MovementParameters movementParams)
        {
            if (PhysicsObj == null)
            {
                // Magic number in the client - result = 8; acclient.c - 344076
                return new Sequence(8);
            }

            Sequence sequence = new Sequence();

            if (ContactAllowsMove(motion))
            {
                if (StandingLongJump && (motion == MotionCommand.WalkForward || motion == MotionCommand.RunForward || motion == MotionCommand.SideStepRight))
                {
                    if (movementParams.ModifyInterpretedState)
                        InterpretedState.RemoveMotion(motion);
                }
                else
                {
                    sequence = PhysicsObj.StopInterpretedMotion(motion, movementParams);

                    if (sequence == null)
                    {
                        // AddToQueue in the client
                        PendingMotions.Add(new MotionNode(movementParams.ContextId, MotionCommand.Ready, 0));

                        if (movementParams.ModifyInterpretedState)
                            InterpretedState.RemoveMotion(motion);
                    }
                }
            }
            else
            {
                if (movementParams.ModifyInterpretedState)
                    InterpretedState.RemoveMotion(motion);
            }

            if (PhysicsObj.Cell == null)
                PhysicsObj.RemoveLinkAnimations();

            return sequence;
        }

        /// <summary>
        /// ----- (005279E0) --------------------------------------------------------
        /// signed int __stdcall CMotionInterp::motion_allows_jump(unsigned int substate)
        /// acclient.c 343296
        /// </summary>
        public uint MotionAllowsJump(MotionCommand subState)
        {
            if (subState >= MotionCommand.Reload && subState <= MotionCommand.Pickup ||
                subState >= MotionCommand.TripleThrustLow && subState <= MotionCommand.MagicPowerUp07Purple||
                subState >= MotionCommand.MagicPowerUp01 && subState <= MotionCommand.MagicPowerUp10 ||
                subState >= MotionCommand.Crouch && subState <= MotionCommand.Sleeping ||
                subState >= MotionCommand.AimLevel && subState <= MotionCommand.MagicPray ||
                subState == MotionCommand.Fallen)
            {
                // magic number in the client 343315
                return 72;
            }
            return 0;
        }

        /// <summary>
        /// ----- (00528970) --------------------------------------------------------
        /// void __thiscall CMotionInterp::SetPhysicsObject(CMotionInterp*this, CPhysicsObj* _physics_obj)
        /// acclient.c 344355
        /// </summary>
        public void SetPhysicsObject(PhysicsObject obj)
        {
            PhysicsObj = obj;
            ApplyCurrentMovement(true, true);
        }

        /// <summary>
        /// ----- (00528920) --------------------------------------------------------
        /// void __thiscall CMotionInterp::SetWeenieObject(CMotionInterp*this, CWeenieObject* _weenie_obj)
        /// acclient.c 344336
        /// </summary>
        public void SetWeenieObject(WeenieObject wobj)
        {
            WeenieObj = wobj;
            ApplyCurrentMovement(true, true);
        }
    }
}
