/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System.Collections.Generic;
using System.Numerics;
using DerethForever.ClientLib.Entity;
using DerethForever.ClientLib.Enum;

public class PhysicsDesc
{
    /// <summary>
    /// +0x004 bitfield         : Uint4B
    /// </summary>
    public uint Bitfield { get; set; }

    /// <summary>
    /// +0x008 movement_buffer  : Ptr32 Void
    /// </summary>
    public object MovementBuffer { get; set; }

    /// <summary>
    /// +0x010 autonomous_movement : Int4B
    /// </summary>
    public bool AutonomousMovement { get; set; }

    /// <summary>
    /// +0x014 animframe_id     : Uint4B
    /// </summary>
    public uint AnimFrameId { get; set; }

    /// <summary>
    /// +0x018 pos              : Position
    /// </summary>
    public Position Pos { get; set; }

    /// <summary>
    /// +0x060 state            : Uint4B
    /// </summary>
    public PhysicsState State { get; set; }

    /// <summary>
    /// +0x064 object_scale     : Float
    /// </summary>
    public float ObjectScale { get; set; }

    /// <summary>
    /// +0x068 friction         : Float
    /// </summary>
    public float Friction { get; set; }

    /// <summary>
    /// +0x06c elasticity       : Float
    /// </summary>
    public float Elasticity { get; set; }

    /// <summary>
    /// +0x070 translucency     : Float
    /// </summary>
    public float Translucency { get; set; }

    /// <summary>
    /// +0x074 velocity         : AC1Legacy::Vector3
    /// </summary>
    public Vector3 Velocity { get; set; }

    /// <summary>
    /// +0x080 acceleration     : AC1Legacy::Vector3
    /// </summary>
    public Vector3 Acceleration { get; set; }

    /// <summary>
    /// +0x08c omega            : AC1Legacy::Vector3
    /// </summary>
    public Vector3 Omega { get; set; }

    /// <summary>
    /// +0x098 num_children     : Uint4B
    /// </summary>
    public uint NumChildren { get; set; }

    /// <summary>
    /// +0x09c child_ids        : Ptr32 Uint4B
    /// </summary>
    public List<int> ChildIds { get; set; }

    /// <summary>
    /// +0x0a0 child_location_ids : Ptr32 Uint4B
    /// </summary>
    public List<int> ChildLocationIds { get; set; }

    /// <summary>
    /// +0x0a4 parent_id        : Uint4B
    /// </summary>
    public uint ParentId { get; set; }

    /// <summary>
    /// +0x0a8 location_id      : Uint4B
    /// </summary>
    public uint LocationId { get; set; }

    /// <summary>
    /// +0x0ac mtable_id        : IDClass<_tagDataID,32,0>
    /// </summary>
    public uint MotionTableId { get; set; }

    /// <summary>
    /// +0x0b0 stable_id        : IDClass<_tagDataID,32,0>
    /// </summary>
    public uint SoundTableId { get; set; }

    /// <summary>
    /// +0x0b4 phstable_id      : IDClass<_tagDataID,32,0>
    /// </summary>
    public uint PhsyicsTableId { get; set; }

    /// <summary>
    /// +0x0b8 default_script   : PScriptType
    /// </summary>
    public PlayScript DefaultScript { get; set; }

    /// <summary>
    /// +0x0bc default_script_intensity : Float
    /// </summary>
    public float DefaultScriptIntensity { get; set; }

    /// <summary>
    /// +0x0bc default_script_intensity : Float
    /// </summary>
    public uint SetupID { get; set; }

    /// <summary>
    /// +0x0c4 timestamps       : [9] Uint2B
    /// </summary>
    public int[] Timestamps;

    // This file looks very similar to world object - need to research Coral Golem.
    // TODO: RE the methods.

}
