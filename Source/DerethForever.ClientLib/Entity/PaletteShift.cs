/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// PalShift in the client
    /// </summary>
    public class PaletteShift
    {
        /// <summary>
        /// 0x000 cur_tex : Uint4B
        /// </summary>
        public uint CurrentTexture;

        /// <summary>
        /// 0x004 land_tex : AC1Legacy::SmartArray(PalShiftTex *)
        /// </summary>
        public List<PaletteShiftTexture> LandTextures = new List<PaletteShiftTexture>();

        /// <summary>
        /// 0x010 sub_pals : Ptr32 Subpalette
        /// </summary>
        public List<SubPalette> SubPallets = new List<SubPalette>();

        /// <summary>
        /// 0x014 maxsubs : Uint4B
        /// </summary>
        public uint MaxSubPallets;

        /// <summary>
        /// private because Unpack is the only valid source of instances
        /// </summary>
        private PaletteShift()
        {
        }

        /// <summary>
        /// //----- (005007A0) --------------------------------------------------------
        /// int __thiscall PalShift::UnPack(PalShift *this, void **addr, unsigned int *size)
        /// acclient.c 301290
        /// </summary>
        public static PaletteShift Unpack(BinaryReader reader)
        {
            throw new NotImplementedException();
        }
    }
}
