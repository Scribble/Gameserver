/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.ClientLib
{
    internal static class Constants
    {
        public const float TOLERANCE = 0.0002f;
        public const float QUANTUM_MAX_97 = 0.2f;
        public const float QUANTUM_ABSURDUM = 2.0f;
        public const float DEGREES_PER_RADIAN = (float)Math.PI / 180f;
        public const float DUMMY_SPHERE_RADIUS = 0.1f;

        // use this site for LODWORD - hex to float conversions   Thanks for the assist Behemoth
        // https://gregstoll.dyndns.org/~gregstoll/floattohex/
        /// <summary>
        /// acclient.c pulled from various parts of the client where magic numbers where used
        /// </summary>
        public const float DefaultMinDistance      = 0.0f;
        public const float DefaultDistanceToObject = 0.6f;
        public const float DefaultFailDistance     = float.MaxValue;
        public const float DefaultSpeed            = 1.0f;
        public const float DefaultWalkRunThreshold = 15.0f;
        public const float DefaultDesiredHeading   = 0f;
        public const float BackwardsFactor         = 6.4999998e-1f;
        public const float MaxSidestepAnimRate     = 3.0f;
        public const float RunAnimSpeed            = 4.0f;
        public const float RunTurnFactor           = 1.5f;
        public const float SidestepAnimSpeed       = 1.25f;
        public const float SidestepFactor          = 0.5f;
        public const float WalkAnimSpeed           = 3.1199999f;
        // Added to replace the repetitive calculation:
        //Constants.SidestepFactor * (Constants.WalkAnimSpeed / Constants.SidestepAnimSpeed)
        public const float DefaultSideStepRightAdj = 1.24799996f;
        public const uint DefaultContextId         = 0;
        public const uint DefaultActionStamp       = 0;
    }
}
