/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.2.10-MariaDB : Database - df_auth
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`df_auth` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `df_auth`;

/*Table structure for table `accesslevel` */

DROP TABLE IF EXISTS `accesslevel`;

CREATE TABLE `accesslevel` (
  `level` int(10) unsigned NOT NULL DEFAULT 0,
  `name` varchar(45) NOT NULL,
  `prefix` varchar(45) DEFAULT '',
  PRIMARY KEY (`level`),
  UNIQUE KEY `level` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `accesslevel` WRITE;
/*!40000 ALTER TABLE `accesslevel` DISABLE KEYS */;
INSERT INTO `accesslevel` VALUES (0,'Player',''),(1,'Advocate',''),(2,'Sentinel','Sentinel'),(3,'Envoy','Envoy'),(4,'Developer',''),(5,'Admin','Admin');
/*!40000 ALTER TABLE `accesslevel` ENABLE KEYS */;
UNLOCK TABLES;


/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `accountId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `accountGuid` binary(16) NOT NULL,
  `accountName` varchar(50) NOT NULL,
  `displayName` varchar(50) NOT NULL,
  `passwordHash` varchar(88) NOT NULL COMMENT 'base64 encoded version of the hashed passwords.  88 characters are needed to base64 encode SHA512 output.',
  `passwordSalt` varchar(88) NOT NULL COMMENT 'base64 encoded version of the password salt.  512 byte salts (88 characters when base64 encoded) are recommend for SHA512.',
  `email` varchar(280) DEFAULT NULL,
  `numContributions` int(11) DEFAULT NULL,
  PRIMARY KEY (`accountId`),
  UNIQUE KEY `accountName_uidx` (`accountName`),
  UNIQUE KEY `accountGuid_uidx` (`accountGuid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `managed_world` */

DROP TABLE IF EXISTS `managed_world`;

CREATE TABLE `managed_world` (
  `worldGuid` binary(16) NOT NULL,
  `accountGuid` binary(16) NOT NULL,
  `serverName` text NOT NULL,
  `address` text NOT NULL,
  PRIMARY KEY (`worldGuid`),
  KEY `fk_managed_world__account` (`accountGuid`),
  CONSTRAINT `fk_managed_world__account` FOREIGN KEY (`accountGuid`) REFERENCES `account` (`accountGuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `subscription` */

DROP TABLE IF EXISTS `subscription`;

CREATE TABLE `subscription` (
  `subscriptionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subscriptionGuid` binary(16) NOT NULL,
  `accountGuid` binary(16) NOT NULL COMMENT 'no foreign key because server might use a different auth server',
  `subscriptionName` varchar(100) NOT NULL,
  `accessLevel` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`subscriptionId`),
  UNIQUE KEY `subscriptionGuid_uidx` (`subscriptionGuid`),
  KEY `accesslevel_idx` (`accessLevel`),
  CONSTRAINT `accesslevel` FOREIGN KEY (`accessLevel`) REFERENCES `accesslevel` (`level`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `vw_account_by_name` */

DROP TABLE IF EXISTS `vw_account_by_name`;

/*!50001 DROP VIEW IF EXISTS `vw_account_by_name` */;
/*!50001 DROP TABLE IF EXISTS `vw_account_by_name` */;

/*!50001 CREATE TABLE  `vw_account_by_name`(
 `accountId` int(10) unsigned ,
 `accountGuid` binary(16) ,
 `accountName` varchar(50) ,
 `displayName` varchar(50) ,
 `passwordHash` varchar(88) ,
 `passwordSalt` varchar(88) ,
 `email` varchar(280) 
)*/;

/*Table structure for table `vw_subscription_by_account` */

DROP TABLE IF EXISTS `vw_subscription_by_account`;

/*!50001 DROP VIEW IF EXISTS `vw_subscription_by_account` */;
/*!50001 DROP TABLE IF EXISTS `vw_subscription_by_account` */;

/*!50001 CREATE TABLE  `vw_subscription_by_account`(
 `subscriptionId` int(10) unsigned ,
 `subscriptionGuid` binary(16) ,
 `accountGuid` binary(16) ,
 `subscriptionName` varchar(100) ,
 `accessLevel` int(10) unsigned 
)*/;

/*View structure for view vw_account_by_name */

/*!50001 DROP TABLE IF EXISTS `vw_account_by_name` */;
/*!50001 DROP VIEW IF EXISTS `vw_account_by_name` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_account_by_name` AS (select `account`.`accountId` AS `accountId`,`account`.`accountGuid` AS `accountGuid`,`account`.`accountName` AS `accountName`,`account`.`displayName` AS `displayName`,`account`.`passwordHash` AS `passwordHash`,`account`.`passwordSalt` AS `passwordSalt`,`account`.`email` AS `email` from `account`) */;

/*View structure for view vw_subscription_by_account */

/*!50001 DROP TABLE IF EXISTS `vw_subscription_by_account` */;
/*!50001 DROP VIEW IF EXISTS `vw_subscription_by_account` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_subscription_by_account` AS (select `subscription`.`subscriptionId` AS `subscriptionId`,`subscription`.`subscriptionGuid` AS `subscriptionGuid`,`subscription`.`accountGuid` AS `accountGuid`,`subscription`.`subscriptionName` AS `subscriptionName`,`subscription`.`accessLevel` AS `accessLevel` from `subscription`) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
